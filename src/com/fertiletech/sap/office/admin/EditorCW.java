package com.fertiletech.sap.office.admin;

import com.fertiletech.sap.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class EditorCW extends CWEditParamsBase{

	public EditorCW() {
		super("Editors List", "Unique ID should be the email address of staff in question. " +
				"Adding an email account grants that account user rights for editing application forms");
	}

	@Override
	protected boolean getShowValues() {
		return true;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_EDITOR;
	}
	
	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(EditorCW.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}
}
