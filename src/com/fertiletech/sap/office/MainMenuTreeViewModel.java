/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.sap.office;

import com.fertiletech.sap.office.admin.AdminCW;
import com.fertiletech.sap.office.admin.EditorCW;
import com.fertiletech.sap.office.admin.MapTypesCW;
import com.fertiletech.sap.office.admin.ReviewerCW;
import com.fertiletech.sap.office.content.acctpanels.acctshell.*;
import com.fertiletech.sap.office.content.moocpanels.moocshell.CourseBlotterCW;
import com.fertiletech.sap.office.content.moocpanels.moocshell.CourseUploadCW;
import com.fertiletech.sap.office.content.shell.*;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.prefetch.RunAsyncCode;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.TreeViewModel;

import java.util.*;

/**
 * The {@link TreeViewModel} used by the main menu.
 */
public class MainMenuTreeViewModel implements TreeViewModel {

  /**
   * The cell used to render categories.
   */
  private static class CategoryCell extends AbstractCell<Category> {
    @Override
    public void render(Context context, Category value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * The cell used to render examples.
   */
  private static class ContentWidgetCell extends AbstractCell<ContentWidget> {
    @Override
    public void render(Context context, ContentWidget value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * A top level category in the tree.
   */
  public class Category {

    private final ListDataProvider<ContentWidget> examples =
        new ListDataProvider<ContentWidget>();
    private final String name;
    private NodeInfo<ContentWidget> nodeInfo;
    private final List<RunAsyncCode> splitPoints =
        new ArrayList<RunAsyncCode>();

    public Category(String name) {
      this.name = name;
    }

    public void addExample(ContentWidget example, String style, RunAsyncCode splitPoint) {
    	if(style != null)
    		example.addStyleName(style);
      examples.getList().add(example);
      if (splitPoint != null) {
        splitPoints.add(splitPoint);
      }
      contentCategory.put(example, this);
      contentToken.put(Showcase.getContentWidgetToken(example), example);
    }

    public String getName() {
      return name;
    }

    /**
     * Get the node info for the examples under this category.
     * 
     * @return the node info
     */
    public NodeInfo<ContentWidget> getNodeInfo() {
      if (nodeInfo == null) {
        nodeInfo = new DefaultNodeInfo<ContentWidget>(examples,
            contentWidgetCell, selectionModel, null);
      }
      return nodeInfo;
    }

    /**
     * Get the list of split points to prefetch for this category.
     * 
     * @return the list of classes in this category
     */
    public Iterable<RunAsyncCode> getSplitPoints() {
      return splitPoints;
    }
  }

  /**
   * The top level categories.
   */
  private final ListDataProvider<Category> categories = new ListDataProvider<Category>();

  /**
   * A mapping of {@link ContentWidget}s to their associated categories.
   */
  private final Map<ContentWidget, Category> contentCategory = new HashMap<ContentWidget, Category>();

  /**
   * The cell used to render examples.
   */
  private final ContentWidgetCell contentWidgetCell = new ContentWidgetCell();

  /**
   * A mapping of history tokens to their associated {@link ContentWidget}.
   */
  private final Map<String, ContentWidget> contentToken = new HashMap<String, ContentWidget>();

  /**
   * The selection model used to select examples.
   */
  private final SelectionModel<ContentWidget> selectionModel;

  public MainMenuTreeViewModel(SelectionModel<ContentWidget> selectionModel) {
    this.selectionModel = selectionModel;
    initializeTree();
  }

  /**
   * Get the {@link Category} associated with a widget.
   * 
   * @param widget the {@link ContentWidget}
   * @return the associated {@link Category}
   */
  public Category getCategoryForContentWidget(ContentWidget widget) {
    return contentCategory.get(widget);
  }

  /**
   * Get the content widget associated with the specified history token.
   * 
   * @param token the history token
   * @return the associated {@link ContentWidget}
   */
  public ContentWidget getContentWidgetForToken(String token) {
    return contentToken.get(token);
  }

  public <T> NodeInfo<?> getNodeInfo(T value) {
    if (value == null) {
      // Return the top level categories.
      return new DefaultNodeInfo<Category>(categories, new CategoryCell());
    } else if (value instanceof Category) {
      // Return the examples within the category.
      Category category = (Category) value;
      return category.getNodeInfo();
    }
    return null;
  }

  public boolean isLeaf(Object value) {
    return value != null && !(value instanceof Category);
  }

  /**
   * Get the set of all {@link ContentWidget}s used in the model.
   * 
   * @return the {@link ContentWidget}s
   */
  Set<ContentWidget> getAllContentWidgets() {
    Set<ContentWidget> widgets = new HashSet<ContentWidget>();
    for (Category category : categories.getList()) {
      for (ContentWidget example : category.examples.getList()) {
        widgets.add(example);
      }
    }
    return widgets;
  }

  /**
   * Initialize the top level categories in the tree.
   */
  private void initializeTree() {
    List<Category> catList = categories.getList();
    //Welcome
    {
      Category category = new Category("Welcome");
      catList.add(category);
      // CwCheckBox is the default example, so don't prefetch it.
      category.addExample(new WelcomePanelCW(),"homeWatermark", null);
    }



    //prospective
    {
    	Category category = new Category("Reports");
    	category.addExample(new ApplicationBlotterCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(ApplicationBlotterCW.class));
    	category.addExample(new MapChartCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(MapChartCW.class));    	
    	category.addExample(new StatusChartCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(StatusChartCW.class));    	
    	catList.add(category);
    }

    //Licensing
    {
      Category category = new Category("Membership Licenses");
      category.addExample(new ConfirmPaymentsCW(), "watermarkT",
              RunAsyncCode.runAsyncCode(ConfirmPaymentsCW.class));
      category.addExample(new GenerateLicenseCW(), "watermarkT",
              RunAsyncCode.runAsyncCode(GenerateLicenseCW.class));

      catList.add(category);
    }


    //accounting
    {
      Category category = new Category("Account");
      category.addExample(new ViewBillDescriptionCW(), "watermarkT",
              RunAsyncCode.runAsyncCode(ViewBillDescriptionCW.class));
      category.addExample(new ViewBillsCW(), "watermarkT",
              RunAsyncCode.runAsyncCode(ViewBillsCW.class));
      category.addExample(new ViewStudentCW(), "watermarkT",
              RunAsyncCode.runAsyncCode(ViewStudentCW.class));
      category.addExample(new DownloadInvoiceCW(), "watermarkT",
              RunAsyncCode.runAsyncCode(DownloadInvoiceCW.class));
      catList.add(category);
    }

    //payments
    {
      Category category = new Category("Pay");
      category.addExample(new BillDescriptionsCW(), "adminWatermark",
              RunAsyncCode.runAsyncCode(BillDescriptionsCW.class));
      category.addExample(new BillStudentCW(), "adminWatermark",
              RunAsyncCode.runAsyncCode(BillStudentCW.class));
      category.addExample(new IndicatePaymentCW(), "adminWatermark",
              RunAsyncCode.runAsyncCode(IndicatePaymentCW.class));
      catList.add(category);
    }

    //moocs
    {
      Category category = new Category("MOOCs");
      category.addExample(new TrackBlotterCW(), "prospectiveWatermark",
              RunAsyncCode.runAsyncCode(TrackBlotterCW.class));
      category.addExample(new TracksForMemberCW(), "prospectiveWatermark",
              RunAsyncCode.runAsyncCode(TracksForMemberCW.class));
        category.addExample(new TrackFeedbackCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(TrackFeedbackCW.class));
      category.addExample(new TrackStatusCW(), "prospectiveWatermark",
              RunAsyncCode.runAsyncCode(TrackStatusCW.class));
      category.addExample(new CourseBlotterCW(), "adminWatermark",
              RunAsyncCode.runAsyncCode(CourseBlotterCW.class));
      category.addExample(new CourseUploadCW(), "adminWatermark",
              RunAsyncCode.runAsyncCode(CourseUploadCW.class));
      catList.add(category);
    }


       
    //search
    {
    	Category category = new Category("Edit/Search");
       	category.addExample(new AllStatusCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(AllStatusCW.class));
       	category.addExample(new StatusCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(StatusCW.class));    	       	
    	catList.add(category);
    }
    
    //Email
    {
    	Category category = new Category("Contact");
    	catList.add(category);
    	category.addExample(new EmailPanelCW(), "adminWatermark",
              RunAsyncCode.runAsyncCode(EmailPanelCW.class));
    	category.addExample(new ApplicationMailTemplateCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(ApplicationMailTemplateCW.class));
    	category.addExample(new ApplicationBlotterMailCW(), "prospectiveWatermark",
                RunAsyncCode.runAsyncCode(ApplicationBlotterMailCW.class));
    }
    
    //config
    {
    	Category category = new Category("Config");
    	catList.add(category);
        category.addExample(new EditorCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(EditorCW.class));     
        category.addExample(new ReviewerCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(ReviewerCW.class));     
        category.addExample(new AdminCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(AdminCW.class));
        category.addExample(new MapTypesCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(MapTypesCW.class));         	        
        category.addExample(new PDFReportImagesCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(PDFReportImagesCW.class));
    }      
  }
  
  public ContentWidget setBackgroundStyle(ContentWidget w, String styl)
  {
	  w.addStyleName(styl);
	  return w;
  }
}
