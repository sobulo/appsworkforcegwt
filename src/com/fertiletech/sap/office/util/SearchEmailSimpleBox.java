package com.fertiletech.sap.office.util;


import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.shared.ValidationUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

public class SearchEmailSimpleBox extends Composite implements HasClickHandlers, HasEnabled {

	@UiField
	TextBox idBox;
	@UiField
	Button search;

	private static SearchEmailSimpleBoxUiBinder uiBinder = GWT
			.create(SearchEmailSimpleBoxUiBinder.class);

	interface SearchEmailSimpleBoxUiBinder extends UiBinder<Widget, SearchEmailSimpleBox> {
	}

	public SearchEmailSimpleBox() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public boolean isEnabled() {
		return search.isEnabled();
	}
	
	public String getEmailAddress()
	{
		String val = idBox.getValue();
		if(val == null || val.trim().length() == 0)
		{
			PanelUtilities.errorBox.show("You must specify an email address");
			return null;			
		}
		
		if(!ValidationUtils.isValidEmail(val))
		{
			PanelUtilities.errorBox.show("Invalid email address specified: " + val);
			return null;
		}
		
		return val.trim().toLowerCase();
	}

	public void setEmailValue(String email)
	{
		idBox.setValue(email);
	}

	@Override
	public void setEnabled(boolean enabled) {
		search.setEnabled(enabled);
		idBox.setEnabled(enabled);
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return search.addClickHandler(handler);
	}
}
