/**
 * 
 */
package com.fertiletech.sap.office;

import com.fertiletech.sap.client.*;
import com.fertiletech.sap.office.util.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.StatusCodeException;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelUtilities {
	public final static SimpleDialog infoBox = new SimpleDialog("<center><b style='color:green'>INFO</b></center>", true);
	public final static SimpleDialog errorBox = new SimpleDialog("<center><b style='color:red'>Error!</b></center>", true);

    final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
    final static MortgageServiceAsync LOAN_MKT_SERVICE = GWT.create(MortgageService.class);
    final static MoocsAsync MOOC_SERVICE = GWT.create(Moocs.class);

    public static MortgageServiceAsync getLoanMktService()
    {
    	return LOAN_MKT_SERVICE;
    }

    public static LoginServiceAsync getLoginService()
    {
    	return READ_SERVICE;
    }
    public static  MoocsAsync getMOOCService()
    {
        return MOOC_SERVICE;
    }

    public static void displaySlowInternetX(Throwable caught)
    {
        if (caught instanceof StatusCodeException)
            errorBox.show("Looks like your internet connection is currently slow. Try connecting later");
    }

}
