package com.fertiletech.sap.office.content.moocpanels;

import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.table.ShowcaseTable;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ResizeComposite;

import java.util.List;

public class CourseBlotter extends ResizeComposite{

    ShowcaseTable display;
    public CourseBlotter()
    {
        display = new ShowcaseTable();
        Moocs.App.getInstance().getAllCourses(new AsyncCallback<List<TableMessage>>() {
            @Override
            public void onFailure(Throwable caught) {
                PanelUtilities.errorBox.show(caught.getMessage());

            }

            @Override
            public void onSuccess(List<TableMessage> result) {
                PanelUtilities.infoBox.show("Retrieved: " + result.size() + " rows");
                display.showTable(result);
            }
        });

        display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
            @Override
            public void onValueChange(ValueChangeEvent<TableMessage> event) {
                TableMessage course = event.getValue();
                PanelUtilities.infoBox.show(course.getText(DTOConstants.COURSE_NAME_TXT_IDX));
            }
        });
        initWidget(display);

    }

}
