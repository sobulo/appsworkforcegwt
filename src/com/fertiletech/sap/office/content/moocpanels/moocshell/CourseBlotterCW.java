package com.fertiletech.sap.office.content.moocpanels.moocshell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.content.moocpanels.CourseBlotter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CourseBlotterCW extends ContentWidget{
    public CourseBlotterCW() {
        super("Course Blotter", "List of all courses currently being tracked");
    }

    @Override
    public Widget onInitialize() {
        return new CourseBlotter();
    }

    @Override
    protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
        GWT.runAsync(CourseBlotterCW.class, new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess() {
                callback.onSuccess(onInitialize());
            }
        });
    }

    @Override
    protected String getHelpUrl() {
        return HelpPageGenerator.HELP_WELCOME_URL;
    }
}
