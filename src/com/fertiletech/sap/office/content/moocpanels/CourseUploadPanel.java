/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.sap.office.content.moocpanels;

import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
/**
 *
 * @author Administrator
 */
public class CourseUploadPanel extends ResizeComposite implements
        ClickHandler, FormPanel.SubmitCompleteHandler{
    
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";
    public final static String INIT_DISPLAY = "Please choose a spreadsheet " +
            "and hit submit to upload</b>";
    private final static String CLASS_KEY_NAME = "classkey";
    
    
    @UiField FileUpload selectFile;
    @UiField Button submitFile;
    @UiField FormPanel studentUploadForm;
    @UiField HTML formResponse;
    @UiField SimpleLayoutPanel topLevelPanel;
    @UiField ListBox classList;
    @UiField Hidden classParam;

    FormPanel confirmationForm;
    Button confirmData;
    Button resetData;
    //HTML confirmDisplay;
    private SimpleDialog errorBox;

    private static CourseUploadPanelUiBinder uiBinder = GWT.create(CourseUploadPanelUiBinder.class);

    interface CourseUploadPanelUiBinder extends UiBinder<Widget, CourseUploadPanel> {
    }

    private final static String QUERY_TYPES[] ={"BASIC"};
    private final static String ACTION_NAME = "/courseupload";
    public CourseUploadPanel()
    {
        initWidget(uiBinder.createAndBindUi(this));
        for (String type : QUERY_TYPES)
        	classList.addItem(type);
        classParam.setName(CLASS_KEY_NAME);
        selectFile.setName("chooseFile");
        submitFile.addClickHandler(this);
        studentUploadForm.setAction(ACTION_NAME);
        studentUploadForm.addSubmitCompleteHandler(this);
        studentUploadForm.setMethod("post");
        studentUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);

        confirmationForm = new FormPanel();
        FlowPanel formComponents = new FlowPanel();
        confirmationForm.setAction(ACTION_NAME);
        confirmationForm.setMethod("post");
        confirmationForm.addSubmitCompleteHandler(this);
        confirmData = new Button("Save Loaded Data");
        resetData = new Button("Load new File");
        confirmData.addClickHandler(this);
        resetData.addClickHandler(this);
        //confirmDisplay = new HTML();
        formComponents.add(new Hidden(LOAD_PARAM_NAME));
        //formComponents.add(confirmDisplay);
        formComponents.add(confirmData);
        formComponents.add(resetData);
        confirmationForm.add(formComponents);
        
        errorBox = PanelUtilities.errorBox;
    }

    public void resetPanel()
    {
        studentUploadForm.reset();
        confirmationForm.reset();
        formResponse.setHTML(INIT_DISPLAY);
        topLevelPanel.remove(topLevelPanel.getWidget());
        topLevelPanel.add(studentUploadForm);
    }

    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(submitFile))
        {
        	if(!selectFile.getFilename().endsWith("xls"))
        	{
        		errorBox.show("Please select an excel 97-2003 worksheet (.xls extension)");
        		return;
        	}
        	classParam.setValue(classList.getSelectedValue());
            formResponse.setHTML("<b>Sending file to server ...</b>");
            studentUploadForm.submit();
        }
        else if(event.getSource().equals(confirmData))
        {
            //confirmDisplay.setHTML("<b>Saving confirmed data ...</b>");
        	formResponse.setHTML("<b>Saving confirmed data ...</b>");
            confirmationForm.submit();
        }
        else if(event.getSource().equals(resetData))
        {
            resetPanel();
        }
    }

    public void onSubmitComplete(SubmitCompleteEvent event)
    {
        String results = event.getResults();
        //confirmDisplay.setHTML(event.getResults());
        formResponse.setHTML(event.getResults());
        if(results.startsWith(GOOD_DATA_PREFIX)) //hacky way 2 determine state
            confirmData.setVisible(true);
        else
            confirmData.setVisible(false);
        if(topLevelPanel.getWidget().equals(studentUploadForm))
        {
            topLevelPanel.remove(studentUploadForm);
            topLevelPanel.add(confirmationForm);
        }
    }


}