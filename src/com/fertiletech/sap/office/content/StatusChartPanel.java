package com.fertiletech.sap.office.content;

import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.MyAsyncCallback;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.Showcase;
import com.fertiletech.sap.office.content.shell.ApplicationBlotterCW;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChart;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;
import com.googlecode.gwt.charts.client.options.Animation;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.Options;

import java.util.Date;
import java.util.HashMap;

public class StatusChartPanel extends MortgageChartPanel{

	@Override
	protected ChartWidget<? extends Options> getChart() {
		ColumnChart barChart = new ColumnChart();
		ColumnChartOptions options = ColumnChartOptions.create();
		Animation aniOpt = Animation.create();
		aniOpt.setDuration(5000);
		options.setAnimation(aniOpt);
		options.setEnableInteractivity(true);
		options.setFontName("Tahoma");
		Date[] dates = dateSearchPanel.getSearchDates();
		options.setTitle("Online Application Status (" + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[0])
				+ " to " + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(dates[1]) + ")");
		Legend legend = Legend.create();
		legend.setPosition(LegendPosition.NONE);
		options.setLegend(legend);
		opt = options;
		barChart.addSelectHandler(new SelectHandler() {
			
			@Override
			public void onSelect(SelectEvent event) {
				History.newItem(Showcase.getContentWidgetToken(ApplicationBlotterCW.class));
			}
		});
		return barChart;
	}

	@Override
	protected ChartPackage getChartType() {
		return ChartPackage.CORECHART;
	}

	@Override
	protected void drawChart(final Date startDate, final Date endDate) {
		MyAsyncCallback<HashMap<String, Integer>> statusCallBack = new MyAsyncCallback<HashMap<String, Integer>>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Failed to load chart");
			}

			@Override
			public void onSuccess(HashMap<String, Integer> stats) {
				DataTable dataTable = DataTable.create();
				dataTable.addColumn(ColumnType.STRING, "Status");
				dataTable.addColumn(ColumnType.NUMBER, "# of Applications");
				addStyleColumn(dataTable);
				TableMessage colNames = DTOConstants.getStatusChartHeaders();
				dataTable.addRows(stats.size());

				TableMessage colors = DTOConstants.getStatusColors();

				for(int row = 0; row < colNames.getNumberOfTextFields(); row++)
				{
					String name = colNames.getText(row);
					Integer statVal = stats.get(name);
					if(statVal == null)
						continue; //skip, colNames can be more than returned keys for stats

					dataTable.setValue(row, 0, name );
					dataTable.setValue(row, 1, statVal);
					dataTable.setValue(row, 2, colors.getText(row));
					row++;
				}
				((ColumnChart) chart).draw(dataTable, (ColumnChartOptions) opt);
			}

			@Override
			protected void callService(
					AsyncCallback<HashMap<String, Integer>> cb) {
				PanelUtilities.getLoanMktService().getLeadAggregates(startDate, endDate, cb);
			}
		};
		statusCallBack.go("Requesting aggregated data from server, please wait ...");
	}
	
	private native void addStyleColumn(DataTable data) /*-{
    data.addColumn({type:'string', role:'style'});
}-*/;
}
