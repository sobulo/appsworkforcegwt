package com.fertiletech.sap.office.content.acctpanels.acctshell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.content.acctpanels.GenerateBillingInvoice;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class DownloadInvoiceCW extends ContentWidget{
    public DownloadInvoiceCW() {
        super("Download Invoice", "Download invoices for selected menbers");
    }

    @Override
    public Widget onInitialize() {
        return new GenerateBillingInvoice();
    }

    @Override
    protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
        GWT.runAsync(DownloadInvoiceCW.class, new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess() {
                callback.onSuccess(onInitialize());
            }
        });
    }

    @Override
    protected String getHelpUrl() {
        return HelpPageGenerator.HELP_WELCOME_URL;
    }

}
