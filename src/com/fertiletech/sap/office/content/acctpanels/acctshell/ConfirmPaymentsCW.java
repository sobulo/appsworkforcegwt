package com.fertiletech.sap.office.content.acctpanels.acctshell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.content.acctpanels.licensing.VerifyPayments;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class ConfirmPaymentsCW extends ContentWidget{
    public ConfirmPaymentsCW() {
        super("Confirm Payments", "Use this panel to confirm or verify payment indications. Perform verification" +
                " only on payments that you have visual confirmation for either via company alert/email or by Segun confirming the traction online on GTB gapps portal");
    }

    @Override
    public Widget onInitialize() {
        return new VerifyPayments();
    }

    @Override
    protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
        GWT.runAsync(ConfirmPaymentsCW.class, new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess() {
                callback.onSuccess(onInitialize());
            }
        });
    }

    @Override
    protected String getHelpUrl() {
        return HelpPageGenerator.HELP_WELCOME_URL;
    }
}
