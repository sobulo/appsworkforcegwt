package com.fertiletech.sap.office.content.acctpanels.acctshell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.content.acctpanels.AddStudentPayment;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class IndicatePaymentCW extends ContentWidget{
    public IndicatePaymentCW(){
        super("Indicate Payment", "Create a payment object to indicate a payment has been made. " +
                "Under normal circumatances you shouldn't have to do this as members are the ones to" +
                " usually indicate payments. Normal workfor");
    }

    @Override
    public Widget onInitialize() {
        return new AddStudentPayment();
    }

    @Override
    protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
        GWT.runAsync(IndicatePaymentCW.class, new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess() {
                callback.onSuccess(onInitialize());
            }
        });
    }

    @Override
    protected String getHelpUrl() {
        return HelpPageGenerator.HELP_WELCOME_URL;
    }

}
