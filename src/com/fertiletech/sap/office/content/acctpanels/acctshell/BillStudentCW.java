package com.fertiletech.sap.office.content.acctpanels.acctshell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.content.acctpanels.AddStudentBill;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class BillStudentCW extends ContentWidget{
    public BillStudentCW() {
        super("Bill Member", "Issue license invoices. Under normal cicrumstances you shouldn't have to" +
                " as invoices are automatically created when a user selects a memnership plan");
    }

    @Override
    public Widget onInitialize() {
        return new AddStudentBill();
    }

    @Override
    protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
        GWT.runAsync(BillStudentCW.class, new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess() {
                callback.onSuccess(onInitialize());
            }
        });
    }

    @Override
    protected String getHelpUrl() {
        return HelpPageGenerator.HELP_WELCOME_URL;
    }

}
