package com.fertiletech.sap.office.content.acctpanels;

import com.fertiletech.sap.client.Accounting;
import com.fertiletech.sap.client.AccountingAsync;
import com.fertiletech.sap.client.utils.BillingItemWrapper;
import com.fertiletech.sap.client.utils.MultipleMessageDialog;
import com.fertiletech.sap.client.utils.Text2ListBox;
import com.fertiletech.sap.client.utils.WidgetHelper;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.shared.BillDescriptionItem;
import com.fertiletech.sap.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class AddBillTemplate extends Composite implements ClickHandler{

	@UiField DateBox dueDate;
	@UiField ListBox billTemplateName;
	@UiField
	Text2ListBox billItems;
	@UiField Button saveBill;
	@UiField Label status;

	@UiField TextBox bank;
	@UiField TextBox acctName;
	@UiField TextBox acctNum;
	@UiField TextBox payInstr;

	private MultipleMessageDialog errorBox;
	private AccountingAsync accountService = Accounting.App.getInstance();
	private boolean doneLoading = false;
	
	private final static int VISIBLE_BILLABLE_ITEMS = 4;
	private final static String VISIBLE_BILLABLE_WIDTH = "300px";
	private final static String DEFAULT_BOX_WIDTH = "150px";
	
	private static AddBillTemplateUiBinder uiBinder = GWT
			.create(AddBillTemplateUiBinder.class);

	interface AddBillTemplateUiBinder extends UiBinder<Widget, AddBillTemplate> {
	}
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> billTemplateCallback = new AsyncCallback<String>() {
        @Override
        public void onSuccess(String result) {
        	billTemplateName.setSelectedIndex(0);
        	dueDate.setValue(null);
        	billItems.clear();
            WidgetHelper.setSuccessStatus(status, "Created bill template [" + result + "]" +
            		" succesfully. Click on 'create bills' on naviagation menu to generate member bills from this template", true);
			Accounting.App.getInstance().getBillTemplateList(billListCallBack);
        }

        @Override
        public void onFailure(Throwable caught) {
        	String msg = "Request failed: " + caught.getMessage();
        	WidgetHelper.setErrorStatus(status, msg, true);
            
        }
    };


	AsyncCallback<HashMap<String, String>> billListCallBack = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			PanelUtilities.errorBox.show("Error Loading existing bills " + caught.getMessage());
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			final HashSet<String> BILL_LIST = new HashSet<String>();
			BILL_LIST.addAll(DTOConstants.CURRENT_TEMPLATES);
			for(String existingBill : result.values())
				if(DTOConstants.CURRENT_TEMPLATES.contains(existingBill))
					BILL_LIST.remove(existingBill);

			billTemplateName.clear();
			for(String bt: BILL_LIST) //add just new items to bill list
				billTemplateName.addItem(bt);
			WidgetHelper.setSuccessStatus(status, "Successfully refreshed: " + BILL_LIST.size() + " left to create", false);
		}
	};

	public AddBillTemplate() {
		initWidget(uiBinder.createAndBindUi(this));
		//Window.alert("creating bill template");
		BillingItemWrapper biw = new BillingItemWrapper();
		billItems.initInput(biw);
        billItems.setVisbileArea(VISIBLE_BILLABLE_ITEMS, VISIBLE_BILLABLE_WIDTH);
        billItems.enableBox();
        billItems.removeAdditionalFeatures();
        billTemplateName.setWidth(DEFAULT_BOX_WIDTH);
        //intitialiaze
		//billTemplateName.addItem("NEW BILLING/PRICING NEEDED?");
		Accounting.App.getInstance().getBillTemplateList(billListCallBack);
        dueDate.setWidth(DEFAULT_BOX_WIDTH);
		dueDate.setValue(null);
		errorBox = new MultipleMessageDialog("Error!!");
		WidgetHelper.setNormalStatus(status, "Do NOT use this screen unless you have requested and received approval via email");
		saveBill.addClickHandler(this);
		bank.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {

				enableBankInfo();
			}
		});
		//Window.alert("Enabling bank info");
		enableBankInfo();
	}

	private void enableBankInfo()
	{
		boolean enable;
		if(getNullOrVal(bank) == null)
			enable = false;
		else
			enable =true;

		bank.setEnabled(true);
		acctName.setEnabled(enable);
		acctNum.setEnabled(enable);
		payInstr.setEnabled(enable);

		if(!enable)
		{
			acctName.setValue(null);
			payInstr.setValue(null);
			acctNum.setValue(null);
		}
	}

	
	private String getSelectedItem(ListBox lb)
	{
		return lb.getValue(lb.getSelectedIndex());
	}
	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		ArrayList<String> errors = new ArrayList<String>();
		String btName = billTemplateName.getSelectedItemText();
		if(btName.length() == 0)
			errors.add("Must enter a value for 'bill template name' field");
		
		//setup account information
		String bankVal=null;
		String accNameVal = null;
		String accNumVal = null;
		String instrVal = null;

		if(getNullOrVal(bank) != null)
		{
			bankVal = getNullOrVal(bank);
			if(getNullOrVal(acctName) == null)
				errors.add("Since bank name specified, account name must be specified as well");
			else
				accNameVal = getNullOrVal(acctName);

			if(getNullOrVal(acctNum) == null)
				errors.add("Since bank name specified, account number must be specified as well");
			else
				accNumVal = getNullOrVal(acctNum);

			if(getNullOrVal(payInstr) == null)
				errors.add("Since bank name specified, payment intruction must be specified as well");
			else
				instrVal = getNullOrVal(payInstr);
		}

		LinkedHashSet<BillDescriptionItem> billDescs = null;
		
		HashSet<String> itemStrings = billItems.getList();
		if(itemStrings.size() == 0)
			errors.add("Bill template must have at least 1 billable ITEM added");
		else
		{
			billDescs = new LinkedHashSet<BillDescriptionItem>(itemStrings.size());
			for(String item : itemStrings)
			{
				String[] parts = item.split(BillingItemWrapper.SEPERATOR_REGEX);
				if(parts.length != 3)
				{
					errors.add("Unable to parse billing item information. Please ensure only alphanumeric characters are used.");
					continue;
				}
				billDescs.add(new BillDescriptionItem(parts[BillingItemWrapper.NAME_IDX], 
						parts[BillingItemWrapper.COMMENT_IDX], Double.parseDouble(parts[BillingItemWrapper.AMOUNT_IDX])));
			}
		}
		
		if(errors.size() > 0)
		{
			errorBox.show("Save request aborted because:", errors);
			return;
		}
		
		//rpc call to persist bill
		WidgetHelper.setNormalStatus(status, "Sending save request to server, please wait ...");
		accountService.createBillTemplate(btName, null,
				null, dueDate.getValue(), billDescs, bankVal, accNameVal, accNumVal, instrVal, billTemplateCallback);
	}

	private String getNullOrVal(TextBox input)
	{
		String result = input.getValue();
		if(result == null || result.trim().length() == 0)
			return null;
		return result;
	}

}
