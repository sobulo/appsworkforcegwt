/**
 * 
 */
package com.fertiletech.sap.office.content.acctpanels;

import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.utils.BillTemplateBox;
import com.fertiletech.sap.client.utils.WidgetHelper;
import com.fertiletech.sap.client.utils.pstables.MessageListToGrid;
import com.fertiletech.sap.office.util.SearchDateBox;
import com.fertiletech.sap.office.util.table.ShowcaseTable;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ViewBillBlotter extends Composite{

	private static ViewBillBlotterUiBinder uiBinder = GWT
			.create(ViewBillBlotterUiBinder.class);
	
	@UiField
	BillTemplateBox btList;
	@UiField Button viewBills;
	@UiField CheckBox filterUnpaid;
	@UiField Label status;
	@UiField FlowPanel dateSearchSlot;

	@UiField
	ShowcaseTable blotter;
	private String lastSelectedBill = "";
	private String lastSelectedStudentBill = "";
	private DialogBox paymentDisplay;
	private Button payHide;
	private SimplePanel historySlot;
	private Grid paySummary;
	
	private final static HashMap<String, List<TableMessage>> cachedBillList = new HashMap<String, List<TableMessage>>();
	private final static HashMap<String, List<TableMessage>> cachedPayments = new HashMap<String, List<TableMessage>>();
	
	final AsyncCallback<List<TableMessage>> viewBillsCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Fetched bill list successfully", false);
				if(lastSelectedBill.length() > 0)
					cachedBillList.put(lastSelectedBill, result);
				blotter.showTable(result);
			}
			else
			{
				WidgetHelper.setErrorStatus(status, "Unable to generate table, no headers found", true);				
			}
		}
	};
	
	final AsyncCallback<List<TableMessage>> viewPaymentsCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			historySlot.clear();
			Label popupStatus = new Label(caught.getMessage());
			WidgetHelper.setErrorStatus(popupStatus, caught.getMessage(), false);
			historySlot.add(popupStatus);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			cachedPayments.put(lastSelectedStudentBill, result);
			setupPaymentTable(result);

		}
	};	

	interface ViewBillBlotterUiBinder extends UiBinder<Widget, ViewBillBlotter> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ViewBillBlotter() {
		initWidget(uiBinder.createAndBindUi(this));
		btList.initData();
		payHide = new Button("OK");
		historySlot = new SimplePanel();


		payHide.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				historySlot.clear();
				paymentDisplay.hide();
			}
		});
		blotter.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
										  @Override
										  public void onValueChange(ValueChangeEvent<TableMessage> event) {
											  onRowSelected(event.getValue());
										  }
									  }

		);
		paymentDisplay = new DialogBox(true);
		paymentDisplay.setHTML("<b>Edit Hold</b>");
        paymentDisplay.setAnimationEnabled(true);
        paymentDisplay.setGlassEnabled(true);
        paySummary = new Grid(5, 2);
        paySummary.addStyleName(GUIConstants.STYLE_STND_TABLE);
        paySummary.addStyleName(GUIConstants.STYLE_CAPITALIZE);
        paySummary.setBorderWidth(1);
        paySummary.setWidth("75%");
        for(int i = 0; i < 5; i++)
        	paySummary.getCellFormatter().addStyleName(i, 0, GUIConstants.STYLE_STND_TABLE_HEADER);
        paySummary.setText(0, 0, "Bill Description");
        paySummary.setText(1, 0, "Student Name");
        paySummary.setText(2, 0, "Total Due");
        paySummary.setText(3, 0, "Amount Paid");
        paySummary.setText(4, 0, "Balance");
        VerticalPanel layout = new VerticalPanel();
        layout.add(new HTML("<b>Summary</b>"));
        layout.add(paySummary);
        layout.add(new HTML("<b>History</b>"));
        layout.add(historySlot);
        layout.add(payHide);
        paymentDisplay.setWidget(layout);
        filterUnpaid.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				ArrayList<TableMessage> filteredMessages;
				List<TableMessage> displayedMessages = cachedBillList.get(lastSelectedBill);
				if(filterUnpaid.getValue().equals(true))
				{
					filteredMessages = new ArrayList<TableMessage>();
					for(TableMessage row : displayedMessages)
						if(row.getText(5).equals("False"))
							filteredMessages.add(row);
				}
				else
					filteredMessages = (ArrayList<TableMessage>) displayedMessages;
				blotter.showTable(filteredMessages);
			}
		});
		final SearchDateBox sdb = new SearchDateBox();
		sdb.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Date[] params = sdb.getSearchDates();
				if(params == null) return;
				sdb.setEnabled(false);
				lastSelectedBill = "";
				btList.getAccountService().getBills(params[0], params[1], viewBillsCallBack);
			}
		});
		dateSearchSlot.add(sdb);
	}

	@UiHandler("viewBills")
	void onViewBillsClick(ClickEvent event) {
		filterUnpaid.setValue(false);
		lastSelectedBill = btList.getSelectedBillTemplate();
		List<TableMessage> cachedMessages = cachedBillList.get(lastSelectedBill); 
		
		if(cachedMessages != null)
		{
			blotter.showTable(cachedMessages);
			return;
		}
		
		if(lastSelectedBill.length() > 0)
		{
			WidgetHelper.setNormalStatus(status, "Fetching billing information for " + 
					btList.getSelectedBillTemplateName() + " Please wait ...");
			
			btList.getAccountService().getBillsForTemplate(btList.getSelectedBillTemplate(), viewBillsCallBack);
		}
		else
			WidgetHelper.setErrorStatus(status, "A bill template must exist and be" +
					" selected before you can pull up its associated student bills", true);
	}
	
	private void setupPaymentTable(List<TableMessage> data)
	{
		historySlot.clear();
		if(data.size() > 1)
			historySlot.add(new MessageListToGrid(data));
		else
			historySlot.add(new Label("No payments have been made"));		
	}


	public void onRowSelected(TableMessage m) {
        paySummary.setText(0, 1, m.getText(3));
        paySummary.setText(1, 1, m.getText(2) + " " + m.getText(1));
        paySummary.setText(2, 1, GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0)));
        paySummary.setText(3, 1, GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(1)));
        paySummary.setText(4, 1, GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0) - m.getNumber(1)));
        historySlot.clear();
        historySlot.add(new Label("Fetching payment history, please wait ..."));
        paymentDisplay.center(); //center and show
        lastSelectedStudentBill = m.getMessageId(); //m.getText(4);
        List<TableMessage> cp = cachedPayments.get(lastSelectedStudentBill);
        if(cp != null)
        	setupPaymentTable(cp);
        else
        	btList.getAccountService().getBillPayments(lastSelectedStudentBill, viewPaymentsCallBack);
		
	}
}
