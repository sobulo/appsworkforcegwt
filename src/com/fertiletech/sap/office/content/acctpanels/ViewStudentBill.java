/**
 * 
 */
package com.fertiletech.sap.office.content.acctpanels;


import com.fertiletech.sap.client.Accounting;
import com.fertiletech.sap.client.AccountingAsync;
import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.utils.StudentSuggestBox;
import com.fertiletech.sap.client.utils.WidgetHelper;
import com.fertiletech.sap.client.utils.pstables.MessageListToGrid;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ViewStudentBill extends Composite implements  ValueChangeHandler<String>, ClickHandler{
	
	@UiField SimplePanel studentListSlot;
	@UiField ListBox billList;
	
	@UiField TextBox billName;
	@UiField TextBox billYear;
	@UiField TextBox billTerm;
	@UiField TextBox billAmount;
	@UiField TextBox billDate;	
	@UiField
	MessageListToGrid itemizedSlot;
	
	@UiField TextBox priorPayments;
	@UiField TextBox balance;
	@UiField MessageListToGrid paySlot;
	
	@UiField Button viewBill;
	
	@UiField Label status;
	
	private static HashMap<String, HashMap<String, String>> cachedBillKeys = new HashMap<String, HashMap<String, String>>();
	private static HashMap<String, List<TableMessage>> cachedBillData = new HashMap<String, List<TableMessage>>();
	private static AccountingAsync accountService = Accounting.App.getInstance();
	
	private StudentSuggestBox studentList;
	private static ViewStudentBillUiBinder uiBinder = GWT
			.create(ViewStudentBillUiBinder.class);

	interface ViewStudentBillUiBinder extends UiBinder<Widget, ViewStudentBill> {
	}

	final AsyncCallback<HashMap<String, String>> billListCallBack = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			result.remove(0); //discard header as its not needed here
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Fetched " + result.size() + " member bills.", false);
				if(studentList != null )
					cachedBillKeys.put(studentList.getSelectedUser(), result);
				populateBillBox(result);
			}
			else
			{
				WidgetHelper.setErrorStatus(status, "No bills found for member. Please generate a bill before attempting to view it.", true);				
			}
		}
	};
	
	final AsyncCallback<List<TableMessage>> viewBillCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Fetched bill template info successfully", true);
				cachedBillData.put(billList.getValue(billList.getSelectedIndex()), result);
				setupDetails(result);
			}
			else
			{
				WidgetHelper.setErrorStatus(status, "No data found for selected member bill!!", true);				
			}
		}
	};	
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ViewStudentBill(boolean isAdmin) {
		initWidget(uiBinder.createAndBindUi(this));
		if(isAdmin)
			setupAdmin();
		else
			setupStudent();

		viewBill.addClickHandler(this);
		billName.setEnabled(false);
		billYear.setEnabled(false);
		billTerm.setEnabled(false);
		billAmount.setEnabled(false);
		billDate.setEnabled(false);
		priorPayments.setEnabled(false);
		balance.setEnabled(false);
	}
	
	private void setupAdmin()
	{
		studentList = new StudentSuggestBox();
		studentListSlot.add(studentList);
		studentList.addValueChangeHandler(this);
	}
	
	private void setupStudent()
	{
		accountService.getStudentBillKeys(billListCallBack);
	}
	
	private void setupDetails(List<TableMessage> info)
	{
		ArrayList<TableMessage> itemizedCopy = new ArrayList<TableMessage>(info.size());
		ArrayList<TableMessage> historyCopy = new ArrayList<TableMessage>(info.size());
	    TableMessage studBillInfo = info.get(0); //settlement info
	    double totalCheck = studBillInfo.getNumber(0);
	    double priorPayAmount = studBillInfo.getNumber(1);
		setupSummaryInfo(info.get(1), priorPayAmount, totalCheck); //bill template info
		itemizedCopy.add(info.get(2));
		boolean switchToHistory = false;
		for(int i = 3; i < info.size(); i++)
		{
			TableMessage m = info.get(i);
			if(m instanceof TableMessageHeader)
				switchToHistory = true;
			if(switchToHistory)
				historyCopy.add(m);
			else
				itemizedCopy.add(m);
		}
		itemizedSlot.populateTable(itemizedCopy);
		paySlot.populateTable(historyCopy);
	}
	
	private void setupSummaryInfo(TableMessage m, double priorPayAmount, double totalCheck)
	{
		billName.setText(m.getText(0));
		billYear.setText(m.getText(1));
		billTerm.setText(m.getText(2));
		billAmount.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0)));
		billDate.setText(m.getDate(0)==null?"":GUIConstants.DEFAULT_DATE_FORMAT.format(m.getDate(0)));
		priorPayments.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(priorPayAmount));
		balance.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0) - priorPayAmount));
		if(Math.abs(m.getNumber(0) - totalCheck) > 0.000001 )
		{
			WidgetHelper.setErrorStatus(status, "Total for bill doesn't match its template. " +
					"This should not be the case, please alert your administrator about the discrepancy", true);
		}
	}	
	
	private void clearFields()
	{
		billName.setText("");
		billYear.setText("");
		billTerm.setText("");
		billAmount.setText("");
		billDate.setValue(null);
		itemizedSlot.clear();
		priorPayments.setText("");
		balance.setText("");
		paySlot.clear();
		status.setText("");
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		GWT.log("RECEIVED change event");
		clearFields();
		billList.clear();
		String selectedStudent = event.getValue();
		WidgetHelper.setNormalStatus(status, "Fetching list of member bills. Please wait ...");
		HashMap<String, String> billKeys = cachedBillKeys.get(selectedStudent);
		GWT.log("Selected Member: " + selectedStudent);
		if(billKeys == null)
			accountService.getStudentBillKeys(selectedStudent, false, billListCallBack);
		else
		{
			GWT.log("BillPayment cache hit!!!");
			populateBillBox(billKeys);
		}
	}
	
	private void populateBillBox(HashMap<String, String> billKeys)
	{
		for(String bk : billKeys.keySet())
			billList.addItem(billKeys.get(bk), bk);
	}	

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		int idx = billList.getSelectedIndex();
		if(idx < 0)
		{
			WidgetHelper.setErrorStatus(status, "No bill found for member. A bill must " +
					"be generated for the member before you can view it. If one was recently generated, try refreshing your browser", true);
			return;
		}
		
		WidgetHelper.setNormalStatus(status, "Fetching billing details for " + billList.getItemText(idx) + ". Please wait ...");
		List<TableMessage> cachedVal = cachedBillData.get(billList.getValue(idx));
		if(cachedVal == null)
			accountService.getStudentBillAndPayment(billList.getValue(billList.getSelectedIndex()), viewBillCallBack);
		else
			setupDetails(cachedVal);
	}

}
