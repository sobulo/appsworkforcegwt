/**
 * 
 */
package com.fertiletech.sap.office.content.acctpanels.licensing;

import com.fertiletech.sap.client.Accounting;
import com.fertiletech.sap.client.AccountingAsync;
import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.utils.StudentSuggestBox;
import com.fertiletech.sap.client.utils.WidgetHelper;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.HashMap;
import java.util.List;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IssueMemberLicense extends Composite implements ValueChangeHandler<String>,
	 ChangeHandler, ClickHandler
{
	@UiField
	StudentSuggestBox studentList;
	@UiField ListBox billList;
	
	@UiField TextBox totalAmount;
	@UiField TextBox priorPayment;
	@UiField TextBox balance;
	
	@UiField Label status;
	@UiField Label description;
	
	@UiField Button savePayment;
	
	private final static String DEFAULT_TITLE = "Please select a member bill to issue license for";
	private final static NumberFormat NUMBER_FORMAT = GUIConstants.DEFAULT_NUMBER_FORMAT;
	
	private static AccountingAsync accountService = Accounting.App.getInstance();

	private static HashMap<String, List<TableMessage>> cachedBillInfo = new HashMap<String, List<TableMessage>>();
	
	private static GenerateMemberLicenseUiBinder uiBinder = GWT
			.create(GenerateMemberLicenseUiBinder.class);

	interface GenerateMemberLicenseUiBinder extends
			UiBinder<Widget, IssueMemberLicense> {
	}
	
	final AsyncCallback<List<TableMessage>> billListCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
			WidgetHelper.setErrorStatus(description, "Unable to retrieve billing data for " + studentList.getSelectedUserDisplay(), false);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				WidgetHelper.setSuccessStatus(status, "Retrieved " + result.size() + " student bill(s)." + DEFAULT_TITLE, true);
				cachedBillInfo.put(studentList.getSelectedUser(), result);
				populateBillBox(result);
				displaySelectedBillInfo();
			}
			else
			{
				WidgetHelper.setErrorStatus(description, "No bills found for this student!!", false);
				WidgetHelper.setErrorStatus(status, "No bills found for student. Please generate a bill before attempting to enter payment.", true);				
			}
		}
	};
	
	final AsyncCallback<TableMessage> saveCallBack = new AsyncCallback<TableMessage>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			String start = GUIConstants.DEFAULT_DATE_FORMAT.format(result.getDate(DTOConstants.DATE_LICENCE_STARTS_IDX));
			String end = GUIConstants.DEFAULT_DATE_FORMAT.format(result.getDate(DTOConstants.DATE_LICENSE_ENDS_IDX));
			String name =  result.getText(DTOConstants.FULL_NAME_IDX);
			String msg = "Succesfully issued license for " + name + " for period of " + start + " to " + end;
			WidgetHelper.setSuccessStatus(status, msg, true);
			cachedBillInfo.remove(studentList.getSelectedUser());
			clearFields();
			studentList.clear();
			billList.clear();
		}
	};	

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public IssueMemberLicense() {
		initWidget(uiBinder.createAndBindUi(this));
		totalAmount.setEnabled(false);
		priorPayment.setEnabled(false);
		balance.setEnabled(false);;
		savePayment.setEnabled(false);
		studentList.addValueChangeHandler(this);
		billList.addChangeHandler(this);
		savePayment.addClickHandler(this);
	}

	private void clearFields()
	{
		totalAmount.setText("");
		priorPayment.setText("");
		balance.setText("");
		description.setText(DEFAULT_TITLE);
		savePayment.setEnabled(false);
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		clearFields();
		billList.clear();
		String selectedStudent = studentList.getSelectedUser();
		savePayment.setEnabled(false);
		List<TableMessage> billInfoList = cachedBillInfo.get(selectedStudent);
		if(billInfoList == null)
		{
			GWT.log("Selected student na: [" + selectedStudent + "]");
			WidgetHelper.setNormalStatus(status, "Fetching list of student bills. Please wait ...");
			accountService.getStudentBills(selectedStudent, billListCallBack);
		}
		else
		{
			GWT.log("BillPayment cache hit!!!");
			populateBillBox(billInfoList);
		}
	}
	
	private void populateBillBox(List<TableMessage> billInfo)
	{
		for(TableMessage m : billInfo)
			billList.addItem(m.getText(1), m.getMessageId());
	}
	
	private TableMessage getBillInfoFromCache(String studentID, String billID)
	{
		List<TableMessage> billInfoList = cachedBillInfo.get(studentID);
		for(TableMessage m : billInfoList)
			if(m.getMessageId().equals(billID))
				return m;
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		clearFields();
		savePayment.setEnabled(false);
		displaySelectedBillInfo();
	}
	
	private void displaySelectedBillInfo()
	{
		TableMessage billInfo = getBillInfoFromCache(studentList.getSelectedUser(), billList.getValue(billList.getSelectedIndex()));

		double total = billInfo.getNumber(0);
		double paid = billInfo.getNumber(1);
		totalAmount.setText(NUMBER_FORMAT.format(total));
		priorPayment.setText(NUMBER_FORMAT.format(paid));
		balance.setText(NUMBER_FORMAT.format(total-paid));
		
		if(billInfo.getText(2).equals("True")) //fully paid already
		{
			WidgetHelper.setSuccessStatus(description, billList.getItemText(billList.getSelectedIndex()), false);
			WidgetHelper.setNormalStatus(status, "Click to proceed for " + studentList.getSelectedUserDisplay() );
			savePayment.setEnabled(true);
		}
		else
		{
			WidgetHelper.setErrorStatus(description, "Bill is yet to be confirmed as settled", false);
			WidgetHelper.setErrorStatus(status, "You can not issue a license until you confirm this bill has been fully paid", false);
		}
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		String billKey = billList.getValue(billList.getSelectedIndex());
		String studKey = studentList.getSelectedUser();
		if (studKey == null)
		{
			WidgetHelper.errorBox.show("Must select a valid member");
			return;
		}

			accountService.issueLicense(studKey, billKey, saveCallBack);

		WidgetHelper.setNormalStatus(status, "Saving payment. Please wait ...");		
	}

}
