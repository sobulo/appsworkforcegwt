package com.fertiletech.sap.office.content.acctpanels.licensing;

import com.fertiletech.sap.client.Accounting;
import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.utils.WidgetHelper;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.SearchDateBox;
import com.fertiletech.sap.office.util.YesNoDialog;
import com.fertiletech.sap.office.util.table.ShowcaseTable;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.NPMBFormConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;

import java.util.Date;
import java.util.List;

public class VerifyPayments extends ResizeComposite{

    AsyncCallback<TableMessage> verifyCb = new AsyncCallback<TableMessage>() {
        @Override
        public void onFailure(Throwable caught) {
            PanelUtilities.errorBox.show("Error: " + caught.getMessage());
        }

        @Override
        public void onSuccess(TableMessage result) {
            PanelUtilities.infoBox.show("Verification OK");
            List<TableMessage> updatedData = paymentTable.getDataList();
            updatedData = updateList(updatedData, result);
            paymentTable.showTable(updatedData);
        }
    };

   AsyncCallback<List<TableMessage>> userInfoCb = new AsyncCallback<List<TableMessage>>() {
       @Override
       public void onFailure(Throwable caught) {
           PanelUtilities.errorBox.show("Error loading user information: " + caught.getMessage());
       }

       @Override
       public void onSuccess(List<TableMessage> result) {
           GWT.log("Here 1 of 6");
           if(result == null || result.size() == 0)
           {
               PanelUtilities.errorBox.show("Looks like this user has not yet commenced registration. Investigate further");
               return;
           }
           GWT.log("Here 2");
           //get rid of the header row
           if(result.get(0) instanceof  TableMessageHeader)
               result.remove(0);
           else
           {
               PanelUtilities.errorBox.show("Bad configuration. Expected a header");
               return;
           }
           GWT.log("Here 3");

           if(result.size() > 1)
           {
               PanelUtilities.errorBox.show("Multiple registrations not yet supported. Found " + result.size());
               return;
           }
           GWT.log("Here 4 with: " + result.size());
           final TableMessage member = result.get(0);
           GWT.log("member fields: " + member.getNumberOfTextFields());
           final TableMessage payment = paymentTable.getSelectedRow();
           GWT.log("payment fields: " + payment.getNumberOfTextFields());
           GWT.log("Here 5");
           String memberEmail = member.getText(DTOConstants.EMAIL_IDX);
           String paymentEmail = payment.getText(DTOConstants.PAY_EMAIL_TXT_IDX);
           GWT.log("Here 6");
           if(!memberEmail.equalsIgnoreCase(paymentEmail))
           {
               PanelUtilities.errorBox.show("Mismatch user email: " + memberEmail + " vs payment email: " + paymentEmail);
           }

           String amount = GUIConstants.DEFAULT_NUMBER_FORMAT.format(payment.getNumber(DTOConstants.PAY_AMT_NUM_IDX));
           String date = GUIConstants.DEFAULT_DATE_FORMAT.format(payment.getDate(DTOConstants.PAY_DUE_DATE_IDX));
           final YesNoDialog verifyPanel = new YesNoDialog("Verify Payment for " + member.getText(DTOConstants.FULL_NAME_IDX)
           + " Email: " + paymentEmail + " Phone: " + member.getText(DTOConstants.PHONE_NUMBER_IDX) + " Amount: " + amount
           + " Payment Date: " + date);

           verifyPanel.show("Click yes if you have visually confirmed payment via bank alert (text/email) or checking account balance" +
                   " online");
           verifyPanel.setClickHandler(new ClickHandler() {
               @Override
               public void onClick(ClickEvent event) {
                   verifyPanel.hide();
                   Accounting.App.getInstance().verifyPayment(member.getMessageId(), payment.getMessageId(), verifyCb );
               }
           });

       }
   };

    ShowcaseTable paymentTable;
    AsyncCallback<List<TableMessage>> payListCb = new AsyncCallback<List<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {

            PanelUtilities.errorBox.show("Error loading payments: " + caught.getMessage());
        }

        @Override
        public void onSuccess(List<TableMessage> result) {
            paymentTable.showTable(result);
        }
    };

    public VerifyPayments()
    {
        paymentTable = new ShowcaseTable();
        paymentTable.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
                @Override
                public void onValueChange(ValueChangeEvent<TableMessage> event) {

                TableMessage m = event.getValue();
                String verItemStr = m.getText(DTOConstants.PAY_VERIFIED_TXT_IDX);
                if(!verItemStr.equals(NPMBFormConstants.PENDING_VERIFICATION))
                {
                    PanelUtilities.errorBox.show("Looks like this payment has already been verified " + verItemStr );
                    return;
                }

                PanelUtilities.getLoginService().getLoanApplications(m.getText(DTOConstants.PAY_EMAIL_TXT_IDX), userInfoCb);
            }
        });
        DockLayoutPanel container = new DockLayoutPanel(Style.Unit.PX);
        SearchDateBox search = new SearchDateBox();
        final Date searchDates [] = search.getSearchDates();
        search.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                Accounting.App.getInstance().getPayments(searchDates[0], searchDates[1], payListCb);
            }
        });
        container.addNorth(search, 50);
        container.add(paymentTable);
        initWidget(container);
    }

    private List<TableMessage> updateList(List<TableMessage> sourceList, TableMessage target)
    {
        for(int idx = 0; idx < sourceList.size(); idx ++)
        {
            TableMessage source = sourceList.get(idx);
            if(source.getMessageId().equals(target.getMessageId()))
            {
                sourceList.remove(idx);
                sourceList.add(idx, target);
                return sourceList;
            }
        }
        WidgetHelper.errorBox.show("Unable to refresh data table, try refresh your browser to see verification changes");
        return sourceList;
    }
}

