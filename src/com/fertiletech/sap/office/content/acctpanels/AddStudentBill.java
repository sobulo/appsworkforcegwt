/**
 * 
 */
package com.fertiletech.sap.office.content.acctpanels;


import com.fertiletech.sap.client.utils.BillTemplateBox;
import com.fertiletech.sap.client.utils.StudentSuggestBox;
import com.fertiletech.sap.client.utils.WidgetHelper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class AddStudentBill extends Composite implements ClickHandler{
;
	@UiField
	StudentSuggestBox studentBox;
	@UiField
	BillTemplateBox billList;
	@UiField
	Label status;
	@UiField
	Button requestBills;
	
    private final static int MAX_DISPLAY_ITEMS = 15;


	private static AddStudentBillUiBinder uiBinder = GWT
			.create(AddStudentBillUiBinder.class);

	interface AddStudentBillUiBinder extends UiBinder<Widget, AddStudentBill> {
	}


	
	final AsyncCallback<String> saveCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			WidgetHelper.setErrorStatus(status, caught.getMessage(), true);
		}

		@Override
		public void onSuccess(String result) {
			WidgetHelper.setSuccessStatus(status, result, true);
		}
	};	

	/**
	 * Because this class has a default constructor, it can be used as a binder
	 * template. In other words, it can be used in other *.ui.xml files as
	 * follows: <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 * xmlns:g="urn:import:**user's package**">
	 * <g:**UserClassName**>Hello!</g:**UserClassName> </ui:UiBinder> Note that
	 * depending on the widget that is used, it may be necessary to implement
	 * HasHTML instead of HasText.
	 */
	public AddStudentBill() {
		initWidget(uiBinder.createAndBindUi(this));
        billList.initData();
        requestBills.addClickHandler(this);
	}
	
    @Override
    public void onClick(ClickEvent event) {
        String chosen = studentBox.getSelectedUser();
        String billTemplateKey = billList.getSelectedBillTemplate();
        
        //perform validation
        if(chosen == null)
        {
        	WidgetHelper.setErrorStatus(status, "You must choose a member!", true);
        	return;
        }
        
        if(billTemplateKey.length() == 0)
        {
        	WidgetHelper.setErrorStatus(status, "No bill template selected. Please create a" +
        			" bill template if none exists, then refresh your browser.", true);
        	return;
        }
        
        WidgetHelper.setNormalStatus(status, "Generating " + billList.getSelectedBillTemplateName() + " bills for " 
        		+ studentBox.getSelectedUserDisplay());
        
        //send list to server to save
        String[] studentKeys = {studentBox.getSelectedUser()};
        billList.getAccountService().createUserBill(billTemplateKey, studentKeys, saveCallBack);

    }
}