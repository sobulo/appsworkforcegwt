package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.Showcase;
import com.fertiletech.sap.office.util.SearchEmailSimpleBox;
import com.fertiletech.sap.office.util.text.RichTextToolbar;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.List;

public class TrackFeedbackCW extends ContentWidget{

	private RichTextArea richText;
	Button save;
	RichTextToolbar tb;
	ListBox  courseBox;
	SearchEmailSimpleBox emailBox;
	private final int MIN_COMMENT_LENGTH = 30;

	public TrackFeedbackCW() {
		super("Member Feedback", "Add feedback for selected member");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		GWT.log("Created  container");
		save = new Button("Save Feedback");
        GWT.log("Created  save button");
		emailBox = new SearchEmailSimpleBox();
        GWT.log("Created  email");
		courseBox = new ListBox();
        GWT.log("Created  course box");
		save.addClickHandler(getSaveHandler());
        GWT.log("Created  save hander");
		emailBox.addClickHandler(getCourseBoxHandler());
        GWT.log("Created  courseBox handler");
        container.addNorth(emailBox, 30);
		container.addNorth(courseBox, 30);
        GWT.log("initialized header");
		container.add(getRichTextArea());
        GWT.log("Created  rich text");
        enableSaveWidgets(false);
        GWT.log("disabled save widgets");
		return container;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(TrackFeedbackCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

	private Widget getRichTextArea()
	{
		HorizontalPanel p = new HorizontalPanel();
		richText = new RichTextArea();
		tb = new RichTextToolbar(richText, save);
		p.add(richText);
		p.add(tb);
		richText.setHeight("100px");
		richText.setWidth("100%");
		//tb.setWidth("100%");
		p.setSpacing(5);
		p.setWidth("100%");
		return p;
	}

	private ClickHandler getCourseBoxHandler()
    {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String email = emailBox.getEmailAddress();
                if(email == null)
                    return;
                AsyncCallback<List<TableMessage>> trackedCoursesCallBack = new AsyncCallback<List<TableMessage>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        PanelUtilities.errorBox.show("Unable to fetch tracks for: " + emailBox.getEmailAddress() +
                                " Error was: " + caught.getMessage());
                    }

                    @Override
                    public void onSuccess(List<TableMessage> result) {
                        result.remove(0); //ditch header
                        if(result.size() == 0)
                            PanelUtilities.errorBox.show("No courses are currently being tracked by: " + emailBox.getEmailAddress());

                        courseBox.clear();
                        for(TableMessage track : result)
                            courseBox.addItem(track.getText(DTOConstants.TRACK_COURSE_TXT_IDX), track.getMessageId());
                        enableSaveWidgets(true);

                    }
                };
                Moocs.App.getInstance().getAllMemberTracks(email, trackedCoursesCallBack);
            }
        };
    }

	private ClickHandler getSaveHandler()
	{
		return new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if(richText.getText().length() < MIN_COMMENT_LENGTH)
				{
					PanelUtilities.errorBox.show("Comment length too short. Please enter a more detailed description");
					return;
				}
				String course = courseBox.getSelectedItemText();
				String email = emailBox.getEmailAddress();
				if(email == null)
					return;

				AsyncCallback<String> feedCallback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						PanelUtilities.errorBox.show("Error updating Feedback");
						enableSaveWidgets(true);
					}

					@Override
					public void onSuccess(String result) {
						PanelUtilities.infoBox.show("Succesfully updated " + result);
						enableSaveWidgets(true);
						History.newItem(Showcase.getContentWidgetToken(TrackStatusCW.class) + "/" + courseBox.getSelectedValue());

					}
				} ;
				enableSaveWidgets(false);
				Moocs.App.getInstance().addFeedBack(email, course, richText.getHTML(), feedCallback);
			}
		};
	}

	private void enableSaveWidgets(boolean enabled)
	{
		if(save != null)
		{
			richText.setEnabled(enabled);
			save.setEnabled(enabled);
			tb.setEnabled(enabled);
			courseBox.setEnabled(enabled);
		}

	}

}
