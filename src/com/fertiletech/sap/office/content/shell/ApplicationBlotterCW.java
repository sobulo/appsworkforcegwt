package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.SearchDateBox;
import com.fertiletech.sap.office.util.table.ShowcaseTable;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.Date;
import java.util.List;

public class ApplicationBlotterCW extends ContentWidget{
	

	public ApplicationBlotterCW() {
		super("Applications Blotter", "View a list of applications created over the date range specified. Click checkbox to view details");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		final SearchDateBox searchBox = new SearchDateBox();
		final ShowcaseTable searchResults = new ShowcaseTable();
		searchResults.addValueChangeHandler(new StatusCW.StatusJumper());
		final CheckBox isCreateQuery = new CheckBox(" by date created: ");
		searchBox.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Error retrieving search results. <br/> " + caught.getMessage());
					searchBox.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					searchResults.showTable(result);
					searchBox.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				Date[] params = searchBox.getSearchDates();
				if(params == null) return;
				searchBox.setEnabled(false);
				if(isCreateQuery.getValue())
					PanelUtilities.getLoanMktService().getSaleLeads(params[0], params[1], searchCallback );
				else
					PanelUtilities.getLoanMktService().getModifiedSaleLeads(params[0], params[1], searchCallback );
					
			}
		});
		HorizontalPanel northBar = new HorizontalPanel();
		northBar.setSpacing(15);
		northBar.add(searchBox);
		northBar.add(isCreateQuery);
		container.addNorth(northBar, 50);
		container.add(searchResults);
		return container;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(ApplicationBlotterCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
