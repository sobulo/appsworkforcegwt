package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.office.CWArguments;
import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.Showcase;
import com.fertiletech.sap.office.content.TrackStatus;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class TrackStatusCW extends ContentWidget implements CWArguments{
	String savedId;
	TrackStatus statusPanel;
	public TrackStatusCW() {
		super("Track Status", "View track updates and feedback");
	}

	@Override
	public void setParameterValues(String args) {
		if(statusPanel == null)
			savedId = args;
		else
			statusPanel.setTrackID(args);
	}

	@Override
	public Widget onInitialize() {
		statusPanel = new TrackStatus();
		if(savedId != null)
			statusPanel.setTrackID(savedId);
		return statusPanel;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(TrackStatusCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	static class TrackStatusJumper implements ValueChangeHandler<TableMessage>
	{

		@Override
		public void onValueChange(ValueChangeEvent<TableMessage> event) {
			History.newItem(Showcase.getContentWidgetToken(TrackStatusCW.class) + "/" + event.getValue().getMessageId());
		}
	}
}
