package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.PanelUtilities;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

public class EmailPanelCW extends ContentWidget{

    public EmailPanelCW()
    {
        super("Email Sender", "Utility to allow sending emails. Great for testing emails to be sent for typos etc");
    }


    private Widget getDisplay()
    {
        DockLayoutPanel container = new DockLayoutPanel(Style.Unit.PX);
        final TextBox toAddress = new TextBox();
        Label toLabel = new Label("To:");
        final TextBox messageSubject = new TextBox();
        Label subjectLabel = new Label("Subject:");
        final TextArea messageBox = new TextArea();
        messageBox.setPixelSize(600, 600);
        Label messageLabel = new Label("HTML Message: ");
        HorizontalPanel toBar = new HorizontalPanel();
        toBar.add(toLabel);
        toBar.add(toAddress);
        HorizontalPanel subjectBar = new HorizontalPanel();
        subjectBar.add(subjectLabel);
        subjectBar.add(messageSubject);
        Button send = new Button("Send Message");
        VerticalPanel body = new VerticalPanel();
        body.add(messageLabel);
        body.add(messageBox);
        container.addNorth(toBar, 30);
        container.addNorth(subjectBar, 30);
        container.add(body);
        container.addSouth(send, 30);
        send.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String to = toAddress.getText();
                String message = messageBox.getText();
                String subject = messageSubject.getText();;
                PanelUtilities.getLoginService().sendMessage(to, subject, message, new AsyncCallback<String>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        PanelUtilities.errorBox.show("Error sending message: " + caught.getMessage());
                    }

                    @Override
                    public void onSuccess(String result) {
                        PanelUtilities.infoBox.show(result);

                    }
                });
            }
        });
        return container;
    }

    @Override
    public Widget onInitialize() {
        return getDisplay();
    }

    @Override
    protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
        GWT.runAsync(EmailPanelCW.class, new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess() {
                callback.onSuccess(onInitialize());
            }
        });

    }

    @Override
    protected String getHelpUrl() {
        return HelpPageGenerator.HELP_ADMIN_URL;
    }
}
