package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.SearchDateBox;
import com.fertiletech.sap.office.util.SearchEmailSimpleBox;
import com.fertiletech.sap.office.util.table.ShowcaseTable;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.Date;
import java.util.List;

public class TracksForMemberCW extends ContentWidget{
	SearchEmailSimpleBox emailBox;
	ShowcaseTable searchResults;
	


	public TracksForMemberCW() {
		super("View Member Tracks", "View a list of member tracks. Click checkbox to view details");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		emailBox = new SearchEmailSimpleBox();
		searchResults = new ShowcaseTable();
		searchResults.addValueChangeHandler(new TrackStatusCW.TrackStatusJumper());
		emailBox.addClickHandler(getCourseBoxHandler());
		container.addNorth(emailBox, 50);
		container.add(searchResults);
		return container;
	}
	
	private ClickHandler getCourseBoxHandler()
    {
		final AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error retrieving search results. <br/> " + caught.getMessage());
				emailBox.setEnabled(true);
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				searchResults.showTable(result);
				emailBox.setEnabled(true);
			}
		};
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String email = emailBox.getEmailAddress();
                if(email == null)
                    return;
                emailBox.setEnabled(false);
                Moocs.App.getInstance().getAllMemberTracks(email, searchCallback);
            }
        };
    }

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(TracksForMemberCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
