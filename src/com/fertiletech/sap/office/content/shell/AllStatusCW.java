package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.SearchEmailBox;
import com.fertiletech.sap.office.util.table.ShowcaseTable;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.List;

public class AllStatusCW extends ContentWidget{

	public AllStatusCW() {
		super("Applicant History", "View all applications submitted for a particular applicant by searching for their email address." +
				" You may also create new applications using this module");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		final SearchEmailBox searchBox = new SearchEmailBox();
		final ShowcaseTable searchResults = new ShowcaseTable();
		searchResults.addValueChangeHandler(new StatusCW.StatusJumper());
		searchBox.addClickHandler(new ClickHandler() {			
			private AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Error retrieving search results. <br/> " + caught.getMessage());
					searchBox.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					searchBox.enableNewApps(Boolean.valueOf(result.get(0).getMessageId()));
					searchResults.showTable(result);
					searchBox.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				String param = searchBox.getEmailAddress();
				if(param == null) return;
				searchBox.setEnabled(false);
				PanelUtilities.getLoginService().getLoanApplications(param, searchCallback);
			}
		});
		container.addNorth(searchBox, 50);
		container.add(searchResults);
		return container;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(AllStatusCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
