package com.fertiletech.sap.office.content.shell;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.YesNoDialog;
import com.fertiletech.sap.office.util.text.RichTextToolbar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ApplicationMailTemplateCW extends ContentWidget{
	
	private RichTextArea richText;
	private TextArea plainText;
	private Button save;
	private Button saveHTML;
	private RichTextToolbar tb;
	private TextBox templateBox;
	private final int MIN_COMMENT_LENGTH = 100;
	

	public ApplicationMailTemplateCW() {
		super("Mail Templates", "Create mail template objects that can be used by bulk mails");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel wrapper = new DockLayoutPanel(Unit.PX);
		templateBox = new TextBox();
		wrapper.addNorth(templateBox, 30);
		
		TabLayoutPanel container = new TabLayoutPanel(20, Unit.PX);
		container.add(getRichTextArea(), "Rich Text");
		container.add(getPlainTextArea(), "HTML");
		wrapper.add(container);
		return wrapper;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(ApplicationMailTemplateCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	private Widget getRichTextArea()
	{
		HorizontalPanel p = new HorizontalPanel();
		richText = new RichTextArea();
		save = new Button("Send Email");
		save.addClickHandler(getSaveHandler());
		tb = new RichTextToolbar(richText, save);
		p.add(richText);
		p.add(tb);
		richText.setHeight("600px");
		richText.setWidth("100%");
		//tb.setWidth("100%");
		p.setSpacing(5);
		p.setWidth("100%");
		return p;
	}
	
	private Widget getPlainTextArea()
	{
		VerticalPanel p = new VerticalPanel();
		plainText = new TextArea();
		saveHTML = new Button("Send HTML Email");
		saveHTML.addClickHandler(getSaveHandler());
		p.add(saveHTML);
		p.add(plainText);
		plainText.setHeight("500px");
		plainText.setWidth("100%");
		//tb.setWidth("100%");
		p.setSpacing(5);
		p.setWidth("100%");
		return p;
	}
	
	private ClickHandler getSaveHandler()
	{
		return new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String html = "";
				String text = "";
				if(event.getSource() == save)
				{
					html = richText.getHTML();
					text = richText.getText();
				
				}
				else
				{
					html = plainText.getText();
					HTML temp = new HTML(html);
					text = temp.getText(); //alternate regex html.replaceAll("\\<[^>]*>","");
				}
				

					
				if(text.length() < MIN_COMMENT_LENGTH)
				{
					PanelUtilities.errorBox.show("Message tempplate too short");
					return;
				}
				
				final YesNoDialog confirmUpload = new YesNoDialog("Message: " + templateBox.getText());
				final String htmlArg = html;
				final String textArg = text; //anonymous class below requires variables be final
				confirmUpload.setClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						confirmUpload.hide();
						createTemplate(textArg, htmlArg);
						
					}
				});
				confirmUpload.show("Go ahead and create template for: " + " [" + html + "]");
				
				
				//if(email == null)
				//	return;

				/*
				  AsyncCallback<String> feedCallback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						PanelUtilities.errorBox.show("Error updating Feedback");
						enableSaveWidgets(true);
					}

					@Override
					public void onSuccess(String result) {
						PanelUtilities.infoBox.show("Succesfully updated " + result);
						enableSaveWidgets(true);
						History.newItem(Showcase.getContentWidgetToken(TrackStatusCW.class) + "/" + courseBox.getSelectedValue());

					}
				} ;
				enableSaveWidgets(false);
				Moocs.App.getInstance().addFeedBack(email, course, richText.getHTML(), feedCallback);
				*/
			}
		};
	}
	
	private void createTemplate(String contentText, String contentHTML)
	{
		AsyncCallback<Void> templateCallback = new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				PanelUtilities.infoBox.show("Template update successfully for: " + templateBox.getText());
				enableSaveWidgets(true);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Update failed: " + caught.getMessage());
				
			}
		};
		enableSaveWidgets(false);
		PanelUtilities.getLoginService().setMessageContent(templateBox.getText(), contentText, contentHTML, templateCallback);
		
	}
	private void enableSaveWidgets(boolean enabled)
	{
		if(save != null)
		{
			richText.setEnabled(enabled);
			save.setEnabled(enabled);
			saveHTML.setEnabled(enabled);
			tb.setEnabled(enabled);
			templateBox.setEnabled(enabled);
		}

	}
}
