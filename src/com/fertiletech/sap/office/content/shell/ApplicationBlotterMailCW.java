package com.fertiletech.sap.office.content.shell;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.fertiletech.sap.office.ContentWidget;
import com.fertiletech.sap.office.HelpPageGenerator;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.SearchDateBox;
import com.fertiletech.sap.office.util.YesNoDialog;
import com.fertiletech.sap.office.util.table.ShowcaseTable;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class ApplicationBlotterMailCW extends ContentWidget{
	
	private ShowcaseTable searchResults;
	final CheckBox selectAll = new CheckBox(" select All: ");
	ListBox mailTemplates;
	

	public ApplicationBlotterMailCW() {
		super("Mail Blotter", "Email a list of Applicants");
	}

	@Override
	public Widget onInitialize() {
		DockLayoutPanel container = new DockLayoutPanel(Unit.PX);
		final SearchDateBox searchBox = new SearchDateBox();
		searchResults = new ShowcaseTable(true);
		final CheckBox isCreateQuery = new CheckBox(" by date created: ");
		searchBox.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Error retrieving search results. <br/> " + caught.getMessage());
					searchBox.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					searchResults.showTable(result);
					searchBox.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				Date[] params = searchBox.getSearchDates();
				if(params == null) return;
				searchBox.setEnabled(false);
				if(isCreateQuery.getValue())
					PanelUtilities.getLoanMktService().getSaleLeads(params[0], params[1], searchCallback );
				else
					PanelUtilities.getLoanMktService().getModifiedSaleLeads(params[0], params[1], searchCallback );
					
			}
		});
		selectAll.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
			searchResults.selectAll(selectAll.getValue());
			}
			
		});
		HorizontalPanel northBar = new HorizontalPanel();
		northBar.setSpacing(15);
		northBar.add(searchBox);
		northBar.add(isCreateQuery);
		northBar.add(selectAll);
		container.addNorth(northBar, 50);
		container.addSouth(getMailTemplates(), 50);
		container.add(searchResults);
		
		
		return container;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(ApplicationBlotterMailCW.class, new RunAsyncCallback() {
			
			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
			
			@Override
			public void onFailure(Throwable reason) {
				callback.onFailure(reason);
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	private final Button save = new Button("Send Email");
	private Widget getMailTemplates()
	{
		HorizontalPanel p = new HorizontalPanel();	
		save.addClickHandler(getSaveHandler());
		save.setEnabled(false);
		mailTemplates = new ListBox();
		//populate mail templates
		PanelUtilities.getLoginService().getAllMessageNames(new AsyncCallback<String[]>() {
			
			@Override
			public void onSuccess(String[] result) {
				if(result.length == 0)
					PanelUtilities.errorBox.show("No Templates Found");
				else
				{
					for(String templateName : result)
						mailTemplates.addItem(templateName);
					save.setEnabled(true);
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error fetching message templates" + caught.getMessage());
				
			}
		});
		p.add(mailTemplates);
		p.add(save);
		p.setWidth("100%");
		return p;
	}
	
	private ClickHandler getSaveHandler()
	{
		return new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
	
				
				final YesNoDialog confirmUpload = new YesNoDialog("WARNING - Bulk Email");
				final Set<TableMessage> mailingList = searchResults.getSelectedSet();
				confirmUpload.setClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						confirmUpload.hide();
						
						if(mailingList.size() == 0)
						{
							PanelUtilities.errorBox.show("no member selected");
							return;
						}
						String category = mailTemplates.getSelectedValue();
						save.setEnabled(false);
						PanelUtilities.getLoginService().scheduleBulkMail(mailingList, category, new AsyncCallback<Integer>() {
							
							@Override
							public void onSuccess(Integer numScheduled) {
								PanelUtilities.infoBox.show(numScheduled + " scheduled, out of " + mailingList.size());
								save.setEnabled(true);
								
							}
							
							@Override
							public void onFailure(Throwable caught) {
								PanelUtilities.errorBox.show("Error sending mail: " + caught.getMessage());
								save.setEnabled(true);
								
							}
						});
					}
				});
				confirmUpload.show(mailingList.size() + " selected, Do you want to proceed with bulk mail?");
				
				
				//if(email == null)
				//	return;

				/*
				  AsyncCallback<String> feedCallback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						PanelUtilities.errorBox.show("Error updating Feedback");
						enableSaveWidgets(true);
					}

					@Override
					public void onSuccess(String result) {
						PanelUtilities.infoBox.show("Succesfully updated " + result);
						enableSaveWidgets(true);
						History.newItem(Showcase.getContentWidgetToken(TrackStatusCW.class) + "/" + courseBox.getSelectedValue());

					}
				} ;
				enableSaveWidgets(false);
				Moocs.App.getInstance().addFeedBack(email, course, richText.getHTML(), feedCallback);
				*/
			}
		};
	}
	

}
