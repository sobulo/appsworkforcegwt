package com.fertiletech.sap.office.content;

import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.office.PanelUtilities;
import com.fertiletech.sap.office.util.SearchEmailSimpleBox;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

import java.util.List;

public class TrackStatus extends ResizeComposite{

	@UiField
	ListBox courseBox;

	@UiField
	SearchEmailSimpleBox emailBox;

	@UiField
	ListBox statusUpdatekBox;

	@UiField
	ListBox feedBackBox;

	@UiField
	HTML feed;

	@UiField
	HTML status;

	AsyncCallback<String> statusCallBack =  new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			PanelUtilities.errorBox.show("Error fetching status update: " + caught.getMessage());

		}

		@Override
		public void onSuccess(String result) {
			status.setHTML(result);

		}
	};

	AsyncCallback<String> feedBackCallBack =  new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			PanelUtilities.errorBox.show("Error fetching feedback: " + caught.getMessage());

		}

		@Override
		public void onSuccess(String result) {
			feed.setHTML(result);

		}
	};

	AsyncCallback<List<TableMessage>[]> trackFeedCallback = new AsyncCallback<List<TableMessage>[]>() {
		@Override
		public void onFailure(Throwable caught) {
			PanelUtilities.errorBox.show("Error fetching track: " + caught.getMessage());

		}

		@Override
		public void onSuccess(List<TableMessage>[] result) {
			displayTrack(result);

		}
	};


	private static TrackStatusUiBinder uiBinder = GWT
			.create(TrackStatusUiBinder.class);

	interface TrackStatusUiBinder extends
			UiBinder<Widget, TrackStatus> {
	}

	boolean courseBoxInitialized = false;
	public TrackStatus() {
		initWidget(uiBinder.createAndBindUi(this));
		emailBox.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				searchTracksByCourse();
			}
		});
		//populate courseBox
		AsyncCallback<List<TableMessage>> courseCallBack = new AsyncCallback<List<TableMessage>>() {
			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Unable to retrieve list of courses: " + caught);

			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				result.remove(0); //ditch the header
				for(TableMessage courseMessage: result)
					courseBox.addItem(courseMessage.getText(DTOConstants.COURSE_NAME_TXT_IDX));
				courseBoxInitialized = true; //used to manage timing issues

			}
		};
		Moocs.App.getInstance().getAllCourses(courseCallBack);
		statusUpdatekBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				//set display to show last item in each drop down
				Moocs.App.getInstance().getTrack(emailBox.getEmailAddress(), Integer.valueOf(statusUpdatekBox.getSelectedValue()), courseBox.getSelectedValue(), statusCallBack);

			}
		});
		feedBackBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				//set display to show last item in each drop down
				Moocs.App.getInstance().getFeed(emailBox.getEmailAddress(), Integer.valueOf(feedBackBox.getSelectedValue()), courseBox.getSelectedValue(), feedBackCallBack);
			}
		});

	}


	public void setTrackID(String id)
	{
		if(id == null || id.length() == 0)
			return;

		PanelUtilities.getMOOCService().getCourseTrackFeed(id, trackFeedCallback);
	}

	private Boolean setCourseName(String name)
	{
	    if(courseBoxInitialized) {
            for (int i = 0; i < courseBox.getItemCount(); i++) {
                if (courseBox.getValue(i).equals(name)) {
                    courseBox.setSelectedIndex(i);
                    return true;
                }
            }
            return false;
        }
        return null;

	}

	private void setUpFeedUpdates(List<TableMessage> sourceList, ListBox target, boolean isUpdate)
	{
	    target.clear();
		String prefix;
		int posIdx;
		int dateIdx;
		if(isUpdate)
		{
			prefix = "Update ";
			posIdx = DTOConstants.FEED_TRACK_POS_NUM_IDX;
			dateIdx = DTOConstants.FEED_TRACK_DATE_IDX;
		}
		else
		{
			prefix = "Feed ";
			posIdx = DTOConstants.FEED_BACK_POS_NUM_IDX;
			dateIdx = DTOConstants.FEED_BACK_DATE_IDX;
		}

		//remove header
		sourceList.remove(0);
		for(TableMessage source : sourceList)
		{
			String pos = NumberFormat.getFormat("###").format(source.getNumber(posIdx));
			String date = GUIConstants.DEFAULT_DATE_FORMAT.format(source.getDate(dateIdx));
			target.addItem(prefix + pos + " - " + date, pos);
		}

	}



	boolean doRecursion = true;
	private void displayTrack(final List<TableMessage>[] trackInfo)
	{
	    status.setHTML("");
	    feed.setHTML("");
		//get appendix
		TableMessage appendix = trackInfo[DTOConstants.COURSE_APPENDIX_LIST_IDX].get(0);
		//set coursename and email using appendix
        String course = appendix.getText(DTOConstants.FEED_APPENDIX_COURSE_TXT_IDX);
        Boolean okSet = setCourseName(course);
        if(okSet == null)
        {
            if(doRecursion) {
                Timer t = new Timer() {
                    @Override
                    public void run() {
                        doRecursion = false;
                        displayTrack(trackInfo);
                    }
                };
                t.schedule(3000);
                return;
            }
        }
        if(okSet == null)
        {
            PanelUtilities.errorBox.show("Timing issue. Course box not loaded. Slow internet?");
            return;
        }

		if(!okSet)
		{
			PanelUtilities.errorBox.show("Unable to find course: [" + course + "]");
			return;
		}
		emailBox.setEmailValue(appendix.getText(DTOConstants.FEED_APPENDIX_EMAIL_TXT_IDX));

		//populate feed and status dropdowns
		setUpFeedUpdates(trackInfo[DTOConstants.COURSE_TRACK_LIST_IDX], statusUpdatekBox, true);
		setUpFeedUpdates(trackInfo[DTOConstants.COURSE_FEED_LIST_IDX], feedBackBox, false);

		//set display to show last item in each drop down
		Moocs.App.getInstance().getTrack(emailBox.getEmailAddress(), statusUpdatekBox.getItemCount(), courseBox.getSelectedValue(), statusCallBack);
		Moocs.App.getInstance().getFeed(emailBox.getEmailAddress(), feedBackBox.getItemCount(), courseBox.getSelectedValue(), feedBackCallBack);
	}
	
	private void searchTracksByCourse()
	{
		String email = emailBox.getEmailAddress();
		if(email == null)
			return;
		Moocs.App.getInstance().getMemberTrackFeed(email, courseBox.getSelectedValue(), trackFeedCallback);
	}
}
