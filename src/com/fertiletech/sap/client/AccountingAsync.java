package com.fertiletech.sap.client;

import com.fertiletech.sap.shared.BillDescriptionItem;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

public interface AccountingAsync
{

    void createBillTemplate(String name, String year, String term, Date dueDate,
                            LinkedHashSet<BillDescriptionItem> itemizedBill, String bank, String accountName, String accountNumber, String instructions, AsyncCallback<String> async);

    void getBillTemplateList(AsyncCallback<HashMap<String, String>> async);

    void createUserBill(String billTemplate, String[] users, AsyncCallback<String> async);

    void getStudentBills(String studentID, AsyncCallback<List<TableMessage>> async);

    void getStudentBillKeys(String studentID, boolean isLogin, AsyncCallback<HashMap<String, String>> async);

    void getStudentBillKeys(AsyncCallback<HashMap<String, String>> async);

    void getStudentBillKeysFromTemplate(String templateKeyStr, AsyncCallback<HashMap<String, String>> async);

    void savePayment(String billKey, double amount, Date payDate, String referenceID,
                     String comments, AsyncCallback<String> async);

    void getBillTemplateInfo(String billTemplateKeyStr, AsyncCallback<List<TableMessage>> async);

    void getStudentBillAndPayment(String billKeyStr, AsyncCallback<List<TableMessage>> async);

    void getBillsForTemplate(String billDescKeyStr, AsyncCallback<List<TableMessage>> async);

    void getBillPayments(String billKeyStr, AsyncCallback<List<TableMessage>> async);

    void getInvoiceDownloadLink(String billTemplateKeyStr, String[] billKeyStrs, AsyncCallback<String> async);


    void getLicenseInfo(String leadKeyStr, AsyncCallback<Date[]> async);

    void getPayments(Date start, Date end, AsyncCallback<List<TableMessage>> async);

    void issueLicense(String leadStr, String billStr, AsyncCallback<TableMessage> async);

    void verifyPayment(String leadKeyStr, String payKeyStr, AsyncCallback<TableMessage> async);

    void getBillDownloadLink(String billKeyStr, AsyncCallback<String> async);

    void getBills(Date startDate, Date endDate, AsyncCallback<List<TableMessage>> async);
}
