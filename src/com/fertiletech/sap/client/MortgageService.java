package com.fertiletech.sap.client;

import com.fertiletech.sap.shared.*;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("../mortgage")
public interface MortgageService extends RemoteService {
	String[] saveLoanApplicationData(HashMap<String, String> appData, String loanKeyStr, boolean isSubmit) throws MissingEntitiesException;
	String[] startLoanApplication(HashMap<String, String> appData) throws DuplicateEntitiesException, MissingEntitiesException;
	HashMap<String, String> getStoredLoanApplication(String loanKeyStr);
	TableMessage getLoanState(String loanID);
	TableMessage getLoanState(long loanID);
	HashMap<String, String> getLoanApplicationWithLoanID(String loanKeyStr);
	String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header);		
	List<TableMessage> getSaleLeads(Date startDate, Date endDate);
	String[] getApplicationFormDownloadLink(String appFormID);
	List<TableMessage> getSalesLeadCollection(String loanID);
	String changeApplicationState(String loanStr, WorkflowStateInstance state,
			String message) throws MissingEntitiesException;
	HashMap<String, Integer> getLeadAggregates(Date startDate, Date endDate);
	List<TableMessage> getModifiedSaleLeads(Date startDate, Date endDate);

}
