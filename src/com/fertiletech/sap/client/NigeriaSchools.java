package com.fertiletech.sap.client;

import com.google.gwt.core.shared.GWT;

import java.util.Arrays;
import java.util.TreeMap;

public class NigeriaSchools {

	public final static String[] POLYTECHNIC_NAME = { "Dorben Polytechnic", "Adamawa State Polytechnic",
			"Federal Polytechnic, Mubi", "Akwa-Ibom College of Agriculture", "Foundation College of Technology",
			"Maritime Academy of Nigeria", "Ekwenugo Okeke Polytechnic", "Federal Polytechnic, Oko",
			"Abubakar Tafari Ali Polytechnic", "Federal Polytechnic, Bauchi",
			"Bayelsa State College of Arts and Science", "Benue State Polytechnic",
			"Akperan Orshi College of Agriculture", "Borno College of Agriculture", "Ramat Polytechnic",
			"Ibrahim Babangida College of Agriculture", "The Polytechnic, Calabar",
			"Delta State College of Agriculture", "Delta State Polytechnic: (three institutions)",
			"Petroleum Training Institute", "Akanu Ibiam Federal Polytechnic",
			"Federal College of Agriculture, Ishiagu", "Auchi Polytechnic", "Kings Polytechnic", "Shaka Polytechnic",
			"Federal Polytechnic, Ado-Ekiti", "Federal School of Dental Technology & Therapy",
			"Institute of Management Technology, Enugu", "Our Saviour Institute of Science and Technology",
			"Federal College of Land Resources Technology, Owerri", "Federal Polytechnic, Nekede",
			"Templegate Polytechnic Aba", "Imo State Polytechnic", "Imo State Technological Skills Acquisition Center",
			"Hussaini Adamu Federal Polytechnic", "Hussani Adamu Polytechnic",
			"College of Agriculture and Animal Science", "Federal College of Chemical and Leather and Technology",
			"Federal College of Forestry Mechanisation", "Kaduna Polytechnic",
			"Nigerian College of Aviation Technology", "Nuhu Bamalli Polytechnic", "Samaru College of Agriculture",
			"Audu Bako School of Agriculture", "Kano State Polytechnic", "Mohammed Abdullahi Wase Polytechnic",
			"Hassan Usman Katsina Polytechnic", "College of Agriculture, Zuru", "Federal Polytechnic, Birnin-Kebbi",
			"Kebbi State Polytechnic", "College of Agriculture, Kabba", "Federal Polytechnic, Idah",
			"Kogi State Polytechnic", "Federal Polytechnic, Offa", "Kwara State Polytechnic",
			"Federal College of Fisheries and Marine Technology", "Grace Polytechnic", "Lagos City Polytechnic",
			"Lagos State Polytechnic", "School of Agriculture, Ikorodu", "Wavecrest College of Hospitality",
			"Wolex Polytechnic", "Yaba College of Technology", "College of Agriculture, Lafia",
			"Maurid Institute of Management & Technology, Nasarawa", "Federal Polytechnic, Nassarawa",
			"Federal Polytechnic, Nassarawa", "Nasarawa State Polytechnic",
			"Federal College of Fresh Water Fisheries Technology", "Federal College of Wildlife Management",
			"Federal Polytechnic, Bida", "Niger State College of Agriculture", "Niger State Polytechnic",
			"Allover Central Polytechnic", "Federal Polytechnic, Ilaro", "Gateway Polytechnic Saapade",
			"Marvic Polytechnic", "Moshood Abiola Polytechnic", "Rufus Giwa Polytechnic", "Federal Polytechnic, Ede",
			"Osun State College of Technology", "Osun State Polytechnic", "Polytechnic Ile-Ife",
			"Southern Nigeria Institute of Innovative Technology(SNIITPOLY)",
			"Federal College of Animal Health & Production Technology",
			"Federal College of Animal Health and Production Technology (Agriculture)",
			"Federal College of Forestry, Ibadan", "The Polytechnic, Ibadan", "The KINGS Poly, Saki",
			"Tower Polytechnic, Ibadan", "Federal College of Animal Health and Production Technology, Vom",
			"Federal College of Education, Pankshin", "Federal College of Forestry. Jos",
			"Federal College of Land Resources Technology, Kuru", "Plateau State College of Agriculture",
			"Plateau State Polytechnic", "Rivers State College of Arts and Science", "Rivers State Polytechnic",
			"College of Agriculture, Jalingo", "Federal Polytechnic, Damaturu", "Mai Idris Alooma Polytechnic",
			"Abdul Gusau Polytechnic", "Federal Polytechnic, Namoda", "Abdul Gusau Polytechnic", };

	public final static String[] POLYTECHNIC_STATE = { "Abuja FCT", "Adamawa ", "Adamawa ", "Akwa Ibom ", "Akwa Ibom ",
			"Akwa Ibom ", "Anambra ", "Anambra ", "Bauchi ", "Bauchi ", "Bayelsa ", "Benue ", "Benue ", "Borno ",
			"Borno ", "Cross River ", "Cross River ", "Delta ", "Delta ", "Delta ", "Ebonyi ", "Ebonyi ", "Edo ",
			"Edo ", "Edo ", "Ekiti ", "Enugu ", "Enugu ", "Enugu ", "Imo ", "Imo ", "Abia ", "Imo ", "Imo ", "Jigawa ",
			"Jigawa ", "Kaduna ", "Kaduna ", "Kaduna ", "Kaduna ", "Kaduna ", "Kaduna ", "Kaduna ", "Kano ", "Kano ",
			"Kano ", "Katsina ", "Kebbi ", "Kebbi ", "Kebbi ", "Kogi ", "Kogi ", "Kogi ", "Kwara ", "Kwara ", "Lagos ",
			"Lagos ", "Lagos ", "Lagos ", "Lagos ", "Lagos ", "Lagos ", "Lagos ", "Nasarawa ", "Nasarawa ", "Nasarawa ",
			"Nasarawa ", "Nasarawa ", "Niger ", "Niger ", "Niger ", "", "Niger State", "Ogun ", "Ogun ", "Ogun ",
			"Ogun ", "Ogun ", "Ondo ", "Osun ", "Osun ", "Osun ", "Osun ", "Osun ", "Oyo ", "Oyo ", "Oyo ", "Oyo ",
			"Oyo ", "Oyo ", "Plateau ", "Plateau ", "Plateau ", "Plateau ", "Plateau ", "Plateau ", "Rivers ",
			"Rivers ", "Taraba ", "Yobe ", "Yobe ", "Zamfara ", "Zamfara ", "Zamfara ", };

	public final static String[] UNIVERSITY_NAME = { "Abubakar Tafawa Balewa University", "Ahmadu Bello University",
			"Bayero University", "Fed. Univ. of Petroleum Resources", "Federal University of Technology",
			"Federal University of Technology", "Federal University of Technology", "Federal University of Technology",
			"Micheal Okpara University of Agriculture", "National Open University of Nigeria",
			"Nigerian Defence Academy", "Nnamdi Azikiwe University", "Obafemi Awolowo University",
			"University of Abuja", "University of Agriculture", "University of Agriculture", "University of Benin",
			"University of Calabar", "University of Ibadan", "University of Ilorin", "University of Jos",
			"University of Lagos", "University of Maiduguri", "University of Nigeria, Nsukka",
			"University of Port-Harcourt", "University of Uyo", "Usuman Danfodiyo University",
			"Abia State University of Uturu.", "Adamawa State University Mubi", "Adekunle Ajasin University",
			"Akwa Ibom State University of Technology", "Ambrose Alli University",
			"Anambra State University of Science & Technology", "Benue State University",
			"Bukar Abba Ibrahim University", "Cross River State University of Science &Technology",
			"Delta State University", "Ebonyi State University", "Enugu State University of Science and Technology",
			"Gombe State Univeristy", "Ibrahim Badamasi Babangida University", "Imo State University",
			"Kaduna State University", "Kano State University of Technology", "Katsina State University",
			"Kebbi State University", "Kogi State University", "Ladoke Akintola University of Technology",
			"Lagos State University", "Nasarawa State University", "Niger Delta Unversity",
			"Olabisi Onabanjo University", "Osun State University, Oshogbo", "Plateau State University",
			"Rivers State University of Science & Technology", "Tai Solarin Univ. of Education",
			"University of Ado-Ekiti", "University of Education, Ikere Ekiti", "Abti-American University",
			"Achievers University", "African University of Science & Technology", "Ajayi Crowther University",
			"Al-Hikmah University", "Babcock University", "Bells University of Technology", "Benson Idahosa University",
			"Bingham University", "Bowen University", "Caleb University", "Caritas University", "Cetep City University",
			"Covenant University", "Crawford University", "Crescent University", "Fountain Unveristy",
			"Igbinedion University", "Joseph Ayo Babalola University", "Katsina University, Katsina",
			"Lead City University", "Madonna University, Okija", "Novena University, Ogume", "Obong University",
			"Pan African University, Lagos", "Redeemer's University", "Renaissance University,Enugu",
			"Salem University,Lokoja", "Tansian University,Umunya", "University of Mkar, Mkar", "Veritas University",
			"Wesley Univ. of Science & Tech.,Ondo", "Western Delta University, Oghara", "Wukari Jubilee University", };

	public final static String[] UNIVERSITY_STATE = { "Bauchi", "Kaduna", "Kano", "Delta", "Adamawa", "Ondo", "Niger",
			"Imo", "Abia", "Lagos", "Kaduna", "Anambra", "Osun", "Abuja", "Ogun", "Benue", "Benin", "Cross River",
			"Oyo", "Kwara", "Plateau", "Lagos", "Borno", "Enugu", "Rivers", "Akwa Ibom", "Sokoto", "Abia", "Adamawa",
			"Ondo", "Akwa Ibom", "Ebonyi", "Anambra", "Benue", "Yobe  ", "Cross river", "Delta", "Ebonyi", "Enugu",
			"Gombe", "Niger", "Imo", "Kaduna", "Kano", "Katsina", "Kebbi", "Kogi", "Oyo", "Lagos", "Nasarawa",
			"Bayelsa", "Ogun", "Osun", "Plateau", "Rivers", "Ogun", "Ekiti", "Ekiti", "Adamawa", "Ondo", "Abuja", "Oyo",
			"Kwara", "Ogun", "Ogun", "Benin", "Nasarawa", "Osun", "Lagos", "Enugu", "Oyo", "Ogun", "Ogun", "Ogun",
			"Ogbomoso", "Edo", "Osun", "Kastina", "Ibadan", "Enugu", "Delta", "Akwa Ibom", "Lagos", "Ogun", "Enugu",
			"Lokoja", "Umunya", "Benue", "Abuja", "Akure", "Delta", "Taraba", };

	private final static String[] DEPARTMENTS = { "AFRICAN & ASIAN STUDIES", "CREATIVE ARTS", "ENGLISH",
			"EUROPEAN LANGUAGES", "HISTORY & STRATEGIC STUDIES", "PHILOSOPHY", "BANKING & FINANCE",
			"INDUSTRIAL RELATIONS & PERSONNEL MANAGEMENT", "ACCOUNTING", "ACTUARIAL SCIENCE & INSURANCE",
			"BUSINESS ADMINISTRATION", "ADULT EDUCATION", "ARTS & SOCIAL SCIENCE EDUCATION",
			"EDUCATIONAL ADMINISTRATION", "EDUCATIONAL FOUNDATION", "HUMAN KINETIC & HEALTH EDUCATION",
			"SCIENCE & TECH. EDUCATION", "CHEMICAL ENGINEERING", "CIVIL & ENVIRONMENTAL ENGINEERING",
			"ELECTRICAL & ELECTRONICS ENGINEERING", "MECHANICAL ENGINEERING", "METALLURGY & MATERIALS ENGINEERING",
			"SURVEYING & GEOINFORMATICS", "SYSTEMS ENGINEERING", "ARCHITECTURE", "BUILDING", "ESTATE MANAGEMENT",
			"URBAN & REGIONAL PLANNING", "COMM. AND IND. LAW", "JURISP. AND INT. LAW", "PRIVATE AND PROPERTY LAW",
			"PUBLIC LAW", "LAW", "CLINICAL PHARMACY & BIOPHARMACY", "PHARMACOGNOSY", "PHARMACEUTICAL CHEMISTRY",
			"PHARMACEUTICS & PHARMACEUTICAL TECHNOLOGY", "ECONOMICS", "GEOGRAPHY", "MASS COMMUNICATION", "PSYCHOLOGY",
			"SOCIOLOGY", "POLITICAL SCIENCE", "BIOCHEMISTRY", "BOTANY", "CELL BIOLOGY AND GENETICS", "COMPUTER SCIENCE",
			"CHEMISTRY", "GEO SCIENCE", "MARINE SCIENCE", "MATHEMATICS", "MICROBIOLOGY", "PHYSICS", "ZOOLOGY" };

	private final static String[] SHORT_CODES = { "AFR&ASIAN. STUD.", "CRTV. ARTS", "ENG.", "EUR. LANG.",
			"HISTR & STRAT. STUD.", "PHIL", "BANK&FIN", "IND. MGT.", "ACCT.", "ACT. SCI.", "BUSS ADMIN.", "ADULT. EDU.",
			"ARTS & SOC. SCI.", "EDU. ADMIN.", "EDU. FOUND.", "HMN KIN. & HLTH EDU.", "SCI. TEC.", "CHEM. ENGR.",
			"CIVIL. ENGR. ", "ELECT. ELECT.", "MECH. ENGR.", "MET&MAT. ENGR.", "SURVEY&GEOINFO.", "SYS. ENGR.",
			"ARCHTEC", "BUILDN", "EST. MGT.", "URB&REG. PLAN.", "COMM&IND. LAW.", "JURISP. & INT. LAW.", "PRI&PRO. LAW",
			"PUB. LAW.", "LAW", "CLN PHAR. & BIO.", "PHARMGSY", "PHARM.CHEM.", "PHARMA.TECH.", "ECONS.", "GEOGRA.",
			"MASS COMM.", "PSHYCO.", "", "PLTCAL. SCI.", "BIO. CHEM.", "BOTN.", "CELL. BIO & GEN.", "COM. SCI.",
			"CHEM.", "GEO. SCI.", "MARINE. SCI.", "MATHS", "MICRO. BIO.", "PHY", "ZOO" };
	public final static String[] YABATECH_DEPARTMENT = { "Fine Art", "Graphics Design", "Industrial Design",
			"Technology", "Civil Engineering", "Electrical & Electronic Engineering",
			"Industrial Maintenance Engineering", "Mechanical Engineering", "Architecture ", "Building Technology",
			"Estate Management", "Quantity Surveying", "Urban and Regional Planning", "Land Surveying",
			"Accountancy and Finance", "Business Administration", "Secretarial Studies", "General Studies",
			"Chemical Science", "Biological Science", "Physical Science", "Mathematics and Statistics",
			"Computer Technology", "Food Technology", "Hotel and Catering Management ",
			"Polymer and Textile Technology", "Languages", "Social Sciences", "Mass Communication",
			"Industrial Technical Education ", "Industrial Technical Education ", "Industrial Technical Education ",
			"Home Economics Education", "Business Education", };
	public final static String[] YABATECH_SHORT_CODES = { "F. ART", "GPH. DESIGN", "INDUS. DESIGN", "PRINT. TECH.",
			"CIVIL. ENGR.", "ELECT. ENGR.", "INDUS. ENGR.", "MECH. ENGR.", "ARCHI. ARCH.", "BUILD. TECH.",
			"ESTAT. MANA.", "QUANT. SURV.", "URBAN. PLAN.", "LAND . SURV.", "ACCOU. FINA.", "BUSS. ADMIN.",
			"SECRE. STUD.", "GENER. STUD.", "CHEM. SCI.", "BIO. SCI.", "PHY. SCI.", "MATH. STAT.", "COMPU. TECH.",
			"FOOD . TECH.", "HOTEL. MANA.", "POLYM. TECH.", "LANGU. LANG.", "SOCIA. SCI.", "MASS. COMM.",
			"ITE ELECT/ELECT", "ITE METAL WRK.", "ITE BUILD.", "HOME. EDUC.", "BUSS. EDUC.", };

	public static String[] getTertiaryInstitutions() {
		String[] result = new String[UNIVERSITY_NAME.length + POLYTECHNIC_NAME.length];
		int index = 0;
		for (index = 0; index < UNIVERSITY_NAME.length; index++)
			result[index] = UNIVERSITY_NAME[index].trim() + " - " + UNIVERSITY_STATE[index].trim() + " State";
		for (int i = 0; i < POLYTECHNIC_NAME.length; i++)
			result[index++] = POLYTECHNIC_NAME[i].trim() + " - " + POLYTECHNIC_STATE[i].trim() + " State";
		Arrays.sort(result);
		return result;
	}
	
	public static String[] getUnis() {
		String[] result = new String[UNIVERSITY_NAME.length];
		int index = 0;
		for (index = 0; index < UNIVERSITY_NAME.length; index++)
			result[index] = UNIVERSITY_NAME[index].trim();
		Arrays.sort(result);
		return result;
	}
	
	public static String[] getPolys() {
		String[] result = new String[POLYTECHNIC_NAME.length];
		int index = 0;
		for (int i = 0; i < POLYTECHNIC_NAME.length; i++)
			result[index++] = POLYTECHNIC_NAME[i].trim();
		Arrays.sort(result);
		return result;
	}

	public static TreeMap<String, String> getDepartments() {
		GWT.log("Creating tree map");
		TreeMap<String, String> result = new TreeMap<>();
		GWT.log("Tree map ok");
		for (int i = 0; i < YABATECH_DEPARTMENT.length; i++)
			result.put(YABATECH_DEPARTMENT[i].toUpperCase(), YABATECH_SHORT_CODES[i]);
		GWT.log("yabatech parsed ok");
		for (int i = 0; i < DEPARTMENTS.length; i++)
			result.put(DEPARTMENTS[i].toUpperCase(), SHORT_CODES[i]);
		GWT.log("Retrieved department info and about to return");
		return result;
	}

}
