package com.fertiletech.sap.client;

import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.Date;
import java.util.List;

@RemoteServiceRelativePath("../moocs")
public interface Moocs extends RemoteService {
    /**
     * Utility/Convenience class.
     * Use Moocs.App.getInstance() to access static instance of MoocsAsync
     */
    public static class App {
        private static final MoocsAsync ourInstance = (MoocsAsync) GWT.create(Moocs.class);

        public static MoocsAsync getInstance() {
            return ourInstance;
        }
    }

    List<TableMessage> getAllCourses(); //ops and members
    List<TableMessage> getTracks(Date start, Date end); //ops
    List<TableMessage> getAllMemberTracks() throws LoginExpiredException;//member
    List<TableMessage>[] getMemberTrackFeed(String courseName) throws MissingEntitiesException, LoginExpiredException; //member
    String startTrack(String courseName) throws DuplicateEntitiesException, LoginExpiredException;//member
    String addTrack(String coursename, String comments) throws MissingEntitiesException, LoginExpiredException; //member
    String addFeedBack(String userName, String courseName, String comments) throws MissingEntitiesException; //ops
    List<TableMessage> getAllMemberTracks(String member); //ops
    List<TableMessage>[] getMemberTrackFeed(String member, String courseName) throws MissingEntitiesException; //ops
    
    String getTrack(int trackId, String courseName) throws MissingEntitiesException, LoginExpiredException; //member
    String getFeed(int feedId, String courseName) throws MissingEntitiesException, LoginExpiredException; //member
    String getTrack(String member, int trackId, String courseName) throws MissingEntitiesException; //ops
    String getFeed(String member, int feedId, String courseName) throws MissingEntitiesException; //ops
    List<TableMessage>[]getCourseTrackFeed(String courseId) throws MissingEntitiesException; //ops

}
