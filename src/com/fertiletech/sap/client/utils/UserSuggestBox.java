package com.fertiletech.sap.client.utils;

import com.fertiletech.sap.client.LoginService;
import com.fertiletech.sap.client.LoginServiceAsync;
import com.fertiletech.sap.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class UserSuggestBox extends Composite 
	implements HasValueChangeHandlers<String>, SelectionHandler<Suggestion>
{
	
	//abstract methods
	protected abstract void populateSuggestBox();
	protected abstract String getRole();


	@UiField
	SuggestBox userBox;
	
	@UiField
	Label label;
	
	private final RegExp ID_MATCHER = RegExp.compile("\\{\\s\\S+\\s\\}");
	
	private static HashMap<String, HashMap<String, String>> cachedSuggestions =
		new HashMap<String, HashMap<String, String>>();
	
	private HashMap<String, String> hackedSuggestions;
	private HashMap<String, String> emailToIDMap;
	private SimpleDialog alertBox;
	private String oldVal = "";		
	private static boolean loadingCompleted = false;
	
    private LoginServiceAsync userService =
        GWT.create(LoginService.class);
    
	private final AsyncCallback<HashMap<String, String>> listCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			loadingCompleted = true;
			alertBox.show("Request failed: " + caught.getMessage());
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			GWT.log("Retrieved: " + result.size() + " from server");
			setupSuggestBox(result);
			cachedSuggestions.put(getCacheKey(), result);
			loadingCompleted = true;
		}
	};

	private static UserSuggestBoxUiBinder uiBinder = GWT
			.create(UserSuggestBoxUiBinder.class);

	interface UserSuggestBoxUiBinder extends UiBinder<Widget, UserSuggestBox> {
	}

	public UserSuggestBox() {
		initWidget(uiBinder.createAndBindUi(this));
		hackedSuggestions = new HashMap<String, String>();
		emailToIDMap = new HashMap<String, String>();
		userBox.addSelectionHandler(this);

		// setup alert box
		alertBox = new SimpleDialog("An error occured");
		initSuggestBox();
	}
	
	public AsyncCallback<HashMap<String, String>> getPopulateCallBack()
	{
		return listCallback;
	}
	
	private String getCacheKey()
	{
		return getRole();
	}
	
	private void initSuggestBox()
	{
		String cacheKey = getCacheKey();
		if(cachedSuggestions.containsKey(cacheKey))
		{
			GWT.log("USER BOX HIT WHILE INITIALIZING");
			setupSuggestBox(cachedSuggestions.get(cacheKey));
		}
		else
		{
			GWT.log("NO CONSTRUCTOR HIT - CALLING POPULATE");
			populateSuggestBox();
		}
	}
	
	public String getUserID(String displayName)
	{
		MatchResult result = ID_MATCHER.exec(displayName);
		if(result.getGroupCount() == 1)
		{
			String matched = result.getGroup(0);
			return matched.substring(2, matched.length()-2);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.logical.shared.SelectionHandler#onSelection(com.
	 * google.gwt.event.logical.shared.SelectionEvent)
	 */
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		String oldValue = oldVal;
		GWT.log("OLD VAL: " + oldValue);
		
		// TODO Auto-generated method stub
		Suggestion suggest = event.getSelectedItem();
		GWT.log("Suggest Display: " + suggest.getDisplayString());
		GWT.log("Suggest Replace: " + suggest.getReplacementString());
		String newValue = suggest.getReplacementString();
		newValue = getUserID(newValue);
		userBox.setValue(newValue);		
		if(newValue.length() > 0)
		{
			ValueChangeEvent.fireIfNotEqual(this, oldValue, getSelectedUser());
			oldVal = newValue;
		}
		GWT.log("NEW VAL: " + newValue);
	}


	private void setupSuggestBox(HashMap<String, String> vals) {
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) userBox
				.getSuggestOracle();
		oracle.clear();
		hackedSuggestions.clear();
		emailToIDMap.clear();
		//int debugCount = 0;
		Iterator<Map.Entry<String, String>> valItr = vals.entrySet().iterator();
		while (valItr.hasNext()) {
			Map.Entry<String, String> item = valItr.next();
			GWT.log("raw val: [" + item.getValue() + "] raw key: [" + item.getKey() +"]");
			String[] comps = item.getValue().split(DTOConstants.USER_SUGGEST_DELIMITER);
			String displayStr = comps[0] + " { " + comps[1] + " }";
			oracle.add(displayStr);
			GWT.log("Added to oracle: " + displayStr);
			GWT.log("hack key: [" + comps[1] + "] hack val: [" + item.getKey() +"]");
			hackedSuggestions.put(comps[1], displayStr);
			emailToIDMap.put(comps[1], item.getKey());
			/* remove this
			if(debugCount == 2)
				break;
			else
				debugCount++;*/
		}
		oracle.add("FOR DEBUG purposes");
		GWT.log("Oracle: " + oracle.toString());
	}

	public String getSelectedUser() {
		String key = userBox.getValue();
		GWT.log("Retrieved key: " + key);
		if (hackedSuggestions.containsKey(key))
			return emailToIDMap.get(key);
		else
			return null;
	}
	
	public String getSelectedUserDisplay()
	{
		String key = userBox.getValue();
		GWT.log("Key is: [" + key + "]");
		if (hackedSuggestions.containsKey(key))
			return hackedSuggestions.get(key);
		else
			return null;
	}

	public LoginServiceAsync getUserService()
	{
		return userService;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	public void setEnabled(boolean enabled)
	{
		userBox.getTextBox().setEnabled(enabled);
		if(!enabled)
			userBox.setValue(null);
	}

	
	public void clear()
	{
		oldVal = "";
		userBox.setValue("");
	}	
}
