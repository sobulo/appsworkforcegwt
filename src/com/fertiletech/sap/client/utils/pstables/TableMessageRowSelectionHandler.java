/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.sap.client.utils.pstables;


import com.fertiletech.sap.shared.TableMessage;

/**
 *
 * @author Administrator
 */
public interface TableMessageRowSelectionHandler {
    public void onRowSelected(TableMessage m);
}
