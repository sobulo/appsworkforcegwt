/**
 * 
 */
package com.fertiletech.sap.client.utils;


/**
 * @author Segun Razaq Sobulo
 * 
 */
public class StudentSuggestBox extends UserSuggestBox {
	
	public StudentSuggestBox()
	{
		super();
		label.setText("Student");
	}

	@Override
	public void populateSuggestBox()
	{
		getUserService().getAllStudentIDs(getPopulateCallBack());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.UserSuggestBox#getRole()
	 */
	@Override
	public String getRole()
	{
		return "Student to Application ID";
	}

}
