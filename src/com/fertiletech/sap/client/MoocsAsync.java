package com.fertiletech.sap.client;

import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.Date;
import java.util.List;

public interface MoocsAsync {

    void getAllCourses(AsyncCallback<List<TableMessage>> async);

    void getTracks(Date start, Date end, AsyncCallback<List<TableMessage>> async); //ops

    void getAllMemberTracks(AsyncCallback<List<TableMessage>> async); //member

    void getMemberTrackFeed(String courseName, AsyncCallback<List<TableMessage>[]> async);//member

    void startTrack(String courseName, AsyncCallback<String> async); //member

    void addTrack(String coursename, String comments, AsyncCallback<String> async); //member

    void addFeedBack(String userName, String courseName, String comments, AsyncCallback<String> async); //ops

    void getAllMemberTracks(String member, AsyncCallback<List<TableMessage>> async); //ops

    void getMemberTrackFeed(String member, String courseName, AsyncCallback<List<TableMessage>[]> async); //ops

    void getFeed(int feedId, String courseName, AsyncCallback<String> async); //member

    void getTrack(int trackId,String courseName,  AsyncCallback<String> async); //member

    void getTrack(String member, int trackId, String courseName, AsyncCallback<String> async);

    void getFeed(String member, int feedId, String courseName, AsyncCallback<String> async);

    void getCourseTrackFeed(String courseId, AsyncCallback<List<TableMessage>[]> async);
}
