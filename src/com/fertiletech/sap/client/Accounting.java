package com.fertiletech.sap.client;

import com.fertiletech.sap.shared.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

@RemoteServiceRelativePath("../accounting")
public interface Accounting extends RemoteService {
    /**
     * Utility/Convenience class.
     * Use Accounting.App.getInstance() to access static instance of AccountingAsync
     */
    public static class App {
        private static final AccountingAsync ourInstance = (AccountingAsync) GWT.create(Accounting.class);

        public static AccountingAsync getInstance() {
            return ourInstance;
        }
    }

    String createBillTemplate(String name, String year, String term, Date dueDate,
                                     LinkedHashSet<BillDescriptionItem> itemizedBill, String bank, String accountName, String accountNumber, String instructions) throws DuplicateEntitiesException;

    HashMap<String, String> getBillTemplateList();

    String createUserBill(String billTemplate, String[] users) throws DuplicateEntitiesException;

    List<TableMessage> getStudentBills(String studentID);

    HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin);

    HashMap<String, String> getStudentBillKeys();

    HashMap<String, String> getStudentBillKeysFromTemplate(String templateKeyStr) throws MissingEntitiesException;

    String savePayment(String billKey, double amount, Date payDate, String referenceID,
                              String comments) throws MissingEntitiesException, ManualVerificationException;

    List<TableMessage> getBillTemplateInfo(String billTemplateKeyStr) throws MissingEntitiesException;

    List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException;

    List<TableMessage> getBillsForTemplate(String billDescKeyStr) throws MissingEntitiesException;

    List<TableMessage> getBillPayments(String billKeyStr) throws MissingEntitiesException;

    String getInvoiceDownloadLink(String billTemplateKeyStr, String[] billKeyStrs) throws MissingEntitiesException;
    String getBillDownloadLink(String billKeyStr) throws MissingEntitiesException;
    TableMessage verifyPayment(String leadKeyStr, String payKeyStr) throws MissingEntitiesException, ManualVerificationException;
    List<TableMessage> getPayments(Date start, Date end);
    TableMessage issueLicense(String leadStr, String billStr) throws MissingEntitiesException, ManualVerificationException;
    Date[] getLicenseInfo(String leadKeyStr);
    public List<TableMessage> getBills(Date startDate, Date endDate);

}
