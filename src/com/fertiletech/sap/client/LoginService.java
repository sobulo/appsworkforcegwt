package com.fertiletech.sap.client;

import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../login")
public interface LoginService extends RemoteService {
	//LoginInfo login(String requestUrl);
	//LoginInfo loginForApplications(String requestUrl);
	List<TableMessage> getLoanApplications(String email);
	List<TableMessage> getLoanApplications();
	String loadActivityComments(String parentKey);
	List<TableMessage> getRecentActivityComments();
	void saveActivityComment(String commentID, String html, boolean b);
	List<TableMessage> getAttachments(String loanID);
	String saveAttachments(String loanId, List<TableMessage> attachments);
	String saveMapAttachment(String loanId, Double lat, Double lng);
	String deleteAttachment(String attachID);	
	List<TableMessage> getMapAttachments(Date startDate, Date endDate);
	List<TableMessage> getUploads(String loanID) throws MissingEntitiesException;
	String getUploadUrl();
	String deleteUpload(String uploadID) throws MissingEntitiesException;
	List<TableMessage> getApplicationParameter(String ID);
	String saveApplicationParameter(String id, HashMap<String, String> val) throws MissingEntitiesException;
	String getImageUploadHistory(String imageID);
	HashMap<String, String> getAllStudentIDs();
	String sendMessage(String to, String subject, String htmlMessage);
	String getActivityComments() throws LoginExpiredException;
	TableMessage getMessageContent(String id) throws MissingEntitiesException;
	void setMessageContent(String id, String contentText, String contentHTML);
	String[] getAllMessageNames();
	String echo(String message);
	int scheduleBulkMail(Set<TableMessage> mailingList, String templateName);
}
