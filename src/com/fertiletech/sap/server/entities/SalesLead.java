/**
 * 
 */
package com.fertiletech.sap.server.entities;

import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.WorkflowStateInstance;
import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class SalesLead{
	
	@Id
	long key;
	
	//customer fields -- potential index candidates, for now useful to have these without having to load forms
	String phoneNumber;
	String fullName;
	String state;
	String lga;
	String displayMessage;
	Date licenseBegins;
	GeoPt  studyLocation;
	String licenseType;

	WorkflowStateInstance currentState;
	
	//indexed fields
	@Indexed String bankLoanId;
	@Indexed Date dateCreated;
	@Indexed Long keyCopy;
	@Indexed Date licenseEnds;

	
	//auto-populated fields
	@Indexed Date dateUpdated; 
	String updateUser;	
	
	@Parent Key<ApplicantUniqueIdentifier> parentKey;

	public SalesLead() {} //empty constructor required by objectify
	
	public SalesLead(String email, long id)
	{
		this.parentKey = new Key<ApplicantUniqueIdentifier>(ApplicantUniqueIdentifier.class, email);
		this.currentState = WorkflowStateInstance.APPLICATION_STARTED;
		this.dateCreated = new Date(); 	
		this.key = id;
	}
	
	void setDecisionFields(
			String phoneNumber,
			String fullName,
			String state,
			String lga
	)
	{
		this.phoneNumber = phoneNumber;
		this.fullName = fullName;
		this.state = state;
		this.lga = lga;
	}
	
	SalesLead makeCopy(long id)
	{
		if(!allowNewSiblings()) throw new RuntimeException("Copying this lead is not allowed. Contact support for assistance");
		SalesLead copy = new SalesLead(parentKey.getName(), id);
		copy.setDecisionFields(phoneNumber, fullName, state, lga);
		return copy;
	}

	public String getEmail(){
		return parentKey.getName();
	}


	public String getBankLoanId() {
		return bankLoanId;
	}

	void setBankLoanId(String bankLoanId) {
		this.bankLoanId = bankLoanId;
	}

	public Boolean getLoanApproved() {
		return currentState.isApprovedState();
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
			
	public String getFullName() {
		return fullName;
	}	
			


	public Date getDateCreated() {
		return dateCreated;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	public long getLeadID()
	{
		return key;
	}
	
	public String getState()
	{
		return state;
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	String getUpdateUser() {
		return updateUser;
	}
	
	public Key<SalesLead> getKey()
	{
		return new Key<SalesLead>(parentKey, SalesLead.class, key);
	}

	public WorkflowStateInstance getCurrentState() {
		return currentState;
	}


	public void setCurrentState(WorkflowStateInstance state) {
		this.currentState = state;
	}
	
	public String getDisplayMessaage()
	{
		return displayMessage;
	}
	
	void setDisplayMessage(String msg)
	{
		displayMessage = msg;
	}



	public String getLicenseType(){return licenseType;}

	static DateFormat DF = DateFormat.getDateInstance();

	public Date getLicenseBegins() {
		return licenseBegins;
	}

	public void setLicenseBegins(Date licenseBegins) {
		this.licenseBegins = licenseBegins;
	}

	public Date getLicenseEnds() {
		return licenseEnds;
	}

	public void setLicenseEnds(Date licenseEnds, String type) {
			if (DTOConstants.BILL_DESC_MAP.containsKey(type)) {
				this.licenseType = type; ///set the type whenever we extend license
				this.licenseEnds = licenseEnds;
			}
			else
				throw new IllegalArgumentException("unsupported license type: " + type);

	}

	public boolean isLicensed()
	{
		if(licenseEnds == null)
			return false;
		if(licenseEnds.before(new Date()))
			return false;
		return true;
	}

	public String getLicenseInfo() {
		if(!isLicensed())
			return null;

		return DF.format(licenseBegins) + " to " + DF.format(licenseEnds);

	}

	public GeoPt getStudyLocation() {
		return studyLocation;
	}

	public void setStudyLocation(GeoPt studyLocation) {
		this.studyLocation = studyLocation;
	}
	
	public boolean allowNewSiblings()
	{
		Calendar oneYearAgo = GregorianCalendar.getInstance();
		oneYearAgo.roll(Calendar.YEAR, false);
		switch (currentState) 
		{
			//case APPLICATION_APPROVED:
			case APPLICATION_CANCEL:
				return true;
			case APPLICATION_DENIED:
				return oneYearAgo.after(dateCreated);
			default:
				return false;
		}
	}
	
	@PrePersist
	void copyMyKeyForIndexing()
	{
		if(keyCopy == null)
			keyCopy = key;
	}
}