/**
 * 
 */
package com.fertiletech.sap.server.entities;

import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class EntityConstants {
	public static final String GCS_HOST = "https://storage.googleapis.com/";
    public final static String EMAIL_CONTROLLER_ID = "EMAIL Controller";    
    public final static String SMS_CONTROLLER_ID = "SMS Controller";
    
	//number and date formats
	public final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat("d MMM yyyy");
	static
	{
		NUMBER_FORMAT.setMaximumFractionDigits(2);
		NUMBER_FORMAT.setMinimumIntegerDigits(1);
	}
	
	public final static NumberFormat INT_FORMAT = NumberFormat.getIntegerInstance();
	
	public static HashMap<String, Integer> getLeadAggregates(Date start, Date end)
	{
		Iterable<SalesLead> salesLeads = EntityDAO.getSalesLeadsByDate(start, end);
		HashMap<String, Integer> aggregates = new HashMap<String, Integer>();

		final TableMessage STATE_NAMES = DTOConstants.getStatusChartHeaders();
		Integer stateCount = null;
		Objectify ofy = ObjectifyService.begin();
		for(SalesLead lead : salesLeads)
		{
			Key<MortgageApplicationFormData> fk = MortgageApplicationFormData.getFormKey(lead.getKey());
			MortgageApplicationFormData form = ofy.get(fk); //TODO serious db hit if voulmes pickup, use lead state instead?
			boolean isSubmit = form.isSubmmited();
			String licenseType = lead.getLicenseType();
			String licenseName = DTOConstants.BILL_DESC_MAP.get(licenseType);
			boolean isLicensed = lead.isLicensed();
			String  state = "";
			//build state
			if(isSubmit)
			{
				if(isLicensed)
				{
					if(licenseName.equals(DTOConstants.LICENSE_MONTHLY))
						state = STATE_NAMES.getText(DTOConstants.STS_MONTHLY_IDX);
					else if (licenseName.equals(DTOConstants.LICENSE_ANNUAL))
						state = STATE_NAMES.getText(DTOConstants.STS_ANNUAL_IDX);
					else if (licenseName.equals(DTOConstants.LICENSE_WORKFORCE))
						state = STATE_NAMES.getText(DTOConstants.STS_WORKFORCE_IDX);
				}
				else
					state = STATE_NAMES.getText(DTOConstants.STS_SUBMIT_IDX);

			}
			else
			{
				if(isLicensed)
				{
					if(licenseName.equals(DTOConstants.LICENSE_MONTHLY))
						state = STATE_NAMES.getText(DTOConstants.STS_NOT_SUBMIT_MONTHLY_IDX);
					else if (licenseName.equals(DTOConstants.LICENSE_ANNUAL))
						state = STATE_NAMES.getText(DTOConstants.STS_NOT_SUBMIT_ANNUAL_IDX);
					else if (licenseName.equals(DTOConstants.LICENSE_WORKFORCE))
						state = STATE_NAMES.getText(DTOConstants.STS_NOT_SUBMIT_WORKFORCE_IDX);
				}
				else
					state = STATE_NAMES.getText(DTOConstants.STS_NOT_SUBMIT_IDX);

			}

			stateCount = aggregates.get(state);
			if(stateCount == null)
				stateCount = 0;
			aggregates.put(state, stateCount + 1);
		}
		return aggregates;
	}
}
