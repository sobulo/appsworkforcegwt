package com.fertiletech.sap.server.entities.moocs;

import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.shared.VerificationStatus;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.util.ArrayList;
import java.util.Date;

@Unindexed
@Cached
public class CourseTracker {
    @Parent
    Key<ApplicantUniqueIdentifier> member;

    @Id
    String courseName;

    ArrayList<Text> statusUpdates;
    ArrayList<Date> statusDates;
    ArrayList<Text> feedBack;
    ArrayList<Date> feedBackDates;
    ArrayList<String> trackTrail;
    ArrayList<String> feedTrail;

    @Indexed
    boolean isVerified;
    @Indexed
    Boolean selfGrade; //no rating, i no sabi, i am capable
    String verificationComments;
    VerificationStatus verification;
    @Indexed
    Date dateUpdated;

    @Indexed
    short progress; //?? 0 to 100 for progress bar

    public final static String STATUS_ZERO = "<b style='color: green'>Week 0</b>I understand that being available to learn online significantly " +
            " increases my chances of understanding the course materials";
    public final static String FEED_ZERO = "<b style='color: orange'>Week 0 feedback</b>Your first step should be registering for this course on " +
            " the course website. Make sure you provide an update after doing so and subsequent weekly updates there after";


    public CourseTracker() {
    }

    public CourseTracker(Key<ApplicantUniqueIdentifier> member, Key<Course> courseKey) {
        this.member = member;
        this.courseName = courseKey.getName();
        this.progress = 0;
        this.statusUpdates = new ArrayList<>();
        this.statusDates = new ArrayList<>();
        this.feedBack = new ArrayList<>();
        this.feedBackDates = new ArrayList<>();
        this.trackTrail = new ArrayList<>();
        feedTrail = new ArrayList<>();
        this.selfGrade = null;
        this.verificationComments = "";
        this.verification = VerificationStatus.NOT_VERIFIED;

        addStatusUpdate(STATUS_ZERO, "apply@appsworkforce.com");
        addFeedback(FEED_ZERO, "hello@appsworkforce.com");
    }



    @PrePersist
    void timeStamp() {
        dateUpdated = new Date();
    }

    private int addComments(String comment, ArrayList<Text> commentList, ArrayList<Date> commentDates, ArrayList<String> auditTrail, String  user) {
        commentList.add(new Text(comment));
        commentDates.add(new Date());
        auditTrail.add(user);
        if (commentList.size() != commentDates.size())
            throw new IllegalStateException("Mismatch between comment size" + commentList.size() +
                    " and corresponding dates: " + commentDates.size());
        if (commentList.size() != auditTrail.size())
            throw new IllegalStateException("Mismatch between comment size" + commentList.size() +
                    " and corresponding trail: " + auditTrail.size());
        return commentList.size();
    }

    public int addStatusUpdate(String track, String user) {
        return addComments(track, statusUpdates, statusDates, trackTrail, user);
    }

    public int addFeedback(String review, String user) {
        return addComments(review, feedBack, feedBackDates, feedTrail, user);
    }

    public String getTrack(int trackIdx) {
        if (trackIdx >= statusUpdates.size())
            throw new IllegalArgumentException("Attempting to go out of bounds, there are " + statusUpdates.size()
                    + " updates and yet looking for element at " + trackIdx);
        return statusUpdates.get(trackIdx).getValue();
    }

    public Date getTrackDate(int trackIdx) {
        if (trackIdx >= statusDates.size())
            throw new IllegalArgumentException("Attempting to go out of bounds, there are " + statusDates.size()
                    + " updates and yet looking for element at " + trackIdx);
        return statusDates.get(trackIdx);
    }

    public String getTrackTrail(int trackIdx) {
        if (trackIdx >= trackTrail.size())
            throw new IllegalArgumentException("Attempting to go out of bounds, there are " + trackTrail.size()
                    + " updates and yet looking for element at " + trackIdx);
        return trackTrail.get(trackIdx);
    }

    public Date getFeedDate(int trackIdx) {
        if (trackIdx >= feedBackDates.size())
            throw new IllegalArgumentException("Attempting to go out of bounds, there are " + feedBackDates.size()
                    + " updates and yet looking for element at " + trackIdx);
        return feedBackDates.get(trackIdx);
    }

    public String getFeedTrail(int trackIdx) {
        if (trackIdx >= feedTrail.size())
            throw new IllegalArgumentException("Attempting to go out of bounds, there are " + feedTrail.size()
                    + " updates and yet looking for element at " + trackIdx);
        return feedTrail.get(trackIdx);
    }

    public String getFeedBack(int feedIdx) {
        if (feedIdx >= feedBack.size())
            throw new IllegalArgumentException("Attempting to go out of bounds, there are " + feedBack.size()
                    + " feedbacks and yet looking for element at " + feedIdx);
        return feedBack.get(feedIdx).getValue();
    }

    String getGradeDescription() {
        if (selfGrade == null)
            return "NR";
        else if (selfGrade)
            return "UTM";
        else
            return "NTL";
    }

    final static int BASE_POINTS = 10;

    int getPublicProgress() {
        if (progress > 0)
            return progress;

        int feed = feedBack.size();
        int stat = statusDates.size();
        feed = Math.min(feed,stat); //feedback should never outweigh status ufate
        int progress = (feed * stat) + BASE_POINTS;
        int maxAllowed = 100 - BASE_POINTS;
        return Math.min(progress, maxAllowed);
    }

    public int getTrackCount(){return statusUpdates.size();}
    public static Key<CourseTracker> getKey(String member, String courseName)
    {
        return new Key<>(ApplicantUniqueIdentifier.getKey(member), CourseTracker.class, courseName);
    }

    public  Key<CourseTracker> getKey()
    {
        return getKey(member.getName(), courseName);
    }

	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	public int getUpdateCount()
	{
		return statusUpdates.size();
	}
	
	public int getFeedCount()
	{
		return feedBack.size();
	}
    
}
