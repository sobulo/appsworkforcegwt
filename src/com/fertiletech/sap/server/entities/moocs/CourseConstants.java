package com.fertiletech.sap.server.entities.moocs;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.NumberColumnAccessor;

import java.text.NumberFormat;

public class CourseConstants {
    public final static NumberFormat NUMBER_FORMATTER = NumberFormat.getInstance();

    public final static String PLATFORM = "platform".toUpperCase();
    public final static String CATEGORY = "category".toUpperCase();
    public final static String NAME = "name".toUpperCase();
    public final static String ORDERING = "ordering".toUpperCase();
    public final static String STATUS = "status".toUpperCase();
    public final static String LINK = "link".toUpperCase();
    public final static String ENROLLMENT = "enrollment".toUpperCase();
    public final static String INSTITUTION = "institution".toUpperCase();
    public final static String ACCRONYM = "accronym".toUpperCase();
    public final static String PROF = "instructors".toUpperCase();
    public final static String FORUM = "discussion".toUpperCase();

    private static String[] SUPPORTED_CATEGORIES = {"Programming Analyst", "Web Design Analyst",
            "English Elective", "Programming Intern", "Programming Elective"};
    private static  String[] SUPPORTED_INSTITUTIONS = {"Massachusets Instititute of Technology",
    "University of California at Berkley", "Codecademy", "Harvard University", "Galileo Universidad"};
    private static String[] SUPPORTED_ACCRONYMS= {"MIT", "UC Berkley", "Codecademy", "Harvard", "GalileoX"};

    ///fields for excel file uplaod
 	public final static ColumnAccessor[] COURSE_UPLOAD_ACCESSORS =
         new ColumnAccessor[11];


 	static
    {
    	NUMBER_FORMATTER.setMaximumFractionDigits(0);
        COURSE_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(PLATFORM, true);
        COURSE_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(NAME, true);
        COURSE_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(CATEGORY, true);
        COURSE_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(STATUS, false);
        COURSE_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(ENROLLMENT, false);
        COURSE_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(LINK, true);
        COURSE_UPLOAD_ACCESSORS[6] = new NumberColumnAccessor(ORDERING, true);
        COURSE_UPLOAD_ACCESSORS[7] = new ExcelManager.DefaultAccessor(INSTITUTION, true);
        COURSE_UPLOAD_ACCESSORS[8] = new ExcelManager.DefaultAccessor(ACCRONYM, true);
        COURSE_UPLOAD_ACCESSORS[9] = new ExcelManager.DefaultAccessor(PROF, false);
        COURSE_UPLOAD_ACCESSORS[10] = new ExcelManager.DefaultAccessor(FORUM, false);

    }

    private static boolean isSupportedField(String[] values, String key)
    {
        for(String val : values)
            if(val.equals(key))
                return true;
        return false;
    }

    public static String[] getSupportedCategories()
    {
        return SUPPORTED_CATEGORIES;
    }

    public static String[] getSupportedInstitutions()
    {
        return SUPPORTED_INSTITUTIONS;
    }

    public static String[] getSupportedAccronyms()
    {
        return SUPPORTED_ACCRONYMS;
    }

    public static boolean isSupportedCategory(String cat)
    {
        return isSupportedField(SUPPORTED_CATEGORIES, cat);
    }

    public static boolean isSupportedInstitutions(String institution)
    {
        return isSupportedField(SUPPORTED_INSTITUTIONS, institution);
    }

    public static boolean isSupportedAccronym(String accronym)
    {
        return isSupportedField(SUPPORTED_ACCRONYMS, accronym);
    }





}
