package com.fertiletech.sap.server.entities.moocs;

import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class CourseDAO {

    private static final Logger log =
            Logger.getLogger(CourseDAO.class.getName());

    private static HashMap<String, List<Course>> getAllCourses() {
        Objectify ofy = ObjectifyService.begin();
        List<Course> courseList = ofy.query(Course.class).list();
        HashMap<String, List<Course>> categoryMap = new HashMap<>();
        for (Course course : courseList) {
            String category = course.category;
            if (!categoryMap.containsKey(category))
                categoryMap.put(category, new ArrayList<Course>());
            List<Course> mappedCourse = categoryMap.get(category);
            mappedCourse.add(course);
            categoryMap.put(category, mappedCourse); //sanity put, list modification above should suffice
        }
        return categoryMap;
    }


    private static List<Course> getCourseByCategory(String category) {
        Objectify ofy = ObjectifyService.begin();
        return ofy.query(Course.class).
                filter("category =", category).list();

    }

    public static List<CourseTracker> getRecentTrackedCourses(Date start, Date end) {
        Objectify ofy = ObjectifyService.begin();
        return ofy.query(CourseTracker.class).filter("dateUpdated >=", start).
                filter("dateUpdated <=", end).list();
    }
    
    public static List<Key<CourseTracker>> getRecentTrackedCourseKeys(Date start, Date end) {
        Objectify ofy = ObjectifyService.begin();
        return ofy.query(CourseTracker.class).filter("dateUpdated >=", start).
                filter("dateUpdated <=", end).listKeys();
    }

    private static List<CourseTracker> getRecentTrackedCoursesByCourseName(String course, int limit) {
        Objectify ofy = ObjectifyService.begin();
        return ofy.query(CourseTracker.class).
                filter("courseName =", course).order("-dateUpdated").limit(limit).list();

    }

    public static List<CourseTracker> getTrackedCourseByStudent(Key<ApplicantUniqueIdentifier> member) {
        Objectify ofy = ObjectifyService.begin();
        return ofy.query(CourseTracker.class).
                ancestor(member).order("-dateUpdated").list();
    }
    
    public static List<Key<CourseTracker>> getTrackedCourseKeysByStudent(Key<ApplicantUniqueIdentifier> member) {
        Objectify ofy = ObjectifyService.begin();
        return ofy.query(CourseTracker.class).
                ancestor(member).order("-dateUpdated").listKeys();
    }

    public static TableMessageHeader getCourseHeader() {
        TableMessageHeader h = new TableMessageHeader(6);
        h.setText(0, "Platform", TableMessageContent.TEXT);
        h.setText(1, "Category", TableMessageContent.TEXT);
        h.setText(2, "Institution", TableMessageContent.TEXT);
        h.setText(3, "Course", TableMessageContent.TEXT);
        h.setText(4, "Ordering", TableMessageContent.NUMBER);
        h.setText(5, "Status", TableMessageContent.TEXT);
        return h;
    }

    public static TableMessage getCourseDTO(Course c) {
        TableMessage result = new TableMessage(10, 1, 0);
        result.setText(DTOConstants.COURSE_PLATFORM_TXT_IDX, c.platform);
        result.setText(DTOConstants.COURSE_CATEGORY_TXT_IDX, c.category);
        result.setText(DTOConstants.COURSE_INST_ACCRONYM_TXT_IDX, c.institutionAccronym);
        result.setText(DTOConstants.COURSE_NAME_TXT_IDX, c.name);
        result.setText(DTOConstants.COURSE_STATUS_TXT_IDX, c.status.toString());
        result.setText(DTOConstants.COURSE_LINK_TXT_IDX, c.link);
        result.setText(DTOConstants.COURSE_ENROLL_TXT_IDX, c.enrollmentInfo);
        result.setText(DTOConstants.COURSE_PROF_TXT_IDX, c.professorInfo);
        result.setText(DTOConstants.COURSE_INSTITUTION_TXT_IDX, c.institution);
        result.setText(DTOConstants.COURSE_FORUM_LINK_TXT_IDX, c.forumLink);
        result.setNumber(DTOConstants.COURSE_ORDER_NUM_IDX, c.ordering);
        return result;
    }

    public static TableMessage getCourseTrackerDTO(CourseTracker t, boolean includeBio) {
        //log.warning("DTO 1");
        int numOfText = includeBio?4:3;
        TableMessage result = new TableMessage(numOfText, 4, 1);
        result.setText(DTOConstants.TRACK_COURSE_TXT_IDX, t.courseName);
        result.setText(DTOConstants.TRACK_GRADE_TXT_IDX, t.getGradeDescription());
        //log.warning("DTO 2");
        result.setText(DTOConstants.TRACK_VERIFY_TXT_IDX, t.verification.getDisplayString());
        //log.warning("DTO 3");;
        result.setNumber(DTOConstants.TRACK_STATUS_COUNT_NUM_IDX, t.statusUpdates.size());
        result.setNumber(DTOConstants.TRACK_FEED_COUNT_NUM_IDX, t.feedBack.size());
        //log.warning("DTO 4");
        result.setNumber(DTOConstants.TRACK_PROGRESS_NUM_IDX, t.getPublicProgress());
        result.setNumber(DTOConstants.TRACK_PRIVATE_PROGRESS_NUM_IDX, t.progress);
        result.setDate(DTOConstants.TRACK_MOD_DATE_IDX, t.dateUpdated);
        if(includeBio)
            result.setText(DTOConstants.TRACK_MEMBER_TXT_IDX, t.getKey().getParent().getName());
        //log.warning("DTO 5");
        result.setMessageId(t.getKey().getString());
        return result;
    }

    public static List<TableMessage> getFeedTrackDTO(CourseTracker t) {
        ArrayList<TableMessage> result = new ArrayList<>(t.statusUpdates.size() + 1); //+1 header
        result.add(DTOConstants.getFeedTrackHeader());
        final int PREVIEW_LENGTH = 10;
        for (int i = 0; i < t.statusUpdates.size(); i++) {
            //String previewText = t.statusUpdates.get(i).getValue();
            TableMessage m = new TableMessage(0, 1, 1);
            //m.setText(DTOConstants.FEED_TRACK_PREV_TEXT_IDX, getPreview(previewText, PREVIEW_LENGTH));
            //m.setText(DTOConstants.FEED_TRACK_TRAIL_TEXT_IDX, t.trackTrail.get(i));
            m.setNumber(DTOConstants.FEED_TRACK_POS_NUM_IDX, i + 1);
            m.setDate(DTOConstants.FEED_TRACK_DATE_IDX, t.statusDates.get(i));
            result.add(m);
        }
        return result;
    }

    public static List<TableMessage> getFeedBackDTO(CourseTracker t) {
        ArrayList<TableMessage> result = new ArrayList<>(t.feedBack.size() + 1); //+1 header
        result.add(DTOConstants.getFeedTrackHeader());
        final int PREVIEW_LENGTH = 10;
        for (int i = 0; i < t.feedBack.size(); i++) {
            String previewText = t.feedBack.get(i).toString();
            TableMessage m = new TableMessage(0, 1, 1);
            //m.setText(DTOConstants.FEED_BACK_PREV_TEXT_IDX, getPreview(previewText, PREVIEW_LENGTH));
            //m.setText(DTOConstants.FEED_BACK_TRAIL_TEXT_IDX, t.feedTrail.get(i));
            m.setNumber(DTOConstants.FEED_BACK_POS_NUM_IDX, i + 1);
            m.setDate(DTOConstants.FEED_BACK_DATE_IDX, t.feedBackDates.get(i));
            result.add(m);
        }
        return result;
    }

    private static String getPreview(String text, int length) {
        if (text == null)
            return "";

        int lastIdx = text.length() - 1;
        if (lastIdx > (length * 2))
            text = text.substring(0, (length * 2));
        text = text.replaceAll("\\<[^>]*>", "");

        //sanity truncation
        lastIdx = text.length() - 1;
        lastIdx = Math.min(lastIdx, length -1);
        return text.substring(0, lastIdx);
    }

    public static CourseTracker startTrack(Key<ApplicantUniqueIdentifier> member, Key<Course> ck) throws DuplicateEntitiesException {
        Objectify ofy = ObjectifyService.beginTransaction();
        CourseTracker newTrack = new CourseTracker(member, ck);
        try {
            CourseTracker oldTrack = ofy.find(newTrack.getKey());
            if (oldTrack != null)
                throw new DuplicateEntitiesException("Already tracking " + ck.getName());
            ofy.put(newTrack);
            ofy.getTxn().commit();
        } finally {
            if (ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }
        return newTrack;
    }

}
