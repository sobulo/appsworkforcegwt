package com.fertiletech.sap.server.entities.moocs;

import com.fertiletech.sap.shared.CourseStatus;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.util.Date;

@Unindexed
@Cached
public class Course {
    @Id
    String name;

    String platform;

    @Indexed
    String category;

    @Indexed
    int ordering;

    //no indices
    CourseStatus status;
    String link;
    String enrollmentInfo;

    String institution;
    String institutionAccronym;
    String professorInfo;
    String forumLink;

    String updateUser;
    Date createDate;

    public Course(){} //empty constructor required by objectify

    public Course(String platform, String category, String name, int ordering, CourseStatus status,
                  String link, String enrollmentInfo, String user, String institution, String accronym, String prof, String forum) {
        this.platform = platform;
        this.category = category;
        this.name = name;
        this.ordering = ordering;
        this.status = status;
        this.link = link;
        this.enrollmentInfo = enrollmentInfo;
        this.updateUser = user;
        this.institution = institution;
        this.institutionAccronym = accronym;
        this.forumLink = forum;
        this.professorInfo = prof;
    }

    @PrePersist
    void updateCreationDate()
    {
        createDate = new Date();
    }

    public static Key<Course> getKey(String name)
    {
        return new Key<>(Course.class, name);
    }

    public Key<Course> getKey()
    {
        return getKey(name);
    }
}
