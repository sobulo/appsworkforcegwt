package com.fertiletech.sap.server.entities;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class MessageText {
	@Id
	String identity;
	
	Text messageTxt;
	Text messageHTML;
	
	Date dateModified;
	String modifiedBy;
	
	public MessageText() {}

	public MessageText(String identity, String contentText, String contentHTML, String modifiedBy) {
		super();
		this.identity = identity;
		this.messageTxt = new Text(contentText);
		this.messageHTML = new Text(contentHTML);
		this.modifiedBy = modifiedBy;
	}
	
	public Key<MessageText> getKey()
	{
		return new Key<>(MessageText.class, identity);
	}
	
	public static Key<MessageText> getKey(String name)
	{
		return new Key<>(MessageText.class, name);
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateModified = new Date();
	}

	public Text getMessageText() {
		return messageTxt;
	}

	public Text getMessageHTML() {
		return messageHTML;
	}
	
	public void setMessageText(String content) {
		messageTxt = new Text(content);
	}
	
	public void setMessageHTML(String content) {
		messageHTML = new Text(content);
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getDateModified() {
		return dateModified;
	}
	
	

}
