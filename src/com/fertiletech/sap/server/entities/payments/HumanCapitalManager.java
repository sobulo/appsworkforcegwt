package com.fertiletech.sap.server.entities.payments;

import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;
import java.util.ArrayList;

@Unindexed
public class HumanCapitalManager {
    public final static String ID = "HCM";
    @Id
    String id;
    @Parent
    private Key<ApplicantUniqueIdentifier> memberKey;
    int coins;

    ArrayList<String> auditTrail;

    HumanCapitalManager(){
        this.id = ID;
        this.coins = 0;
        this.auditTrail = new ArrayList<String>();
    }

    HumanCapitalManager(Key<ApplicantUniqueIdentifier> userKey){
        this();
        memberKey = userKey;
    }

    void increaseHustle(int point, String reason)
    {
        coins += point;
        auditTrail.add(generateReasonPrefix(true, point) + reason);
    }

    Boolean hasEnoughHustle(HustleCoins hc)
    {
        if(hc.spent)
            return null;
        else
            return (hc.coins < this.coins); //spending within budget
    }

    boolean decreaseHustle(HustleCoins hc, String reason)
    {
        if(hasEnoughHustle(hc))
        {
            this.coins -= hc.coins;
            hc.spent = true;
            auditTrail.add(generateReasonPrefix(false, hc.coins) + reason);

        }
        return false;
    }

    String generateReasonPrefix(boolean isIncrease, int amount)
    {
        String result = "";
        if(isIncrease)
            result = "Increasing from ";
        else
            result = "Decreasing from ";

        result = result + coins + " coins to " + amount + " coins. ";

        return result;
    }
}

