/**
 * 
 */
package com.fertiletech.sap.server.entities.payments;

import com.fertiletech.sap.shared.BillDescriptionItem;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class InetBillDescription
{
    @Id private String name;
    private Date createDate;
    String userName;
    String bank;
    String accountName;
    String instructions;
    String accountNumber;

    private final static String DEFAULT_BANK = "Guaranty Trust Bank (GTBank)";
    private final static String DEFAULT_ACCOUNT_NAME = "Fertile-Tech Business Systems (FTBS)";
    private final static String DEFAULT_ACCOUNT_NUMBER = "0128981396";
    private final static String DEFAULT_INSTRUCTIONS ="Pay into the specified account via electronic transfer or" +
			" by making a deposit at the bank. Once you've paid come back and verify your payment on our website";
    
    @Serialized
    private LinkedHashSet<BillDescriptionItem> itemizedBill;
    
	public InetBillDescription() {
		itemizedBill = new LinkedHashSet<BillDescriptionItem>();
	}
    
    /*InetBillDescription(String name, String user, Date createDate, LinkedHashSet<BillDescriptionItem> itemizedBill)
    {
    	this(name, user, createDate, itemizedBill, null, null, null, null);
    }*/

	InetBillDescription(String name, String user, Date createDate, LinkedHashSet<BillDescriptionItem> itemizedBill,
						String bank, String accountName, String accountNumber, String instructions)
	{
		this();
		this.name = name;
		this.createDate = createDate;
		this.itemizedBill = itemizedBill;
		setAccountName(accountName);
		setAccountNumber(accountNumber);
		setBank(bank);
		setIntstructions(instructions);
		this.userName = user;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		if(bank == null || bank.trim().length() == 0)
			bank = DEFAULT_BANK;
		this.bank = bank;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		if(accountName == null || accountName.trim().length() == 0)
			accountName = DEFAULT_ACCOUNT_NAME;
		this.accountName = accountName;
	}

	public String getIntstructions() {
		return instructions;
	}

	public void setIntstructions(String intstructions) {
		if(intstructions == null || instructions.trim().length() == 0)
			intstructions = DEFAULT_INSTRUCTIONS;
		this.instructions = intstructions;
	}

	public String getAccountNumber() {
		return accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		if(accountNumber == null || accountNumber.trim().length() == 0)
			accountNumber = DEFAULT_ACCOUNT_NUMBER;
		if(accountNumber.length() < 10 || accountNumber.length() > 11) //10 digit nubian or 11 digit phone number
			throw new IllegalArgumentException("Only nubian accounts supported: " + accountNumber);
		//TODO regex check that it's just digits
		this.accountNumber = accountNumber;
	}

	public Key<InetBillDescription> getKey() {
		return new Key(InetBillDescription.class, name);
	}


	public String getName() {
		return name;
	}
	
	public Date getCreateDate()
	{
		return createDate;
	}

	public HashSet<BillDescriptionItem> getItemizedBill() {
		return itemizedBill;
	}

	
	public double getTotalAmount()
	{
		double total = 0;
		for(BillDescriptionItem bdi : itemizedBill)
			total += bdi.getAmount();
		return total;
	}
}