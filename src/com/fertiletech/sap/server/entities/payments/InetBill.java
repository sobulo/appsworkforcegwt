/**
 * 
 */
package com.fertiletech.sap.server.entities.payments;

import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.text.DateFormat;
import java.util.Date;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Indexed
@Cached
public class InetBill{
	@Id private String key;
	private Key<InetBillDescription> billTemplateKey;
	@Unindexed private double totalAmount;
	@Unindexed private double settledAmount;
	@Parent private Key<ApplicantUniqueIdentifier> userKey;
	private boolean settled;
	private Date issueDate;
	private boolean spent = false;

	public boolean isSpent() {
		return spent;
	}

	public void setSpent(boolean spent) {
		if(settled)
			this.spent = spent;  //should this just be set to settled?
		else if(!spent)
			this.spent = spent; //sanity check, we shouldn't hit a case where we want to unspend a bill
		else
			throw new RuntimeException("Attempted to spend bill not yet settled: " + key);
	}


	
	final static double THRESHOLD = 0.01;
	
	InetBill(){}
	
	InetBill(InetBillDescription billTemplate, Key<ApplicantUniqueIdentifier> billedUserKey)
	{
		this();
		this.billTemplateKey = billTemplate.getKey();
		this.issueDate = new Date();
		this.userKey = billedUserKey;
		this.key = getKeyString();
		this.settled = false;
		this.totalAmount = billTemplate.getTotalAmount();
	}
	
    private String getKeyString()
    {
        String format = "%s~%s";
        return String.format(format, billTemplateKey.getName(), DateFormat.getDateInstance().format(issueDate));
    }	

	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	public Key<InetBill> getKey() {
		return new Key<InetBill>(userKey, InetBill.class, key);
	}

	public static String getKeyDescription(Key<InetBill> bk)
	{
		Key<ApplicantUniqueIdentifier> parent = bk.getParent();
		return parent.getName() + "-" + bk.getName();
	}
	
	public boolean isSettled()
	{
		return settled;
	}
	
	public double getTotalAmount()
	{
		return totalAmount;
	}
	
	public double getSettledAmount()
	{
		return settledAmount;
	}
	
	public Date getIssueDate()
	{
		return issueDate;
	}
	
	public void setSettledAmount(double amount)
	{
		settledAmount = amount;
	}
	
	
	
	public Key<ApplicantUniqueIdentifier> getUserKey()
	{
		return userKey;
	}
	
	public Key<InetBillDescription> getBillTemplateKey()
	{
		return billTemplateKey;
	}
	
	@PrePersist void updateSettled() { settled = Math.abs(totalAmount - settledAmount) < THRESHOLD; }; 
}
