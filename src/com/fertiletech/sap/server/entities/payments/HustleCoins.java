package com.fertiletech.sap.server.entities.payments;

import com.fertiletech.sap.shared.DTOConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;

/*
How it works?
Step 1
User chooses amount on indicate payment
User has option of *use hustle instead
hustle disables reference and comments for use
reference is hardcoded to HC
comment does payment divided by 360

//Step 2 (one transaction)
User creates coin - check to see hcm balance > hc balance .... read only op
affiliate HC with unverified payment ... create HustleCoin object of naira equivalance of payment
Deduct HC from HCM
mark payment as verified

Implementation of HustleCoin object below. Search usage for logic described above
*/

@Unindexed
public class HustleCoins {
    public final static String HUSTLE = "HC";
    @Parent
    private Key<InetBillPayment> paymentKey;
    @Id
    String id;

    boolean spent;

    int coins;

    final static int DEFAULT_FX = 360;

    HustleCoins(){ id = HUSTLE; }

    HustleCoins(InetBillPayment payment) {
        this();
        if(payment.getReferenceId().equals(DTOConstants.HUSTLE)) {
            paymentKey = payment.getKey();
            coins = (int) (payment.getAmount() / DEFAULT_FX);
            this.spent = false;
        }
    }

    public int getSpentCoins()
    {
        return coins;
    }
}
