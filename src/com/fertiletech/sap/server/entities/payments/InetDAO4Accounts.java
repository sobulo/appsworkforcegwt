/**
 * 
 */
package com.fertiletech.sap.server.entities.payments;

import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.server.tasks.TaskQueueHelper;
import com.fertiletech.sap.shared.*;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import java.text.DateFormat;
import java.util.*;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class InetDAO4Accounts {
	private static final Logger log = Logger.getLogger(InetDAO4Accounts.class.getName());

	public static InetBillDescription createBillDescription(Date dueDate, String name, String user,
			LinkedHashSet<BillDescriptionItem> itemizedBill, String bank, String accountName, String accountNumber,
			String instructions) throws DuplicateEntitiesException {
		InetBillDescription bd = new InetBillDescription(name, user, dueDate, itemizedBill, bank, accountName,
				accountNumber, instructions);
		// bd.setItemizedBill(itemizedBill);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			InetBillDescription bdCheck = ofy.find(bd.getKey());
			if (bdCheck != null) {
				String msgPrefix = "Bill description already exists. Details: ";
				DuplicateEntitiesException ex = new DuplicateEntitiesException(msgPrefix + bdCheck.getKey().getName());
				log.severe(msgPrefix + bd.getKey());
				throw ex;
			}
			ofy.put(bd);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return bd;
	}

	public static InetBill createBill(InetBillDescription billTemplate, Key<ApplicantUniqueIdentifier> billedUserKey)
			throws DuplicateEntitiesException {
		InetBill bill = new InetBill(billTemplate, billedUserKey);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			InetBill billCheck = ofy.find(bill.getKey());
			if (billCheck != null) {
				String msgPrefix = "Bill already exists. Details: ";
				DuplicateEntitiesException ex = new DuplicateEntitiesException(
						msgPrefix + billCheck.getKey().getName());
				log.severe(msgPrefix + bill.getKey());
				throw ex;
			}
			ofy.put(bill);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return bill;
	}

	public static InetBillPayment createPayment(Key<InetBill> billKey, double amount, String referenceId,
			String comments, Date paymentDate) throws MissingEntitiesException, ManualVerificationException {
		InetBillPayment bp = new InetBillPayment(billKey, amount, referenceId, comments, paymentDate);
		Objectify ofy = ObjectifyService.beginTransaction();

		InetBill bill = ofy.find(billKey);
		if (bill == null) {
			String msgFmt = "Unable to find Bill: %s";
			log.warning(String.format(msgFmt, billKey));
			throw new MissingEntitiesException(String.format(msgFmt, billKey.getName()));
		}

		double unpaidBillAmount = bill.getTotalAmount() - bill.getSettledAmount();
		if (unpaidBillAmount - amount < (0 - InetBill.THRESHOLD)) {
			String msgFmt = "Payment amount %f exceeds unpaid bill amount %f."
					+ " If your payment exceeds bill for (Bill ID: %s)., specify your payment amount to match the "
					+ " bill amount. This will enable you to bypass this exception. Afterwards email hello@appsworkforce,com to "
					+ " explain the excess. You should also provide a brief explanation in the comments field";
			log.warning(String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
			throw new ManualVerificationException(
					String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
		}

		try {
			ofy.put(bp); // we don't update the bill until the payment is
							// verified
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}

		return bp;
	}

	public static HustleCoins createHustleCoins(InetBillPayment bp, HumanCapitalManager hcm, String reason) {
		HustleCoins hc = new HustleCoins(bp);
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			hcm.decreaseHustle(hc, reason);
			ofy.put(hc, hcm);
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return hc;
	}

	public static InetBillPayment verifyPayment(Key<InetBillPayment> paymentKey, boolean isVerfied, String vID,
			Date vDate) throws MissingEntitiesException, ManualVerificationException {
		Objectify ofy = ObjectifyService.beginTransaction();

		InetBill bill = ofy.find(paymentKey.<InetBill> getParent());
		InetBillPayment bp = ofy.find(paymentKey);
		if (bill == null || bp == null) {
			String msgFmt = "Unable to find Bill or Payment: [%s] or [%s] ";
			log.warning(String.format(msgFmt, paymentKey.getParent(), paymentKey));
			throw new MissingEntitiesException(String.format(msgFmt, paymentKey.getParent().getName()));
		}

		if (bp.isVerified())
			throw new ManualVerificationException("This Payment has already been verified");

		double unpaidBillAmount = bill.getTotalAmount() - bill.getSettledAmount();
		double amount = bp.getAmount();
		if (unpaidBillAmount - bp.getAmount() < (0 - InetBill.THRESHOLD)) {
			String msgFmt = "Payment amount %f exceeds unpaid bill amount %f."
					+ " If your payment exceeds bill for (Bill ID: %s)., specify your payment amount to match the "
					+ " bill amount. This will enable you to bypass this exception. Afterwards email hello@appsworkforce,com to "
					+ " explain the excess. You should also provide a brief explanation in the comments field";
			log.warning(String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
			throw new ManualVerificationException(
					String.format(msgFmt, amount, unpaidBillAmount, bill.getKey().getName()));
		}

		try {
			ArrayList objectsToSave = new ArrayList(2);
			if (isVerfied) // we don't update the bill until the payment is
							// verified
			{
				bill.setSettledAmount(amount + bill.getSettledAmount()); // update
																			// the
																			// bill
				objectsToSave.add(bill);

			}
			bp.setVerified(isVerfied, vID, vDate);
			objectsToSave.add(bp);
			ofy.put(objectsToSave);
			ofy.getTxn().commit();
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}

		return bp;
	}

	public static SalesLead issueLease(Key<InetBillDescription> bdKey, Key<SalesLead> leadKey, String referenceId,
			String comments, Date paymentDate, String userId)
			throws MissingEntitiesException, ManualVerificationException, DuplicateEntitiesException {
		// create bill
		Objectify ofyR = ObjectifyService.begin();
		InetBillDescription bd = ofyR.get(bdKey);

		Objectify ofy = ObjectifyService.beginTransaction();
		SalesLead lead = null;
		try {
			lead = ofy.get(leadKey);
			Key<ApplicantUniqueIdentifier> ak = leadKey.getParent();
			InetBill bill = new InetBill(bd, ak);
			InetBill billCheck = ofy.find(bill.getKey());
			if (billCheck != null) {
				String msgPrefix = "Bill already exists. Details: ";
				DuplicateEntitiesException ex = new DuplicateEntitiesException(
						msgPrefix + billCheck.getKey().getName());
				log.severe(msgPrefix + bill.getKey());
				throw ex;
			}
			// ofy.put(bill);

			// create payment
			InetBillPayment bp = new InetBillPayment(bill.getKey(), bill.getTotalAmount(), referenceId, comments,
					paymentDate);
			// ofy.put(bp); //we don't update the bill until the payment is
			// verified

			// verify payment
			bill.setSettledAmount(bill.getTotalAmount()); // update the bill
			bill.updateSettled(); //handled by prepersist but needed here for below to work, unless we commit first
			bill.setSpent(true);
			bp.setVerified(true, userId, paymentDate);

			// issue license

			String description = bill.getBillTemplateKey().getName();
			Date today = new Date();
			if (lead.getLicenseBegins() == null)
				lead.setLicenseBegins(today);

			Date le = lead.getLicenseEnds();
			Calendar gc = GregorianCalendar.getInstance();
			gc.setTime(today);
			if (le != null) {

				if (today.after(le)) {
					lead.setLicenseBegins(today);
				} else {
					throw new ManualVerificationException("License extention not yet supported. Wait for expiration"
							+ description + " Current license expiration date is: " + le + " But trying to begin on: "
							+ gc.getTime());
				}
			}

			String licenseName = DTOConstants.BILL_DESC_MAP.get(description);

			if (licenseName.equals(DTOConstants.LICENSE_MONTHLY))
				gc.add(Calendar.MONTH, 1);
			// else if(licenseName.equals(DTOConstants.LICENSE_ANNUAL))
			// gc.add(Calendar.YEAR, 1);
			// else if(licenseName.equals(DTOConstants.LICENSE_WORKFORCE))
			// gc.add(Calendar.YEAR, 1);
			else
				throw new ManualVerificationException("Auto creation supported only for monthly: " + description);
			lead.setLicenseEnds(gc.getTime(), description);
			


			// save objects
			ArrayList objectsToSave = new ArrayList(3);
			objectsToSave.add(bill); // new bill
			objectsToSave.add(bp); // new payment and verification
			objectsToSave.add(lead); // generate license

			ofy.put(objectsToSave);
			ofy.getTxn().commit();
			licenceNotification(lead, bill.getKey(), null);
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}

		return lead;
	}

	public static SalesLead issueLease(Key<InetBill> billKey, Key<SalesLead> leadKey, HttpServletRequest req)
			throws MissingEntitiesException, ManualVerificationException {
		Objectify ofy = ObjectifyService.beginTransaction();
		SalesLead lead = null;
		try {
			InetBill bill = ofy.find(billKey);
			lead = ofy.find(leadKey);
			if (bill == null || lead == null) {
				String msgFmt = "Unable to find Bill or Regristration: [%s] or [%s] ";
				log.warning(String.format(msgFmt, billKey, leadKey));
				throw new MissingEntitiesException(String.format(msgFmt, billKey.getParent().getName()));
			}

			if (!bill.isSettled()) {
				String msgFmt = "This bill has not yet been paid for : [%s]";
				log.warning(String.format(msgFmt, billKey.getParent().getName()));
				throw new ManualVerificationException(String.format(msgFmt, billKey.getParent().getName()));
			}
			if (bill.isSpent()) {
				String msgFmt = "This bill has already been used/spent for : [%s]";
				log.warning(String.format(msgFmt, billKey.getParent().getName()));
				throw new ManualVerificationException(String.format(msgFmt, billKey.getParent().getName()));
			}

			String description = bill.getBillTemplateKey().getName();
			Date today = new Date();
			if (lead.getLicenseBegins() == null)
				lead.setLicenseBegins(today);

			Date le = lead.getLicenseEnds();
			Calendar gc = GregorianCalendar.getInstance();
			gc.setTime(today);
			if (le != null) {

				if (today.after(le)) {
					lead.setLicenseBegins(today);
				} else {
					throw new ManualVerificationException("License extention not yet supported. Wait for expiration"
							+ description + " Current license expiration date is: " + le + " But trying to begin on: "
							+ gc.getTime());
				}
			}

			String licenseName = DTOConstants.BILL_DESC_MAP.get(description);

			if (licenseName.equals(DTOConstants.LICENSE_MONTHLY))
				gc.add(Calendar.MONTH, 1);
			else if (licenseName.equals(DTOConstants.LICENSE_ANNUAL))
				gc.add(Calendar.YEAR, 1);
			else if (licenseName.equals(DTOConstants.LICENSE_WORKFORCE))
				gc.add(Calendar.YEAR, 1);
			else
				throw new ManualVerificationException("Unrecognized bill type: " + description);
			lead.setLicenseEnds(gc.getTime(), description);
			bill.setSpent(true);
			ofy.put(lead, bill);
			ofy.getTxn().commit();
			licenceNotification(lead, billKey, req);
		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}

		return lead;
	}

	private static void licenceNotification(SalesLead lead, Key<InetBill> billKey, HttpServletRequest req) {
		// Schedule success email
		String startDateStr = DateFormat.getDateInstance().format(lead.getLicenseBegins());
		String endDateStr = DateFormat.getDateInstance().format(lead.getLicenseEnds());
		String subject = "AW license confirmation for AI Number: " + lead.getLeadID();
		String txtMessage = "A " + billKey.getName()
				+ " has been successfuly applied to your profile. Your license lasts for the period of [" + startDateStr
				+ " to " + endDateStr + "]";
		String hm = "A " + billKey.getName()
				+ " has been successfuly applied to your profile. Your license lasts for the period of [" + startDateStr
				+ " to " + endDateStr + "] <br/><br/>Continue tracking your 6.0.0.1x (Introduction to computer science and programming) milestones @ AW.  "
				+ " <a href='https://www.appsworkforce.com/lab/#continue'> Bookmark this link! </a><br/>br/>State whether you've started or completed the course. For help on how to start or continue your course tracks, <a href='http://about.appsworkforce.com/learn'> click on start and continue on the subpage menu listed at the bottom of the learn page on the AW reference site</a>. <br/><br/><br/><br/>";
		String htmlMsg = "<div style='text-align: center; font-weight: bold;'> " + subject + "</div>";
		htmlMsg += "<p>" + "Dear " + lead.getFullName() + ",<br/></br>" + hm + "</p>";
		String[] messages = { txtMessage, htmlMsg };

		String[] commentString = { billKey.getName() + " issued for: " + lead.getEmail() };
		String user = "aw-auto";
		if (req != null)
			user = LoginHelper.getLoggedInUser(req);
		if (user == null)
			user = "aw-default";

		MessagingDAO.scheduleSendEmail(lead.getEmail(), messages, subject, true);
		TaskQueueHelper.scheduleCreateComment(commentString, lead.getKey().getString(), user);
	}

	public static List<InetBill> getBills(Key<InetBillDescription> bdKey, Objectify ofy) {
		return ofy.query(InetBill.class).filter("billTemplateKey", bdKey).list();
	}

	public static List<InetBill> getBills(Date start, Date end, Objectify ofy) {
		return ofy.query(InetBill.class).filter("issueDate >=", start).filter("issueDate <=", end).list();
	}

	public static List<InetBill> getStudentBills(Key<ApplicantUniqueIdentifier> userKey, Objectify ofy) {
		return ofy.query(InetBill.class).ancestor(userKey).order("-issueDate").list();
	}

	public static HashMap<String, String> getStudentBillKeys(Key<ApplicantUniqueIdentifier> userKey, Objectify ofy) {
		HashMap<String, String> result = new HashMap<String, String>();
		QueryResultIterable<Key<InetBill>> billKeys = ofy.query(InetBill.class).ancestor(userKey).fetchKeys();
		for (Key<InetBill> bk : billKeys)
			result.put(ofy.getFactory().keyToString(bk), bk.getName());
		return result;
	}

	public static List<InetBillPayment> getPayments(Key<InetBill> bk, Objectify ofy) {
		return ofy.query(InetBillPayment.class).ancestor(bk).list();
	}

	public static HashMap<String, String> getAllBillDescriptions(Objectify ofy) {
		HashMap<String, String> result = new HashMap<String, String>();

		QueryResultIterable<Key<InetBillDescription>> bdKeySet = ofy.query(InetBillDescription.class).fetchKeys();
		for (Key<InetBillDescription> bdKey : bdKeySet)
			result.put(ofy.getFactory().keyToString(bdKey), bdKey.getName());

		return result;
	}

	public static List<InetBillPayment> getPayments(Date startDate, Date endDate) {
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(InetBillPayment.class).filter("paymentDate >=", startDate).filter("paymentDate <=", endDate)
				.list();
	}

	public static TableMessage getPaymentMessage(InetBillPayment bp) {
		TableMessage m = new TableMessage(6, 1, 2);
		m.setText(DTOConstants.PAY_EMAIL_TXT_IDX, bp.getKey().getParent().getParent().getName());
		m.setText(DTOConstants.PAY_NAME_TXT_IDX, bp.getKey().getParent().getName());
		m.setNumber(DTOConstants.PAY_AMT_NUM_IDX, bp.getAmount());
		m.setDate(DTOConstants.PAY_DUE_DATE_IDX, bp.getPaymentDate());
		m.setText(DTOConstants.PAY_VERIFIED_TXT_IDX, (bp.isVerifiedDisplay()));
		m.setText(DTOConstants.PAY_COMMENTS_TXT_IDX, bp.getComments());
		m.setText(DTOConstants.PAY_REF_TXT_IDX, bp.getReferenceId());
		m.setText(DTOConstants.PAY_VUSR_TXT_IDX, bp.getVerifyID());
		m.setDate(DTOConstants.PAY_VDT_DATE_IDX, bp.getVerifyDate());
		return m;
	}
}
