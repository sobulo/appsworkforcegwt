/**
 * 
 */
package com.fertiletech.sap.server.entities.payments;


import com.fertiletech.sap.shared.NPMBFormConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

import javax.persistence.Id;
import java.util.Date;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class InetBillPayment{
	@Id Long key;
	@Indexed private Date paymentDate;
	@Parent private Key<InetBill> allocatedBill;
	@Indexed private String referenceId;
	private String comments;
	private double amount;
	private Boolean verified;
	String verifyID;
	Date verifyDate;
	
	InetBillPayment(){}
	
	InetBillPayment(Key<InetBill> billKey, double amount, String referenceId, String comments, Date paymentDate)
	{
		this.amount = amount;
		this.comments = comments;
		this.referenceId = referenceId;
		this.paymentDate = paymentDate;
		this.allocatedBill = billKey;
		this.verified = null;
	}

	public boolean isVerified()
	{
		 return verified==null?false:verified;
	}

	public String isVerifiedDisplay()
	{
		if(verified == null)
			return NPMBFormConstants.PENDING_VERIFICATION;
		else
			return verified?"Confirmed":"Rejected";
	}

	public String getVerifyID()
	{
		return verifyID;
	}

	public Date getVerifyDate()
	{
		return verifyDate;
	}

	public void setVerified(boolean verified, String userId, Date verificationDate)
	{
		this.verified = verified;
		this.verifyID = userId;
		this.verifyDate = verificationDate;

	}

	public Key<InetBillPayment> getKey() {
		return new Key<InetBillPayment>(allocatedBill, InetBillPayment.class, key);
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public String getComments() {
		return comments;
	}

	public double getAmount() {
		return amount;
	}
}
