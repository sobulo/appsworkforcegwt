/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.sap.server.pub;


import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;


// [START example]
@SuppressWarnings("serial")
public class UpdateBookServlet extends HttpServlet {
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {

	    req.setAttribute("action", "AW Application Form");          // Part of the Header in form.jsp
	    req.setAttribute("destination", "update");  // The urlPattern to invoke (this Servlet)
	    req.setAttribute("page", "form");           // Tells base.jsp to include form.jsp
	    req.setAttribute("bannerlink", "http://about.appsworkforce.com/learn/certificate");
	      req.setAttribute("bannerimg", "images/certified.jpg");
	      req.setAttribute("bannerdesc", "AppsWorkce certification image");
	    req.getRequestDispatcher("/base.jsp").forward(req, resp);
  }

  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
    BookDao dao = (BookDao) this.getServletContext().getAttribute("dao");



    try {

      // [START bookBuilder]
      Book book = new Book.Builder()

  	        .state(req.getParameter("state"))
  	        .name(req.getParameter("name"))
  	        .email(req.getParameter("email"))
  	        .build();
      // [END bookBuilder]
      book.setMessage(req.getParameter("message"));


      boolean isValidBooking = DatastoreDao.populateFormErrors(book);
      if(!isValidBooking)
      {
    	  book.setStyle("disabled");
    	     req.setAttribute("backstyle", "btn-danger");;
      }
      else
    	     req.setAttribute("backstyle", "btn-warning");
      
      /*
       * Need a switch
       *  case back - set attribute page to form
       *  case error - set attribute page to form but add error attributes in error object?
       *  case forward - set attribute to another form that will get to call create
       *  
       *  review page displays all fields
       *  back button to allow editing
       *  submit button to post to create
       *  
       */

      req.setAttribute("book", book);
      req.setAttribute("page", "review");
      req.setAttribute("bannerlink", "http://about.appsworkforce.com/learn/code-of-conduct");
      req.setAttribute("bannerimg", "images/intcode.jpg");
      
      req.setAttribute("bannerdesc", "AppsWorkce member integrity");
      req.getRequestDispatcher("/base.jsp").forward(req, resp);
    } catch (Exception e) {
      throw new ServletException("Error updating book", e);
    }
  }
}
// [END example]
