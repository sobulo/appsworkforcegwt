/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.sap.server.pub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.persistence.Lob;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mortbay.log.Log;

import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListOptions;
import com.google.appengine.tools.cloudstorage.ListResult;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

// [START example]
@SuppressWarnings("serial")
public class LoanStatusServlet extends HttpServlet {
	
	final  GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
		      .initialRetryDelayMillis(10)
		      .retryMaxAttempts(10)
		      .totalRetryPeriodMillis(15000)
		      .build());
	
	private static final Logger log = Logger.getLogger(LoanStatusServlet.class.getName());	
	
	public static String GCS_BUCKET_NAME = ""; 
	static
	{
		EntityDAO.registerClassesWithObjectify();
		try
		{
			GCS_BUCKET_NAME = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
		}
		catch(Exception Ex)
		{
			log.severe("Failed to auto initialize GCS Bucket Name, defaulting to string literal");
			log.severe(Ex.toString());
			GCS_BUCKET_NAME = "appsworkforce.appspot.com";
		}
	}
	
	public List<TableMessage> getUploads(String loanID) throws MissingEntitiesException{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "File Name", TableMessageContent.TEXT);
		h.setText(1, "Date", TableMessageContent.DATE);
		h.setMessageId(loanID);
		result.add(h);
		try {
			ListOptions listOpt = new ListOptions.Builder().setPrefix(loanID).build();
			ListResult items = gcsService.list(GCS_BUCKET_NAME, listOpt);
			while(items.hasNext())
			{
				ListItem record = items.next();
				if(record.isDirectory()) continue;
				TableMessage m = new TableMessage(2, 0, 1);
				m.setText(0, record.getName().replace(loanID, ""));
				m.setText(1, record.getName());
				m.setDate(0, record.getLastModified());
				m.setMessageId(EntityConstants.GCS_HOST + GCS_BUCKET_NAME + "/" + record.getName());
				//log.warning("Message id is: " + m.getMessageId());
				result.add(m);
			}
			//log.warning("Found Records: " + (result.size() - 1));
		} catch (IOException e) {
			throw new MissingEntitiesException("Unable to connect to cloud storage");
		}
		return result;
	}
	
	public String imageLink(String loanID) throws MissingEntitiesException
	{
		List<TableMessage> files = getUploads(loanID);
		for(TableMessage f : files)
		{
			if(f.getText(1).endsWith(".jpg") || f.getText(1).endsWith(".gif")|| f.getText(1).endsWith(".ong"))
				return f.getMessageId();
			
		}
		return null;
		
	}
	


	private final static String ERR_KEY = "awerror";
	// [START setup]
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
													// (this Servlet)
		req.setAttribute("page", "status"); // Tells base.jsp to include
											// form.jsp
		String email = req.getParameter("email");
		String id = req.getParameter("id");
		if(email == null || id == null)
		{
			req.setAttribute("bannerlink", "http://about.appsworkforce.com/fun");
		      req.setAttribute("bannerimg", "images/independent.jpg");
		      req.setAttribute("bannerdesc", "AppsWorkce team projects image");
			req.getRequestDispatcher("/base.jsp").forward(req, resp);
		}
		else
			doVerify(req, resp, email, id);

	}
	// [END setup]

	//post from clicking submit on  
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.warning("starting ... verification");
		HttpSession session = req.getSession();
		// [END bookBuilder]


		String email = req.getParameter("email");
		String id = req.getParameter("id");
		doVerify(req, resp, email, id);
		
	}
	
	private void doVerify(HttpServletRequest req, HttpServletResponse resp, String email, String id) throws ServletException, IOException
	{
		log.warning("ID: " + id + " email: " + email);

		Objectify ofy = ObjectifyService.begin();
		List<Key<SalesLead>> result = ofy.query(SalesLead.class).filter("keyCopy =", Long.valueOf(id)).listKeys();
		if(result.size() == 0)
			{
			log.warning("ID not found");
			req.setAttribute(ERR_KEY, id + " not found in AW Google Cloud Datastore" );
			saveOldformDate(req);
			}
		else
		{
			SalesLead ld = ofy.get(result.get(0));
			
			if(ld.getEmail().equals(email.trim().toLowerCase()))
			{
			Book bk = DatastoreDao.leadStatusToBook(ld);
			req.setAttribute("book", bk);
			req.setAttribute("awid", ld.getKey().getString());
			String link;
			try {
				link = imageLink(ld.getKey().getString());
				if(link != null)
					req.setAttribute("memimg", link);
			} catch (MissingEntitiesException e) {
			}

			log.warning("ok " + bk.toString());
			saveOldformDate(req);
			}
			else
			{
				log.warning(" email:not found ");
				req.setAttribute(ERR_KEY, id + " is not associated with " + email);
				saveOldformDate(req);
				
			}
		}

	      log.warning("dispatching ....");
	      req.setAttribute("page", "status");
	      req.setAttribute("bannerlink", "http://about.appsworkforce.com/learn/code-of-conduct");
	      req.setAttribute("bannerimg", "images/intcode.jpg");
	      req.setAttribute("bannerdesc", "AppsWorkce member integrity");
	      
	       req.getRequestDispatcher("/base.jsp").forward(req, resp);
	      log.warning("all done");
	}
	
	public static String getVerifyURL(String email, String id)
	{
		return "<br/><br/><a href='https://appsworkforce.com/verified?email=" + email + "&id=" + id + "'>Verify " + email + " @ AppsWorkforce</a> (right click to copy link)";
	}
	
	private void saveOldformDate(HttpServletRequest req)
	{
		req.setAttribute("oldmail", req.getParameter("email"));
		req.setAttribute("oldid", req.getParameter("id"));
	}
}
// [END example]
