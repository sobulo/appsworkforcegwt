/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Modified for the purpose of loan applications - sobulo@fertiletech.com
 */

package com.fertiletech.sap.server.pub;

// [START example]
public class Book {
	// [START book]
	private String name;
	private String email;
	private String state;
	private String ofykey;
	private String style;
	private String message;
	private String crdate;
	private String mddate;

	private Long id;
	
	public static String SANG_CREATE = "sang-create-message";

	// [END book]


	// [START constructor]
	// We use a Builder pattern here to simplify and standardize construction of
	// Book objects.
	private Book(Builder builder) {
		this.name = builder.name;
		this.email = builder.email;;
		this.state = builder.state;
		this.id = builder.id;
		this.ofykey = builder.ofykey;

	}
	// [END constructor]

	// [START builder]
	public static class Builder {
		private String name;
		private String email;
		private String state;
		private String ofykey;
		private Long id;


		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}



		public Builder state(String state) {
			this.state = state;
			return this;
		}
		
		public Builder ofykey(String ok) {
			this.ofykey = ok;
			return this;
		}


		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Book build() {
			return new Book(this);
		}
	}
	// [END builder]



	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	
	}

	public void setOfykey(String ofykey) {
		this.ofykey = ofykey;
	}
	
	public String getOfykey() {
		return ofykey;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getCrdate() {
		return crdate;
	}

	public void setCrdate(String datestr) {
		this.crdate = datestr;
	}



	public String getMddate() {
		return  mddate;
	}

	public void setMddate(String datestr) {
		this.mddate = datestr;
	}
	
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	
	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return "Name: " + name + ", email: " + email + ", Access to AW datastore on <a href='cloud.google.com'> on Google </a> granted";
	}
}
// [END example]
