/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.sap.server.pub;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.server.tasks.TaskQueueHelper;




// [START example]
@SuppressWarnings("serial")
public class CreateBookServlet extends HttpServlet {

  // [START setup]
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
    req.setAttribute("action", "AW Request Form");          // Part of the Header in form.jsp
    req.setAttribute("destination", "update");  // The urlPattern to invoke (this Servlet)
    req.setAttribute("page", "form");           // Tells base.jsp to include form.jsp
    req.getRequestDispatcher("/base.jsp").forward(req, resp);
  }
  // [END setup]

  // [START formpost]
  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {

    HttpSession session = req.getSession();


    // [START bookBuilder]
    Book book = new Book.Builder()
	   
	        .state(req.getParameter("state"))
	        .name(req.getParameter("name"))
	        .email(req.getParameter("email"))
	        .build();
    // [END bookBuilder]

    BookDao dao = (BookDao) this.getServletContext().getAttribute("dao");
    try {
      String []  id= dao.createBook(book);
      String msg = req.getParameter("message");
      String[] comments = {"Application for: " + book.toString(), msg, " Response sent: " + id[1]};
      TaskQueueHelper.scheduleCreateComment(comments, id[0], book.getEmail());
      String[] message = {book.toString() + "\n\n" + msg, book.toString() + "<br/><br/><b>Sys Response</b><br/>" + id[1] + "<br/><hr/><br/><b>Desire to learn</b><br/>" + msg + "<br/>"};
      MessagingDAO.scheduleSendEmail("sobulo@fertiletech.com", message, "AW reguest by " +req.getParameter("name"), true);
      book.setStyle(id[2]);
      book.setMessage(id[1]);
      req.setAttribute("bannerlink", "http://about.appsworkforce.com/learn/mit-python-1");
      req.setAttribute("bannerimg", "images/devcode.jpg");
      req.setAttribute("bannerdesc", "AppsWorkce learn  java, python and ruby");
      req.setAttribute("book", book); 
      req.setAttribute("page", "view"); 
      req.getRequestDispatcher("/base.jsp").forward(req, resp);
    } catch (Exception e) {
      throw new ServletException("Cloud servlet failed - try refreshing your browser", e);
    }
  }
  // [END formpost]
}
// [END example]
