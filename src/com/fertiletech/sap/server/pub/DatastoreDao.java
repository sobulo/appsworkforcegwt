/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.sap.server.pub;



import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;


import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.MortgageApplicationFormData;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.fertiletech.sap.shared.FormValidator;
import com.fertiletech.sap.shared.NPMBFormConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

// [START example]
public class DatastoreDao implements BookDao {

  // [START constructor]
	
  	 public final static NumberFormat NUM_FMT = NumberFormat.getInstance();
  	 {
  		NUM_FMT.setMaximumFractionDigits(2);
  	 }

  public DatastoreDao() {}
  // [END constructor]
  
  public static String parseNumber( String formatedStr)
  {
	  try {
		return NUM_FMT.parse(formatedStr).toString();
	} catch (ParseException e) {
		return "";
	}
  }
  
  private static String getStyleErr(String errMessage)
  {
	  return "<span style='color:red'>" + errMessage + "</span>";
  }
  
 /* public boolean populateFormErrors(Book book)
  {
	   HashMap<String, FormValidators[]> validatorMap = FormConstants.pubFieldsMap;
   	
       FormValidators[] nameCheck = validatorMap.get(FormConstants.APPLICANT_SURNAME_KEY);
       FormValidators[] phoneCheck = validatorMap.get(FormConstants.PRIMARY_PHONE_KEY);
       FormValidators[]  mailCheck= validatorMap.get(FormConstants.EMAIL_KEY);
       FormValidators[] employerCheck = validatorMap.get(FormConstants.EMPLOYER_KEY);
       FormValidators[] salaryCheck = validatorMap.get(FormConstants.SALARY_KEY);
       FormValidators[] loanCheck = validatorMap.get(FormConstants.REQUESTED_LOAN_AMOUNT_KEY);
       
       boolean formErr = false;
       //checkName;
       StringBuilder errors = new StringBuilder();
       for(FormValidators check : nameCheck)
    	   formErr = !check.validate(FormConstants.APPLICANT_SURNAME_KEY,book.getName(),  errors);
       if(formErr)
    	   book.setName(getStyleErr(errors.toString()));
       
       check phone 
       
       return formErr;

  }*/
  
  private HashMap<String, String> bookToForm(Book book)
  {
	  String email = book.getEmail();
		HashMap<String, String> appData = new HashMap<>();
		String[] fullName = book.getName().split(",");
		String lastName = fullName[0].trim();
		String firstName = fullName.length > 1 ? fullName[1].trim() : "";
		appData.put(NPMBFormConstants.EMAIL, email);
		appData.put(NPMBFormConstants.FIRST_NAME, firstName);
		appData.put(NPMBFormConstants.SURNAME, lastName);
		appData.put(NPMBFormConstants.STATE_ADDRESS, book.getState());
		return appData;
	  
  }
  // [START create]
  @Override
  public String[] createBook(Book book) {


		HashMap<String, String> appData = bookToForm(book);
		String email = appData.get(NPMBFormConstants.EMAIL);
		if(email == null || email.trim().length() == 0)
			throw new IllegalArgumentException("Email required to start application");


		SalesLead salesLead= new SalesLead(email, EntityDAO.getAllocatedSalesID());
		String suffix ="<br/><hr/><br/><a href='https://www.youtube.com/watch?v=ww2BdhILIio'>Become a programmer</a><br/>";
		appData.put(NPMBFormConstants.ID_STATE, salesLead.getCurrentState().toString());
		try {
			MortgageApplicationFormData data = EntityDAO.createLoanLeadAndFormData(salesLead, appData, email);
			
		} catch (DuplicateEntitiesException e) {
	
			String id = e.getMessage().split(",")[0];
			String[] error = {id, 
					"MOOC activity updated for  "
					+ email +"<br/>AppsWorkforce is a talent pool of Nigerians learning 6.0.0.1x." + suffix, "color:red"};
			return error;
		}
		
		String[] result = new String[3];
		result[0] = salesLead.getKey().getString();
		result[1] = "AppsWorkforce is a talent pool of Nigerian's learning 6.0.0.1x. ping hello@appsworkforce.com if you have urgent needs, otherwise we'll contact you asap."
				+ "  - aw id: " + 
		salesLead.getLeadID() + " and email: " + email + suffix;
		result[2] = "color:green";
		return result;		

  }
  // [END create]

  // [START read]
  @Override
  public Book readBook(String bookId) {
	  Objectify ofy = ObjectifyService.begin();
	  Key<SalesLead> sk = ObjectifyService.factory().stringToKey(bookId);
	  SalesLead lead = ofy.find(sk);
	  if(lead == null)
		  return null;
	  return leadStatusToBook(lead);
  }
  
  public static Book leadStatusToBook(SalesLead salesLead)
  {
	 DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);

	  Book result =     new Book.Builder().id(salesLead.getLeadID())
		        .ofykey(salesLead.getKey().getString())
		        .state(salesLead.getState())
		        .email(salesLead.getEmail())
		        .name(salesLead.getFullName())
		        .build();
		        
		        
		        result.setMessage(salesLead.getDisplayMessaage());
		        result.setStyle(salesLead.getCurrentState().toString());
		        result.setCrdate(df.format(salesLead.getDateCreated()));
		        result.setMddate(df.format(salesLead.getDateUpdated()));
		        return result;
	  
  }
  
  // [END read]
  public final static HashMap<String, FormValidator[]> pubFieldsMap = new HashMap<String, FormValidator[]>();
 	final static FormValidator[] mandatoryOnly = {FormValidator.MANDATORY};
	final static FormValidator[] mandatoryPlusEmail = {FormValidator.MANDATORY, FormValidator.EMAIL};
  static
  {
      pubFieldsMap.put("name", mandatoryOnly);
      pubFieldsMap.put("message", mandatoryOnly);
      pubFieldsMap.put("email", mandatoryPlusEmail);

  }

  public  static boolean populateFormErrors(Book book)
  {
       
       boolean formErr = false;
       boolean oktosubmit = true;
       //checkName;
       StringBuilder errors = new StringBuilder();
       FormValidator[] checkList= pubFieldsMap.get("name");
       for(FormValidator check : checkList)
       {
    	   formErr = !check.validate("name",book.getName(),  errors);
    	   oktosubmit = oktosubmit && !formErr;
       }
       if(!oktosubmit) //ok works cuz first check ... used cuz mutliple validators ...errros.length should also work
    	   book.setName(getStyleErr(errors.toString()));
       errors = new StringBuilder();
       
       //check email
       checkList = pubFieldsMap.get("email");
       for(FormValidator check : checkList)
       {
    	   formErr = !check.validate("email",book.getEmail(),  errors);
    	   oktosubmit = oktosubmit && !formErr;
       }
       if(formErr)
    	   book.setEmail(getStyleErr(errors.toString()));
       errors = new StringBuilder();
       
       //check message
       checkList = pubFieldsMap.get("message");
       for(FormValidator check : checkList)
       {
    	   formErr = !check.validate("message",book.getMessage(),  errors);
    	   oktosubmit = oktosubmit && !formErr;
       }
       if(formErr)
    	   book.setMessage(getStyleErr(errors.toString()));
       
       
       return oktosubmit;

  }


}
// [END example]
