/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fertiletech.sap.server.pub;


import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.server.tasks.TaskQueueHelper;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


// [START example]
@SuppressWarnings("serial")
public class CustomerBookServlet extends HttpServlet {
	
	private static final Logger log = Logger.getLogger(CustomerBookServlet.class.getName());	
	
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
	    req.setAttribute("destination", "customer");  // The urlPattern to invoke (this Servlet)
	    req.setAttribute("page", "request");           // Tells base.jsp to include form.jsp
	    req.setAttribute("bannerlink", "http://about.appsworkforce.com/learn/certificate");
	      req.setAttribute("bannerimg", "images/certified.jpg");
	      req.setAttribute("bannerdesc", "AppsWorkce certification image");
	    req.getRequestDispatcher("/base.jsp").forward(req, resp);
  }

  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {


    try {

      // [START bookBuilder]
      Book book = new Book.Builder()

  	        .state(req.getParameter("id"))
  	        .name(req.getParameter("program"))
  	        .email(req.getParameter("laptop"))
  	        .build();
      // [END bookBuilder]
      book.setMessage(req.getParameter("6x"));
      book.setOfykey(req.getParameter("role"));
      
      String lead = req.getParameter("lead");
      Key<SalesLead> leadKey = null;
      if(lead != null && lead.length() > 0)
      {
    	  leadKey = ObjectifyService.factory().stringToKey(lead);
    	  String[] params = {"id", "program", "laptop", "6x", "role"};

    	  String app = "";
    	  String aTxt = "";
    	  for(String p : params)
    	  {
    		  aTxt += p + ": " + req.getParameter(p) + "\n";
    		  app += p + ": " + req.getParameter(p) + "<br/>";
    	  }
          String[] comments = {"<b>MRS: Member Request Support</b> " +  app};
          TaskQueueHelper.scheduleCreateComment(comments, lead, leadKey.getParent().getName());
          String[] message = {aTxt, app};
          MessagingDAO.scheduleSendEmail("sobulo@fertiletech.com", message, "MOOC Program reguest by " + leadKey.getParent().getName(), true);
          MessagingDAO.scheduleSendEmail(leadKey.getParent().getName(), message, "MOOC Program reguest by " + leadKey.getParent().getName(), true);
          req.setAttribute("book", book);
          req.setAttribute("page", "thanks");
          req.setAttribute("bannerlink", "http://about.appsworkforce.com/learn/code-of-conduct");
          req.setAttribute("bannerimg", "images/intcode.jpg");
          
          req.setAttribute("bannerdesc", "AppsWorkce member integrity");
          req.getRequestDispatcher("/base.jsp").forward(req, resp);
          return;
      }
    		  lead = doVerify(req, resp);


      if(lead == null)
      {
  
    	  book.setStyle("disabled");
    	     req.setAttribute("backstyle", "btn-danger");;
      }
      else
      {
          req.setAttribute("lead", lead);
    	     req.setAttribute("backstyle", "btn-warning");
      }
      
      /*
       * Need a switch
       *  case back - set attribute page to form
       *  case error - set attribute page to form but add error attributes in error object?
       *  case forward - set attribute to another form that will get to call create
       *  
       *  review page displays all fields
       *  back button to allow editing
       *  submit button to post to create
       *  
       */

      req.setAttribute("book", book);
      req.setAttribute("page", "order");
      req.setAttribute("bannerlink", "http://about.appsworkforce.com/learn/code-of-conduct");
      req.setAttribute("bannerimg", "images/intcode.jpg");
      
      req.setAttribute("bannerdesc", "AppsWorkce member integrity");
      req.getRequestDispatcher("/base.jsp").forward(req, resp);
    } catch (Exception e) {
      throw new ServletException("Error updating book", e);
    }
  }
  private final static String ERR_KEY = "awerror";
  
  private String doVerify(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
	  String id = req.getParameter("id");
		log.warning("ID: " + id);
		Long idNum = null;
		try
		{
			idNum = Long.valueOf(id);
		}
		catch(Exception e)
		{
			log.warning("ID not found");
			req.setAttribute(ERR_KEY, id + " must be numeric only" );
			return null;
		}
		

		Objectify ofy = ObjectifyService.begin();
		List<Key<SalesLead>> result = ofy.query(SalesLead.class).filter("keyCopy =", idNum).listKeys();
		if(result.size() == 0)
			{
			log.warning("ID not found");
			req.setAttribute(ERR_KEY, id + " not found in AW Google Cloud Datastore" );
			return null;
			}
	
			Key<SalesLead> ldKey = result.get(0);
			return ldKey.getString();
	}
}
// [END example]
