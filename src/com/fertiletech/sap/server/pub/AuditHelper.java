package com.fertiletech.sap.server.pub;

import java.util.List;

import com.fertiletech.sap.server.LoginServiceImpl;
import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.entities.moocs.CourseDAO;
import com.fertiletech.sap.server.entities.moocs.CourseTracker;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

public class AuditHelper {
    static
    {
        EntityDAO.registerClassesWithObjectify();
    }
	

	public final static String getAudutTrail(String leadID)
	{
		//Key<SalesLead> key = ObjectifyService.factory().stringToKey(leadID);
		//return LoginServiceImpl.getActivityComments(key, true);
		return "deprecated";
	}
	
	public final static String getTracks(String leadID)
	{
		Key<SalesLead> key = ObjectifyService.factory().stringToKey(leadID);
		Key<ApplicantUniqueIdentifier> mk = key.getParent();

        List<CourseTracker> tracks = CourseDAO.getTrackedCourseByStudent(mk);
        String outputHTML = "<div style='background-color:purple; color:gold'>";
        for(CourseTracker t : tracks)
        {
        	if(t.getKey().getName().equals("Introduction to Computer Science and Programming Using Python"))
        		outputHTML += trackToHTMLSummary(t);
        }
       
        return outputHTML + "</div>";
	}
	
	static String trackToHTMLSummary(CourseTracker t)
	{
		String name = t.getKey().getName();
		int f = t.getFeedCount();
		int c = t.getTrackCount() - 1;
        String outputHTML = name + " - # <i>Weekly progress report:</i> " + c + " - # Monthly 360 <i>review</i>: " + f + "<br/>";
        return outputHTML;
	}
	
	//http://www.sitepoint.com/google-analytics-track-javascript-ajax-events/ for how to use
	public static native void trackEvent(String category, String action, String label) /*-{
		$wnd._gaq.push([ '_trackEvent', category, action, label ]);
	}-*/;

	public static native void trackEvent(String category, String action, String label, int intArg) /*-{
		$wnd._gaq.push([ '_trackEvent', category, action, label, intArg ]);
	}-*/;

	public static native void trackPageview(String url) /*-{
		$wnd._gaq.push([ '_trackPageview', url ]);
	}-*/;
	
	private static native void forceReload() /*-{
    $wnd.location.reload(true);
   }-*/;

}
