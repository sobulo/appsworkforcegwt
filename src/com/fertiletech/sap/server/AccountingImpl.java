package com.fertiletech.sap.server;

import com.fertiletech.sap.client.Accounting;
import com.fertiletech.sap.server.downloads.print.BillingInvoiceGenerator;
import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.entities.payments.InetBill;
import com.fertiletech.sap.server.entities.payments.InetBillDescription;
import com.fertiletech.sap.server.entities.payments.InetBillPayment;
import com.fertiletech.sap.server.entities.payments.InetDAO4Accounts;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.server.tasks.TaskQueueHelper;
import com.fertiletech.sap.shared.*;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import java.text.DateFormat;
import java.util.*;
import java.util.logging.Logger;

public class AccountingImpl extends RemoteServiceServlet implements Accounting {

    private static final Logger log = Logger.getLogger(AccountingImpl.class.getName());

    static
    {
        EntityDAO.registerClassesWithObjectify();
    }

    @Override
    public String createBillTemplate(String name, String year, String term,
                                     Date dueDate, LinkedHashSet<BillDescriptionItem> itemizedBill, String bank, String accountName, String accountNumber, String instructions) throws DuplicateEntitiesException{
        String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        user = (user == null)? "anonymous user entry" : user;
        InetBillDescription billTemplate = InetDAO4Accounts.createBillDescription(dueDate, name, user, itemizedBill, bank, accountName, accountNumber, instructions);
        log.warning("CREATED " + billTemplate.getKey() + " --by user " + user);
        return billTemplate.getKey().getName();
    }


    @Override
    public HashMap<String, String> getBillTemplateList(){
        return InetDAO4Accounts.getAllBillDescriptions(ObjectifyService.begin());
    }

    @Override
    public String createUserBill(String billTemplate, String[] users) throws DuplicateEntitiesException {
        StringBuilder failures = new StringBuilder();
        int count = 0;
        if(users.length == 1)
        {
            Objectify ofy = ObjectifyService.begin();
            Key<InetBillDescription> billDescKey = ofy.getFactory().stringToKey(billTemplate);
            Key<SalesLead> userKey = ofy.getFactory().stringToKey(users[0]);
            InetBillDescription bd = ofy.get(billDescKey);
            InetBill bill = InetDAO4Accounts.createBill(bd, userKey.<ApplicantUniqueIdentifier>getParent());
            return bill.getKey().getString();

        }
        for(String student : users)
        {
            log.warning("attempting bill creation for: " + student);
            try
            {
                TaskQueueHelper.scheduleCreateUserBill(billTemplate, student);
                count++;
            }
            catch(RuntimeException ex)
            {
                log.warning("Error occured: " + ex.getMessage());
                failures.append(student).append(",");
            }
        }
        String userAuditId = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        userAuditId = (userAuditId == null)? "anonymous user entry" : userAuditId;
        StringBuilder result = new StringBuilder("Bill creation scheduled succesfully for ").append(count).append(" member.");
        if(count != users.length)
            result.append(" Please retry for the following members as failures occured: ").append(failures);
        log.warning("BILL CREATION SCHEDULED for: " + users.length + " members --by user " + userAuditId);
        return result.toString();
    }

    private List<TableMessage> getBillListSummary(List<InetBill> billList)
    {
            return getBillListSummary(billList, false, true, null);
    }

    private TableMessageHeader getBillSummaryHeaderForBlotter()
    {
        TableMessageHeader result = new TableMessageHeader(5);
        result.setText(0, "Email", TableMessageHeader.TableMessageContent.TEXT);
        result.setText(1, "Template", TableMessageHeader.TableMessageContent.TEXT);
        result.setText(2, "Bill Name", TableMessageHeader.TableMessageContent.TEXT);
        result.setText(3, "Total Due", TableMessageHeader.TableMessageContent.NUMBER);
        result.setText(4, "Amount Paid", TableMessageHeader.TableMessageContent.NUMBER);
        return result;
    }

    private static TableMessage getBillSummaryMessage(InetBill bill, boolean includeBillBio, boolean includeStudentBio, String[] studFields)
    {
        int numOfTextFields = 1; int textFieldCursor = 0;
        if(includeBillBio) numOfTextFields += 2;
        if(includeStudentBio) numOfTextFields += 3;
        TableMessage billInfo = new TableMessage(numOfTextFields, 2, 0);

        if(includeStudentBio)
        {
            billInfo.setText(textFieldCursor++, studFields[0]);
            billInfo.setText(textFieldCursor++, studFields[1]);
            billInfo.setText(textFieldCursor++, studFields[2]);
        }

        if(includeBillBio)
        {
            billInfo.setText(textFieldCursor++, bill.getBillTemplateKey().getName());
            billInfo.setText(textFieldCursor++, bill.getKey().getName());
        }

        billInfo.setText(textFieldCursor++, bill.isSettled()? "True" : "False");
        billInfo.setNumber(0, bill.getTotalAmount());
        billInfo.setNumber(1, bill.getSettledAmount());
        billInfo.setMessageId(ObjectifyService.factory().keyToString(bill.getKey()));
        return billInfo;
    }

    @Override
    public List<TableMessage> getStudentBills(String studentID){
        Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(studentID);
        List<InetBill> billInfoList = InetDAO4Accounts.getStudentBills(leadKey.<ApplicantUniqueIdentifier>getParent(), ObjectifyService.begin());
        return getBillListSummary(billInfoList);
    }

    @Override
    public String savePayment(String billKey, double amount, Date payDate,
                              String referenceID, String comments) throws MissingEntitiesException, ManualVerificationException
    {
        String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        user = (user == null)? "anonymous user entry" : user;
        Key<InetBill> bk = ObjectifyService.factory().stringToKey(billKey);
        InetBillPayment payment = InetDAO4Accounts.createPayment(bk, amount, referenceID, comments, payDate);
        log.warning("CREATED: " + payment.getKey() + " members --by user " + user);
        return new StringBuilder("Payment ID: ").append(payment.getKey().getId()).append( " Amount: ").
                append(payment.getAmount()).append(" date: ").append(payment.getPaymentDate()).append( " Reference ID: ").append(payment.getReferenceId()).
                append(" Bill: ").append(payment.getKey().getParent().getName()).toString();
    }

    public static List<TableMessage> getBillDescriptionInfo(Key<InetBillDescription> bdKey) throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.begin();
        InetBillDescription billDesc = ofy.find(bdKey);
        if(billDesc == null)
        {
            String msgFmt = "Unable to find bill description: ";
            log.severe(msgFmt + bdKey);
            throw new MissingEntitiesException(msgFmt + bdKey.getName());
        }

        HashSet<BillDescriptionItem> itemizedBill = billDesc.getItemizedBill();
        List<TableMessage> result = new ArrayList<TableMessage>(1 + itemizedBill.size());
        //setup summary info
        TableMessage summary = new TableMessage(7, 1, 1);
        summary.setText(DTOConstants.BTD_NAME_TXT_IDX, billDesc.getName());
        summary.setText(1, "Get Started Now");
        summary.setText(2, "Register for your suggested courses at any time");
        summary.setText(DTOConstants.BTD_BANK_IDX, billDesc.getBank());
        summary.setText(DTOConstants.BTD_ACC_NAME_IDX, billDesc.getAccountName());
        summary.setText(DTOConstants.BTD_ACC_NUM_IDX, billDesc.getAccountNumber());
        summary.setNumber(DTOConstants.BTD_AMT_NUM_IDX, billDesc.getTotalAmount());
        summary.setText(DTOConstants.BTD_ACC_PAY_INSTR_IDX, billDesc.getIntstructions());
        summary.setDate(0, new Date());
        summary.setMessageId(bdKey.getString());
        result.add(summary);

        //setup header for itemized bill
        TableMessageHeader itemizedHeader = new TableMessageHeader(3);
        itemizedHeader.setText(DTOConstants.BTD_ITM_TYPE_TXT_IDX, "Type", TableMessageHeader.TableMessageContent.TEXT);
        itemizedHeader.setText(DTOConstants.BTD_ITM_DESC_TXT_IDX, "Description", TableMessageHeader.TableMessageContent.TEXT);
        itemizedHeader.setText(DTOConstants.BTD_ITM_AMT_HDR_TXT_IDX, "Amount", TableMessageHeader.TableMessageContent.NUMBER);
        result.add(itemizedHeader);

        for(BillDescriptionItem item : itemizedBill)
        {
            TableMessage itemRow = new TableMessage(2, 1, 0);
            itemRow.setText(DTOConstants.BTD_ITM_TYPE_TXT_IDX, item.getName());
            itemRow.setText(DTOConstants.BTD_ITM_DESC_TXT_IDX, item.getComments());
            itemRow.setNumber(DTOConstants.BTD_ITM_AMT_NUM_IDX, item.getAmount());
            result.add(itemRow);
        }
        return result;
    }

    @Override
    public List<TableMessage> getBillTemplateInfo(String billTemplateKeyStr) throws MissingEntitiesException
    {
        Key<InetBillDescription> bdKey = ObjectifyService.begin().getFactory().stringToKey(billTemplateKeyStr);
        return getBillDescriptionInfo(bdKey);
    }

    @Override
    public HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin) {

        Key<ApplicantUniqueIdentifier> studKey = null;
        if(isLogin)
            studKey = ApplicantUniqueIdentifier.getKey(studentID);

        else
            studKey = ObjectifyService.begin().getFactory().stringToKey(studentID).getParent();
        return getStudentBillKeys(studKey);
    }

    private static HashMap<String, String> getStudentBillKeys(Key<ApplicantUniqueIdentifier> studKey)
    {
        return InetDAO4Accounts.getStudentBillKeys( studKey, ObjectifyService.begin());
    }

    private List<TableMessage> getBillPayments(Key<InetBill> billKey, Objectify ofy)
    {
        List<InetBillPayment> payments = InetDAO4Accounts.getPayments(billKey, ofy);
        List<TableMessage> result = new ArrayList<TableMessage>(1 + payments.size());
        //setup header message
        TableMessageHeader header = new TableMessageHeader(6);
        header.setText(0, "Sfx ID", TableMessageHeader.TableMessageContent.TEXT);
        header.setText(1, "Amount", TableMessageHeader.TableMessageContent.NUMBER);
        header.setText(2, "Date", TableMessageHeader.TableMessageContent.DATE);
        header.setText(3, "Ref ID", TableMessageHeader.TableMessageContent.TEXT.TEXT);
        header.setText(4, "Comments", TableMessageHeader.TableMessageContent.TEXT.TEXT);
        header.setText(5, "Status", TableMessageHeader.TableMessageContent.TEXT.TEXT);
        result.add(header);

        for(InetBillPayment payInfo : payments)
        {
            TableMessage m = new TableMessage(4, 1, 1);
            String paySuffix = Long.toString(payInfo.getKey().getId());
            int suffixIdx = paySuffix.length() - 4;
            paySuffix = paySuffix.substring(suffixIdx);
            m.setText(0, paySuffix);
            m.setText(1, payInfo.getReferenceId());
            m.setText(2, payInfo.getComments());
            m.setText(3, payInfo.isVerifiedDisplay());
            m.setNumber(0, payInfo.getAmount());
            m.setDate(0, payInfo.getPaymentDate());
            result.add(m);
        }
        return result;
    }


    @Override
    public List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException
    {
        Objectify ofy = ObjectifyService.begin();
        Key<InetBill> billKey = ofy.getFactory().stringToKey(billKeyStr);
        InetBill studBill = ofy.find(billKey);

        if(studBill == null)
        {
            String msgFmt = "Unable to find student bill: ";
            log.severe(msgFmt + billKey);
            throw new MissingEntitiesException(msgFmt + billKey.getName());
        }
        List<TableMessage> result = new ArrayList<TableMessage>();

        result.add(getBillSummaryMessage(studBill, false, false, null)); //add student bill
        result.addAll(getBillDescriptionInfo(studBill.getBillTemplateKey())); //add bill description
        result.addAll(getBillPayments(billKey, ofy));

        return result;
    }

    @Override
    public List<TableMessage> getBillsForTemplate(String billDescKeyStr) throws MissingEntitiesException{
        Objectify ofy = ObjectifyService.begin();
        Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(billDescKeyStr);
        //TODO change this code to use query cursors otherwise could turn out to be a performance
        //bottle neck
        List<InetBill> bills = InetDAO4Accounts.getBills(bdKey, ofy);

        List<TableMessage> result = new ArrayList<TableMessage>(bills.size() + 1);
        result.add(getBillSummaryHeaderForBlotter());
        result.addAll(getBillListSummary(bills, true, true, ofy));
        return result;
    }

    public List<TableMessage> getBills(Date startDate, Date endDate){
        Objectify ofy = ObjectifyService.begin();
        List<InetBill> bills = InetDAO4Accounts.getBills(startDate, endDate, ofy);

        List<TableMessage> result = new ArrayList<TableMessage>(bills.size() + 1);
        result.add(getBillSummaryHeaderForBlotter());
        result.addAll(getBillListSummary(bills, true, true, ofy));
        return result;
    }

    public static List<TableMessage> getBillListSummary(Collection<InetBill> bills, boolean includeStudentBio,
                                                        boolean includeBillBio, Objectify ofy)
    {
        ArrayList<TableMessage> result = new ArrayList<TableMessage>(bills.size());
        String[] studFields = null;

        for(InetBill b : bills)
        {
            if(includeStudentBio)
            {
                studFields = new String[3];
                studFields[0] = b.getUserKey().getName();
                studFields[1] = b.getBillTemplateKey().getName();
                studFields[2] = b.getKey().getName();
            }
            TableMessage m = getBillSummaryMessage(b, includeBillBio, includeStudentBio, studFields);
            result.add(m);
        }
        return result;
    }

    @Override
    public List<TableMessage> getBillPayments(String billKeyStr)
            throws MissingEntitiesException
    {

        Key<InetBill> billKey = ObjectifyService.factory().stringToKey(billKeyStr);
        return getBillPayments(billKey, ObjectifyService.begin());
    }

    @Override
    public HashMap<String, String> getStudentBillKeysFromTemplate(
            String templateKeyStr) throws MissingEntitiesException {
        List<TableMessage> billInfo = getBillsForTemplate(templateKeyStr);
        if(billInfo.size() == 0) return new HashMap<String, String>();
        billInfo.remove(0); //discard header
        HashMap<String, String> result = new HashMap<String, String>(billInfo.size());
        for(TableMessage billMessage : billInfo)
            result.put(billMessage.getText(4), billMessage.getText(2) + ", " + billMessage.getText(1));
        return result;
    }

    /* (non-Javadoc)
     * @see j9educationgwtgui.client.AccountManager#getInvoiceDownloadLink(java.lang.String, java.lang.String[])
     */
    @Override
    public String getInvoiceDownloadLink(String billTemplateKeyStr,
                                         String[] billKeyStrs) throws MissingEntitiesException{

        return BillingInvoiceGenerator.getBillingInvoicesLink(billTemplateKeyStr, billKeyStrs, getThreadLocalRequest());
    }

    @Override
    public String getBillDownloadLink(String billKeyStr) throws MissingEntitiesException {
        return BillingInvoiceGenerator.getBillingInvoicesLink(billKeyStr, null, getThreadLocalRequest(), true);
    }

    @Override
    public TableMessage verifyPayment(String leadKeyStr, String payKeyStr) throws MissingEntitiesException, ManualVerificationException {
        Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(leadKeyStr); //TODO? might need if we want to auto issue licenses

        Key<InetBillPayment> paymentKey = ObjectifyService.factory().stringToKey(payKeyStr);
        InetBillPayment payment = InetDAO4Accounts.verifyPayment(paymentKey, true, LoginHelper.getLoggedInUser(getThreadLocalRequest()), new Date());
        TableMessage result = InetDAO4Accounts.getPaymentMessage(payment);
        result.setMessageId(payment.getKey().getString());
        return result;
    }

    @Override
    public List<TableMessage> getPayments(Date start, Date end) {
        List<InetBillPayment> payList = InetDAO4Accounts.getPayments(start, end);
        ArrayList<TableMessage> result = new ArrayList<>();
        result.add(DTOConstants.getPayDetailsHeader());
        for(InetBillPayment bp : payList) {
            TableMessage msg = InetDAO4Accounts.getPaymentMessage(bp);
            msg.setMessageId(bp.getKey().getString());
            result.add(msg);
        }
        return result;
    };

    @Override
    public TableMessage issueLicense(String leadStr, String billStr) throws MissingEntitiesException, ManualVerificationException {
        Key<InetBill> billKey = ObjectifyService.factory().stringToKey(billStr);
        Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(leadStr);
        SalesLead lead = InetDAO4Accounts.issueLease(billKey,leadKey, getThreadLocalRequest());
        TableMessage leadMessage = EntityDAO.getSalesLeadInfo(lead);
        leadMessage.setMessageId(lead.getKey().getString());
        return leadMessage;
    }


    @Override
    public Date[] getLicenseInfo(String leadKeyStr) {
        Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(leadKeyStr);
        SalesLead lead = ObjectifyService.begin().find(leadKey);
        if(lead == null)
            return null;
        if(lead.getLicenseBegins() == null)
            return null;
        Date[] result = {lead.getLicenseBegins(), lead.getLicenseEnds()};
        return result;
    }


    @Override
    public HashMap<String, String> getStudentBillKeys()
    {
        String userID = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        if(userID == null)
            throw new RuntimeException("Unable to retrieve user information");

        return getStudentBillKeys(ApplicantUniqueIdentifier.getKey(userID));
    }

}