package com.fertiletech.sap.server.downloads.print;

import com.fertiletech.sap.shared.NPMBFormConstants;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

public class PrintConstants {
	public final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat("d MMM yyyy");
	public final static String SESS_PREFIX = "appsworkforce.";
	public static HashMap<String, String> PDFMappings = new HashMap<String, String>();
	static
	{
		PDFMappings.put(NPMBFormConstants.ID_NUM, "Member Registration ID");
	}
}
