/**
 * 
 */
package com.fertiletech.sap.server.downloads.print;

import com.fertiletech.sap.server.AccountingImpl;
import com.fertiletech.sap.server.downloads.PDFGenerationHelper;
import com.fertiletech.sap.server.entities.InetImageBlob;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.entities.payments.InetBill;
import com.fertiletech.sap.server.entities.payments.InetBillDescription;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.fertiletech.sap.shared.TableMessage;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BillingInvoiceGenerator extends HttpServlet
{
    private final static int RIGHT_MARGIN = 70;
    private final static int TOP_MARGIN = 40;
    private final static int ITEM_COL_WIDTH = 120;
    private final static int NOTES_COL_WIDTH = 300;
    private final static int AMOUNT_COL_WIDTH = 50;
    private final static int PADDING_SIZE = 5;
    private final static int SPACE_BTW_BOXES = 15;
    private final static int HEADER_FONT_SIZE = 24;
    private final static int REGULAR_FONT_SIZE = 12;
    private final static int SMALL_FONT_SIZE = 9;
 
    private final static int BOX_STUDENT_INFO_HEIGHT = 80;
    private final static int BOX_COMMENT_INFO_HEIGHT = 80;
    private final static int BOX_OFFICIAL_INFO_HEIGHT = 30;
    private final static int BOX_DEFAULT_SCHOOL_HEIGHT = 80;

	private final static String COMPANY_INFO_WEB = "www.appsworkforce.com";
	private final static String COMPANY_INFO_ADDR = "Strictly Online! Visit the website";
	private final static String COMPANY_INFO_EMAIL = "hello@appsworkforce.com";
	private final static String COMPANY_INFO_NUM = "0906-176-6041";

	public final static double[] AW_ORANGE = { 255 / 255.0, 165 / 255.0, 0 / 255.0 };
	public final static double[] AW_GREEN = { 0 / 255.0, 165 / 255.0, 0 / 255.0 };
	public final static double[] AW_GREY = { 83 / 255.0, 83 / 255.0, 83 / 255.0 };
	public final static double[] AW_GREY_ALTERNATE = { 50 / 255.0, 50 / 255.0, 50 / 255.0 };
	public final static double[] AW_RED = { 165 / 255.0, 0 / 255.0, 0 / 255.0 };
	public final static double[] AW_BLUE_T = { 219 / 255.0, 229 / 255.0, 240 / 255.0 };
	public final static double[] AW_LIGHT_GREY_T = {  230/ 255.0, 230 / 255.0, 230 / 255.0 };
	private final static int UNDER_LINE_SIZE = 1;


    
    private static final Logger log = Logger.getLogger(BillingInvoiceGenerator.class.getName());
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{

		
    	Objectify ofy = ObjectifyService.begin();
    	
    	String billTemplateKeyStr = req.getParameter(DTOConstants.BILL_DESC_KEY_PARAM);
    	String bkStr = req.getParameter(DTOConstants.BILL__KEY_PARAM);

    	
    	if(billTemplateKeyStr == null && bkStr == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unable to process request. Please ensure this page is accessed via a link" +
    				" recently generated from the invoice panel. If problem persists contact IT</font></b>");
    		return;
    	}
    	HttpSession sess = req.getSession();
		List<TableMessage> billDesc = null;
		List<TableMessage> reportData = null;
		StringBuffer errorBuffer = new StringBuffer("<p>DEBUG INFO: ");
    	if(bkStr == null)
		{
			Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(billTemplateKeyStr);
			billDesc = (List<TableMessage>) sess.getAttribute(getBillDescSessionName(bdKey));
			reportData = (List<TableMessage>) sess.getAttribute(getBillListSessionName(bdKey));
			sess.setAttribute(getBillListSessionName(bdKey), null);
			sess.setAttribute(getBillDescSessionName(bdKey), null);
			String rStr = reportData == null?"null":String.valueOf(reportData.size());
			String bStr = billDesc == null?"null":String.valueOf(billDesc.size());
			errorBuffer.append("Template: Report size: ").append(rStr).append(" Bill Size: ").append(bStr).append("</p>");

		}
		else
		{
			Key<InetBill> bk = ofy.getFactory().stringToKey(bkStr);
			billDesc = (List<TableMessage>) sess.getAttribute(getBillKeyDescSessionName(bk));
			reportData = (List<TableMessage>) sess.getAttribute(getBillKeyListSessionName(bk));
			sess.setAttribute(getBillKeyListSessionName(bk), null);
			sess.setAttribute(getBillKeyDescSessionName(bk), null);
			String rStr = reportData == null?"null":String.valueOf(reportData.size());
			String bStr = billDesc == null?"null":String.valueOf(billDesc.size());
			errorBuffer.append("Bill: Report size: ").append(rStr).append(" Bill Size: ").append(bStr).append("</p>");

		}

    	//String[] schoolInfo = (String[]) sess.getAttribute(getSchoolInfoSessionName(bdKey));

    	if(reportData == null || billDesc == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='brown'>Please attempt download from a laptop/computer." +
					" Alternatively you may use the <a href='" + DTOConstants.getPanelAddress("invoicehistory")
					+ "'>billing history panel</a> to view your invoices</font></b>");
    		res.getOutputStream().println(errorBuffer.append("</p>").toString());
    		return;    		
    	}
    	errorBuffer = null;
		
		String fileName = String.valueOf(new Date().getTime());
		fileName = "aw-" + fileName + "-invoice.pdf";
        res.setContentType("application/octet-stream");
        
        res.setHeader("Content-disposition", "attachment; filename=" + fileName);
        OutputStream out = res.getOutputStream();
        writePDFToOutputStream(ofy, out, billDesc, reportData);
    }
    
    private static void writePDFToOutputStream(Objectify ofy, OutputStream out, List<TableMessage> billDesc, List<TableMessage> reportData)
    {
        PDF pdf;
    	
    	//convert bill desc itemized data to format experted by print report fn
        TableMessage billDescSummary = billDesc.remove(0); 
    	List<List<Cell>> itemizedBillDesc = null;
    	
    	//init pdf file
    	try
    	{
	        pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(REGULAR_FONT_SIZE);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(REGULAR_FONT_SIZE);
	        
	        itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(billDesc, f1, f2, null);
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, DTOConstants.NPMB_FORM_HEADER);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your school hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	printReportCards(itemizedBillDesc, billDescSummary, reportData, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.app"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
    public static byte[] getInvoiceData(String bdKeyStr, String[] studentLoginNames)
    {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	Objectify ofy = ObjectifyService.begin();
    	Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(bdKeyStr);
    
    	//TODO, this part is quite inefficient, share data better possibly, then again is it that inefficient?
    	//so long as we're not loading numerous student objects, the price of loading a inet level/tests probably isn't
    	//much? or is it? ha, if you're reading this and you care? run some performance tests
    	
    	try
    	{
	    	List<TableMessage>[] billingInfo = getBillingInvoicesObjects(ofy, bdKey, studentLoginNames, false);
	    	writePDFToOutputStream(ofy, out, billingInfo[1], billingInfo[0]);
    	}
    	catch(Exception ex)
    	{
    		log.severe(ex.getMessage());
    		return null;
    	}
    	return out.toByteArray();
    }
    
    public static String[] getSummaryMessages(String bdKeyStr, String[] studentLoginNames)
    {
    	Objectify ofy = ObjectifyService.begin();
    	Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(bdKeyStr);
    
    	//TODO, this part is quite inefficient, share data better possibly, then again is it that inefficient?
    	//so long as we're not loading numerous student objects, the price of loading a inet level/tests probably isn't
    	//much? or is it? ha, if you're reading this and you care? run some performance tests
    	
    	String[] result = new String[studentLoginNames.length];
    	try
    	{
	    	List<TableMessage>[] billingInfo = getBillingInvoicesObjects(ofy, bdKey, studentLoginNames, false);
	    	String billName = bdKey.getName().toUpperCase();
	    	DecimalFormat df = new DecimalFormat();
	    	df.setGroupingSize(3);
	    	df.setMaximumFractionDigits(0);
	    	for(int i = 0; i < billingInfo[0].size(); i++)
	    	{	    		
	    		TableMessage studentBill = billingInfo[0].get(i);
	    		StringBuilder summaryMessage = new StringBuilder();
	    		summaryMessage.append(studentBill.getText(2)).append(", ").
	    			append(studentBill.getText(1)).append("'s invoice: ").
	    			append(billName).append(" Total:").append(df.format(studentBill.getNumber(0))).
	    			append(" Paid:").append(df.format(studentBill.getNumber(1))).append(" Balance:").
	    			append(df.format(studentBill.getNumber(0)-studentBill.getNumber(1)));	
	    		result[i] = summaryMessage.toString();
	    	}
    	}
    	catch(Exception ex)
    	{
    		log.severe(ex.getMessage());
    		return null;
    	}
    	return result;
    }
    
    private static void printReportCards(List<List<Cell>> itemizedBill, TableMessage billDescSummary,
    		List<TableMessage> studentBills, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {

    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE);

        
        
        for(int i = 0; i < studentBills.size(); i++)
        {
			TableMessage studentBill = studentBills.get(i);

        	//begin
			Font regularFontBold = new Font(pdf, CoreFont.HELVETICA_BOLD);
			regularFontBold.setSize(SMALL_FONT_SIZE + 2);
			Font smallFont = new Font(pdf, CoreFont.HELVETICA);
			smallFont.setSize(SMALL_FONT_SIZE - 2);

			// setup additional fonts
			Font headerFont = new Font(pdf, CoreFont.HELVETICA_BOLD);
			headerFont.setSize(HEADER_FONT_SIZE);

			Font regularFont = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
			regularFont.setSize(SMALL_FONT_SIZE + 2);

			// new page for student report card
			Page page = new Page(pdf, Letter.PORTRAIT);

			// draw logo
			Image awlogo = new Image(pdf, imageStream, ImageType.JPEG);
			//logo.scaleBy(1.3);
			double maxWidth = 550;
			awlogo.setPosition(RIGHT_MARGIN + (PADDING_SIZE * 2), TOP_MARGIN);
			awlogo.drawOn(page);
			double barHeight = 25;

			// draw mortgate text
			String clr = billDescSummary.getText(0);
			headerFont.setSize(14);
			TextLine passportText = new TextLine(headerFont, clr);
			double baseX = RIGHT_MARGIN + awlogo.getWidth() + (PADDING_SIZE * 4);
			double baseY = TOP_MARGIN + awlogo.getHeight() - barHeight;
			passportText.setColor(RGB.WHITE);
			passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());
			Box passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
			passportBox.setColor(AW_GREY);
			passportBox.setFillShape(true);
			passportBox.drawOn(page);
			passportText.drawOn(page);

			double[] typeColor = null;
			if(studentBill.getText(3).equals("True"))
			{
				clr = "RECEIPT                ";
				typeColor = AW_GREEN;
			}
			else
			{
				clr = "INVOICE                  ";
				typeColor = AW_RED;
			}
			headerFont.setSize(12);
			passportText = new TextLine(headerFont, clr);
			baseX = RIGHT_MARGIN;
			baseY = TOP_MARGIN + awlogo.getHeight() + PADDING_SIZE;
			passportText.setColor(RGB.WHITE);
			passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());
			passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
			passportBox.setColor(typeColor);
			passportBox.setFillShape(true);
			passportBox.drawOn(page);
			passportText.drawOn(page);

			baseY = baseY + barHeight + PADDING_SIZE;
			barHeight = 22;
			headerFont = new Font(pdf, CoreFont.TIMES_BOLD_ITALIC);
			headerFont.setSize(10);
			clr = "Generated for: " + studentBill.getText(0);
			passportText = new TextLine(headerFont, clr);
			baseX = RIGHT_MARGIN + awlogo.getWidth() - awlogo.getWidth() / 4;
			passportText.setColor(RGB.WHITE);
			passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());
			passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
			passportBox.setColor(AW_ORANGE);
			passportBox.setFillShape(true);
			passportBox.drawOn(page);
			passportText.drawOn(page);

			baseY = TOP_MARGIN + PADDING_SIZE + 80 + PADDING_SIZE * 3;
			StringBuilder contactMsg = new StringBuilder();
			String filler = "   ";
			contactMsg.append("ADDRESS: ").append(COMPANY_INFO_ADDR).append(filler).append("TELEPHONE: ")
					.append(COMPANY_INFO_NUM).append(filler).append("WEBSITE: ").append(COMPANY_INFO_WEB).append(filler)
					.append(" EMAIL: ").append(COMPANY_INFO_EMAIL);
			String contact = contactMsg.toString();
			passportText = new TextLine(smallFont, contact);
			passportText.setPosition(RIGHT_MARGIN + (maxWidth - smallFont.stringWidth(contact)) / 2, baseY);
			passportText.setColor(AW_GREY);
			passportText.drawOn(page);
			baseY += PADDING_SIZE;
			Line headerLine = new Line(RIGHT_MARGIN, baseY, RIGHT_MARGIN + maxWidth, baseY);
			headerLine.setWidth(2);
			headerLine.setColor(AW_ORANGE);
			headerLine.drawOn(page);

			final double yOffset = PADDING_SIZE + REGULAR_FONT_SIZE + UNDER_LINE_SIZE;
			baseY = baseY + REGULAR_FONT_SIZE + yOffset + REGULAR_FONT_SIZE;

			double edgeX = RIGHT_MARGIN + maxWidth;
			double edgeY = 730;
			Point cursor = new Point();
			cursor.setPosition(RIGHT_MARGIN, baseY);;
			//end


			final int BOX_SCHOOL_INFO_HEIGHT = (int) Math.max(awlogo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_SCHOOL_HEIGHT);

			int textStart = RIGHT_MARGIN + PADDING_SIZE;

			int tableY = (int) baseY + SPACE_BTW_BOXES;

		    
		    //setup table
			Font tf1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
			tf1.setSize(9);
			Font tf2 = new Font(pdf, CoreFont.HELVETICA);
			tf2.setSize(7);
		    Table table = new Table(tf1, tf2); //seems to ignore fonts, we setup fonts again later for each row
		    table.setData(itemizedBill, Table.DATA_HAS_1_HEADER_ROWS);
		    table.setLineWidth(0.1);
		    table.setPosition(RIGHT_MARGIN, tableY);
		    table.setCellPadding(PADDING_SIZE);
		    table.autoAdjustColumnWidths();
		    table.setColumnWidth(0, ITEM_COL_WIDTH);
		    table.setColumnWidth(1, NOTES_COL_WIDTH);
		    table.setColumnWidth(2, AMOUNT_COL_WIDTH);
		    table.rightAlignNumbers();
		    int numOfPages = table.getNumberOfPages(page);
		    if(numOfPages != 1)
		    	throw new IllegalArgumentException("Billing template data too Large. Contact IT to setup custom invoice report for this template");

		    table.setTextFontInRow(0, new Font(pdf, CoreFont.HELVETICA_BOLD), 9); //bold header
			List<Cell> headerCells = table.getRow(0);
			Border bottomOnly = new Border(false, false, true, false);
			for(Cell header : headerCells) {
				header.setBgColor(AW_BLUE_T);
				header.setBorder(bottomOnly);
			}
			for(int tc = 1; tc < itemizedBill.size(); tc++ )
			{
				if(i % 2 == 0)
					table.setTextColorInRow(tc, AW_GREY);
				else
					table.setTextColorInRow(tc, AW_GREY_ALTERNATE);

				table.setTextFontInRow(tc, new Font(pdf, CoreFont.HELVETICA), 7);
				List<Cell> rowCells = itemizedBill.get(tc);
				for( int rc = 0; rc < rowCells.size(); rc++) {
					Cell c = rowCells.get(rc);
					if (tc == itemizedBill.size() - 1) //last cell
						c.setBorder(bottomOnly);
					else
						c.setNoBorders();
				}
			}
			table.setLineColor(AW_LIGHT_GREY_T);
		    Point tableEnd = table.drawOn(page);

		    //deprecated hack, keeping for numeric use, doesnt print to the pdf
		    /*Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, table.getWidth(), BOX_SCHOOL_INFO_HEIGHT);
		    TextLine text = new TextLine(f3, schoolInfo[DTOConstants.SCHOOL_INFO_NAME_IDX]);
		    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
		    text.setPosition(textStart, textY);

		    TextLine text1 = new TextLine(f4, schoolInfo[DTOConstants.SCHOOL_INFO_ADDR_IDX] );
		    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
		    text1.setPosition(textStart, text1Y);

		    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[DTOConstants.SCHOOL_INFO_NUMS_IDX]);
		    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2.setPosition(textStart, text2Y);

		    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[DTOConstants.SCHOOL_INFO_WEB_IDX] + "   Email: " + schoolInfo[DTOConstants.SCHOOL_INFO_EMAIL_IDX]);
		    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2b.setPosition(textStart, text2bY);*/

		    //setup comments box
		    double commentsBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
		    
		    int textY = (int) Math.ceil(commentsBoxY + REGULAR_FONT_SIZE);
		    printLine("Total Due: " + PrintConstants.NUMBER_FORMAT.format(studentBill.getNumber(0)) + " Naira",
		    		textStart, textY, f2, page);
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Due Date: " + (billDescSummary.getDate(0) == null?"":PrintConstants.DATE_FORMAT.format(billDescSummary.getDate(0))),
		    		textStart, textY, f2, page);
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Amount Paid: " + PrintConstants.NUMBER_FORMAT.format(studentBill.getNumber(1)) + " Naira",
		    		textStart, textY, f2, page);
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    Font fob = studentBill.getText(3).equals("True") ? f2 : f1;
		    printLine("Invoice Balance: " + PrintConstants.NUMBER_FORMAT.format(studentBill.getNumber(0) - studentBill.getNumber(1)) + " Naira" ,
		    		textStart, textY, fob, page);  

            //commentsBox.drawOn(page);

            //setup official box
            double officialBoxY = textY + SPACE_BTW_BOXES + PADDING_SIZE;
            Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , table.getWidth(), BOX_SCHOOL_INFO_HEIGHT);
            textY = (int) (officialBoxY + PADDING_SIZE * 2+ REGULAR_FONT_SIZE);
			printLine("Payment Information: ",
					textStart, textY, f2, page);
			textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
			printLine("Bank: " + billDescSummary.getText(3),
					textStart, textY, f2, page);
			textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;

			printLine("Account Name: " + billDescSummary.getText(4),
					textStart, textY, f2, page);
			textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;

			printLine("Account Number: " + billDescSummary.getText(5),
					textStart, textY, f2, page);
            officialBox.drawOn(page);       
        }
    }
    
    private static void printLine(String line, int xPos, int yPos, Font font, Page page) throws Exception
    {
	    TextLine text = new TextLine(font, line);
	    text.setPosition(xPos, yPos);
	    text.drawOn(page);
    }

    public static List<TableMessage>[] getBillingInvoicesObjects(Objectify ofy, Key<InetBillDescription> bdKey,String[] billingKeyStrs, boolean isBillingKey) throws MissingEntitiesException
    {
        HashSet<Key<InetBill>> billKeys = new HashSet<Key<InetBill>>(billingKeyStrs.length);
        
        for(String bkStr : billingKeyStrs)
        {
			Key<InetBill> sk = null;
            String keyStr = bkStr;
			Key<SalesLead> userKey = null;
			if(!isBillingKey)
				userKey = new Key<SalesLead>(SalesLead.class, bkStr); //TODO sales lead is an id this shou
			else
				userKey = ObjectifyService.factory().stringToKey(bkStr);

			List<Key<InetBill>> klist = ofy.query(InetBill.class).ancestor(userKey.getParent()).filter("billTemplateKey",bdKey).listKeys();
			billKeys.addAll(klist);
        }


        
        Collection<InetBill> billList = ofy.get(billKeys).values();
        
        List<TableMessage> billListMessages = AccountingImpl.getBillListSummary(billList, true, false, ofy);
        List<TableMessage> billDescMessage = AccountingImpl.getBillDescriptionInfo(bdKey);
        List<TableMessage>[] result = new List[2];
        result[0] = billListMessages;
        result[1] = billDescMessage;
        return result;
    }

	public static List<TableMessage>[] getBillInvObject(Objectify ofy, Key<InetBill> billKey) throws MissingEntitiesException
	{
		InetBill bill = ofy.get(billKey);
		ArrayList<InetBill> billList = new ArrayList<InetBill>(1);
		billList.add(bill);
		List<TableMessage> billListMessages = AccountingImpl.getBillListSummary(billList, true, false, ofy);
		List<TableMessage> billDescMessage = AccountingImpl.getBillDescriptionInfo(bill.getBillTemplateKey());
		List<TableMessage>[] result = new List[2];
		result[0] = billListMessages;
		result[1] = billDescMessage;
		return result;
	}
	public static String getBillingInvoicesLink(String billDescKeyStr, String[] billingKeyStrs,
												HttpServletRequest req)
			throws MissingEntitiesException
	{
		return getBillingInvoicesLink(billDescKeyStr, billingKeyStrs, req, false);
	}
    
	public static String getBillingInvoicesLink(String billDescKeyStr, String[] billingKeyStrs,
			HttpServletRequest req, boolean isSingleBill)
		throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
        List<TableMessage>[] billingInfo = null;
        String paramType = null;
        String paramVal = null;
		Key<InetBill> billKey = null;
        if(isSingleBill)
		{
			paramType = DTOConstants.BILL__KEY_PARAM;
			paramVal = billDescKeyStr;
			billKey = ObjectifyService.factory().stringToKey(billDescKeyStr);
			billingInfo = getBillInvObject(ofy, billKey);
			billDescKeyStr = billingInfo[1].get(0).getMessageId();
		}

		Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(billDescKeyStr);
		if(!isSingleBill) {
			paramType = DTOConstants.BILL_DESC_KEY_PARAM;
			paramVal = billDescKeyStr;
			billingInfo = getBillingInvoicesObjects(ofy, bdKey, billingKeyStrs, true);
		}

        List<TableMessage> billListMessages = billingInfo[0];
        List<TableMessage> billDescMessage = billingInfo[1];        
        HttpSession sess = req.getSession();

        if(isSingleBill)
		{
			sess.setAttribute(getBillKeyListSessionName(billKey), null);
			sess.setAttribute(getBillKeyDescSessionName(billKey), null);
			sess.setAttribute(getBillKeyListSessionName(billKey), billListMessages);
			sess.setAttribute(getBillKeyDescSessionName(billKey), billDescMessage);
		}
		else {
			sess.setAttribute(getBillListSessionName(bdKey), null);
			sess.setAttribute(getBillDescSessionName(bdKey), null);
			sess.setAttribute(getBillListSessionName(bdKey), billListMessages);
			sess.setAttribute(getBillDescSessionName(bdKey), billDescMessage);
		}
        sess.setAttribute(getSchoolInfoSessionName(bdKey), DTOConstants.getSchoolInfo());
        
        return "<a href='" + "http://" + req.getHeader("Host") + "/bill-invoice?" + 
        		paramType + "=" + paramVal + "'>Download " +
        		bdKey.getName() + " invoices for " +
				(isSingleBill? 1: billingKeyStrs.length) + " member" +
				(isSingleBill?"":"s") + "</a>";
        
	}
	
	public static String getBillListSessionName(Key<InetBillDescription> bdKey)
	{
		return PrintConstants.SESS_PREFIX + bdKey.getName() + "bioinv";
	}
	
	public static String getBillDescSessionName(Key<InetBillDescription> bdKey)
	{
		return PrintConstants.SESS_PREFIX + bdKey.getName() + "tableinv";
	}

	public static String getBillKeyListSessionName(Key<InetBill> bKey)
	{
		return PrintConstants.SESS_PREFIX + InetBill.getKeyDescription(bKey) + "bioinv";
	}

	public static String getBillKeyDescSessionName(Key<InetBill> bKey)
	{
		return PrintConstants.SESS_PREFIX + InetBill.getKeyDescription(bKey) + "tableinv";
	}
	
	public static String getSchoolInfoSessionName(Key<InetBillDescription> bdKey)
	{
		return PrintConstants.SESS_PREFIX + bdKey.getName() + "schoolinfoinv";
	}


}
