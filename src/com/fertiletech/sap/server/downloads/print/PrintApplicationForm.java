/**
 * 
 */
package com.fertiletech.sap.server.downloads.print;

import com.fertiletech.sap.server.downloads.ListGCSFiles;
import com.fertiletech.sap.server.downloads.PDFGenerationHelper;
import com.fertiletech.sap.server.entities.*;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.shared.*;
import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;
import com.google.appengine.tools.cloudstorage.*;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class PrintApplicationForm extends HttpServlet {

	static {
		EntityDAO.registerClassesWithObjectify();
	}

	private static final Logger log = Logger.getLogger(PrintApplicationForm.class.getName());
	private final static String disclaimerBegin = "Please note that fraudulent activity is punishable under Banks and Other Financial Institution Act (BOFIA 1991) "
			+ "I hereby confirm applying for the above credit facility (";

	private final static String disclaimerEnds = ") and certify that all the information provided by me"
			+ " above and attached thereto is true, correct and complete. I authorize you to make any enquiry you consider necessary and appropriate for the purpose of evaluating this application.";
	private final static String HEADER_MESSAGE = "<center><table border='0' align='left' cellspacing=20 width='80%'><tr><td><img src='http://www.addosser.com/addosser-doc-logo.jpg'/></td>"
			+ "<td valign='middle'><b>ADDOSSER MICROFINANCE LOAN APPLICATION FORM</b></td></tr></table><hr /></center>";

	private final static int RIGHT_MARGIN = 40;
	private final static int TOP_MARGIN = 40;
	private final static int ITEM_COL_WIDTH = 170;
	private final static int NOTES_COL_WIDTH = 225;
	private final static int AMOUNT_COL_WIDTH = 75;
	private final static int PADDING_SIZE = 5;
	private final static int UNDER_LINE_SIZE = 1;
	private final static int SPACE_BTW_BOXES = 12;
	private final static int HEADER_FONT_SIZE = 12;
	private final static int REGULAR_FONT_SIZE = 10;
	private final static int SMALL_FONT_SIZE = 8;
	private final static float REQ_REP_WIDTH = 550;
	public final static double LEFT_EDGE = REQ_REP_WIDTH + RIGHT_MARGIN;

	private final static int BOX_STUDENT_INFO_HEIGHT = 80;
	private final static int BOX_ADDRESS_INFO_HEIGHT = 50;
	private final static int BOX_COMMENT_INFO_HEIGHT = 80;
	private final static int BOX_OFFICIAL_INFO_HEIGHT = 45;
	private final static int BOX_DEFAULT_COMPANY_HEIGHT = 65;

	private final static String SESS_PREFIX = "appsworkforce.";
	private final static String APP_FORM_OP = "appform";
	private final static String OP_TYPE = "type";
	private final static String LOAN_ID_PARAM = "form";
	private final static String COMPANY_INFO_WEB = "www.appsworkforce.com";
	private final static String COMPANY_INFO_ADDR = "Strictly Online! Visit the website";
	private final static String COMPANY_INFO_EMAIL = "hello@appsworkforce.com";
	private final static String COMPANY_INFO_NUM = "0906-176-6041";

	public final static double[] AW_ORANGE = { 255 / 255.0, 165 / 255.0, 0 / 255.0 };
	public final static double[] AW_GREEN = { 0 / 255.0, 165 / 255.0, 0 / 255.0 };
	public final static double[] AW_GREY = { 83 / 255.0, 83 / 255.0, 83 / 255.0 };


	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
			.initialRetryDelayMillis(10).retryMaxAttempts(10).totalRetryPeriodMillis(15000).build());

	/**
	 * web.xml contains entries to ensure only registered developers for the app
	 * can execute this script. Does not go through login logic
	 */

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletOutputStream out = res.getOutputStream();

		String loanId = req.getParameter(LOAN_ID_PARAM);
		String type = req.getParameter(OP_TYPE);

		if (loanId == null || loanId.trim().length() == 0) {
			out.println("<b><font color='red'>No resource requested</font></b></body></html>");
			return;
		}
		Objectify ofy = ObjectifyService.begin();
		HttpSession sess = req.getSession();
		if (APP_FORM_OP.equals(type)) {
			HashMap<String, String> data = (HashMap<String, String>) sess.getAttribute(getAppFormDataName(loanId));

			if (data == null || data.size() == 0) {
				res.setContentType("text/html");
				res.getOutputStream().println("<h1>Downloads</h1><br/><hr>");
				res.getOutputStream()
						.println("<a href='" + getApplicationFormLink(loanId, req)[0] + "'>Application Form</a>");
				return;
			}
			String id = "" + ObjectifyService.factory().stringToKey(loanId).getParent().getId();
			data.put(NPMBFormConstants.ID_NUM, id);
			String fileName = "appsworkforce-" + id + ".pdf";
			res.setContentType("application/octet-stream");
			res.setHeader("Content-disposition", "attachment; filename=" + fileName);
			String folderID = (String) sess.getAttribute(getAppFormID(loanId));
			writeAppFormToStream(ofy, out, data, req, folderID);

		}
	}

	private void writeAppFormToStream(Objectify ofy, OutputStream out, HashMap<String, String> data,
			 HttpServletRequest req, String id) {
		// init pdf file
		try {
			PDF pdf = new PDF(out);

			// logo stream
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, DTOConstants.NPMB_FORM_HEADER);
			InetImageBlob logo = ofy.find(logoKey);
			if (logo == null) {
				throw new IllegalArgumentException("Your company hasn't been properly setup for report"
						+ " generation or setup data has been corrupted. Please contact IT to investigate");
			}
			BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));

			printAppForm(data, bis, pdf, req, id);
			pdf.flush();
		} catch (Exception ex) {
			StackTraceElement[] exLocations = ex.getStackTrace();
			String msg = "";
			for (StackTraceElement exLoc : exLocations) {
				String exMsg = exLoc.toString();
				if (exMsg.startsWith("com.fertile"))
					msg += exMsg + "\n";
			}
			log.severe(msg);
			throw new RuntimeException(ex.fillInStackTrace());
		}
	}

	private void printAppForm(HashMap<String, String> data,
			BufferedInputStream imageStream, PDF pdf, HttpServletRequest req, String id) throws Exception {
		Font regularFontBold = new Font(pdf, CoreFont.HELVETICA_BOLD);
		regularFontBold.setSize(SMALL_FONT_SIZE + 2);
		Font smallFont = new Font(pdf, CoreFont.HELVETICA);
		smallFont.setSize(SMALL_FONT_SIZE - 2);

		// setup additional fonts
		Font headerFont = new Font(pdf, CoreFont.HELVETICA_BOLD);
		headerFont.setSize(HEADER_FONT_SIZE);

		Font regularFont = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
		regularFont.setSize(SMALL_FONT_SIZE + 2);

		// new page for student report card
		Page page = new Page(pdf, Letter.PORTRAIT);

		// draw logo
		Image logo = new Image(pdf, imageStream, ImageType.JPEG);
		logo.scaleBy(1.3);
		double maxWidth = 550;
		logo.setPosition(RIGHT_MARGIN + (PADDING_SIZE * 2), TOP_MARGIN);
		logo.drawOn(page);
		double barHeight = 25;

		// attempt pport photo
		GcsFilename picName = getImageFile(id, req);

		double boxHeight = logo.getHeight() + PADDING_SIZE + barHeight;
		boxHeight = Math.max(boxHeight, 80);
		double imageAfterPos = TOP_MARGIN + PADDING_SIZE + boxHeight + PADDING_SIZE * 3;
		boolean passportFailed = false;
		if (picName != null) {
			try {
				GcsInputChannel readChannel = gcsService.openReadChannel(picName, 0);
				InputStream in = Channels.newInputStream(readChannel);
				//log.warning("Any DATA thus far: " + in.available());
				Image pport = new Image(pdf, in, ImageType.JPEG);
				if (pport.getWidth() > boxHeight) {
					pport.scaleBy(boxHeight / pport.getWidth());
				}
				if (pport.getHeight() > boxHeight) {
					pport.scaleBy(boxHeight / pport.getHeight());
				}
				pport.setPosition(RIGHT_MARGIN + maxWidth - pport.getWidth() - PADDING_SIZE, TOP_MARGIN + PADDING_SIZE);
				pport.drawOn(page);
			} catch (Exception e) {
				passportFailed = true;
				log.severe("Found gcs file but failed to draw on pdf document. "
						+ "Contact user to reupload file. Error message: " + e.getMessage());
				log.severe("USER TO CONTACT IS: " + LoginHelper.getLoggedInUser(req));
			}
		} else
			passportFailed = true;

		if (passportFailed) {
			// draw passport box
			Box passportBox = new Box((RIGHT_MARGIN + maxWidth) - (boxHeight + PADDING_SIZE), TOP_MARGIN + PADDING_SIZE,
					boxHeight, boxHeight);
			passportBox.drawOn(page);
			String recent = "please upload recent";
			TextLine passportText = new TextLine(smallFont, recent);
			double baseY = TOP_MARGIN + (boxHeight / 2 - PADDING_SIZE);
			double baseX = (RIGHT_MARGIN + maxWidth) - boxHeight / 2 - smallFont.stringWidth(recent) / 2
					- SMALL_FONT_SIZE;
			passportText.setPosition(baseX, baseY);
			passportText.drawOn(page);
			String clr = "color passport photograph";
			passportText = new TextLine(smallFont, clr);
			baseX = (RIGHT_MARGIN + maxWidth) - boxHeight / 2 - smallFont.stringWidth(clr) / 2 - PADDING_SIZE*2;
			passportText.setPosition(baseX, baseY + PADDING_SIZE + SMALL_FONT_SIZE);
			passportText.drawOn(page);
		}

		// draw mortgate text
		String clr = "NEW MEMBER REGISTRATION FORM";
		TextLine passportText = new TextLine(headerFont, clr);
		double baseX = RIGHT_MARGIN + logo.getWidth() + (PADDING_SIZE * 4);
		double baseY = TOP_MARGIN + logo.getHeight() - barHeight;
		passportText.setColor(RGB.WHITE);
		passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());
		Box passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
		passportBox.setColor(AW_ORANGE);
		passportBox.setFillShape(true);
		passportBox.drawOn(page);
		passportText.drawOn(page);

		clr = "AppsWorkforce.com - A learning initiative by FTBS";
		passportText = new TextLine(headerFont, clr);
		baseX = RIGHT_MARGIN;
		baseY = TOP_MARGIN + logo.getHeight() + PADDING_SIZE;
		passportText.setColor(RGB.WHITE);
		passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());
		passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
		passportBox.setColor(AW_GREY);
		passportBox.setFillShape(true);
		passportBox.drawOn(page);
		passportText.drawOn(page);

		baseY = baseY + barHeight + PADDING_SIZE;
		barHeight = 22;
		headerFont = new Font(pdf, CoreFont.TIMES_BOLD_ITALIC);
		headerFont.setSize(10);
		clr = "LEARN, EARN and HAVE FUN";
		passportText = new TextLine(headerFont, clr);
		baseX = RIGHT_MARGIN + logo.getWidth() - logo.getWidth() / 4;
		passportText.setColor(RGB.WHITE);
		passportText.setPosition(baseX + PADDING_SIZE, baseY + PADDING_SIZE + headerFont.getSize());
		passportBox = new Box(baseX, baseY, headerFont.stringWidth(clr) + (PADDING_SIZE * 2), barHeight);
		passportBox.setColor(AW_GREEN);
		passportBox.setFillShape(true);
		passportBox.drawOn(page);
		passportText.drawOn(page);

		baseY = imageAfterPos;
		StringBuilder contactMsg = new StringBuilder();
		String filler = "   ";
		contactMsg.append("ADDRESS: ").append(COMPANY_INFO_ADDR).append(filler).append("TELEPHONE: ")
				.append(COMPANY_INFO_NUM).append(filler).append("WEBSITE: ").append(COMPANY_INFO_WEB).append(filler)
				.append(" EMAIL: ").append(COMPANY_INFO_EMAIL);
		String contact = contactMsg.toString();
		passportText = new TextLine(smallFont, contact);
		passportText.setPosition(RIGHT_MARGIN + (maxWidth - smallFont.stringWidth(contact)) / 2, baseY);
		passportText.setColor(AW_GREY);
		passportText.drawOn(page);
		baseY += PADDING_SIZE;
		Line headerLine = new Line(RIGHT_MARGIN, baseY, RIGHT_MARGIN + maxWidth, baseY);
		headerLine.setWidth(2);
		headerLine.setColor(AW_ORANGE);
		headerLine.drawOn(page);

		final double yOffset = PADDING_SIZE + REGULAR_FONT_SIZE + UNDER_LINE_SIZE;
		baseY = baseY + REGULAR_FONT_SIZE + yOffset + REGULAR_FONT_SIZE;

		double edgeX = RIGHT_MARGIN + maxWidth;
		double edgeY = 730;
		Point cursor = new Point();
		cursor.setPosition(RIGHT_MARGIN, baseY);;

		printLine("REGISTRATION DETAILS", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFontBold, page);
		cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset + PADDING_SIZE);
		Point startPoint = new Point();
		copyPoint(cursor, startPoint);
		printFormKey(data, "Name", edgeX, edgeY, 1, false, cursor, regularFont, page, pdf);
		String surname = data.get(NPMBFormConstants.SURNAME);
		String firstname = data.get(NPMBFormConstants.FIRST_NAME);
		String middlename = data.get(NPMBFormConstants.MIDDLE_NAME);
		surname = surname == null ? "" : surname;
		firstname = firstname == null ? "" : firstname;
		middlename = middlename == null ? "" : middlename;
		double base1 = startPoint.getX() + regularFont.stringWidth("Name: ");
		double base2 = base1 + maxWidth / 3;
		double base3 = base2 + maxWidth / 3.5;

		double top1 = base1 + maxWidth / 6 - regularFont.stringWidth(surname) / 2;
		double top2 = base2 + maxWidth / 6 - regularFont.stringWidth(middlename) / 2;
		double top3 = base3 + maxWidth / 6 - regularFont.stringWidth(firstname) / 2;
		double under1 = base1 + maxWidth / 6 - smallFont.stringWidth("Surname") / 2;
		double under2 = base2 + maxWidth / 6 - smallFont.stringWidth("First Name") / 2;
		double under3 = base3 + maxWidth / 6 - smallFont.stringWidth("Middle Name") / 2;
		final double underOffset = 4.0 + smallFont.getSize();
		cursor.setPosition(top1, startPoint.getY());
		printLine(surname, false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
		cursor.setPosition(under1, startPoint.getY() + underOffset);
		printLine("Surname", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
		cursor.setPosition(top2, startPoint.getY());
		printLine(firstname, false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
		cursor.setPosition(under2, startPoint.getY() + underOffset);
		printLine("First Name", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
		cursor.setPosition(top3, startPoint.getY());
		printLine(middlename, false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
		cursor.setPosition(under3, startPoint.getY() + underOffset);
		printLine("Middle Name", false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
		cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);

		// underlineRegion(cursor.getX(), cursor.getY(), maxWidth/2, page);
		printFormKey(data, NPMBFormConstants.EMAIL, RIGHT_MARGIN + maxWidth / 2, edgeY, 1, false, cursor, regularFont,
				page, pdf);
		//cursor.setPosition(cursor.getX() + maxWidth / 2 + PADDING_SIZE, cursor.getY());
		printFormKey(data, NPMBFormConstants.TEL_NO, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);

		printFormKey(data, NPMBFormConstants.DATE_OF_BIRTH, RIGHT_MARGIN + maxWidth / 3, edgeY, 1, false, cursor,
				regularFont, page, pdf);
		printFormKey(data, NPMBFormConstants.STATE_ADDRESS, RIGHT_MARGIN + maxWidth / 1.6, edgeY, 1, false, cursor,
				regularFont, page, pdf);
		printFormKey(data, NPMBFormConstants.LGA_ADDRESS, LEFT_EDGE, edgeY, 1, true, cursor,
				regularFont, page, pdf);
		//cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
		printFormKey(data, NPMBFormConstants.STREET_ADDRESS, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);


		cursor.setPosition(RIGHT_MARGIN, cursor.getY() + (yOffset * 2));

		printLine("MEMBERSHI DECLARATION", false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor,
				regularFontBold, page);
		cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
		printLine(NPMBFormConstants.PDF_DECLARATION, false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor,
				regularFont, page);
		cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset);
		String[] nextPage = printLine(NPMBFormConstants.PDF_DECLARATION_2, false, RIGHT_MARGIN, LEFT_EDGE, edgeY, cursor,
				regularFont, page);

		cursor.setPosition(RIGHT_MARGIN, cursor.getY() + yOffset * 2);

		final String SIG = "Signature";
		data.put(SIG, "I digitally signed by choosing to submit");
		if(!data.containsKey(NPMBFormConstants.DATE_OF_REGISTRATION))
			data.put(NPMBFormConstants.DATE_OF_REGISTRATION, "Feb 14th 2018");

		printFormKey(data, SIG, RIGHT_MARGIN + maxWidth / 2, edgeY, 1, false, cursor, regularFont, page, pdf);
		printFormKey(data, NPMBFormConstants.DATE_OF_REGISTRATION, LEFT_EDGE, edgeY, 1, true, cursor, regularFont, page, pdf);
	}

	private static ArrayList<TableMessage> getSupplementaryTable(ArrayList<HashMap<String, String>> supplementaryData,
			String[][] configTable, int defaultRows) throws NumberFormatException, Exception {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(supplementaryData.size() + 1 + defaultRows);
		// build header
		TableMessageHeader header = new TableMessageHeader(configTable[0].length);
		int textFields, doubleFields, dateFields;
		textFields = doubleFields = dateFields = 0;
		boolean addFooter = false;

		for (int h = 0; h < configTable[0].length; h++) {
			TableMessageContent type = TableMessageContent.valueOf(configTable[3][h]);
			header.setText(h, configTable[0][h], type);
			switch (type) {
			case DATE:
				dateFields++;
				break;
			case NUMBER:
				addFooter = true;
				doubleFields++;
				break;
			case TEXT:
				textFields++;
				break;
			default:
				break;
			}
		}
		result.add(header);

		TableMessage footer = new TableMessage(textFields, doubleFields, dateFields);
		footer.setText(0, "Total");
		boolean invalidFooter = false;
		for (int i = 0; i < supplementaryData.size(); i++) {
			TableMessage m = new TableMessage(textFields, doubleFields, dateFields);
			HashMap<String, String> rowData = supplementaryData.get(i);
			textFields = doubleFields = dateFields = 0;
			for (int j = 0; j < header.getNumberOfHeaders(); j++) {
				TableMessageContent type = header.getHeaderType(j);
				String val = rowData.get(header.getText(j));
				switch (type) {
				case DATE:
					dateFields++;
					throw new RuntimeException("Dates not currently supported");
				case NUMBER:
					Double dVal = null;
					if (val != null) {
						try {
							dVal = Double.valueOf(val);
							if (!invalidFooter) {
								if (footer.getNumber(doubleFields) == null)
									footer.setNumber(doubleFields, dVal);
								else
									footer.setNumber(doubleFields, dVal + footer.getNumber(doubleFields));
							}
						} catch (NumberFormatException ex) {
							invalidFooter = true;
						}
					} else
						invalidFooter = true;
					m.setNumber(doubleFields++, dVal);
					break;
				case TEXT:
					m.setText(textFields++, val);
					break;
				default:
					break;

				}
			}
			result.add(m);
		}

		for (int i = supplementaryData.size(); i < defaultRows; i++)
			result.add(new TableMessage(textFields, doubleFields, dateFields));

		if (addFooter)
			result.add(footer);

		return result;
	}

	private static Page drawSupplementaryTable(ArrayList<HashMap<String, String>>[] supplementaryData, int idx,
			Point cursor, double maxWidth, int defaultRows, Font f, Font h, Page page, PDF pdf) throws Exception {
		Table table = new Table(h, f);
		;

		// form table settings
		List<TableMessage> tableData = getSupplementaryTable(supplementaryData[idx], NPMBFormConstants.ALL_CONFIG[idx],
				defaultRows);
		List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(tableData, h, f, null);

		table.setData(itemizedBillDesc, Table.DATA_HAS_0_HEADER_ROWS);
		table.setLineWidth(0.2);
		table.setPosition(cursor.getX(), cursor.getY());
		table.setCellPadding(PADDING_SIZE / 2.0);
		table.rightAlignNumbers();
		for (int i = 0; i < NPMBFormConstants.ALL_CONFIG[idx][1].length; i++) {
			double width = maxWidth * Double.valueOf(NPMBFormConstants.ALL_CONFIG[idx][1][i]) / 100.0;
			table.setColumnWidth(i, width);
		}
		log.warning("WIDTH 2: " + table.getWidth());

		// print payments table for this deposit
		while (true) {
			Point tableEnd = table.drawOn(page);
			if (!table.hasMoreData()) {
				cursor.setY(tableEnd.getY() + f.getSize() + h.getSize());
				break;
			} else {
				page = new Page(pdf, Letter.PORTRAIT);
				cursor.setPosition(RIGHT_MARGIN, TOP_MARGIN);
				table.setPosition(RIGHT_MARGIN, TOP_MARGIN);
			}
		}
		return page;
	}

	public static void drawCheckBox(Point cursor, String text, boolean checked, double gap, Font f, Page page)
			throws Exception {
		Point startPoint = new Point();
		printLine(text, cursor.getX(), cursor.getY(), f, page, null);
		cursor.setX(cursor.getX() + gap + f.stringWidth(text));
		copyPoint(cursor, startPoint);
		startPoint.setRadius(f.getSize() / 2);
		startPoint.setY(startPoint.getY() - f.getSize() / 2 + 2);
		startPoint.setShape(Point.BOX);
		startPoint.drawOn(page);
		if (checked) {
			startPoint.setRadius(f.getSize() / 2 - 2);
			startPoint.setShape(Point.X_MARK);
			startPoint.drawOn(page);
		}
		cursor.setX(cursor.getX() + f.getSize() + PADDING_SIZE);
	}

	public static void drawBigCheckBox(Point cursor, String text, boolean checked, Font f, Page page) throws Exception {
		Point startPoint = new Point();
		copyPoint(cursor, startPoint);
		startPoint.setRadius(f.getSize() / 3 * 2);
		startPoint.setY(startPoint.getY() - f.getSize() / 2 + 2);
		startPoint.setShape(Point.BOX);
		startPoint.drawOn(page);
		if (checked) {
			startPoint.setRadius((f.getSize() / 3 * 2) - 2);
			startPoint.setShape(Point.X_MARK);
			startPoint.drawOn(page);
		}
		cursor.setX(cursor.getX() + f.getSize() + PADDING_SIZE);
		printLine(text, cursor.getX(), cursor.getY(), f, page, null);
	}

	public static Page printFormKey(HashMap<String, String> data, String key, double edgeX, double edgeY,
			double lineCount, boolean nextLine, Point cursor, Font regularFont, Page page, PDF pdf) throws Exception {
		return printFormKey(data, key, false, edgeX, edgeY, lineCount, nextLine, cursor, regularFont, page, pdf);
	}

	public static Page printFormKey(HashMap<String, String> data, String key, boolean format, double edgeX,
			double edgeY, double lineCount, boolean nextLine, Point cursor, Font regularFont, Page page, PDF pdf)
					throws Exception {
		String result = printLine(getLabel(key), false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page)[0];

		if (result != null) {
			page = new Page(pdf, Letter.PORTRAIT);
			result = printLine(getLabel(key), false, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page)[0];
		}

		cursor.setPosition(cursor.getX() + PADDING_SIZE, cursor.getY());
		String val = data.get(key);
		if (format && val != null && val.trim().length() > 0)
			val = EntityConstants.NUMBER_FORMAT.format(Double.valueOf(val));

		String resultArr[] = printLine(val, true, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
		int countIdx = Integer.valueOf(resultArr[1]);
		if (resultArr[0] != null) {
			page = new Page(pdf, Letter.PORTRAIT);
			resultArr = printLine(resultArr[0], true, RIGHT_MARGIN, edgeX, edgeY, cursor, regularFont, page);
			countIdx += Integer.valueOf(resultArr[1]);
		}

		while (countIdx < lineCount) {
			cursor.setPosition(RIGHT_MARGIN, cursor.getY() + regularFont.getSize() + PADDING_SIZE + UNDER_LINE_SIZE);
			underlineRegion(RIGHT_MARGIN, cursor.getY(), LEFT_EDGE - RIGHT_MARGIN, page);
			countIdx++;
		}
		if (nextLine)
			cursor.setPosition(RIGHT_MARGIN, cursor.getY() + regularFont.getSize() + PADDING_SIZE);
		return page;
	}

	public static String[] getApplicationFormLink(String loanID, HttpServletRequest req) {
		Key<MortgageApplicationFormData> formKey = ObjectifyService.factory().stringToKey(loanID);
		Key<SalesLead> leadKey = formKey.getParent();
		Objectify ofy = ObjectifyService.begin();
		Map<Key<Object>, Object> objMap = ofy.get(formKey, leadKey);
		MortgageApplicationFormData loanObj = (MortgageApplicationFormData) objMap.get(formKey);
		HashMap<String, String> formData = loanObj.getFormData();
		SalesLead lead = (SalesLead) objMap.get(leadKey);
		HttpSession sess = req.getSession();
		sess.setAttribute(getAppFormDataName(loanID), formData);
		sess.setAttribute(getAppFormID(loanID), leadKey.getString());
		String checklistUrl = "http://about.appsworkforce.com/learn/requirements";

		String[] result = { "http://" + req.getHeader("Host") + "/print/form?" + LOAN_ID_PARAM + "=" + loanID + "&"
				+ OP_TYPE + "=" + APP_FORM_OP, checklistUrl };
		return result;
	}

	public static String getLabel(String key) {
		if (key == null || key.length() == 0)
			return "";

		if (PrintConstants.PDFMappings.containsKey(key))
			key = PrintConstants.PDFMappings.get(key);

		return key + ": ";
	}

	private static void copyPoint(Point source, Point target) {
		target.setPosition(source.getX(), source.getY());
	}

	private static void printLine(String line, int xPos, int yPos, Font font, Page page) throws Exception {
		printLine(line, xPos, yPos, font, page, null);
	}

	private final static int NUM_OF_EXIS_LOAN_ROWS = 3;

	private static void printLine(String line, double xPos, double yPos, Font font, Page page, double[] color)
			throws Exception {
		if (line == null)
			line = "{BLANK}";

		TextLine text = new TextLine(font, line);
		text.setPosition(xPos, yPos);

		if (color != null)
			text.setColor(color);

		text.drawOn(page);
	}

	public static String getAppFormID(String id) {
		return SESS_PREFIX + id + "-formID";
	}

	public static String getAppFormDataName(String id) {
		return SESS_PREFIX + id + "-formdata";
	}

	public static String getAppSuppDataName(String id) {
		return SESS_PREFIX + id + "-suppdata";
	}

	public static String[] printLine(String text, boolean underline, double beginX, double edgeX, double edgeY, Point p,
			Font f, Page page) throws Exception {
		if (text == null)
			text = "";
		String[] tokens = text.split(" ");
		int returnIdx = -1;
		StringBuilder outputBuffer = new StringBuilder();
		Point s = new Point(p.getX(), p.getY());
		int lineCount = 0;
		for (int i = 0; i < tokens.length; i++) {
			double x = p.getX() + f.stringWidth(tokens[i]) + f.stringWidth(" ");
			double y = p.getY();
			if (x > (LEFT_EDGE - PADDING_SIZE - f.getSize())) {
				double edge = edgeX - s.getX();
				edgeX = LEFT_EDGE; // we've exceeded bounds user wanted, so
									// stretch to end of page
				TextLine line = new TextLine(f, outputBuffer.toString());
				outputBuffer.setLength(0); // empty the buffer
				line.setPosition(s.getX(), s.getY());
				line.drawOn(page);
				if (underline)
					underlineRegion(s.getX(), s.getY(), edgeX - s.getX(), page);
				x = beginX;
				y = y + PADDING_SIZE + f.getSize(); // go to next line
				s.setPosition(x, y);
				lineCount++;
			}

			if (y > edgeY) {
				y = TOP_MARGIN;
				returnIdx = i;
				p.setPosition(x, y);
				break;
			}

			p.setPosition(x, y);
			if (outputBuffer.length() > 0)
				outputBuffer.append(" ");
			outputBuffer.append(tokens[i]);

		}

		if (returnIdx < 0) {
			TextLine line = new TextLine(f, outputBuffer.toString());
			line.setPosition(s.getX(), s.getY());
			line.drawOn(page);
			if (underline) {
				underlineRegion(s.getX(), s.getY(), edgeX - s.getX(), page);
				p.setPosition(edgeX + PADDING_SIZE, p.getY());
			}
			lineCount++;
			String[] result = { null, String.valueOf(lineCount) };
			return result;
		}
		outputBuffer.setLength(0);
		outputBuffer.append(tokens[returnIdx]);
		for (int i = returnIdx + 1; i < tokens.length; i++)
			outputBuffer.append(" ").append(tokens[i]);
		String[] result = { outputBuffer.toString(), String.valueOf(lineCount) };
		return result;
	}

	public static void underlineRegion(double x, double y, double length, Page page) throws Exception {
		Line l = new Line(x, y + UNDER_LINE_SIZE, x + length, y + UNDER_LINE_SIZE);
		l.setPattern("[1 2] 4");

		l.drawOn(page);
	}

	public GcsFilename getImageFile(String loanID, HttpServletRequest req) throws MissingEntitiesException {
		if (loanID == null) {
			String email = LoginHelper.getLoggedInUser(req);
			loanID = ApplicantUniqueIdentifier.getKey(email).getString();
		} else {
			log.warning(new Key(loanID).toString());
		}
		try {
			ListOptions listOpt = new ListOptions.Builder().setPrefix(loanID).build();
			ListResult items = gcsService.list(ListGCSFiles.GCS_BUCKET_NAME, listOpt);
			int count = 0;
			while (items.hasNext()) {
				count++;
				ListItem record = items.next();
				String fileName = record.getName().toLowerCase();
				log.warning("file (lower cased): " + fileName);
				//if (record.isDirectory())
				//	continue;
				//return only jpeg files as that's what pdf image drawer supports
				if(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))
				{
					GcsFilename result = new GcsFilename(ListGCSFiles.GCS_BUCKET_NAME, record.getName());
					log.warning("returning " + result.toString());
					return result;
				}
			}
			log.warning("Found Records: " + count);
		} catch (IOException e) {
			throw new MissingEntitiesException("Unable to connect to cloud storage");
		}
		log.warning("null returned");
		return null;
	}

}