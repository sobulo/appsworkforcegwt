package com.fertiletech.sap.server;

import com.fertiletech.sap.client.LoginService;
import com.fertiletech.sap.server.entities.*;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.server.pub.LoanStatusServlet;
import com.fertiletech.sap.server.tasks.TaskQueueHelper;
import com.fertiletech.sap.shared.*;
import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.fertiletech.sap.shared.oauth.OurException;
import com.fertiletech.sap.shared.oauth.SocialUser;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.tools.cloudstorage.*;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.logging.Logger;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {

	private static final Logger log =
	        Logger.getLogger(LoginServiceImpl.class.getName());	
	
	  /**
	   * This is where backoff parameters are configured. Here it is aggressively retrying with
	   * backoff, up to 10 times but taking no more that 15 seconds total to do so.
	   */
	  private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
	      .initialRetryDelayMillis(10)
	      .retryMaxAttempts(10)
	      .totalRetryPeriodMillis(15000)
	      .build());

	  /**Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB */
	  private static final int BUFFER_SIZE = 2 * 1024 * 1024;
	
	BlobstoreService blobstore = BlobstoreServiceFactory.getBlobstoreService();

	public static String GCS_BUCKET_NAME = ""; 
	static
	{
		EntityDAO.registerClassesWithObjectify();
		try
		{
			GCS_BUCKET_NAME = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
		}
		catch(Exception Ex)
		{
			log.severe("Failed to auto initialize GCS Bucket Name, defaulting to string literal");
			log.severe(Ex.toString());
			GCS_BUCKET_NAME = "appsworkforce.appspot.com";
		}
	}
	private final static String LAST_OPS_LOAN_ID_REQUEST = "com.fertiletech.npmb.opslastloanidviewed";
	
	public static void setLastOpsLoan(String loandIDViewed, HttpServletRequest req)
	{
		LoginRoles role = LoginHelper.getRole(req);
		HttpSession sess = req.getSession();
		if(!role.equals(LoginRoles.ROLE_PUBLIC))
		{
			//log.warning("Last OPS ID Set to: " + LAST_OPS_LOAN_ID_REQUEST);
			sess.setAttribute(LAST_OPS_LOAN_ID_REQUEST, loandIDViewed);
		}
	}
	
    private static void saveEmailToSession(String email, HttpSession session) throws OurException
    {
        if (session == null)
        {
            throw new OurException(ClientUtils.SESSION_EXPIRE_MESSAGE, true);
        }
        /*
        ServersideSession sss = getServersideSession();
        if (sss == null)
        {
            sss = new ServersideSession();
            sss.setState(state);
            session.setAttribute(SESSION,sss);
        }
        else
        {
            sss.setState(state);
        }
        */
        session.setAttribute(LoginHelper.SESSION_EMAIL,email);
    }

	public static void stampNPMBInfo(SocialUser user, HttpSession sess) throws OurException
	{
		String email = user.getEmail();
		saveEmailToSession(email, sess);
    	user.role = LoginHelper.getRole(email);    	
		
    	if(user.role.equals(LoginRoles.ROLE_PUBLIC))
    	{
    		List<SalesLead> leads = EntityDAO.getConsumerLoanLeadViaEmail(ObjectifyService.begin(), email);
        	String[] IDs = new String[leads.size()];
			String[] memberID = new String[leads.size()];
        	String[] licences = new String[leads.size()];
        	String[] types = new String[leads.size()];
        	String[] expiration = new String[leads.size()];
        	int count = 0;
        	
        	for(SalesLead ld : leads)
        	{
        		IDs[count] = ld.getKey().getString();
        		licences[count] = ld.getLicenseInfo();
        		if(licences[count] != null) {
					expiration[count] = String.valueOf(ld.getLicenseEnds().getTime());
					types[count] = ld.getLicenseType();
					memberID[count] = String.valueOf(ld.getLeadID());
				}
				else
				{
					DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
					DateFormat dfOnly = DateFormat.getDateInstance(DateFormat.MEDIUM);
					final String EXP_PREFIX = "<br/><hr/><b style='color:red'>";
					final String EXP_SUFFIX = " Contact <a href='mailto:apply@appsworkforce.com'>apply@appsworkforce.com</a> "
							+ " for enquiries</b>";
					String expiredLicense = EXP_PREFIX + "You do not have a memberhip License." + EXP_SUFFIX;
							
					Date licenseDate = ld.getLicenseEnds();
					if(licenseDate != null)
					{
						expiredLicense = EXP_PREFIX + ld.getLicenseType() + " issued for a period of " + dfOnly.format(ld.getLicenseBegins()) + 
								" to " + dfOnly.format(ld.getLicenseEnds()) + " has expired" + EXP_SUFFIX;
					}
					memberID[count] = "<b>Summary</b><hr/><br/>" +
							"State: " + ld.getCurrentState().getDisplayString() + "<br/>Name: " + ld.getFullName() + "<br/>"
							+ "Message: " + ld.getDisplayMessaage() + "<br/>Phone: " + ld.getPhoneNumber() + "<br/>" +
					         "Email: " + ld.getEmail() + "<br/>" +  "Last AW Update: " + df.format(ld.getDateUpdated()) + "<br/>" +
					         "AW introduction: " + df.format(ld.getDateCreated()) + "<br/>" + expiredLicense;
	
				}
        		count++;
        	}
        	user.loanIDs = IDs;
        	user.licenseType = types;
        	user.expiration = expiration;
			user.license = licences;
			user.memberIDs = memberID;
    	}
    	else
    	{
			String[] loanIDs = {(String) sess.getAttribute(LAST_OPS_LOAN_ID_REQUEST)};
        	if(!(sess.getAttribute(LAST_OPS_LOAN_ID_REQUEST) == null))
    		{
    			if(loanIDs[0] != null)
    			{
    				user.loanIDs = loanIDs;
    				return;
    			}
    		}	    		
    	}
	}
	

	@Override
	public List<TableMessage> getLoanApplications(String email) 
	{
		Objectify ofy = ObjectifyService.begin();
		List<Key<SalesLead>> leadKeys = EntityDAO.getConsumerLoanLeadKeysViaEmail(ofy, email);
		Map<Key<SalesLead>, SalesLead> leads = ofy.get(leadKeys);
		return EntityDAO.getConsumerLeadsAsTable(leads.values());
	}

	@Override
	public List<TableMessage> getLoanApplications() {
		return getLoanApplications(LoginHelper.getLoggedInUser(getThreadLocalRequest()));
	}

	@Override
	public String loadActivityComments(String loanStr) {
		Key<SalesLead> key = ObjectifyService.factory().stringToKey(loanStr);
		String commentsDisplay = getActivityComments(key, false);
		LoginServiceImpl.setLastOpsLoan(loanStr, getThreadLocalRequest());
		return commentsDisplay;
	}
	
	@Override
	public String getActivityComments() throws LoginExpiredException
	{
		String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(email == null)
			throw new LoginExpiredException("Looks like your session is stale, refresh browser login again");
		
		List<Key<SalesLead>> leads = EntityDAO.getConsumerLoanLeadKeysViaEmail(ObjectifyService.begin(), email);
		String prefix = "";
		if (leads.size() != 1)
		{
			prefix = "<div style='color:red'>Expected 1 registration but found: " + leads.size();
			prefix += " Please cut and paste this entire message and mail to "
					+ "info@fertiletech.com to resolve the issue: Message content: </div><br/>";
		}
		StringBuilder sb = new StringBuilder(prefix);
		for(Key<SalesLead> key : leads)
			sb.append(getActivityComments(key, true));
		return sb.toString();
			
	}
	
	public static String getActivityComments(Key<SalesLead> key, boolean filterMail)
	{
		final String FILTER = "https://groups.google.com/a/";
		List<MortgageComment> commentList = EntityDAO.getLoanComments(ObjectifyService.begin(), key);
		FormHTMLDisplayBuilder result = new FormHTMLDisplayBuilder();
		result.formBegins();
		result.headerBegins().appendTextBody("<b>History</b>").headerEnds();
		boolean commentFound = false;
		
		for(MortgageComment c : commentList)
		{
			String comment = c.getComment().getValue();
			if(filterMail)
			{
				if(comment.contains(FILTER))
					continue;
			}
			commentFound = true;
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			result.sectionBegins();
	
			result.lineBegins().appendTextBody(comment).lineEnds();
			result.lineBeginsRight().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEndsRight();			
			result.blankLine();
			result.sectionEnds();
		}
		if(!commentFound)
			result.lineBegins().appendTextBody("No comments found").lineEnds();
		result.formEnds();
		
		return result.toString();
	}
	
	@Override
	public TableMessage getMessageContent(String id) throws MissingEntitiesException
	{
		Key<MessageText> mk = MessageText.getKey(id);
		MessageText msg = ObjectifyService.begin().find(mk);
		if(msg == null)
			throw new MissingEntitiesException("No object found named: " + id);
		TableMessage result = new TableMessage(3,0,1);
		result.setText(DTOConstants.MSG_TXT_CONTENT_IDX, msg.getMessageText().getValue());
		result.setText(DTOConstants.MSG_TXT_CONTENT_HTML_IDX, msg.getMessageHTML().getValue());
		result.setText(DTOConstants.MSG_TEXT_USER_IDX, msg.getModifiedBy());
		result.setDate(DTOConstants.MSG_DATE_MODE_IDX, msg.getDateModified());
		return result;
		
		
	}
	
	@Override
	public void setMessageContent(String id, String contentTxt, String contentHTML)
	{
		Key<MessageText> mk = MessageText.getKey(id);
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			
			MessageText msg = ofy.find(mk);
			if(msg == null)
				msg = new MessageText(id, contentTxt, contentHTML, user);
			else
			{
				msg.setMessageText(contentTxt);
				msg.setMessageHTML(contentHTML);
				msg.setModifiedBy(user);
			}
			ofy.put(msg);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
			
		}
	}
	
	@Override
	public String[] getAllMessageNames()
	{
		List<Key<MessageText>> messageKeys = ObjectifyService.begin().query(MessageText.class).listKeys();
		String[] result = new String[messageKeys.size()];
		for(int i = 0; i < messageKeys.size(); i++)
			result[i] = messageKeys.get(i).getName();
		return result;
	}

	@Override
	public List<TableMessage> getRecentActivityComments() {
		Objectify ofy = ObjectifyService.begin();
		List<MortgageComment> comments = ofy.query(MortgageComment.class).order("-dateUpdated").limit(100).list();
		List<TableMessage> result = new ArrayList<TableMessage>();
		for(MortgageComment c : comments)
		{
			TableMessage m = new TableMessage(3, 0, 1);
			m.setMessageId(c.getKey().getString());
			Text descriptiveComment = c.getComment();
			m.setText(0, c.getUser());
			m.setText(1, descriptiveComment == null?"Oops, no comment found, contact IT":descriptiveComment.getValue());
			m.setText(2, c.getLoanKey().getString());
			//log.warning(descriptiveComment.toString());
			m.setDate(0, c.getDateUpdated());
			result.add(m);
		}
		return result;
	}

	@Override
	public void saveActivityComment(String loanKeyStr, String text, boolean showPublic) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		EntityDAO.createComment(new Text(text), showPublic, leadKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		LoginServiceImpl.setLastOpsLoan(leadKey.getString(), getThreadLocalRequest());
	}

	@Override
	public List<TableMessage> getAttachments(String loanID) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanID);
		return EntityDAO.getAttachmentsDTO(leadKey);
	}
	
	@Override
	public List<TableMessage> getMapAttachments(Date startDate, Date endDate) {
		return EntityDAO.getMapAttachments(startDate, endDate);
	}	

	@Override
	public String saveAttachments(String loanId, List<TableMessage> attachments) {
		Key<SalesLead> leadKey = ObjectifyService.factory().stringToKey(loanId);
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		user = (user == null)? "anonymous user entry" : user; 
		List<MortgageAttachment> attch = EntityDAO.createAttachment(leadKey, attachments, user);
		return "Attached " + attch.size() + " new files for M.A.P. ID: " + leadKey.getId();
	}
	
	@Override
	public String saveMapAttachment(String loanId, Double lat, Double lng) {
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(user == null) user = "anoymous user entry";
		TableMessage clientAttachment = DrivePickerUtils.getUserMapAttachment("User Edits", lat, lng, user);
		HttpServletRequest request = getThreadLocalRequest();
		Double altLat, altLng;
		altLng = altLat = null;
		String title="Untitled";
		try
		{
			String[] latlng = request.getHeader("X-AppEngine-CityLatLong").split(",");
			title = request.getHeader("X-AppEngine-Country") + "/" + request.getHeader("X-AppEngine-City"); 
			altLat = Double.valueOf(latlng[0]);
			altLng = Double.valueOf(latlng[1]);
		}
		catch(Exception e)
		{
			log.severe("Ignoring error while detecting lat/lng server side. Error: " + e.getMessage());
		}
		TableMessage serverAttachment = DrivePickerUtils.getUserMapAttachment("User Edits - " + title, altLat, altLng, user);
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(3);
		TableMessageHeader h = new TableMessageHeader();
		h.setCaption("User Edits - Location Detection");
		result.add(h);
		if(clientAttachment != null)
			result.add(clientAttachment);
		if(serverAttachment != null)
			result.add(serverAttachment);
		if(result.size() == 1)
			return null;
		return saveAttachments(loanId, result);
	}	

	@Override
	public String deleteAttachment(String attachID) {
		Key<MortgageAttachment> attachmentKey = ObjectifyService.factory().stringToKey(attachID);
		EntityDAO.removeAttachment(attachmentKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		return "Removed 1 attachment for M.A.P. ID: " + attachmentKey.getParent().getId();
	}

	@Override
	public List<TableMessage> getUploads(String loanID) throws MissingEntitiesException{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "File Name", TableMessageContent.TEXT);
		h.setText(1, "Date", TableMessageContent.DATE);
		h.setMessageId(loanID);
		result.add(h);
		try {
			ListOptions listOpt = new ListOptions.Builder().setPrefix(loanID).build();
			ListResult items = gcsService.list(GCS_BUCKET_NAME, listOpt);
			while(items.hasNext())
			{
				ListItem record = items.next();
				if(record.isDirectory()) continue;
				TableMessage m = new TableMessage(2, 0, 1);
				m.setText(0, record.getName().replace(loanID, ""));
				m.setText(1, record.getName());
				m.setDate(0, record.getLastModified());
				m.setMessageId(EntityConstants.GCS_HOST + GCS_BUCKET_NAME + "/" + record.getName());
				//log.warning("Message id is: " + m.getMessageId());
				result.add(m);
			}
			//log.warning("Found Records: " + (result.size() - 1));
		} catch (IOException e) {
			throw new MissingEntitiesException("Unable to connect to cloud storage");
		}
		return result;
	}

	@Override
	public String getUploadUrl() {
		UploadOptions opts = UploadOptions.Builder.withGoogleStorageBucketName(GCS_BUCKET_NAME);
		String url = blobstore.createUploadUrl("/mydownload/success", opts);
		return url;
	}

	@Override
	public String deleteUpload(String uploadID) throws MissingEntitiesException {
		GcsFilename fileName = new GcsFilename(GCS_BUCKET_NAME, uploadID);
		boolean deleted = false;
		try {
			log.warning("Deleting: " + fileName);
			deleted = gcsService.delete(fileName);
			String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
			String[] loanParts = uploadID.split("/");
			if(loanParts.length > 1)
			{
				log.warning("deleted file");
				String comments[] = {"<div style='background-color:#c40069; color: white; text-align:center;'>Upload Deleted</div>", "Filename: " + loanParts[1]};
				TaskQueueHelper.scheduleCreateComment(comments, loanParts[0], user ==null?"anoymous":user);
			}
			else
				log.warning("Nothing deleted");
		} catch (IOException e) {
			throw new MissingEntitiesException("Failed to delete " + fileName.toString() 
					+ " from cloud storage. Error was: " + e.getMessage());
		}
		
		return deleted?  ("Successfully deleted " + fileName.getObjectName()) : ("Failed to delete: " + fileName.getObjectName()); 
	}
	
	@Override
	public List<TableMessage> getApplicationParameter(String ID) {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		ApplicationParameters appObj = ObjectifyService.begin().get(pk);
		HashMap<String, String> params = appObj.getParams();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "Unique Identifier", TableMessageContent.TEXT);
		h.setText(1, "Description", TableMessageContent.TEXT);
		h.setMessageId(appObj.getKey().getString());
		h.setCaption(appObj.getAuditTrail());
		result.add(h);
		for(String s : params.keySet())
		{
			TableMessage m = new TableMessage(2, 0, 0);
			m.setText(0, s);
			m.setText(1, params.get(s));
			m.setMessageId(s);
			result.add(m);
		}
		Collections.sort(result);
		return result;
	}

	@Override
	public String saveApplicationParameter(String ID, HashMap<String, String> val) throws MissingEntitiesException {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		String updateUser = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		return updateApplicationParameters(updateUser, pk, val);
	}
	public static String updateApplicationParameters(String userID,
			Key<ApplicationParameters> key, HashMap<String, String> parameters)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ApplicationParameters paramsObject = ofy.find(key);
			if (paramsObject == null)
				throw new MissingEntitiesException("Parameter does not exist, unable to update" + key);
 
			HashMap<String, String> oldParameters = paramsObject.getParams();
			paramsObject.setParams(parameters);
			StringBuffer diffs = new StringBuffer("<ol>");
			for(String o : oldParameters.keySet())
			{
				if(parameters.containsKey(o))
				{
					if(!parameters.get(o).equalsIgnoreCase(oldParameters.get(o)))
						diffs.append("<li>Description for " + o + " changed from: <font color='red'>[" + oldParameters.get(o) + "]</font> to: <font color='purple'>[" + parameters.get(o) + "]</font><li>");
				}
				else
					diffs.append("<li>Entry for " + o +" <b style='color:red'>REMOVED</b>. Its description at time of removal was: [" + oldParameters.get(o) + "]");
					
			}
			for(String o : parameters.keySet())
			{
				if(!oldParameters.containsKey(o))
					diffs.append("<li>Entry for " + o + " <b style='color:purple'>ADDED</b>. Its description is: [" + parameters.get(o) + "]</li>");
			}
			paramsObject.addToAuditTrail(diffs.toString(), userID);
			ofy.put(paramsObject);
			ofy.getTxn().commit();
			return paramsObject.getAuditTrail(); 

		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	@Override
	public String getImageUploadHistory(String imageID) {
		return ObjectifyService.begin().get(InetImageBlob.getKey(imageID)).getAuditTrail();
	}

	public HashMap<String, String> getAllStudentIDs(){
		Objectify ofy = ObjectifyService.begin();
		List<SalesLead> students = ofy.query(SalesLead.class).list(); //TODO this will need to be reworked when volumes increase
		HashMap<String, String> result = new HashMap<String, String>(
				students.size());
		for (SalesLead stud : students)
		{
			String key;
			String val = stud.getFullName() + DTOConstants.USER_SUGGEST_DELIMITER
					+ stud.getKey().getParent().getName();

			key = ofy.getFactory().keyToString(stud.getKey());
			result.put(key, val);
		}
		return result;
	}

	@Override
	public String sendMessage(String to, String subject, String htmlMessage) {
		String[] messageFormat = {"This is a text only client, contact hello@fertiletech.com for the actual meessage" +
				" or try viewing in another email client that supports html", htmlMessage};
		sendMessageHack(to, messageFormat, subject);
		return "Email successfuly scheduled for [" + subject + "] to be sent to [" + to +"]";
	}

	public boolean sendMessageHack(String toAddress, String[] messageContent, String subject) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);
		try
		{
			//create multipart message
			Multipart multipart = new MimeMultipart();

			//add plain text
			MimeBodyPart textPart = new MimeBodyPart();
			textPart.setContent(messageContent[0], "text/plain");
			multipart.addBodyPart(textPart);

			//add html for clients that support it
			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(messageContent[1], "text/html");
			multipart.addBodyPart(htmlPart);

			//setup the message
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("hello@appsworkforce.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
			message.setSubject(subject);
			message.setContent(multipart);

			//send message
			Transport.send(message);
		}
		catch(Exception ex)
		{
			throw new RuntimeException(ex.fillInStackTrace());
		}
		return true;
	}

	@Override
	public String echo(String message) {

		return message;
	}

	@Override
	public int scheduleBulkMail(Set<TableMessage> mailingList, String category) {
		int numScheduled = 0;
		for(TableMessage member : mailingList)
		{
			String name = member.getText(DTOConstants.FULL_NAME_IDX);
			String email = member.getText(DTOConstants.EMAIL_IDX);
			double id = member.getNumber(DTOConstants.LEAD_ID_IDX);
			NumberFormat fmt = NumberFormat.getIntegerInstance();
			fmt.setGroupingUsed(false);
			String idName = fmt.format(id);
			if(email.contains("noreply"))
				continue;
			String subjectPrefix = " -  " + idName;
		
			String text = "You can verify your app with email: " + email + " and id pair "+ idName;
			String html = LoanStatusServlet.getVerifyURL(email, idName);

	
			TaskQueueHelper.scheduleMessageViaGrid("segun.sobulo@appsworkforce.com", "hello@appsworkforce.com", email, category + subjectPrefix, category, category, html, text);
			numScheduled += 1;
		}
		return numScheduled;

	}
}
