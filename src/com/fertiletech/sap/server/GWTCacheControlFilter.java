package com.fertiletech.sap.server;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * {@link Filter} to add cache control headers for GWT generated files to ensure
 * that the correct files get cached.
 * From Blog: https://seewah.blogspot.com.ng/2009/02/gwt-tips-2-nocachejs-getting-cached-in.html
 *
 * @author See Wah Cheng
 * @created 24 Feb 2009
 */
public class GWTCacheControlFilter implements Filter {

    public void destroy() {
    }

    public void init(FilterConfig config) throws ServletException {
    }


    //helps append trailing / to urls
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException,
            ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String requestURI = httpRequest.getRequestURI();

        //go to https


        HttpServletResponse httpResponse = (HttpServletResponse) response;
        if (requestURI.startsWith("http://"))
            httpResponse.sendRedirect(requestURI.replace("http://", "https://"));

        else if (requestURI.startsWith("www"))
            httpResponse.sendRedirect("https://" + requestURI);
        else
            filterChain.doFilter(request, response);
    }
}

