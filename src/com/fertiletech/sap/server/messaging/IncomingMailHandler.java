/**
 * 
 */
package com.fertiletech.sap.server.messaging;

import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.scripts.SendEmailServlet;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IncomingMailHandler extends HttpServlet {
	static {
		EntityDAO.registerClassesWithObjectify();
	}
	private static final Logger log = Logger.getLogger(IncomingMailHandler.class.getName());

	String textPart = "";
	String htmlPart="";
	int textCounts = 0;
	int htmlCounts = 0;
	int recCount = 0;
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);
		try {
			MimeMessage message = new MimeMessage(session, req.getInputStream());
			Address[] addresses = message.getFrom();
			Address[] recipients = message.getAllRecipients();
			Address[] replyTo = message.getReplyTo();
			int numOfLines = message.getLineCount();
			Address sender = message.getSender();
			String subject = message.getSubject();
			MimeMultipart parts = (MimeMultipart) message.getContent();
			for(int i = 0; i < parts.getCount(); i++)
			{
				BodyPart pt = parts.getBodyPart(i);
				processmessageParts(pt);
				
			}
			
			
	
			log.warning("##########INCOMING MAIL#########");
			log.warning("subject: " + subject + " line count: " + numOfLines);
			log.warning("sender " + sender);
			log.warning("text: [" + textPart + "] html: [" + htmlPart + "]");
			log.warning("##########INCOMING MAIL#########");
			log.warning("ntmlCount: " + htmlCounts + " textCounts: " + textCounts+ " recCounts: " + recCount);
		
///			
			
			String [] deeq = {"Search news on AW reference site for fall 2020 registration link. Lab School enrollment is independent of edX schedule, but you must register on edX to get a MITx certificate.",
					"Search news on <a href='about.appsworkforce.com'>AW reference site</a> for fall 2020 registration link. Lab School enrollment is independent of edX schedule, but you must register on edX to get a MITx certificate."};
			String labs = "labs@appsworkforce.com";
			for (Address addy : addresses)
				log.warning("FROM: " + addy.toString());
			for (Address rp : recipients)
			{
				if(addresses.length > 0)
				{
					String adrs = addresses[0].toString();
					String tag = "From " + adrs + " to " + rp.toString();
					String txt = ""; String html = "";
					if(rp.toString().toLowerCase().equals("sodeeq@appsworkforce.appspotmail.com") ||
							rp.toString().toLowerCase().equals("principal@appsworkforce.appspotmail.com")	)
						txt = deeq[0] + " -- " + tag; html = deeq[1] +"<br/><br/>" + tag;
				
	
					String[] messageContent = {txt + " --- Sent to: " + adrs, html + "<br/><br/> --- Email to principal from: <a href='mailto:" + adrs + "'>" + adrs +"</a>"};
					MessagingDAO.scheduleSendEmail(adrs, messageContent, subject, true);
					MessagingDAO.scheduleSendEmail(labs, messageContent, subject, true);
				}
				log.warning("RECIPIENT: " + rp.toString());
			}
			for (Address rt : replyTo)
				log.warning("REPLY: " + rt.toString());
		} catch (MessagingException e) {
			log.severe("Error occurred on inbound email message: " + e.getMessage());
		}
	}

	/**
	 * @param p
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void processmessageParts(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String) p.getContent();
			if(p.isMimeType("text/html"))
			{
				htmlPart = s;
				htmlCounts += 1;
				
			}
			else
			{
				textPart =s;
				textCounts +=1;
			}
		}
		else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				recCount +=1;
				if(recCount > 5)
					break;
				 processmessageParts(mp.getBodyPart(i));
			}
		}
	}

}
