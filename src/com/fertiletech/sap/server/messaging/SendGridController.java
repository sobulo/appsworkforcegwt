/**
 * 
 */
package com.fertiletech.sap.server.messaging;


import java.util.logging.Logger;

import com.fertiletech.sap.server.OAuthLoginSrvImpl;
import com.googlecode.objectify.Key;
import com.sendgrid.SendGrid;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SendGridController extends MessagingController {
	// TODO see if we can use api key instead of appengine version
	public final static String API_KEY = "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";
	String bccAddress;
	public final static String FB_DUMMY_MAIL_PREFIX = "@noreplyfacebookid.com";

	SendGridController() {
		super();
	}
	   private static final Logger log =
	            Logger.getLogger(SendGridController.class.getName());

	SendGridController(String key, String fromAddress, String bccAddress, int characterLimit, int dailyMessageLimit,
			int totalMessageLimit) {
		super(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
		this.bccAddress = bccAddress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<SendGridController> getKey() {
		return (Key<SendGridController>) super.getKey();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * j9educationentities.messaging.MessagingController#sendMessage(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public boolean sendMessage(String toAddress, String[] messageContent, String subject) {

		boolean succeeded = false;
			
		try 
		{
			//log.warning("SEND GRID ACTIVATED for --- " + toAddress);
			if(toAddress.endsWith(FB_DUMMY_MAIL_PREFIX))
				return false;
			//password "learnearnfun1" //for portal -- used to be where api key was
			SendgridDeprecated sendgrid = new SendgridDeprecated("AppsWorkforce", "learnearnfun1");
			sendgrid.setTo(toAddress).setFrom(getFromAddress());
			if(bccAddress != null) {
				sendgrid.setBcc(bccAddress);
				sendgrid.setReplyTo(bccAddress);
			}
			String yahooDisclaimer = "";
			String yahooHTMLDisclaimer = "";
			if(SendgridDeprecated.isYahoo(toAddress))
			{
				yahooDisclaimer ="!!!YAHOO!!!! login is no longer supported. Google has a better audit trail."
						+ " Please send an email to hello@appsworce.com  and provide a gmail/google account "
						+ "to transfer your profile to. Apologies for the inconvenience";
				yahooHTMLDisclaimer = "<p style='color: red'>" + yahooDisclaimer + "</p>";
			}
			String messageFooter = " This message was sent to: " + toAddress + " " + yahooDisclaimer;
			String messageFooterHTML = "<br/><br/><p font-size=0.5em;>This message was sent to: " + toAddress + "</p>" + yahooHTMLDisclaimer;
			sendgrid.setSubject(subject).setText(messageContent[0] + messageFooter).setHtml(messageContent[1] + messageFooterHTML);
			sendgrid.addCategory("Application Portal");
			sendgrid.send();
			succeeded = sendgrid.getServerResponse().toLowerCase().contains("success");
			//log.warning("----SEND GRID FINISHED WITH: " + sendgrid.getServerResponse());
		} catch (Exception ex) {
			throw new RuntimeException(ex.fillInStackTrace());
		}
		return succeeded;
	}

}
