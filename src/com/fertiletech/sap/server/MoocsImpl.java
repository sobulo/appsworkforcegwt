package com.fertiletech.sap.server;

import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.moocs.Course;
import com.fertiletech.sap.server.entities.moocs.CourseDAO;
import com.fertiletech.sap.server.entities.moocs.CourseTracker;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class MoocsImpl extends RemoteServiceServlet implements Moocs {
    static
    {
        EntityDAO.registerClassesWithObjectify();
    }

    private static final Logger log =
            Logger.getLogger(MoocsImpl.class.getName());

    @Override
    public List<TableMessage> getAllCourses() {

        List<Course> courseList = ObjectifyService.begin().query(Course.class).list();
        List<TableMessage> result = new ArrayList<>(courseList.size() + 1);
        result.add(CourseDAO.getCourseHeader());
        for(Course c : courseList)
            result.add(CourseDAO.getCourseDTO(c));
        return result;
    }

    @Override
    public List<TableMessage> getTracks(Date start, Date end) {
        List<CourseTracker> tracks = CourseDAO.getRecentTrackedCourses(start, end);
        List<TableMessage> result = new ArrayList<>(tracks.size() + 1);
        result.add(DTOConstants.getTrackHeader(true));
        for(CourseTracker t : tracks)
            result.add(CourseDAO.getCourseTrackerDTO(t, true));
        return result;
    }

    @Override
    public List<TableMessage> getAllMemberTracks() throws LoginExpiredException{
        String member = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        if(member == null)
        	throw new LoginExpiredException("Your login session expired due to inactivity.");
        return getAllMemberTracks(member);
    }

    @Override
    public List<TableMessage>[] getMemberTrackFeed(String courseName) throws MissingEntitiesException, LoginExpiredException {
        String member = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        if(member == null)
        	throw new LoginExpiredException("Your login session expired due to inactivity.");
        return getMemberTrackFeed(member, courseName);
    }

    @Override
    public String startTrack(String courseName) throws DuplicateEntitiesException, LoginExpiredException {
        String member = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        if(member == null)
        	throw new LoginExpiredException("Your login session expired due to inactivity.");
        Key<ApplicantUniqueIdentifier> mk = ApplicantUniqueIdentifier.getKey(member);
        Key<Course> ck = Course.getKey(courseName);
        CourseTracker track = CourseDAO.startTrack(mk, ck);
        return track.getKey().getName();
    }

    @Override
    public String addTrack(String coursename, String comments) throws MissingEntitiesException, LoginExpiredException {
        Objectify ofy = ObjectifyService.beginTransaction();
        int newCount = 0;
        try {
            String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
            if(user == null)
            	throw new LoginExpiredException("Your login session expired due to inactivity.");
            Key<CourseTracker> ck = CourseTracker.getKey(user, coursename);
            CourseTracker tracker = ofy.find(ck);
            if(tracker == null)
                throw new MissingEntitiesException(user + " is currently not tracking " + coursename);
            tracker.addStatusUpdate(comments, user);
            ofy.put(tracker);
            ofy.getTxn().commit();
        }
        finally {
            if(ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }
        return coursename;
    }

    @Override
    public String addFeedBack(String userName, String courseName, String comments) throws MissingEntitiesException {
        Objectify ofy = ObjectifyService.beginTransaction();
        int feedCount = 0;
        int trackCount = 0;
        try {
            Key<CourseTracker> ck = CourseTracker.getKey(userName, courseName);
            CourseTracker tracker = ofy.find(ck);
            if(tracker == null)
                throw new MissingEntitiesException(userName + " is currently not tracking " + courseName);
             String audit = LoginHelper.getLoggedInUser(getThreadLocalRequest());
            feedCount = tracker.addFeedback(comments, audit);
            trackCount = tracker.getTrackCount();
            ofy.put(tracker);
            ofy.getTxn().commit();

        }
        finally {
            if(ofy.getTxn().isActive())
                ofy.getTxn().rollback();
        }

        //email notifcation
        String dateStr = DateFormat.getDateInstance(DateFormat.SHORT).format(new Date());
        
        log.warning("%%%Sending email to " + userName + " for " + courseName + "feed back");

        String mailBody = getSalutation() + ",<br/><p>The purpose of this message is to inform you that feedback was recently provided for " + courseName + " being tracked by " + userName + ". You now have " +
              feedCount +  " reviews on file and you've also provided " + trackCount + " progress report(s)</p>" +
                "<p>Login to <a href='https://www.appsworkforce.com/lab/#continue'> AW site to view feedback under monitor tracks page</a></p>";
        String[] messages = {"Recent feedback available. Login to appsworkforce.com and check monitor tracks page to view", mailBody};
        
        MessagingDAO.scheduleSendEmail(userName, messages, "AW Feedback for " + courseName + " on " + dateStr, true);
        
        //String[] debugMessages = {"Really Text", "Yeah Right HTML<br/><br/>" + mailBody};
        //MessagingDAO.scheduleSendEmail(userName, debugMessages, "AW DEBUG Feedback for " + courseName + " on " + dateStr, false);
        log.warning( "feed back mail scheduking complete%%%");

        StringBuffer resultBuffer = new StringBuffer(comments).append("<hr/><p>Feed addition ok, you now have " +
                feedCount + "feeds on file");

        //result
        String result = resultBuffer.toString();
        return result;
    }

    private String getSalutation()
    {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if(timeOfDay >= 0 && timeOfDay < 12){
            return "Good Morning";
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            return "Good Afternoon";
        }else if(timeOfDay >= 16 && timeOfDay < 21){
            return "Good Evening";
        }else if(timeOfDay >= 21 && timeOfDay < 24){
            return "Good Day";
        }
        return "Hello";

    }

    @Override
    public List<TableMessage> getAllMemberTracks(String member) {
        Key<ApplicantUniqueIdentifier> mk = ApplicantUniqueIdentifier.getKey(member);
        //log.warning("Fetching tracks");
        List<CourseTracker> tracks = CourseDAO.getTrackedCourseByStudent(mk);
        //log.warning("Found " + tracks.size() + "tracks");
        ArrayList<TableMessage> result = new ArrayList<>(tracks.size() + 1);
        result.add(DTOConstants.getTrackHeader(false));
        //log.warning("Added track header ");
        for(CourseTracker t: tracks)
            result.add(CourseDAO.getCourseTrackerDTO(t, false));
        //log.warning("Returning result of size " + tracks.size() + " tracks and one header");
        return result;
    }

    @Override
    public List<TableMessage>[] getMemberTrackFeed(String member, String courseName) throws MissingEntitiesException {
        Objectify ofy = ObjectifyService.begin();
        Key<CourseTracker> ck = CourseTracker.getKey(member, courseName);
        List<TableMessage>[] result = getCourseTrackFeed(ck);
        if(result == null)
            throw new MissingEntitiesException(member + " is currently not tracking " + courseName);
        return result;
    }

    @Override
    public List<TableMessage>[] getCourseTrackFeed(String courseId) throws MissingEntitiesException {
        Key<CourseTracker> ctk = ObjectifyService.factory().stringToKey(courseId);
        List<TableMessage>[] result = getCourseTrackFeed(ctk);
        if(result == null)
            throw new MissingEntitiesException("Unable to retrieve requested track");
        return result;
    }

    private List<TableMessage>[] getCourseTrackFeed(Key<CourseTracker> ck) {
        Objectify ofy = ObjectifyService.begin();
        CourseTracker tracker = ofy.find(ck);
        if(tracker == null)
           return null;
        List<TableMessage> tracks = CourseDAO.getFeedTrackDTO(tracker);
        List<TableMessage> feeds = CourseDAO.getFeedBackDTO(tracker);
        List<TableMessage>[] result = new ArrayList[3];
        result[DTOConstants.COURSE_TRACK_LIST_IDX] = tracks;
        result[DTOConstants.COURSE_FEED_LIST_IDX] = feeds;
        result[DTOConstants.COURSE_APPENDIX_LIST_IDX] = getTrackAppendix(tracker);
        return result;
    }

    private List<TableMessage> getTrackAppendix(CourseTracker c)
    {
        List<TableMessage> result = new ArrayList<>(1); //only 1 element, use list for consistency with caller
        TableMessage appendix = new TableMessage(2,0,0);

        appendix.setText(DTOConstants.FEED_APPENDIX_COURSE_TXT_IDX, c.getKey().getName());
        appendix.setText(DTOConstants.FEED_APPENDIX_EMAIL_TXT_IDX, c.getKey().getParent().getName());
        result.add(appendix);
        return result;
    }

    @Override
    public String getTrack(int trackId, String courseName) throws MissingEntitiesException, LoginExpiredException {
        String member = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        if(member == null)
        	throw new LoginExpiredException("Your login session expired due to inactivity.");
        return getTrack(member, trackId, courseName);
    }

    @Override
    public String getFeed(int feedId, String courseName) throws MissingEntitiesException, LoginExpiredException {
        String member = LoginHelper.getLoggedInUser(getThreadLocalRequest());
        if(member == null)
        	throw new LoginExpiredException("Your login session expired due to inactivity.");
        return getFeed(member, feedId, courseName);
    }

    @Override
    public String getTrack(String member, int trackId, String courseName) throws MissingEntitiesException {
        Key<CourseTracker>  tk = CourseTracker.getKey(member, courseName);
        Objectify ofy = ObjectifyService.begin();
        CourseTracker tracker = ofy.find(tk);
        if(tracker == null)
            throw new MissingEntitiesException(member + " is currently not tracking " + courseName);
        StringBuffer result = new StringBuffer(  tracker.getTrack(trackId -1));
        String date = DateFormat.getDateTimeInstance().format(tracker.getTrackDate(trackId - 1));
        String audit = tracker.getTrackTrail(trackId - 1);
        return result.append("<p><hr/>Report by ").append(audit).append(" on ").append(date).append("</p>").toString();

    }

    @Override
    public String getFeed(String member, int feedId, String courseName) throws MissingEntitiesException {
        Key<CourseTracker>  tk = CourseTracker.getKey(member, courseName);
        Objectify ofy = ObjectifyService.begin();
        CourseTracker tracker = ofy.find(tk);
        if(tracker == null)
            throw new MissingEntitiesException(member + " is currently not tracking " + courseName);
        StringBuffer result = new StringBuffer(  tracker.getFeedBack(feedId -1));
        String date = DateFormat.getDateTimeInstance().format(tracker.getFeedDate(feedId - 1));
        String audit = tracker.getFeedTrail(feedId - 1);
        return result.append("<p><hr/>Review by ").append(audit).append(" on ").append(date).append("</p>").toString();
    }
}