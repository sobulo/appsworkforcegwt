/**
 * 
 */
package com.fertiletech.sap.server.scripts;

import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.moocs.CourseDAO;
import com.fertiletech.sap.server.entities.moocs.CourseTracker;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.shared.DTOConstants;
import com.googlecode.objectify.Key;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IntraDayTrackCountMail extends HttpServlet{
	private static final Logger log = Logger.getLogger(IntraDayTrackCountMail.class.getName());
	
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		//ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		String recipientList1 = "labs@appsworkforce.com";
		//String recipientList2 = "apply@appsworkforce.com";

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date start = cal.getTime();
		Date end = new Date();
	    Date startDate = new GregorianCalendar(2015, 1, 30).getTime();
		String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(startDate);
		//String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(start);
		String endTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(end);
		
		
		StringBuilder textMessage = new StringBuilder("Status report on this month's track counts, start date: " + startTime + " end date: " + endTime + " .\n\n");
		StringBuilder htmlMessage = new StringBuilder("Status report on this month's track counts: " + startTime + " end time: " + endTime + "<br/><hr>");

		
		textMessage.append("Course Name\tCounts");
		htmlMessage.append("<table border='1' cellspacing='5' cellpadding='5' style='border:1px solid grey; color:black'><tr><th>Course Name</th><th>Counts</th></tr>");
		List<Key<CourseTracker>> tracks = CourseDAO.getRecentTrackedCourseKeys(start, end);
		
		//aggregate data
		final HashMap<String, Integer> TRACK_MAP = new HashMap<String, Integer>();
		String courseName = "";
		int courseCount = 0;
		for(Key<CourseTracker> tk : tracks)
		{
			courseName = tk.getName();
			if(!TRACK_MAP.containsKey(courseName))
				TRACK_MAP.put(courseName, 0);
			courseCount = TRACK_MAP.get(courseName) + 1;
			TRACK_MAP.put(courseName, courseCount);
		}

		for(String name : TRACK_MAP.keySet())
		{
			courseCount = TRACK_MAP.get(name);
			if(courseCount == 0)
				continue;
			textMessage.append(name).append("\t").append(courseCount).append("\n");
			htmlMessage.append("<tr><td>").append(name).append("</td><td>").append(courseCount).append("</td></tr>");
		}
		String[] messageContent = {textMessage.toString(), htmlMessage.append("</table>").toString()};
		MessagingDAO.scheduleSendEmail(recipientList1, messageContent, "AppsWorkforce Weekly Track Summary Counts", false);
		//MessagingDAO.scheduleSendEmail(recipientList2, messageContent, "AppsWorkforce Weekly Track Summary Counts", false);
		log.info("Message sent for Status report on this month's track counts, start date: " + startTime + " end date: " + endTime );
	}
}
