/**
 * 
 */
package com.fertiletech.sap.server.scripts;

import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.shared.DTOConstants;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IntraDayStatusCounts extends HttpServlet{
	private static final Logger log = Logger.getLogger(IntraDayStatusCounts.class.getName());
	
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		String recipientList1 = "labs@appsworkforce.com";
		String recipientList2 = "apply@appsworkforce.com";

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
	    Date startDate = new GregorianCalendar(2015, 1, 30).getTime();
		String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(startDate);
		//String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(cal.getTime());
		String endTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(new Date());
		
		
		StringBuilder textMessage = new StringBuilder("Status report on this month's member registrations, start date: " + startTime + " end date: " + endTime + " .\n\n");
		StringBuilder htmlMessage = new StringBuilder("Status report on member registrations, start time: " + startTime + " end time: " + endTime + "<br/><hr>");

		
		textMessage.append("IT Name\tOps Name\tCounts\t");
		htmlMessage.append("<table border='1' cellspacing='5' cellpadding='5' style='border:1px solid grey; color:black'><tr><th>M.A.P. Status</th><th>Status Name</th><th>Counts</th></tr>");
		HashMap<String, Integer> aggregates = EntityConstants.getLeadAggregates(cal.getTime(), new Date());

		for(int i = 0; i < DTOConstants.getStatusChartHeaders().getNumberOfTextFields(); i++)
		{
			String st = DTOConstants.getStatusChartHeaders().getText(i);
			int stateCount = (aggregates.get(st) == null)? 0 : aggregates.get(st);
			textMessage.append("\t").append(st.toString()).append("\t").append(st).append("\t").append(stateCount).append("\n");
			htmlMessage.append("<tr><td>").append(st.toString()).append("</td><td>").append(st).append("</td><td>").append(stateCount).append("</td></tr>");
		}
		String[] messageContent = {textMessage.toString(), htmlMessage.append("</table>").toString()};
		MessagingDAO.scheduleSendEmail(recipientList1, messageContent, "AppsWorkforce Registration Intra-Day Counts", false);
		//MessagingDAO.scheduleSendEmail(recipientList2, messageContent, "AppsWorkforce Registration Intra-Day Counts", false);
		out.println("<b>message sent</b><br/>");
	}
}
