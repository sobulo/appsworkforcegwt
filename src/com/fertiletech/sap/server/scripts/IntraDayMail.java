/**
 * 
 */
package com.fertiletech.sap.server.scripts;

import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.shared.DTOConstants;
import com.googlecode.objectify.Query;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IntraDayMail extends HttpServlet{
	private static final Logger log = Logger.getLogger(IntraDayMail.class.getName());
	
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		String recipientList1 = "labs@appsworkforce.com";
		String recipientList2 = "apply@appsworkforce.com";

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
	    Date startDate = new GregorianCalendar(2015, 1, 30).getTime();
		String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(startDate);
		//String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(cal.getTime());
		String endTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(new Date());
		
		
		StringBuilder textMessage = new StringBuilder("Status report on this month's member registrations, start date: " + startTime + " end date: " + endTime + " .\n\n");
		StringBuilder htmlMessage = new StringBuilder("Status report on member registrations, start time: " + startTime + " end time: " + endTime + "<br/><hr>");

		
		textMessage.append("Name\tEmail\tState\t");
		htmlMessage.append("<table border='1' cellspacing='5' cellpadding='5' style='border:1px solid grey; color:black'><tr><th>Name</th><th>Email</th><th>State</th></tr>");
		Query<SalesLead> leadQuery = EntityDAO.getModifiedSalesLeadsByDate(cal.getTime(), new Date());
		List<SalesLead> leads = leadQuery.list();

		for(SalesLead ld : leads)
		{
			textMessage.append("\t").append(ld.getFullName()).append("\t").append(ld.getEmail()).append("\t").append(ld.getState()).append("\n");
			htmlMessage.append("<tr><td>").append(ld.getFullName()).append("</td><td>").append(ld.getEmail()).append("</td><td>").append(ld.getState()).append("</td></tr>");
		}
		String[] messageContent = {textMessage.toString(), htmlMessage.append("</table>").toString()};
		MessagingDAO.scheduleSendEmail(recipientList1, messageContent, "AppsWorkforce Registration Intra-Day Counts", false);
		//MessagingDAO.scheduleSendEmail(recipientList2, messageContent, "AppsWorkforce Registration Intra-Day Counts", false);
		out.println("<b>message sent</b><br/>");
	}
}
