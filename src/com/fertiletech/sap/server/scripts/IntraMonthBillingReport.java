/**
 * 
 */
package com.fertiletech.sap.server.scripts;

import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.entities.moocs.CourseDAO;
import com.fertiletech.sap.server.entities.moocs.CourseTracker;
import com.fertiletech.sap.server.entities.payments.InetBill;
import com.fertiletech.sap.server.entities.payments.InetBillDescription;
import com.fertiletech.sap.server.entities.payments.InetDAO4Accounts;
import com.fertiletech.sap.server.messaging.EmailController;
import com.fertiletech.sap.server.messaging.MessagingController;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 * Entries to enable server answer where? web.xml and when? cron.xml
 *
 */
public class IntraMonthBillingReport extends HttpServlet{
	private static final Logger log = Logger.getLogger(IntraMonthBillingReport.class.getName());
	
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		// Get email from query string.
	    final String type = req.getParameter("type") == null ? "all" : req.getParameter("type");
	   
		//ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		String recipientList1 = "labs@appsworkforce.com";
		//String recipientList2 = "apply@appsworkforce.com";

		Date start = new GregorianCalendar(2015, 8, 14).getTime();
		Date end = new GregorianCalendar(2018, 11, 30).getTime();
		DateFormat dateFormater = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM);
		String startTime = dateFormater.format(start);
		String endTime = dateFormater.format(end);
		String timeStamp = dateFormater.format(new Date());
		
		
		StringBuilder textMessage = new StringBuilder("Report on this , start date: " + startTime + " end date: " + endTime + " .\n\n");
		StringBuilder htmlMessage = new StringBuilder("Report on billing counts start time: " + startTime + " end time: " + endTime + "<br/><hr>");

		
		textMessage.append("Name\tEmail\tPhone\tState\tDate\tLicense Info\tAmount\tSettled\tType");
		htmlMessage.append("<table border='1' cellspacing='5' cellpadding='5' style='border:1px solid grey; color:black'>"
				+ "<tr><th>Name</th><th>Email</th><th>Phone</th><th>State</th><th>Date</th><th>Current License</th>"
				+ "<th>Amount</th><th>Settled</th><th>Application</th>"
				+ "<th>Display Message</th><th>Requested License</th></tr>");
	
        Objectify ofy = ObjectifyService.begin();
        List<InetBill> bills = InetDAO4Accounts.getBills(start, end, ofy);
        HashMap<Key<ApplicantUniqueIdentifier>, List<SalesLead>> cachdedeads = new HashMap<>();
        for(InetBill b : bills)
        {
        	String billTemplate = b.getBillTemplateKey().getName();
        	if(!DTOConstants.BILL_DESC_MAP.containsKey(billTemplate))
        		continue;
        	Key<ApplicantUniqueIdentifier> user = b.getUserKey();
        	NumberFormat fmt = NumberFormat.getIntegerInstance();
        	String amount = fmt.format(b.getTotalAmount());
        	String settled = fmt.format(b.getSettledAmount());
        	String billDate = dateFormater.format((b.getIssueDate()));
        	
        	

        	
        	//expensive as we query seperately for each identifier
        	
        	List<SalesLead> leads = cachdedeads.get(user);
        	if(!cachdedeads.containsKey(user))
        	{
        		leads = EntityDAO.getConsumerLoanLeadViaEmail(ofy, user.getName());
        		cachdedeads.put(user, leads);
        	}
        	String email = user.getName();
        	String name = "";
        	String phone = "";
        	String state = "";
        	String opsDisplay = "";
        	String opsName = "";
        	String licenseInfo = "";
        	
        	for (SalesLead lead : leads)
        	{
        		name +=(lead.getFullName() == null? " n/a | " :(lead.getFullName() + " | "));
        		phone += (lead.getPhoneNumber() == null? " n/a | " :(lead.getPhoneNumber() + " | "));
        		state += (lead.getState() == null? " n/a | " : (lead.getState() + " | "));
        		opsName += (lead.getCurrentState().getDisplayString() + " | ");
        		opsDisplay += (lead.getDisplayMessaage() == null? " n/a | " :(lead.getDisplayMessaage() + " | "));
        		Date expiry = lead.getLicenseEnds();
        		String lt = (lead.getLicenseType() == null? " n/a | " : lead.getLicenseType());
        		licenseInfo += ((expiry==null?"Never Licensed":dateFormater.format(expiry) + "-" + lt) + " | ");
        	}
        	
    		textMessage.append(name).append("\t").append(email).
			append("\t").append(phone).append("\t").append(state).append("\t").append(billDate).
			append("\t").append(licenseInfo).append("\t").append(amount).append("\t").append(settled).
			append("\t").append(billTemplate);
			htmlMessage.append("<tr>"
					+ "<td>").append(name).append("</td><td>").append(email).append("</td>"
					+ "<td>").append(phone).append("</td><td>").append(state).append("</td>"
					+ "<td>").append(billDate).append("</td><td>").append(licenseInfo).append("</td>"
					+ "<td>").append(amount).append("</td><td>").append(settled).append("</td>"
					+ "<td>").append(opsName).append("</td><td>").append(opsDisplay).append("</td>"
					+ "<td>").append(billTemplate).append("</td></tr>");
        }

		
		String[] messageContent = {textMessage.toString(), htmlMessage.append("</table>").toString()};
		//MessagingDAO.scheduleSendEmail(recipientList1, messageContent, "AppsWorkforce Daily Track Detailed Counts", false);
		//scheduling mail fails cause content to large for the task
        Key<? extends MessagingController> controllerKey =  new Key<EmailController>(EmailController.class, MessagingDAO.SYSTEM_EMAILER); 
    	MessagingController controller = ofy.find(controllerKey);
    	if(controller == null)
    		throw new IllegalStateException("Unable to find controller: " + controllerKey.getName() );
    	boolean status = controller.sendMessage(recipientList1, messageContent, "AppsWorkforce Biling  Counts (generated on "
    			+ timeStamp + ")");

    	String msgSuffix = "message: " + messageContent[1].length() +
		" to " + recipientList1 + ". Controller Type: " + controller.getClass().getSimpleName();
    	if(status)
    		log.info("Successfully sent " + msgSuffix);
    	else
    		log.severe("Error sending " + msgSuffix);
		log.info("Message sent for detail report on billing fof licenses, start date: " + startTime + " end date: " + endTime );
	}
}
