/**
 * 
 */
package com.fertiletech.sap.server.scripts;

import com.fertiletech.sap.server.entities.EntityConstants;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.moocs.CourseDAO;
import com.fertiletech.sap.server.entities.moocs.CourseTracker;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.shared.DTOConstants;
import com.googlecode.objectify.Key;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class IntraDayTrackDetailsMail extends HttpServlet{
	private static final Logger log = Logger.getLogger(IntraDayTrackDetailsMail.class.getName());
	
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		//ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		String recipientList1 = "labs@appsworkforce.com";
		//String recipientList2 = "apply@appsworkforce.com";

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		//Date start = cal.getTime();
		Date end = new Date();
		DateFormat dateFormater = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM);
	    Date start = new GregorianCalendar(2015, 1, 30).getTime();
		String startTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(start);
		//String startTime = dateFormater.format(start);
		String endTime = dateFormater.format(end);
		
		
		StringBuilder textMessage = new StringBuilder("Detail report on this month's track counts, start date: " + startTime + " end date: " + endTime + " .\n\n");
		StringBuilder htmlMessage = new StringBuilder("Detail report on this month's track counts: " + startTime + " end time: " + endTime + "<br/><hr>");

		
		textMessage.append("Course Name\tCourse\tUdate\tFeed\tDate");
		htmlMessage.append("<table border='1' cellspacing='5' cellpadding='5' style='border:1px solid grey; color:black'>"
				+ "<tr><th>Email</th><th>Course</th><th>#UC</th><th>#FC</th><th>Date</th></tr>");
		List<CourseTracker> tracks = CourseDAO.getRecentTrackedCourses(start, end);
		

		//detail fields
		String courseName;
		String email;
		int feedCount;
		int updateCount;
		String updateDate;

		for(CourseTracker t : tracks)
		{
			courseName = t.getKey().getName();
			email = t.getKey().getParent().getName();
			feedCount = t.getFeedCount();
			updateCount = t.getUpdateCount();
			updateDate = dateFormater.format(t.getDateUpdated());
			
			textMessage.append(email).append("\t").append(courseName).
			append("\t").append(updateCount).append("\t").append(feedCount).append("\t").append(updateDate);
			htmlMessage.append("<tr>"
					+ "<td>").append(email).append("</td><td>").append(courseName).append("</td>"
					+ "<td>").append(updateCount).append("</td><td>").append(feedCount).append("</td>"
					+ "<td>").append(updateDate).append("</td></tr>");
		}
		String[] messageContent = {textMessage.toString(), htmlMessage.append("</table>").toString()};
		MessagingDAO.scheduleSendEmail(recipientList1, messageContent, "AppsWorkforce Daily Track Detailed Counts", false);
		//MessagingDAO.scheduleSendEmail(recipientList2, messageContent, "AppsWorkforce Daily Track Detailed Counts", false);
		log.info("Message sent for detail report on this month's track counts, start date: " + startTime + " end date: " + endTime );
	}
}
