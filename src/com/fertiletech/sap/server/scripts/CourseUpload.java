/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.sap.server.scripts;

import com.fertiletech.sap.server.entities.moocs.Course;
import com.fertiletech.sap.server.entities.moocs.CourseConstants;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.shared.CourseStatus;
import com.fertiletech.sap.shared.GeneralFuncs;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class CourseUpload extends HttpServlet {
    //TODO review classes of j9educationaction package and ensure no instance
    //of services being created. instead create static versions of such fns
    private static final Logger log =
            Logger.getLogger(CourseUpload.class.getName());
    private final static String STUD_UPLD_SESSION_NAME = "studentUploadData";
    private final static String CLASS_KEY_SESSION_NAME = "classkey";
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException 
    {
		//step two .. save the fetched sheet into an orbit object
        res.setContentType("text/html");
        ServletOutputStream out = res.getOutputStream();
        if (req.getParameter(LOAD_PARAM_NAME) != null)
        {
        	String userEmail = LoginHelper.getLoggedInUser(req);
            Object upldSessObj = req.getSession().getAttribute(STUD_UPLD_SESSION_NAME);
            Object clsSessObj = req.getSession().getAttribute(CLASS_KEY_SESSION_NAME);
            if (upldSessObj != null && clsSessObj != null) 
            {
                //retrive data from session object
                ArrayList<StudentStruct> courseDataStruct = (ArrayList<StudentStruct>) upldSessObj;
                //String feedName = (String) clsSessObj;

                String badMsg = "";
                String okMsg = "<b>Platform,Name,Ordering,Link, Status,Enrolllent</b><br/>";
                ArrayList<Course> coursesToSave = new ArrayList<Course>(courseDataStruct.size());
                int okCount = 0;
                int badCount = 0;
;                for(StudentStruct ci : courseDataStruct)
                {
                    long x= 20;
                    int y = (int) x;
                    int order = -1;
                    try {
                         order = ((Long) CourseConstants.NUMBER_FORMATTER.parse(ci.ordering)).intValue();
                    } catch (ParseException e) {
                       badMsg += "Failed to parse order number for " + ci.name + e.getMessage() + "<br/>";
                       badMsg += "Ordering number is [" + ci.ordering + "] control [ ]";
                       badCount++;
                       continue;
                    }
                    CourseStatus status = null;
                    try
                    {
                        status = CourseStatus.valueOf(ci.status);
                    }
                    catch (IllegalArgumentException ex)
                    {
                        badMsg += "Unrecognized status" + ci.name + ex.getMessage() + "<br/>";
                        badCount++;
                        continue;
                    }

                    Course course = new Course(ci.platform, ci.category, ci.name, order, status,
                        ci.link, ci.enrollment, userEmail, ci.institution, ci.accronym, ci.prof, ci.forum);
                    coursesToSave.add(course);
                	
                	okMsg += ci.platform + "," + ci.name + "," + ci.ordering + "," +
                            ci.link + "," + ci.status + "," + ci.enrollment + "<br/>";
                	okCount++;
                }

                Objectify ofy = ObjectifyService.begin();
                ofy.put(coursesToSave);

                //set session data to null as we've saved the data
                req.setAttribute(STUD_UPLD_SESSION_NAME, null);
                req.setAttribute(CLASS_KEY_SESSION_NAME, null);

                out.println("Ok Count: " + okCount + " Save Count: " + coursesToSave.size() + "<br/>");
                out.println("Bad Count: " + badCount + "<br/><br/><hr/>");
                out.println(badMsg);
                out.println("<br/><hr/>");
                out.println(okMsg);
                out.flush();
            }
            else
            {
                out.println("<b>unable to retrieve session info. Try " +
                        "enabling cookies in your browser.</b>");
            }
        }
        else
        {
        	//Step 1 fetch the spreadsheet
            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iterator = upload.getItemIterator(req);
                String feedName = "";
                while (iterator.hasNext()) {
                    FileItemStream item = iterator.next();
                    InputStream stream = item.openStream();
                    if (item.isFormField() && item.getFieldName().equals(CLASS_KEY_SESSION_NAME)) {
                    	feedName = Streams.asString(stream);;
                        req.getSession().setAttribute(CLASS_KEY_SESSION_NAME, feedName);
                    } else {
                        log.warning("Course upload attempt: " + item.getFieldName() + ", name = " + item.getName());
                        ColumnAccessor[] headerAccessors = CourseConstants.COURSE_UPLOAD_ACCESSORS;
                        String[] expectedHeaders = ColumnAccessor.getAccessorNames(headerAccessors);
                        //TODO, return error page if count greater than 200 rows
                        ExcelManager sheetManager = new ExcelManager(stream);
                        //confirm headers in ssheet match what we expect
                        String[] sheetHeaders;
                        try {
                            sheetHeaders = sheetManager.getHeaders();

                            //expectedheaders are uppercase so convert sheet prior to compare
                            for (int s = 0; s < sheetHeaders.length; s++) {
                                sheetHeaders[s] = sheetHeaders[s].toUpperCase();
                            }
                        } catch (InvalidColumnValueException ex) {
                        	log.severe(ex.getMessage());
                            out.println("<b>Unable to read excel sheet headers</b><br/> Error was: " 
                            		+ ex.getMessage());
                            return;
                        }
                        ArrayList<String> sheetOnly = new ArrayList<String>();
                        ArrayList<String> intersect = new ArrayList<String>();
                        ArrayList<String> expectedOnly = new ArrayList<String>();
                        GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders, intersect, expectedOnly, sheetOnly);
                        //handle errors in column headers
                        if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
                            out.println("<b>Uploaded sheet should contain only these headers:</b>");
                            printHtmlList(expectedHeaders, out);
                            if (expectedOnly.size() > 0) {
                                out.println("<b>These headers are missing from ssheet:</b>");
                                printHtmlList(expectedOnly, out);
                            }
                            if (sheetOnly.size() > 0) {
                                out.println("<b>These headers are not expected but were found in ssheet</b>");
                                printHtmlList(sheetOnly, out);
                            }
                            return;
                        }

                        sheetManager.initializeAccessorList(headerAccessors);
                        //todo, replace these with string builders
                        String goodDataHTML = "";
                        String badDataHTML = "";
                        String tempVal = "";
                        String[] htmlHeaders = new String[headerAccessors.length];
                        ArrayList<StudentStruct> sessionData = new ArrayList<StudentStruct>();
                

                        for (int i = 0; i < headerAccessors.length; i++) {
                            htmlHeaders[i] = headerAccessors[i].getHeaderName();
                        }

                        int numRows = sheetManager.totalRows();
                        for (int i = 1; i < numRows; i++) 
                        {
                        	try
                        	{
	                            String row = "<tr>";
	                            StudentStruct tempStudData = new StudentStruct();
	                            for (int j = 0; j < headerAccessors.length; j++) {
	                                tempVal = getValue(i, headerAccessors[j], sheetManager, tempStudData);
	                                row += "<td>" + tempVal + "</td>";
	                            }
	                            	                            
	                            if(tempStudData.isValid)
	                            {
	                                //append to display result
	                                goodDataHTML += row;
	                                sessionData.add(tempStudData);
	                            }
	                            else 
	                            {
	                                badDataHTML += (row + "</tr>");
	                            }
                        	}
                        	catch(EmptyColumnValueException ex) {} //logic to help skip blank lines
                        }

                        String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
                        String htmlHeader = "";
                        for (String s : htmlHeaders) {
                            htmlHeader += "<TH>" + s + "</TH>";
                        }
                        String htmlTableEnd = "</TABLE>";

                        if (goodDataHTML.length() > 0) 
                        {
                            out.println(GOOD_DATA_PREFIX);
                            goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data for: " + 
                            feedName + "</b>" + htmlTableStart + 
                            "<TR>" + htmlHeader + "</TR>" + goodDataHTML + htmlTableEnd;
                        }

                        if (badDataHTML.length() > 0) 
                        {
                            out.println("<b>Below shows records with errors</b>");
                            out.println(htmlTableStart);
                            out.println("<TR>");
                            out.println(htmlHeader);
                            out.println("</TR>");
                            out.print(badDataHTML);
                            out.println(htmlTableEnd);
                        }

                        out.println(goodDataHTML);

                        req.getSession().setAttribute(STUD_UPLD_SESSION_NAME,
                                sessionData);
                    }
                }
            } catch (FileUploadException ex) {
                out.println("<b>File upload failed:</b>" + ex.getMessage());
            }
            catch (InvalidColumnValueException ex) {
                log.severe(ex.getMessage());
                out.println("<b>Unable to read excel sheet</b><br/> Error was: " + ex.getMessage());
            }
        }
    }

    private String getValue(int row, ColumnAccessor accessor, ExcelManager sheetManager, StudentStruct studData) throws InvalidColumnValueException {
        Object temp;
        String val;
        String headerName = accessor.getHeaderName();
        try {

            temp = sheetManager.getData(row, accessor);

            
            //handle case of number
            if(headerName.equals(CourseConstants.ORDERING))
            {

	            	temp = sheetManager.getData(row, accessor);
	            	val = CourseConstants.NUMBER_FORMATTER.format((temp));
	            	studData.ordering = val;
	            	return val;
            }             
            
            
            //handle other fields
            if(temp == null)
                val = "";
            else
            {
                val = ((String) temp);
                if(val.equals("[NULL]"))
                	val = "";
            }

            val = val.trim();
            if(headerName.equals(CourseConstants.NAME))
            	studData.name = val;
            else if(headerName.equals(CourseConstants.PLATFORM))
            	studData.platform = val;
            else if(headerName.equals(CourseConstants.LINK))
            	studData.link = val;
            else if(headerName.equals(CourseConstants.STATUS)) {
                val = val.replace('-', '_');
                val = val.toUpperCase();
                CourseStatus.valueOf(val); //throw illegal argument exception is status is not recognized
                studData.status = val;
            }
            else if(headerName.equals(CourseConstants.ENROLLMENT))
            	studData.enrollment = val;
            else if(headerName.equals(CourseConstants.CATEGORY)) {
                if (!CourseConstants.isSupportedCategory(val))
                    throw new IllegalArgumentException("Category not recognized: [" + val + "] Suppoted: " +
                            Arrays.deepToString(CourseConstants.getSupportedCategories()));
                studData.category = val;
            }
            else if(headerName.equals(CourseConstants.INSTITUTION)) {
                if (!CourseConstants.isSupportedInstitutions(val))
                    throw new IllegalArgumentException("institution not recognized: [" + val + "] Suppoted: " +
                            Arrays.deepToString(CourseConstants.getSupportedInstitutions()));
                studData.institution = val;
            }
            else if(headerName.equals(CourseConstants.ACCRONYM)) {
                if (!CourseConstants.isSupportedAccronym(val))
                    throw new IllegalArgumentException("Accronym not recognized: [" + val + "] Suppoted: " +
                            Arrays.deepToString(CourseConstants.getSupportedAccronyms()));
                studData.accronym = val;
            }
            else if(headerName.equals(CourseConstants.PROF)) {
                    studData.prof = val;
            }
            else if(headerName.equals(CourseConstants.FORUM)) {
                if (!CourseConstants.isSupportedAccronym(val))
                    studData.forum = val;
            }
        }
        catch(EmptyColumnValueException ex)
        {
        	if(sheetManager.isBlankRow(row))
        		throw ex;
        	val = ex.getMessage();
        	log.warning(ex.getMessage());
        	studData.isValid = false;
        }        
        catch (InvalidColumnValueException ex) {
            val = ex.getMessage();
            studData.isValid = false;
        }
        catch (IllegalArgumentException ex)
        {
            val = ex.getMessage();
            studData.isValid = false;
        }
        return val;
    }

    private void printHtmlList(String[] list, ServletOutputStream out) throws IOException {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }

    public void printHtmlList(List<String> list, ServletOutputStream out) throws IOException
    {
        out.println("<ul>");
        for (String s : list) {
            out.println("<li>" + s + "</li>");
        }
        out.println("</ul>");
    }
}

class StudentStruct implements Serializable
{	
    String platform;
    String name;
    String ordering;
    String link;
    String status;
    String enrollment;
    String category;
    String institution;
    String accronym;
    String prof;
    String forum;

    boolean isValid = true;
}


