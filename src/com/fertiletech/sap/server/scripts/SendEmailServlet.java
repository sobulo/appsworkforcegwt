package com.fertiletech.sap.server.scripts;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// [END gae_sendgrid_import]

/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//TODO look into simple client https://github.com/sendgrid/java-http-client/tree/master/src/main/java/com/sendgrid

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

@SuppressWarnings("serial")
public class SendEmailServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(SendEmailServlet.class.getName());
	private final static String sendgridApiKey = System.getenv("SENDGRID_API_KEY");

	@Override
	public void service(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		// Get parameters from environment variables.

		final String sendgridSender = "labs@appsworkforce.com";
		final String sendgridReply = "hello@appsworkforce.com";

		// Get email from query string.
		final String toEmail = req.getParameter("to");
		if (toEmail == null) {
			resp.getWriter().print("Please provide an email address in the \"to\" query string parameter.");
			return;
		}
		try
		{
			String html = "<b>Hello Members in Lagos</b><br/>";
			String imgUrl1 = "<img src='https://sites.google.com/a/appsworkforce.com/appsworkforce/images/mit2.jpg'/>";
			String imgUrl2 = "<img src='http://about.appsworkforce.com/images/devcode.jpg'/>";
			html += imgUrl1 + "<br/>The rain in spain stays mainly in the plane<br></br> And you know this men <br/>" + imgUrl2;
			
			boolean ok = sendEmailViaGrid(sendgridSender, sendgridReply, toEmail, "image testing", "funky", "Hello Word", html);
			ServletOutputStream out = resp.getOutputStream();
			if(ok)
				out.println("Ran succesfully");
			else
				out.println("Failed,, check logs");
			
			
		} catch (IOException e) {
			throw new ServletException("SendGrid error", e);
		}
		// [END gae_sendgrid]
	}

	public static boolean sendEmailViaGrid(String fromAddy, String replyTo, String toAddy, String msgSubject,
			String msgCategory, String txtMesssae, String htmlMessage) throws IOException 
	{
		if(toAddy.contains("noreply"))
		{
			log.warning("No reply received: " + toAddy);
			return false;
		}
		
		final String HTML_LOGO = "<a href='https://www.appsworkforce.com'><img src='https://www.appsworkforce.com/logo.png'/></a><br/>";
		Email to = new Email(toAddy);
		Email from = new Email(fromAddy);
		Email reply = new Email(replyTo);
		Content textContent = new Content("text/plain", txtMesssae);
		Content htmlContent = new Content("text/html", HTML_LOGO + htmlMessage);
		Mail mail = new Mail(from, msgSubject, to, textContent);
		log.warning("TEXT########: " + txtMesssae);
		log.warning("TEXT########: " + htmlMessage);
		mail.setReplyTo(reply);
		mail.addContent(htmlContent);
		mail.addCategory(msgCategory);
		
		if(!(fromAddy.endsWith("@appsworkforce.com") && replyTo.endsWith("@appsworkforce.com")))
			throw new IllegalArgumentException("Unsupported domains, not AW for from and/or reply" + fromAddy + " and/or " + replyTo);

		// Instantiates SendGrid client.
		SendGrid sendgrid = new SendGrid(sendgridApiKey);

		// Instantiate SendGrid request.
		Request request = new Request();

		// Set request configuration.
		request.setMethod(Method.POST);
		request.setEndpoint("mail/send");
		request.setBody(mail.build());

		// Use the client to send the API request.
		Response response = sendgrid.api(request);

		if (response.getStatusCode() == 202) // 202 is sendgrid success code
			return true;

		log.warning(String.format("An error occured: %s", response.getStatusCode()));
		// return;

		return false;
	}

}
