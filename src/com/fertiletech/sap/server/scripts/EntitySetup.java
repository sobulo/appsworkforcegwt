/**
 * 
 */
package com.fertiletech.sap.server.scripts;

import com.fertiletech.sap.server.entities.ApplicationParameters;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.entities.payments.InetBillDescription;
import com.fertiletech.sap.server.login.LoginHelper;
import com.fertiletech.sap.server.messaging.MessagingController;
import com.fertiletech.sap.server.messaging.MessagingDAO;
import com.fertiletech.sap.server.tasks.TaskQueueHelper;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Query;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class EntitySetup extends HttpServlet {
	private static final Logger log = Logger.getLogger(EntitySetup.class.getName());

	static {
		EntityDAO.registerClassesWithObjectify();
	}

	/**
	 * web.xml contains entries to ensure only registered developers for the app
	 * can execute this script. Does not go through login logic
	 */

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		try {
			String type = req.getParameter("type");
			if (type == null) {
				out.println("<b><font color='red'>Please specify a type</font></b>");
				return;
			}
			out.println("<b>setup starting</b><br/>");
			if (type.equals("params")) {
				String result = createParameter();
				out.println(result);
				log.warning(result + " triggered by " + LoginHelper.getLoggedInUser(req));
			} else if (type.equals("email")) {
				String fromAddress = "labs@appsworkforce.com";
				String bccAddress = "labs@appsworkforce.com";
				MessagingController controller = MessagingDAO.createEmailController(MessagingDAO.PUBLIC_EMAILER,
						fromAddress, bccAddress, 50000, 1000, 100000);
				log.warning("created: " + controller.getKey());
				fromAddress = "hello@appsworkforce.com";
				controller = MessagingDAO.createEmailController(MessagingDAO.SYSTEM_EMAILER, fromAddress, null, 5000,
						100, 10000);
				log.warning("created: " + controller.getKey());
			} else if (type.equals("admins")) {
				String result = createParameterAdmin();
				out.println(result);
				log.warning(result + " triggered by " + LoginHelper.getLoggedInUser(req));
			} else if (type.equals("aw2018")) {
				Date st = new GregorianCalendar(2018, 0, 1).getTime();
				Date ed = new GregorianCalendar(2020, 10, 2).getTime();
				int[] result = autoLicense(st, ed);
				out.println("Total: " + result[0] + " Scheduled " + result[1] + "<br/>");
				// out.println(result + " triggered by " +
				// LoginHelper.getLoggedInUser(req));
			} else if (type.equals("aw2017")) {
				Date st = new GregorianCalendar(2017, 0, 1).getTime();
				Date ed = new GregorianCalendar(2017, 11, 31).getTime();
				int[] result = autoLicense(st, ed);
				out.println("Total: " + result[0] + " Scheduled " + result[1] + "<br/>");
				// out.println(result + " triggered by " +
				// LoginHelper.getLoggedInUser(req));
			} else if (type.equals("aw2016")) {
				Date st = new GregorianCalendar(2016, 0, 1).getTime();
				Date ed = new GregorianCalendar(2016, 11, 31).getTime();
				int[] result = autoLicense(st, ed);
				out.println("Total: " + result[0] + " Scheduled " + result[1] + "<br/>");
				// out.println(result + " triggered by " +
				// LoginHelper.getLoggedInUser(req));
			} else if (type.equals("aw2015")) {
				Date st = new GregorianCalendar(2015, 0, 1).getTime();
				Date ed = new GregorianCalendar(2015, 11, 31).getTime();
				int[] result = autoLicense(st, ed); // createParameterAdmin();
				out.println("Total: " + result[0] + " Scheduled " + result[1] + "<br/>");
				// out.println(result + " triggered by " +
				// LoginHelper.getLoggedInUser(req));
			}

			out.println("<b>setup done</b><br/>");
		} catch (DuplicateEntitiesException ex) {
			out.println("An error occured when creating objects: " + ex.getMessage());
		}
	}

	private int[] autoLicense(Date start, Date end) {
		Key<InetBillDescription> bdKey = new Key(InetBillDescription.class, DTOConstants.LICENSE_ED_TRIAL);
		Query<SalesLead> lq = EntityDAO.getSalesLeadsByDate(start, end);
		List<Key<SalesLead>> leadKeys = lq.listKeys();
		int[] result = { leadKeys.size(), 0 };
		for (Key<SalesLead> lk : leadKeys) {
			if(!lk.getParent().getName().endsWith("@gmail.com"))
				continue;
			TaskQueueHelper.scheduleCreateUserLicense(bdKey.getString(), lk.getString());
			result[1]++;
		}
		return result;

	}

	private static String createParameter() throws DuplicateEntitiesException {
		HashMap<String, String> params = new HashMap<String, String>();
		// params.put("sobulo@fertiletech.com", "added for support purposes
		// during deployment of admin profiles");
		// String[] paramNames = {DTOConstants.APP_PARAM_ADMINS,
		// DTOConstants.APP_PARAM_EDITOR, DTOConstants.APP_PARAM_REVIEWER};
		params.put("Current Residence", "");
		params.put("Secondary Education Location", "");
		params.put("Tertiary Education Location", "");
		params.put("Employer Location", "");
		String[] paramNames = { DTOConstants.APP_PARAM_MAP_TITLES };
		StringBuilder result = new StringBuilder();
		for (String name : paramNames) {
			ApplicationParameters paramObj = EntityDAO.createApplicationParameters(name, params);
			result.append("<p>Created: " + paramObj.getKey() + "</p>");
		}
		return result.toString();
	}

	private static String createParameterAdmin() throws DuplicateEntitiesException {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("sobulo@fertiletech.com", "added for support purposes during deployment of admin profiles");
		String[] paramNames = { DTOConstants.APP_PARAM_ADMINS, DTOConstants.APP_PARAM_EDITOR,
				DTOConstants.APP_PARAM_REVIEWER };
		// params.put("Current Residence", "");
		// params.put("Location where Diploma Obtained", "");
		// params.put("Industrial Training Location", "");
		// String[] paramNames = {DTOConstants.APP_PARAM_MAP_TITLES};
		StringBuilder result = new StringBuilder();
		for (String name : paramNames) {
			ApplicationParameters paramObj = EntityDAO.createApplicationParameters(name, params);
			result.append("<p>Created: " + paramObj.getKey() + "</p>");
		}
		return result.toString();
	}
}
