/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.sap.server.tasks;


import com.fertiletech.sap.shared.DTOConstants;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Builder;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

import java.util.logging.Logger;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class TaskQueueHelper {
    private static final Logger log = Logger.getLogger(TaskQueueHelper.class.getName());
    private final static String COMMON_TASK_HANDLER = "/tasks/runner";

    private static int MAX_RETRY_COUNT = 3;
    
    public static void scheduleMessageViaGrid(String fromAddy, String replyTo, String toAddy, String msgSubject,
			String msgCategory, String contentName, String suffix, String suffixText)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue();
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_SEND_GRID).
	                    param(TaskConstants.SEND_GRID_CATEGGORY, msgCategory).
	                    param(TaskConstants.SEND_GRID_CONTENT, contentName).
	                    param(TaskConstants.SEND_GRID_FROM, fromAddy).
	                    param(TaskConstants.SEND_GRID_REPLY, replyTo).
	                    param(TaskConstants.SEND_GRID_SUBJECT, msgSubject).
	                    param(TaskConstants.SEND_GRID_SUFFIX, suffix).
	                    param(TaskConstants.SEND_GRID_SUFFIX_TEXT, suffixText).
	                    param(TaskConstants.SEND_GRID_TO, toAddy);
            
	            queue.add(opt.method(Method.POST));
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task for sendgrid failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, message not sent for: " + toAddy);
    }
    
    public static void scheduleCreateComment(String[] comments, String loanKeyStr, String updateUser)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue();
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_CREATE_COMMENT).
	                    param(TaskConstants.LOAN_ID_PARAM,
	                    loanKeyStr).param(TaskConstants.UPDATE_USR_PARAM, updateUser);

    	        for(String msg : comments)
    	        	if(msg != null && msg.length() > 0)
    	        		opt = opt.param(TaskConstants.MSG_BODY_PARAM, msg);	            
	            queue.add(opt.method(Method.POST));
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, comments will not be created. This is a silent failure, best debug info: [" + loanKeyStr + "]");
    }
    
	public static void scheduleMessageSending(String controllerId, String toAddress, String messages[], String subject)
	{
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	log.warning("scheduling sending of msg to: " + toAddress);
    	    	log.warning("text message:\n" + messages[0]);
    	    	log.warning("html message:\n" + messages[1]);
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(TaskConstants.SERVICE_TYPE, 
    	                		TaskConstants.SERVICE_SEND_MESSAGE).
    	                param(TaskConstants.MSG_CONTROLLER_KEY_PARAM, controllerId).
    	                param(TaskConstants.TO_ADDR_PARAM, toAddress).
    	                param(TaskConstants.MSG_SUBJECT_PARAM, subject);
    	        
    	        for(String msg : messages)
    	        	if(msg != null && msg.length() > 0)
    	        		url = url.param(TaskConstants.MSG_BODY_PARAM, msg);
    	        url.method(Method.POST);
    	        queue.add(url);    	    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule message sending failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed for message sending, pls check logs");
	}
	
    public static void scheduleDelayedLoanAppStateChangeMessage(String loanKeyStr)
    {
    	log.warning("schedule of delayed loan app state change requested");
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue(); 
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_SEND_MTG_MESSAGE).
	                    param(TaskConstants.LOAN_ID_PARAM,
	                    loanKeyStr).method(Method.POST);            
	            queue.add(opt);
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, applicant state will not change. This is a silent failure, best debug info: [" + loanKeyStr + "]");
    	else
    		log.warning("applicant state change has been scheduled");
    }

	public static void scheduleCreateUserBill(String billDescriptionKey, String userKey)
	{
		boolean succeeded = false;
		int retryCount = 0;
		while(!succeeded && retryCount < MAX_RETRY_COUNT)
		{
			if(retryCount != 0)
				log.warning("retrying schedule task, retry number " + retryCount);
			try
			{
				Queue queue = QueueFactory.getDefaultQueue();
				TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
						param(TaskConstants.SERVICE_TYPE,
								TaskConstants.SERVICE_CREATE_USER_BILL).
						param(DTOConstants.BILL_DESC_KEY_PARAM, billDescriptionKey).
						param(TaskConstants.STUDENT_KEY_PARAM, userKey).method(Method.POST);

				queue.add(url);

				succeeded = true;
			}
			catch(Exception e)
			{
				retryCount++;
				log.warning("schedule task create user bill failed: " + e.getMessage());
			}
		}
		if(!succeeded)
			throw new RuntimeException("Task scheduling failed for bill creation pls check logs");
	}
	
	public static void scheduleCreateUserLicense(String billDescriptionKey, String userKey)
	{
		boolean succeeded = false;
		int retryCount = 0;
		while(!succeeded && retryCount < MAX_RETRY_COUNT)
		{
			if(retryCount != 0)
				log.warning("retrying schedule task, retry number " + retryCount);
			try
			{
				Queue queue = QueueFactory.getDefaultQueue();
				TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
						param(TaskConstants.SERVICE_TYPE,
								TaskConstants.SERVICE_CREATE_USER_LICENSE).
						param(DTOConstants.BILL_DESC_KEY_PARAM, billDescriptionKey).
						param(TaskConstants.STUDENT_KEY_PARAM, userKey).method(Method.POST);

				queue.add(url);

				succeeded = true;
			}
			catch(Exception e)
			{
				retryCount++;
				log.warning("schedule task create user license failed: " + e.getMessage());
			}
		}
		if(!succeeded)
			throw new RuntimeException("Task scheduling failed for license creation pls check logs" );
	}
	
}
