/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.sap.server.tasks;


import com.fertiletech.sap.server.ServiceImplUtilities;
import com.fertiletech.sap.server.entities.ApplicantUniqueIdentifier;
import com.fertiletech.sap.server.entities.EntityDAO;
import com.fertiletech.sap.server.entities.MessageText;
import com.fertiletech.sap.server.entities.MortgageComment;
import com.fertiletech.sap.server.entities.SalesLead;
import com.fertiletech.sap.server.entities.payments.InetBillDescription;
import com.fertiletech.sap.server.entities.payments.InetDAO4Accounts;
import com.fertiletech.sap.server.messaging.MessagingController;
import com.fertiletech.sap.server.scripts.SendEmailServlet;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.DuplicateEntitiesException;
import com.fertiletech.sap.shared.ManualVerificationException;
import com.fertiletech.sap.shared.MissingEntitiesException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Logger;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class CommonTasksHandler extends HttpServlet{
	static
	{
		EntityDAO.registerClassesWithObjectify();
	}
	
    private static final Logger log =
            Logger.getLogger(CommonTasksHandler.class.getName());
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
    	log.warning("*******TASK EXECUTION BEGINNING!!!********");
        StringBuilder requestDetails = new StringBuilder(64);
        Enumeration<String> paramNames = req.getParameterNames();
        while(paramNames.hasMoreElements())
        {
        	String name = paramNames.nextElement();
        	requestDetails.append("Param Name: " + name.toString() + " Values: ").append(Arrays.toString(req.getParameterValues(name))).append("\n");
        }
        paramNames = req.getHeaderNames();
        while(paramNames.hasMoreElements())
        {
        	String name = paramNames.nextElement();
        	requestDetails.append("Header Name: " + name.toString() + " Values: ").append(req.getHeader(name)).append("\n");
        }

        //log.warning(requestDetails.toString());
        
        Objectify ofy = ObjectifyService.begin();

        String service = req.getParameter(TaskConstants.SERVICE_TYPE).trim();
        validateParamName(service, "No service specified");
        
        if(service.equals(TaskConstants.SERVICE_SEND_MESSAGE))
        {
        	//log.warning("Running send message task");
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String controllerKeyStr = req.getParameter(TaskConstants.MSG_CONTROLLER_KEY_PARAM);
            String toAddress = req.getParameter(TaskConstants.TO_ADDR_PARAM);
            String[] messageBody = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
            String msgSubject = req.getParameter(TaskConstants.MSG_SUBJECT_PARAM);
            validateParamName(controllerKeyStr, "Value must be specified for message controller key");
            validateParamName(toAddress, "Value must be specified for recipeint address");
            validateParamName(messageBody, "Value must be specified for message body");
            validateParamName(msgSubject, "Value must be specified for subject field");


            Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerKeyStr);
             
        	MessagingController controller = ofy.find(controllerKey);
        	if(controller == null)
        		throw new IllegalStateException("Unable to find controller key");
        	boolean status = controller.sendMessage(toAddress, messageBody, msgSubject);
        	//for(String s : messageBody)
        	//	log.warning("EMAIL: \n" + s);
        	//TODO modify log to reflect byte size of entire messageBody object rather than just text component
        	String msgSuffix = "message: " + messageBody[0].length() +
			" to " + toAddress + ". Controller Type: " + controller.getClass().getSimpleName();
        	if(status)
        		log.warning("Successfully sent " + msgSuffix);
        	else
        		log.severe("Error sending " + msgSuffix);
        }
        else if(service.equals(TaskConstants.SERVICE_SEND_GRID))
        {
        	//log.warning("Running send message task");
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String contentName = req.getParameter(TaskConstants.SEND_GRID_CONTENT);
            String category = req.getParameter(TaskConstants.SEND_GRID_CATEGGORY);
            String toAddress = req.getParameter(TaskConstants.SEND_GRID_TO);
            String fromAddress = req.getParameter(TaskConstants.SEND_GRID_FROM);
            String replyTo = req.getParameter(TaskConstants.SEND_GRID_REPLY);
            String msgSubject = req.getParameter(TaskConstants.SEND_GRID_SUBJECT);
            String htmlSuffix = req.getParameter(TaskConstants.SEND_GRID_SUFFIX);
            String textSuffix = req.getParameter(TaskConstants.SEND_GRID_SUFFIX_TEXT);
            validateParamName(contentName, "Value must be specified for message content object");
            validateParamName(contentName, "Value must be specified for message category");
            validateParamName(toAddress, "Value must be specified for recipient address");
            validateParamName(fromAddress, "Value must be specified for address msg is from");
            validateParamName(replyTo, "Value must be specified for address to reply to");
            validateParamName(msgSubject, "Value must be specified for subject field");


            Key<MessageText> contentKey = MessageText.getKey(contentName);
             
        	MessageText content = ofy.find(contentKey);
        	if(content == null)
        		throw new IllegalStateException("Unable to find content object: [" + contentName +"]");
        	
        	boolean status = SendEmailServlet.sendEmailViaGrid("segun.sobulo@appsworkforce.com", replyTo, toAddress, msgSubject, category, content.getMessageText().getValue() + textSuffix, content.getMessageHTML().getValue() + htmlSuffix);
        	//	log.warning("EMAIL: \n" + s);
        	//TODO modify log to reflect byte size of entire messageBody object rather than just text component

        	if(status)
        		log.warning("Successfully sent message to " + toAddress);
        	else
        		log.info("ignoring sending message to " + toAddress);
        }
        else if(service.equals(TaskConstants.SERVICE_CREATE_COMMENT))
        {
        	log.warning("running create comment task");
        	String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
        	Key<SalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
        	String[] comments = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
        	String updateUser = req.getParameter(TaskConstants.UPDATE_USR_PARAM);
        	validateParamName(loanKeyStr, "value most be specified for loan id");
        	validateParamName(comments, "at least 1 comment must be specified");
        	String loginUser = null;
        	try
        	{
        		validateParamName(updateUser, "ignore");
        		loginUser = updateUser;
        	}
        	catch(IllegalArgumentException EX)
        	{
        		loginUser = "map-alert";
        	}
        	
        	MortgageComment storedObj = EntityDAO.createComment(comments, false, leadKey, loginUser);
        	ServiceImplUtilities.logSystemUpdate(log, storedObj.getKey(), " comments associated with: " + storedObj.getLoanKey());
        }
        else if(service.equals(TaskConstants.SERVICE_SEND_MTG_MESSAGE))
        {
        	log.warning("running change loan state");
        	String loanKeyStr = req.getParameter(TaskConstants.LOAN_ID_PARAM);
        	Key<SalesLead> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
    		EntityDAO.sendLeadStateNotificationMessage(leadKey, "map-alert");
        }
		else if(service.equals(TaskConstants.SERVICE_CREATE_USER_BILL))
		{
			String billDescKeyStr = req.getParameter(DTOConstants.BILL_DESC_KEY_PARAM);
			String userKeyStr = req.getParameter(TaskConstants.STUDENT_KEY_PARAM);
			validateParamName(billDescKeyStr, "No bill description key specified");
			validateParamName(userKeyStr, "No user key specified");
			try {
				Key<InetBillDescription> billDescKey = ofy.getFactory().stringToKey(billDescKeyStr);
				Key<SalesLead> userKey = ofy.getFactory().stringToKey(userKeyStr);
				InetBillDescription bd = ofy.get(billDescKey);
				InetDAO4Accounts.createBill(bd, userKey.<ApplicantUniqueIdentifier>getParent());
			}catch(DuplicateEntitiesException ex)
			{
				log.warning("IGNORING possible duplicate task run: " + ex.getMessage());
			}
		}
		else if(service.equals(TaskConstants.SERVICE_CREATE_USER_LICENSE))
		{
			String billDescKeyStr = req.getParameter(DTOConstants.BILL_DESC_KEY_PARAM);
			String userKeyStr = req.getParameter(TaskConstants.STUDENT_KEY_PARAM);
			validateParamName(billDescKeyStr, "No bill description key specified");
			validateParamName(userKeyStr, "No user key specified");
			try {
				Key<InetBillDescription> bdKey = ofy.getFactory().stringToKey(billDescKeyStr);
				Key<SalesLead> leadKey = ofy.getFactory().stringToKey(userKeyStr);
				InetDAO4Accounts.issueLease(bdKey, leadKey, "aw education trial", "availability intellingence (ai)", new Date(), "aw-auto");
			}catch(DuplicateEntitiesException ex)
			{
				log.warning("IGNORING duplicate error: " + ex.getMessage());
			}
			catch(ManualVerificationException mv)
			{
				log.warning("IGNORING missing entity error: " + mv.getMessage());
			} catch (MissingEntitiesException me) {
				log.warning("IGNORING missing entity error: " + me.getMessage());
			}
		}
	}

    private void validateParamName(String param, String message)
    {
        if(param == null || param.length() == 0)
            throw new IllegalArgumentException(message);
    }
    
    private void validateParamName(String[] params, String message)
    {
    	//log.warning("PARAMS: " + params);
        if( params.length == 0 || params[0] == null || params[0].length() == 0)
            throw new IllegalArgumentException(message);
    }    
}
