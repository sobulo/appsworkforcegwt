/**
 * 
 */
package com.fertiletech.sap.server.tasks;

import com.google.appengine.api.utils.SystemProperty;

import java.util.Random;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class TaskConstants {
    public final static String SERVICE_TYPE = "service";    
    public final static String SERVICE_SEND_MESSAGE = "sndmsg";  
    public final static String SERVICE_CREATE_COMMENT = "crtcmt";
    public final static String SERVICE_SEND_MTG_MESSAGE = "loanst";
    public final static String SERVICE_CREATE_USER_BILL = "newuserbill";
    public final static String SERVICE_CREATE_USER_LICENSE = "licensewizard";
    
    public final static String SERVICE_SEND_GRID = "sndgrd";
    public final static String SEND_GRID_TO = "sndgrdTo";
    public final static String SEND_GRID_FROM = "sndgrdFr";
    public final static String SEND_GRID_REPLY = "sndgrdRp";
    public final static String SEND_GRID_SUBJECT = "sndgrdSb";
    public final static String SEND_GRID_SUFFIX = "sndgrdSfx";
    public final static String SEND_GRID_SUFFIX_TEXT = "sndgrdSfxT";
    public final static String SEND_GRID_CONTENT = "sndgrdMs";
    public final static String SEND_GRID_CATEGGORY = "sndgrdCt";

    //parameters
    public final static String STUDENT_KEY_PARAM = "studks";
    public final static String TO_ADDR_PARAM = "toAddy";
    public final static String MSG_BODY_PARAM = "msgCntent";  
    public final static String MSG_SUBJECT_PARAM = "msgSubject";
    public final static String MSG_CONTROLLER_KEY_PARAM = "mcks";
    public final static String CMT_PUBLIC = "cmtpub";
    public final static String LOAN_ID_PARAM = "loanid";
    public final static String UPDATE_USR_PARAM = "upsr";
    public final static String EXCEL_DOWN_KEY_PARAM = "excl";   
    
    //task delay constant
    private final static int MINUTE_MILLISECONDS = 1000 * 60; //1000milliseconds * 60seconds
    private final static int MIN_TASK_DELAY = 5 * MINUTE_MILLISECONDS; //5 minute minimum
    private final static int MAX_TASK_DELAY = 60 * MINUTE_MILLISECONDS; //1 hour maximum
    private final static int DEV_TASK_DELAY = 1000 * 30; //30 seconds
    public final static long TASK_DELAY; // =
    static
    {
    	if(SystemProperty.environment.value() == SystemProperty.Environment.Value.Production)
    		TASK_DELAY =  new Random().nextInt(MAX_TASK_DELAY - MIN_TASK_DELAY) + MIN_TASK_DELAY;
    	else
    		TASK_DELAY = DEV_TASK_DELAY;
    }
}
