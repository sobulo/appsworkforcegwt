/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.sap.shared;



/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginExpiredException extends Exception{
    public LoginExpiredException(){}

    public LoginExpiredException(String s)
    {
        super(s);
    }
}
