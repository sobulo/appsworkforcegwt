package com.fertiletech.sap.shared;

public enum  CourseStatus {
    SELF_PACED, ARCHIVED
}
