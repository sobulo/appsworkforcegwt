/**
 * 
 */
package com.fertiletech.sap.shared;

import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class DTOConstants {

    public final static String DEPRECATED = "deprecated";

	public final static String GOOGLE_CLOUD_LOGOUT = "http://accounts.google.com/Logout";
    public final static String YAHOO_CLOUD_LOGOUT = "http://login.yahoo.com/config/login?logout=1";	
	public final static String GCS_LOAN_ID_PARAM = "com.fertiletech.gcsloan";
	public final static String GCS_FILE_NAME_PARAM = "com.fertiletech.gcsfilename";
    public final static int TM_DIFF_HEADER = 0;
    public final static int TM_DIFF_VALS = 1;
    public final static int TM_DIFF_POS = 2;
    
    public final static String APP_PARAM_ADMINS = "user-admin";
    public final static String APP_PARAM_EDITOR = "user-editor";
    public final static String APP_PARAM_REVIEWER = "user-reviewer";
    public final static String APP_PARAM_MAP_TITLES = "map-titles";
    
	public final static int CATEGORY_IDX = 0;
	public final static int DETAIL_IDX = 1;
	public final static int LAT_IDX = 0;
	public final static int LNG_IDX = 1;
	
	public final static int MSG_TXT_CONTENT_IDX = 0;
	public final static int MSG_TXT_CONTENT_HTML_IDX = 1;
	public final static int MSG_TEXT_USER_IDX = 2;
	public final static int MSG_DATE_MODE_IDX = 0;

	public final static HashMap<String, String> BILL_DESC_MAP = new HashMap<String, String>();
    public final static HashMap<String, Integer> BILL_DESC_HUSTLE_MAP = new HashMap<String, Integer>();
	public final static String LICENSE_ANNUAL = "Annual License";
	public final static String LICENSE_WORKFORCE = "Workforce License";
	public final static String LICENSE_MONTHLY = "Monthly License";

	public final static HashSet<String> CURRENT_LICENCE = new HashSet<String>();
	//strictly online
    public final static String LICENSE_STRICT_ANNUAL = "Annual License (Strictly Online)";
    public final static String LICENSE_STRICT_WORKFORCE = "Workforce License (Strictly Online)";
    public final static String LICENSE_STRICT_TRIAL = "1 Month Evaluation License (Strictly Online)";

    public final static String LICENSE_WS_ANNUAL = "Annual License (2018 Work-Study Edition)";
    public final static String LICENSE_WS_WORKFORCE = "Workforce License (2018 Work-Study Edition)";
    public final static String LICENSE_WS_TRIAL = "1 Month Evaluation License (2018 Work-Study Edition)";

    public final static String LICENSE_ED_ANNUAL = "Annual License (Education Edition)";
    public final static String LICENSE_ED_WORKFORCE = "Workforce License (Education Edition)";
    public final static String LICENSE_ED_TRIAL = "1 Month Evaluation License (Education Edition)";


    public final static String HUSTLE = "HCM";

	static {
        BILL_DESC_MAP.put("Annual License", LICENSE_ANNUAL);
        BILL_DESC_MAP.put("Annual License (Early Bird)", LICENSE_ANNUAL);
        BILL_DESC_MAP.put("Workforce License", LICENSE_WORKFORCE);
        BILL_DESC_MAP.put("Workforce License (Early Bird)", LICENSE_WORKFORCE);
        BILL_DESC_MAP.put("Monthly License", LICENSE_MONTHLY);
        BILL_DESC_MAP.put(LICENSE_STRICT_ANNUAL, LICENSE_ANNUAL);
        BILL_DESC_MAP.put(LICENSE_STRICT_WORKFORCE, LICENSE_WORKFORCE);
        BILL_DESC_MAP.put(LICENSE_STRICT_TRIAL, LICENSE_MONTHLY);

        BILL_DESC_MAP.put(LICENSE_ED_ANNUAL, LICENSE_ANNUAL);
        BILL_DESC_MAP.put(LICENSE_ED_WORKFORCE, LICENSE_WORKFORCE);
        BILL_DESC_MAP.put(LICENSE_ED_TRIAL, LICENSE_MONTHLY);

        BILL_DESC_MAP.put(LICENSE_WS_ANNUAL, LICENSE_ANNUAL);
        BILL_DESC_MAP.put(LICENSE_WS_WORKFORCE, LICENSE_WORKFORCE);
        BILL_DESC_MAP.put(LICENSE_WS_TRIAL, LICENSE_MONTHLY);

        CURRENT_LICENCE.add(LICENSE_ED_TRIAL);
        CURRENT_LICENCE.add(LICENSE_ED_ANNUAL);
        CURRENT_LICENCE.add(LICENSE_ED_WORKFORCE);
    }

    static {
        BILL_DESC_HUSTLE_MAP.put("Annual License", 28);
        BILL_DESC_HUSTLE_MAP.put("Annual License (Early Bird)",28);
        BILL_DESC_HUSTLE_MAP.put("Workforce License", 56);
        BILL_DESC_HUSTLE_MAP.put("Workforce License (Early Bird)", 56);
        BILL_DESC_HUSTLE_MAP.put("Monthly License", 5);
        BILL_DESC_HUSTLE_MAP.put(LICENSE_STRICT_ANNUAL, 139);
        BILL_DESC_HUSTLE_MAP.put(LICENSE_STRICT_WORKFORCE,278);
        BILL_DESC_HUSTLE_MAP.put(LICENSE_STRICT_TRIAL, 6);

        BILL_DESC_HUSTLE_MAP.put(LICENSE_ED_ANNUAL, 2500);
        BILL_DESC_HUSTLE_MAP.put(LICENSE_ED_WORKFORCE, 5000);
        BILL_DESC_HUSTLE_MAP.put(LICENSE_ED_TRIAL, 1);

        BILL_DESC_HUSTLE_MAP.put(LICENSE_WS_ANNUAL, 556);
        BILL_DESC_HUSTLE_MAP.put(LICENSE_WS_WORKFORCE, 1112);
        BILL_DESC_HUSTLE_MAP.put(LICENSE_WS_TRIAL, 28);

    }

    public static String getDefaultLicenseHC(String licenseType)
    {
        if(licenseType == null)
            return "Improper access, license type is null";

        Integer hc = BILL_DESC_HUSTLE_MAP.get(licenseType);
        if( hc == null)
            return "Oops, contact hello@appsworkforce.com. HCM improperly configured";
        else return hc + " HC points";

    }

    //products and services
    public final static HashMap<String, Integer> AW_SERVICES = new HashMap<>();
    public final static HashMap<String, Integer> AW_PRODUCTS = new HashMap<>();
    public final static HashMap<String, Integer> AW_REQUESTS = new HashMap<>();
    public final static HashMap<String, Integer> AW_TOOLS = new HashMap<>();
    public final static HashMap<String, Integer> AW_PROJECTS = new HashMap<>();
    public final static HashMap<String, Integer> AW_LEARN = new HashMap<>();

    public final static String AW_JAVA_SHIRT_KEY = "White Java T-Shirt";
    public final static String AW_PYTHON_SHIRT_KEY = "Black Pyhton T-Shirt";
    public final static String AW_PIXEL_PHONE_KEY = "Black Google Pixel2 phone";
    public final static String AW_BOSE_WIRE_KEY = "Bose Wireless Headphones";
    public final static String AW_HUAWEI_WATCH_KEY = "Huawei Watch 2 - Android";
    public final static String AW_LEATHER_BAG_KEY = "Leather Laptop Bag";
    public final static String AW_MAC_BOOK_KEY = "Apple 15\" MacBook Pro, Retina";
    public final static String AW_MOOC_BOOK_KEY = "MOOCS Book";
    public final static String AW_DELL_LAPTOP_KEY = "Dell Inspiron 15.6\" HD Laptop";
    public final static String AW_MOTOROLA_PHONE_KEY = "Motorola Moto C Plus Phone";

    //public final static String AW_REQUEST_LETTER = "Discretionary recommendation letter";

    public final static String AW_PROJECT_TIPP = "Team Project";
    public final static String AW_PROJECT_VIP = "Individual Project";
    //bill template list
    public final static  HashSet<String> CURRENT_TEMPLATES = new HashSet<>();

    private static void updateTemplateLiist(HashMap<String, Integer> sourceMap)
    {
        //Window.alert("calling with size: " + sourceMap.size() + " null? " + CURRENT_TEMPLATES);
        CURRENT_TEMPLATES.addAll(sourceMap.keySet());
        //Window.alert("add ok :" + CURRENT_TEMPLATES.size());
    }

    //bill template list
    public final static  HashMap<String, Integer> CURRENT_HUSTLE = new HashMap<>();

    static
    {

        //mall for africa
        AW_PRODUCTS.put(AW_MOOC_BOOK_KEY, 25); //25HC Jonathan Haber
        AW_PRODUCTS.put(AW_MOTOROLA_PHONE_KEY, 200); //
        AW_PRODUCTS.put(AW_DELL_LAPTOP_KEY, 500); //
        //AW_PRODUCTS.put(AW_BOSE_WIRE_KEY, 400); //400 HC QuietComfort Series(I) noise cancelling Silver
        //AW_PRODUCTS.put(AW_HUAWEI_WATCH_KEY, 325); //Classic smart watch 325 HC
        //AW_PRODUCTS.put(AW_PIXEL_PHONE_KEY, 825); //825 HC 64GB 5" Android
        //AW_PRODUCTS.put(AW_MAC_BOOK_KEY, 3000); // 3000 HC 2.9GHz Intel Core i7 Quad Core, 16GB RAM, 512GB SSD
        //AW_PRODUCTS.put(AW_LEATHER_BAG_KEY, 125); // 125 HC office Buffalo leather laptop bag

        //products
       // AW_PRODUCTS.put(AW_JAVA_SHIRT_KEY, 15); //15HC
        //AW_PRODUCTS.put(AW_PYTHON_SHIRT_KEY, 15); //15HC



        updateTemplateLiist(AW_PRODUCTS);
        CURRENT_HUSTLE.putAll(AW_PRODUCTS);

        //services - representation cell table
        //AW_SERVICES.put("Mentorship", 100); //100HC/hour
        AW_SERVICES.put("360 Reviews", 150); //150 HC/review cycle
        AW_SERVICES.put("Resume/CV Review", 50); //50HC/hour
        AW_SERVICES.put("Mock interviews", 200); //200HC/hour
        updateTemplateLiist(AW_SERVICES);
        CURRENT_HUSTLE.putAll(AW_SERVICES);

        //Tools - representation cell table
        AW_TOOLS.put("G-Suite AppsWorkforce.com account", 120); //50HC
        //AW_TOOLS.put("G-Suite Time Management", 50); //calendar 50HC
        //AW_TOOLS.put("G-Suite Project Management", 50); //sheets 50HC
        AW_TOOLS.put("G-Suite Project Proposals", 50); //docs 50HC
        AW_TOOLS.put("G-Suite Project Presentation", 50); //presentation 50HC
        updateTemplateLiist(AW_TOOLS);
        CURRENT_HUSTLE.putAll(AW_TOOLS);

        //requests - representation cell table

        //AW_REQUESTS.put("Course Verificaction", 50); //50HC per verification
        AW_REQUESTS.put("Career Services", 100); //100HC/hour
        updateTemplateLiist(AW_REQUESTS);
        CURRENT_HUSTLE.putAll(AW_REQUESTS);

        //AW_REQUESTS.put("Course Verificaction", 50); //50HC per verification
        AW_LEARN.put("MOOC Support", 100); //100HC/hour
        //AW_LEARN.put("Course Verificaction", 50); //50HC per verification
        updateTemplateLiist(AW_LEARN);
        CURRENT_HUSTLE.putAll(AW_LEARN);



        /// spills in display so isolated from request group above 1HC/letter
        //CURRENT_HUSTLE.put(AW_REQUEST_LETTER, 1);

        //projects
        AW_PROJECTS.put(AW_PROJECT_TIPP, 50); //50HC/supervised hour
        AW_PROJECTS.put(AW_PROJECT_VIP, 50); //50HC/supervised hour
        updateTemplateLiist(AW_PROJECTS);
        CURRENT_HUSTLE.putAll(AW_PROJECTS);



    }




	public static String getLicenseType(String description)
    {
        return BILL_DESC_MAP.get(description);
    }

    

    //fields for displaying sales leads to users
    
    public final static int PUB_LAB_IDX = 0;
    public final static int PUB_MODULE_IDX = 1;
    public final static int PUB_PLACEMENT_IDX = 2;
    public final static int PUB_AVAIL_IDX = 3;
    public final static int PUB_STATUS_IDX = 4;
    public final static int PUB_MSG_IDX = 5;    
    public final static int PUB_CRT_IDX = 0;
    public final static int PUB_MOD_IDX = 1;
     
	//text indices
	public final static int FULL_NAME_IDX = 0;
	public final static int EMAIL_IDX = 1;
	public final static int PHONE_NUMBER_IDX = 2;
	public final static int STATE_IDX = 3;
	public final static int NOTE_IDX = 4;
	
	
	//number indices
    public final static int LEAD_ID_IDX = 0;
	public final static int OLEV1_IDX = 1;
	public final static int OLEV2_IDX = 2;
	public final static int DIPL_GPA_IDX = 1;
	public final static int DIPL_MAX_GPA_IDX = 2;
    
    //date indices
    public final static int DATE_CREATED_IDX = 0;
    public final static int DATE_MODIFIED_IDX = 1;
    public final static int DATE_LICENCE_STARTS_IDX = 2;
    public final static int DATE_LICENSE_ENDS_IDX = 3;
    //------------------------------------------------ 
    
    //ID indices
    public final static int LOAN_KEY_IDX = 0;
    public final static int LOAN_STATUS_IDX = 1;
    public final static int LOAN_MESSAGE_IDX = 2;
    
    //below should be deprecated
    public final static int FORM_KEY_IDX = 1;
    public final static int SUBMITTED_KEY_IDX = 2; //only the form not ack objects etc 
    
    public final static int LOAN_IDX = 0;
    public final static int LOAN_ID_IDX = 1;
    public final static int LOAN_STATE_IDX = 2;
    public final static int LOAN_EDIT_IDX = 3;
	public static final String NPMB_FORM_HEADER = "appsworkforce-header-pdf";
	public static final String NPMB_FORM_LOGO = "appsworkforce-logo-pdf";
	public static final String PDF_NO_IMAGE = "passportbox";

	//script parameters
    public final static String BILL_DESC_KEY_PARAM = "btks";
    public final static String BILL__KEY_PARAM = "blks";

    public final static int SCHOOL_INFO_NAME_IDX = 0;
    public final static int SCHOOL_INFO_ADDR_IDX = 1;
    public final static int SCHOOL_INFO_NUMS_IDX = 2;
    public final static int SCHOOL_INFO_EMAIL_IDX = 3;
    public final static int SCHOOL_INFO_WEB_IDX = 4;
    public final static int SCHOOL_INFO_ACCR_IDX = 5;


    public final static String[] getSchoolInfo()
    {
        String[] result = new String[6];
        result[SCHOOL_INFO_NAME_IDX] = "AppsWorkforce";
        result[SCHOOL_INFO_ADDR_IDX] = "Strictly Online!";
        result[SCHOOL_INFO_NUMS_IDX] = "0906-176-6041";
        result[SCHOOL_INFO_EMAIL_IDX] = "hello@appsworkforce.com";
        result[SCHOOL_INFO_WEB_IDX] = "https://www.appsworkforce.com";
        result[SCHOOL_INFO_ACCR_IDX] = "AW";
        return result;
    }

    public static int BTD_NAME_TXT_IDX=0;
    public static int BTD_AMT_NUM_IDX = 0;
    public static int BTD_BANK_IDX=3;
    public static int BTD_ACC_NAME_IDX = 4;
    public static int BTD_ACC_NUM_IDX=5;
    public static int BTD_ACC_PAY_INSTR_IDX =6;

    public static int BTD_ITM_TYPE_TXT_IDX = 0;
    public static int BTD_ITM_DESC_TXT_IDX = 1;
    public static int BTD_ITM_AMT_HDR_TXT_IDX = 2;
    public static int BTD_ITM_AMT_NUM_IDX = 0;

    public final static int PAY_EMAIL_TXT_IDX  = 0;
    public final static int PAY_NAME_TXT_IDX=1;
    public final static int PAY_AMT_NUM_IDX=0;
    public final static int PAY_DUE_DATE_IDX=0;
    public final static int PAY_VERIFIED_TXT_IDX = 2;
    public final static int PAY_COMMENTS_TXT_IDX = 3;
    public final static int PAY_REF_TXT_IDX = 4;
    public final static int PAY_VUSR_TXT_IDX = 5;
    public final static int PAY_VDT_DATE_IDX=1;

    public final static String USER_SUGGEST_DELIMITER = "%";



    public static TableMessageHeader getPayDetailsHeader()
    {
        TableMessageHeader h = new TableMessageHeader(9);
        h.setText(0, "Email", TableMessageHeader.TableMessageContent.TEXT);
        h.setText(1, "Bill Name", TableMessageContent.TEXT);
        h.setText(2, "Amount", TableMessageContent.NUMBER);
        h.setText(3, "Due Date", TableMessageContent.DATE);
        h.setText(4, "Verified?", TableMessageContent.TEXT);
        h.setText(5, "Reference", TableMessageContent.TEXT);
        h.setText(6, "Comments", TableMessageContent.TEXT);
        h.setText(7,"Ver-User", TableMessageContent.TEXT);
        h.setText(8,"Ver-Date", TableMessageContent.DATE);
        return h;
    }
    public final static int STS_NOT_SUBMIT_IDX = 0;
    public final static int STS_SUBMIT_IDX = 1;
    public final static int STS_MONTHLY_IDX = 2;
    public final static int STS_ANNUAL_IDX = 3;
    public final static int STS_WORKFORCE_IDX = 4;
    public final static int STS_NOT_SUBMIT_MONTHLY_IDX = 5;
    public final static int STS_NOT_SUBMIT_ANNUAL_IDX = 6;
    public final static int STS_NOT_SUBMIT_WORKFORCE_IDX = 7;



    public final static int STATS_HEADERS_IDX = 0;
    public final static int STATS_NUMBERS_IDX = 1;
    public final static int STATS_COLORS_IDX = 2;

    public static TableMessage getStatusChartHeaders()
    {
        TableMessage h = new TableMessage(8, 0, 0);
        h.setText(STS_NOT_SUBMIT_IDX, "Not Registered");
        h.setText(STS_SUBMIT_IDX, "Registered");
        h.setText(STS_MONTHLY_IDX, "Monthly");
        h.setText(STS_ANNUAL_IDX, "Annual");
        h.setText(STS_WORKFORCE_IDX, "Workforce");
        h.setText(STS_NOT_SUBMIT_MONTHLY_IDX, "0 Reg Monthly");
        h.setText(STS_NOT_SUBMIT_ANNUAL_IDX, "0 Reg Annual");
        h.setText(STS_NOT_SUBMIT_WORKFORCE_IDX, "0 Reg Workforce");
        return h;
    }

    public static TableMessage getStatusColors()
    {
        TableMessage h = new TableMessage(8, 0, 0);
        h.setText(STS_NOT_SUBMIT_IDX, "color: yellow");
        h.setText(STS_SUBMIT_IDX, "color: pink");
        h.setText(STS_MONTHLY_IDX, "color: blue");
        h.setText(STS_ANNUAL_IDX, "color: green");
        h.setText(STS_WORKFORCE_IDX, "color: orange");
        h.setText(STS_NOT_SUBMIT_MONTHLY_IDX, "color: grey");
        h.setText(STS_NOT_SUBMIT_ANNUAL_IDX, "color: black");
        h.setText(STS_NOT_SUBMIT_WORKFORCE_IDX, "color: red");
        return h;
    }

    //course dto
    public static int COURSE_PLATFORM_TXT_IDX = 0;
    public static int COURSE_CATEGORY_TXT_IDX = 1;
    public static int COURSE_INST_ACCRONYM_TXT_IDX = 2;
    public static int COURSE_NAME_TXT_IDX = 3;
    public static int COURSE_STATUS_TXT_IDX = 4;
    public static int COURSE_LINK_TXT_IDX = 5;
    public static int COURSE_ENROLL_TXT_IDX = 6;
    public static int COURSE_PROF_TXT_IDX = 7;
    public static int COURSE_INSTITUTION_TXT_IDX = 8;
    public static int COURSE_FORUM_LINK_TXT_IDX = 9;
    public static int COURSE_ORDER_NUM_IDX = 0;




    //course tracker dto
    public static int TRACK_COURSE_TXT_IDX = 0;
    public static int TRACK_GRADE_TXT_IDX = 1; //no rating, i no sabi, i am capable
    public static int TRACK_VERIFY_TXT_IDX = 2;
    public static int TRACK_MEMBER_TXT_IDX = 3;
    public static int TRACK_STATUS_COUNT_NUM_IDX = 0;
    public static int TRACK_FEED_COUNT_NUM_IDX = 1;
    public static int TRACK_PROGRESS_NUM_IDX = 2;
    public static int TRACK_PRIVATE_PROGRESS_NUM_IDX = 3;
    public static int TRACK_MOD_DATE_IDX = 0;
    

    ///tracker details dto
    public static int FEED_TRACK_DATE_IDX = 0;
    public static int FEED_TRACK_POS_NUM_IDX = 0;
    //public static int FEED_TRACK_TRAIL_TEXT_IDX = 0;
    //public static int FEED_TRACK_PREV_TEXT_IDX = 1;


    ///feed details dto
    public static int FEED_BACK_DATE_IDX = 0;
    public static int FEED_BACK_POS_NUM_IDX = 0;
    //public static int FEED_BACK_TRAIL_TEXT_IDX = 0;
    //public static int FEED_BACK_PREV_TEXT_IDX = 1;

    //appendix details dtp
    public final static int FEED_APPENDIX_COURSE_TXT_IDX = 0;
    public final static int FEED_APPENDIX_EMAIL_TXT_IDX = 1;

    public static TableMessageHeader getFeedTrackHeader()
    {
        TableMessageHeader h = new TableMessageHeader(2);
        h.setText(0, "ID", TableMessageContent.NUMBER);
        h.setText(1, "Date", TableMessageContent.DATE);
        //h.setText(2, "Trail", TableMessageContent.TEXT);
        //h.setText(3, "Preview", TableMessageContent.TEXT);
        h.setFormatOption(0, "###");
        h.setTimeOption(1);
        return h;

    }

    public static int COURSE_TRACK_LIST_IDX = 0;
    public static int COURSE_FEED_LIST_IDX = 1;
    public static int COURSE_APPENDIX_LIST_IDX = 2;


    public static TableMessageHeader getTrackHeader(boolean includeBio)
    {
        int headerSize = includeBio?8:7;

        TableMessageHeader h = new TableMessageHeader(headerSize);
        h.setText(0, "Course", TableMessageContent.TEXT);
        h.setText(1, "#U", TableMessageContent.NUMBER);
        h.setText(2, "#R", TableMessageContent.NUMBER);
        h.setText(3, "AI", TableMessageContent.NUMBER);
        h.setText(4, "SG", TableMessageContent.TEXT);
        h.setText(5, "VR", TableMessageContent.TEXT);
        h.setText(6, "Modified", TableMessageContent.DATE);
        if(includeBio)
            h.setText(7, "Member", TableMessageContent.TEXT);
        h.setFormatOption(1, "###");
        h.setFormatOption(2, "###");
        h.setFormatOption(3, "###");
        return h;
    }

    public static String getPanelAddress(String target)
    {
        return "https://www.appsworkforce.com/lab/#" + target;
    }
}
