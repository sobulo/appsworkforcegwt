package com.fertiletech.sap.shared;

import com.fertiletech.sap.shared.TableMessageHeader.TableMessageContent;

import java.util.HashMap;


public class NPMBFormConstants {
	
	//personal fields
	public static String ID = "AW-Portal-ID";
	public static String ID_NUM = "AW-Portal-NO";
	public static String ID_STATE = "AW-State";
	public static String ID_EDIT = "AW-Write";
	public static String SURNAME = "Surname"; //k
	public static String FIRST_NAME = "First Name"; //k
	public static String MIDDLE_NAME = "Middle Name"; //k
	//public static String ADDRESS = "Address";  //k
	public static String TEL_NO = "Tel No";  //k
	//public static String MOBILE_NO = "Alternate No"; //k
	public static String EMAIL = "Personal Email";
	public static String DATE_OF_BIRTH = "Date of Birth"; //k
	//public static String PLACE_OF_BIRTH = "Place of Birth"; //k
	public static String STATE_ADDRESS = "State"; //k
	public static String LGA_ADDRESS = "L.G.A";
	//public static String NATIONALITY = "Nationality";	//k
	public static String STREET_ADDRESS = "Residential Address"; //k
	public static String DATE_OF_REGISTRATION = "Registration Date";
	

	
	//public static String NO_OF_CHILDREN = "Children/Other Dependents";
	//public static String NO_OF_PARENTS = "Parents/Guardians"; //needs to be added



	private final static String CONDUCT = "http://about.appsworkforce.com/learn/code-of-conduct";
	private final static String REQUIREMENT = "http://about.appsworkforce.com/learn/requirements";
	
	//declaration/signature by user upon completion
	public static String DECLARATION = "I hereby declare that the information provided in this registration form is accurate and properly identifies me. " +
			"I agree that any material misstatement/falsification of information on this form renders my registration null/void";

	public static String PDF_DECLARATION = "I hereby declare that the information provided in this registration form is accurate and properly identifies me.";
	public static String PDF_DECLARATION_2 = "I agree that any material misstatement/falsification of information on " +
			"this form renders my registration null/void.";
	
	public static int MOOC_IDX = 0;
	public final static String PENDING_VERIFICATION = "Pending";

	
	public static String[] TABLE_DESCRIPTIONS = {"MOOCs/Online Courses",};
	
	public final static String MOOC_LIST = "Other,Alison,Carnegie Mellon OLI,Coursera,EdX,MIT Open CourseWare,Udacity";

	public final static String FILE_TYPE_PASSPORT = "Identification Photo";
	public final static String FILE_TYPE_EDIT_CV = "Resume Revision";
	public final static String FILE_LIST_PASSPORT[] = {FILE_TYPE_PASSPORT};
	public final static String FILE_LIST_CV[] = {"cv ms word", "resume pdf"};
	public final static String[] FILE_TYPES = {FILE_TYPE_PASSPORT, "Resume/CV", "Transcripts/Report Card", "A-Levels", "O-Levels"};
	public final static String[] O_LEV_RESULT_LIST_OLD = {"Less than 5 credits", "Up to 5 credits", "B in Math + 4 or more credits", "A in Math + 4 or more credits", "Other"};
	

	
	//flex panel headers and widths
	public static String[][] MOOC_TABLE_CFG = {{"Course Code/Title", "MOOC Provider", "Registration Date", "Grade"},
											   {"35", "25", "20", "20"}, 
											   {"course", "courses"},
											   {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString()},
											   {null,MOOC_LIST,null,null}};
	public static String[][][] ALL_CONFIG = {MOOC_TABLE_CFG,};
	
	//form validators for welcome page
	public static HashMap<String, FormValidator[]> WELCOME_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_PERSONAL_VALIDATORS = new HashMap<String, FormValidator[]>();

	static
	{
    	FormValidator[] mandatoryOnly = {FormValidator.MANDATORY};
    	FormValidator[] emailOnly = {FormValidator.EMAIL};
    	FormValidator[] phoneOnly = {FormValidator.PHONE};
    	FormValidator[] mandatoryPlusPhone = {FormValidator.MANDATORY, FormValidator.PHONE};
    	FormValidator[] mandatoryPlusEmail = {FormValidator.MANDATORY, FormValidator.EMAIL};
    	FormValidator[] mandatoryPlusParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_INTEGER};
    	FormValidator[] dateParsesOk = {FormValidator.DATE_FORMAT_OK};
    	FormValidator[] mandatoryPlusFutureDateOk = {FormValidator.MANDATORY, FormValidator.DATE_IS_FUTURE};    	
    	FormValidator[] mandatoryPlusLoanParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_DOUBLE};
    	FormValidator[] decimalParsesOk = {FormValidator.PARSES_DOUBLE};	
    	FormValidator[] atLeastOneParsesOk = {FormValidator.PARSES_INTEGER_1};		
    	FormValidator[] atLeastFiveParsesOk = {FormValidator.PARSES_INTEGER_5};		

    	
    	//welcome page
    	WELCOME_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	WELCOME_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	WELCOME_VALIDATORS.put(SURNAME, mandatoryOnly);
    	WELCOME_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	
    	//personal validators
    	NPMB_PERSONAL_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	NPMB_PERSONAL_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	NPMB_PERSONAL_VALIDATORS.put(SURNAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(STREET_ADDRESS, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(DATE_OF_BIRTH, dateParsesOk);

	}
	
	public static Boolean[] getSubmissionValues(TableMessage result)
	{
		Boolean appSubmitted, guard1Submitted, guard2Submitted;
		
		long subVal = Math.round(result.getNumber(DTOConstants.LOAN_KEY_IDX));
		if( subVal == 1)
			appSubmitted = true;
		else 
			appSubmitted = false;
		
		Boolean[] subs = new Boolean[1];
		subs[0] = appSubmitted;
		return subs;
	}

	public static String[] getOlevelRankings()
	{
		String[] result = {
				"A in Math, A in English",
				"A in Math, B in English",
				"A in Math, C in English",
				"B in Math, A in English",
				"B in Math, B in English",
				"B in Math, C in English",
				"C in Math, A in English",
				"C in Math, B in English",
				"C in Math, C in English"
		};
		return  result;
	}
	

}
