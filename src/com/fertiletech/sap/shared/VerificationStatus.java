package com.fertiletech.sap.shared;

public enum VerificationStatus {
    NOT_VERIFIED, PENDING_VERIFICATION, FRAUDELENT_VERIFICATION, FAILED_VERIFICATION, PASSED_VERIFICATION;
	
	public String getDisplayString()
	{
		switch (this) {
		case NOT_VERIFIED:
			return "NV";
		case PENDING_VERIFICATION:
			return "PV";
		case FRAUDELENT_VERIFICATION:
			return "ZTF";
		case FAILED_VERIFICATION:
			return "FV";
		case PASSED_VERIFICATION:
			return "ZTV";
		}
		return "Unsupported status: " + this.toString();
	}
	
	public static String getLegend()
	{
		StringBuilder result = new StringBuilder("Verification -- ");
		for(VerificationStatus vs : VerificationStatus.values())
			result.append(getVSLengend(vs));
		
		return result.toString();
	}
	
	private static String getVSLengend(VerificationStatus vs)
	{
		return "[" + vs.getDisplayString() + ": <i>" + vs.toString().replace('_', ' ').toLowerCase() + "</i>] ";
	}
	
	public static String getSelfGradeLegendHack()
	{
		return "Self Grade -- [NR: <i>No rating</i>] [NTL: <i>I need more tme to learn<i>] [UTM: <i>I understand the course material</i>]";
	}
}
