package com.fertiletech.sap.shared;

import java.util.HashSet;

public enum WorkflowStateInstance {
	APPLICATION_EDUCATION, APPLICATION_FAMILY, //deprecated fields
	APPLICATION_STARTED, APPLICATION_PERSONAL,
	APPLICATION_CONFIRM, APPLICATION_PENDING,
	APPLICATION_CANCEL, APPLICATION_DENIED, 
	APPLICATION_APPROVED;
	
	private static HashSet<WorkflowStateInstance> APPROVED_STATES = new HashSet<WorkflowStateInstance>();
	private static HashSet<WorkflowStateInstance> DENIED_STATES = new HashSet<WorkflowStateInstance>();
	
	static
	{
		APPROVED_STATES.add(APPLICATION_APPROVED);
		DENIED_STATES.add(APPLICATION_DENIED);
	}
	
	public Boolean isApprovedState()
	{
		if(APPROVED_STATES.contains(this))
			return true;
		else if(DENIED_STATES.contains(this))
			return false;
		else
			return null;
	}

	public String getDisplayString()
	{
		switch (this) {
		case APPLICATION_APPROVED:
			return "Admission Approved";
		case APPLICATION_CANCEL:
			return "Canceled";
		case APPLICATION_CONFIRM:
			return "Signature Page";
		case APPLICATION_DENIED:
			return "Admission Denied";
		case APPLICATION_PENDING:
			return "Under Review";
		case APPLICATION_PERSONAL:
			return "Personal Page";
		case APPLICATION_STARTED:
			return "Start Page";
			case APPLICATION_EDUCATION:
				return "Start Page (ED)";
			case APPLICATION_FAMILY:
				return "Start Page (FD)";
		}
		return "Unsupported state: " + this.toString();
	}
	
}
