/**
 * 
 */
package com.fertiletech.sap.apply.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface GeneralImageBundle extends ClientBundle{
	
	@Source("logo.png")
	ImageResource logoSmall();
	
	
	ImageResource banner3();
	
	@Source("aw-code-count.png")
	ImageResource awCount();
}
