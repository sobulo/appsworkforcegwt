package com.fertiletech.sap.apply;

/*
 * #%L
 * GwtBootstrap3
 * %%
 * Copyright (C) 2013 GwtBootstrap3
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * @author Joshua Godi
 */
public class NameTokens {
    // General Pages
   public static final String WELCOME = "school";
   // public static final String APPLY = "apply";
    //public static final String PRINT = "print";
    //public static final String UPLOAD = "attach";
    //public static final String STATUS = "status";
    //public static final String CALCULATOR = "calculator";
    //public static final String MARKET = "market";
    //public static final String CONDUCT = "conduct";
    //public static final String CURRICULUM = "curriculum";
    public static final String IQ = "questions";
	public static final String SCHOOL = "member";
	public static final String BIO = "bio";
	//public static final String BILL_HISTORY="invoicehistory";
	//public static final String BILL_CHOICE = "joinus";
	//public static final String BILL_DOWNLOAD="downloadinvoice";
	//public static final String BILL_PAYMENT="ihavepaid";
	//public static final String LICENSE_MONTHLY="monthly";
	//public static final String LICENSE_ANNUAL="annual";
	//public static final String LICENSE_WORKFORCE="workforce";

	public static final String MOOC_ADVISE = "start";
	public static final String MOOC_TRACK="continue";
	public static final String MOOC_LEARN="moocs";

	//public static final String CAREER_HELPER="earn";
	//public static final String SEMINAR_HELPER="seminars";
	//public static final String COMPETION_HELPER="fun";
	//public static final String REQUEST_HELPER="requests";
	//public static final String PROJECTS_HELPER="projects";
	//public static final String STUDY_CENTER ="ace";


	private final static String URL1 = "http://about.appsworkforce.com/home/what-is-appsworkforce";
    private final static String URL2 = "http://about.appsworkforce.com/learn";
    private final static String URL3 = "http://about.appsworkforce.com/learn/code-of-conduct";
    private final static String URL4 = "http://about.appsworkforce.com/learn/membership-benefits";
    private final static String URL5 = "http://about.appsworkforce.com/learn/requirements";
	private final static String URL6 = "http://about.appsworkforce.com/news-update/appsworkforcestrictlyonline";


	private final static String EARN_IT = "http://about.appsworkforce.com/earn/industrial-training-it";
	private final static String EARN_WS = "http://about.appsworkforce.com/earn/recruiting-associates";
	private final static String EARN_WS2 = "http://about.appsworkforce.com/earn/work-study";
	private final static String EARN_TA = "http://about.appsworkforce.com/earn/teaching-assistant";
	private final static String EARN_JB = "http://about.appsworkforce.com/earn/jamb-job-acceptance-matching-board";
	private final static String EARN_MB = "http://about.appsworkforce.com/earn/jamb---junior-analyst-mailbox";
	private final static String VID_EARN = "http://about.appsworkforce.com/earn/";
	private final static String VID_LEARN = "http://about.appsworkforce.com/learn";
	private final static String VID_FUN = "http://about.appsworkforce.com/fun";

	public static String earnit() {
		return EARN_IT;
	}
	public static String earnws() { return EARN_WS; }
	public static String earnws2() { return EARN_WS2; }
	public static String earnta() { return EARN_TA; }
	public static String earnjb() {
		return EARN_JB;
	}
	public static String earnmb() {
		return EARN_MB;
	}


	public static String getUrl1() {
		return URL1;
	}

	public static String getUrl2() {
		return URL2;
	}

	public static String getUrl3() {
		return URL3;
	}

	public static String getUrl5() {
		return URL5;
	}


	//TODO review links
	public static String getUrl4() {
		return URL4;
	}
	public static String getUrl6() {
		return URL6;
	}

	//vid links
	public static String getVidEarnUrl() {
		return VID_EARN;
	}

	public static String getVidLearnUrl() {
		return VID_LEARN;
	}

	public static String getVidFunUrl() {
		return VID_FUN;
	}



	public static String getWelcome() {
		return WELCOME;
	}

	public final static String URL_PREFIX = "https://www.appsworkforce.com/lab/#";
	public static String getWelcomeUrl(){return URL_PREFIX + WELCOME;}




	//begin org.apache.commons.lang3.text; -- no gwt support for lib
	public static String capitalize(String str) {
		return capitalize(str, (char[])null);
	}

	private static String capitalize(String str, char... delimiters) {
		int delimLen = delimiters == null ? -1 : delimiters.length;
		if (!isEmpty(str) && delimLen != 0) {
			char[] buffer = str.toCharArray();
			boolean capitalizeNext = true;

			for(int i = 0; i < buffer.length; ++i) {
				char ch = buffer[i];
				if (isDelimiter(ch, delimiters)) {
					capitalizeNext = true;
				} else if (capitalizeNext) {
					buffer[i] = Character.toUpperCase(ch);
					capitalizeNext = false;
				}
			}

			return new String(buffer);
		} else {
			return str;
		}
	}

	private static boolean isDelimiter(char ch, char[] delimiters) {
		if (delimiters == null) {
			return Character.isWhitespace(ch);
		} else {
			char[] arr$ = delimiters;
			int len$ = delimiters.length;

			for(int i$ = 0; i$ < len$; ++i$) {
				char delimiter = arr$[i$];
				if (ch == delimiter) {
					return true;
				}
			}

			return false;
		}
	}

	private static boolean isEmpty(CharSequence cs) {
		return cs == null || cs.length() == 0;
	}
	//end org.apache.commons.lang3.text;
}
