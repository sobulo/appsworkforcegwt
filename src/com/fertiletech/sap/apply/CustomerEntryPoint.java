package com.fertiletech.sap.apply;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

public class CustomerEntryPoint implements EntryPoint {


	private void setupCustomerApplyArea(final Panel rootPanel) {
		Window.alert("4");
		setupExceptionHandler();
		CustomerWebApp c = new CustomerWebApp();
		Window.alert("5");
		rootPanel.add(c);
	}

	public void setupExceptionHandler() {
		UncaughtExceptionHandler h = new UncaughtExceptionHandler() {

			@Override
			public void onUncaughtException(Throwable e) {
				if(e instanceof StatusCodeException)
					CustomerAppHelper.showErrorMessage("Possibly slow internet connection. Please try refreshing your browser");
				/*StackTraceElement[] stackTrace = e.getStackTrace();
				String errorChain = "";
				for(StackTraceElement s : stackTrace)
					errorChain+=s;
				Window.alert(errorChain + " -- Uncaught exception. Error was:" + e.getMessage());
				//RootLayoutPanel.get().clear();
				//RootLayoutPanel.get().add(new HTML("An error occured. "));*/
			}
		};
		GWT.setUncaughtExceptionHandler(h); //turn on when debugging
	}


	@Override
	public void onModuleLoad() {
		Window.alert("1");
		Panel rootPanel = RootPanel.get();
		Window.alert("2");
		setupCustomerApplyArea(rootPanel);
		Window.alert("3");
	}
}
