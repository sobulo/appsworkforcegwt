package com.fertiletech.sap.apply;

import com.fertiletech.sap.apply.members.SchoolPanel;
import com.fertiletech.sap.apply.members.moocs.MoocPanelsHelper;
import com.fertiletech.sap.apply.members.util.GeneralFunctions;
import com.fertiletech.sap.apply.resources.AppMenu;
import com.fertiletech.sap.client.MyAsyncCallback;
import com.fertiletech.sap.client.MyRefreshCallback;
import com.fertiletech.sap.client.OAuthLoginService;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.fertiletech.sap.shared.oauth.ClientUtils.NPMBUserCookie;
import com.fertiletech.sap.shared.oauth.SocialUser;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.constants.IconPosition;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Toggle;

import java.util.Date;

public class CustomerWebApp extends Composite implements ValueChangeHandler<String> {

    @UiField
    org.gwtbootstrap3.client.ui.gwt.FlowPanel nav;

    @UiField
    SimplePanel contentWidget;

    @UiField
    AnchorButton loginDropHeader;

    @UiField
    Image logo;


    AppMenu cssResource = GWT.create(AppMenu.class);



    private static CustomerWebAppUiBinder uiBinder = GWT.create(CustomerWebAppUiBinder.class);

    interface CustomerWebAppUiBinder extends UiBinder<Widget, CustomerWebApp> {
    }

    // different views
    HyperlinkedPanel currentContent; // keeps track of view currently displayed
    HyperlinkedPanel[] appPanels;
    SchoolPanel homePage;
    private boolean loggedInMenuInitialized;
    private boolean loggedOutMenuInitialized;

    private void fetchUser() {

        if (!ClientUtils.alreadyLoggedIn()) {
            // GWT.log("Not logged in, updating the app");
            updateWebApp();
            return;
        }

        final MyAsyncCallback<SocialUser> ensureLoginCallback = new MyAsyncCallback<SocialUser>() {

            @Override
            public void onSuccess(SocialUser result) {
                // GWT.log("Fetch returned, making a call to update the
                // screen");
                updateWebApp();
            }

            @Override
            public void onFailure(Throwable caught) {
                // GWT.log("Failure");
                String displayMessage = "Login credential check failed." + " Error was: [" + caught.getMessage()
                        + "]<br/>";
                CustomerAppHelper.showErrorMessage(displayMessage);
            }

            @Override
            protected void callService(AsyncCallback<SocialUser> cb) {
                super.enableWarning(false);
                OAuthLoginService.Util.getInstance().fetchMe(ClientUtils.getSessionIdFromCookie(), cb);
            }
        };
        // GWT.log("Making call to verify user credentials");
        ensureLoginCallback.go("Verifying user credentials ...");
    }

    private void updateWebApp() {
        GWT.log("Updating web app");
        boolean isOps = false;
        boolean isLicensed = false;
        String[] tokenArgument = new String[4];
        String tokArg = Cookies.getCookie(TOKEN_COOKIE);
        String tokExtrs = Cookies.getCookie(TOKEN_COOKIE_ARG);
        if (tokArg != null && (tokArg.equals("null") || tokArg.equals("undefined")))
            tokArg = null;
        if (tokExtrs != null && (tokExtrs.equals("null") || tokExtrs.equals("undefined")))
            tokExtrs = null;

        tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX] = tokArg;
        tokenArgument[CustomerAppHelper.TOKEN_ARGS_EXTRA_IDX] = tokExtrs;

        // GWT.log("Token Arg: " +
        // tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX]);
        if (ClientUtils.alreadyLoggedIn()) {
            GWT.log("Updating web app for logged in user");
            // GWT.log("Fetching user cookie");
            NPMBUserCookie usercookie = ClientUtils.NPMBUserCookie.getCookie();
            tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] = usercookie.getLoanID();
            tokenArgument[CustomerAppHelper.TOKEN_ARGS_EMAIL_IDX] = usercookie.getEmail();
            isOps = usercookie.isOps();
            isLicensed = usercookie.isLicensed();
            // GWT.log("Is OPS: " + isOps);
            if (!loggedInMenuInitialized) {
                String displayName = usercookie.getUserName();
                if ((displayName != null) && (displayName.length() > 0)) {
                    displayName = displayName.split(" ")[0];
                } else {
                    displayName = usercookie.getEmail();
                    if (displayName.length() > 12)
                        displayName = displayName.substring(0, 9) + "..";
                }
                setupLoggedInMenu(displayName);
                loggedInMenuInitialized = true;
                loggedOutMenuInitialized = false;
            }
            // GWT.log("IN: " +
            // tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] +
            // " AND " + tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX]);
        } else {
            GWT.log("Updating web app for logged out user");
            tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] = null;
            tokenArgument[CustomerAppHelper.TOKEN_ARGS_EMAIL_IDX] = null;
            if (!loggedOutMenuInitialized) {
                setupLoggedOutMenu();
                loggedOutMenuInitialized = true;
                loggedInMenuInitialized = false;
            }
            // GWT.log("OUT: " +
            // tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] +
            // " AND " + tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX]);
            // GWT.log("Not logged in, so nothing to do");
        }
        GWT.log("Loading panel");
        loadCurrentDisplay(tokenArgument, isOps, ClientUtils.alreadyLoggedIn(), isLicensed);
    }

    public CustomerWebApp() {
    	Window.alert("This is AppsWorkforce!!!!!");
        initWidget(uiBinder.createAndBindUi(this));
        cssResource.css().ensureInjected();
        //GWT.log("App Created");
        //// logo.setResource(CustomerAppHelper.generalImages.logoSmall());
        logo.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(NameTokens.WELCOME);
            }
        });
        appPanels = CustomerAppHelper.initializePanels();
        //GWT.log("Debug 1");
        for(int i = 0; i < appPanels.length; i++) {
            if ((appPanels[i]) instanceof SchoolPanel) {
                homePage = (SchoolPanel) appPanels[i];
                break;
            }
        }
       // GWT.log("Debug 2");
        if(homePage == null)
        {
            homePage = new SchoolPanel();
            GWT.log("Profile missing, used mirror object as an alternative.Browser refresh recommended if you're unable to view your tracks.");

        }
    	Window.alert("God no go shame us! united make top 4 o!");
        currentContent = homePage;
        //GWT.log("Debug 3");
        OAuthLoginService.Util.handleRedirect(new MyRefreshCallback() {

            @Override
            public void updateScreen() {
                //GWT.log("Debug A");
                setupHistoryHandler();
                //GWT.log("Debug B");

                //if(ClientUtils.alreadyLoggedIn()) {
                GWT.log("building menu");
               // GWT.log("Debug C");
                ClientUtils.alreadyLoggedIn(); //this is hacky but apparently when gwt gets compiled into javascript, this call ensures proper handshake for oauth login
                //GWT.log("Debug C2");
                setupMenu();
                //setupBioMenu();
                //GWT.log("Debug D");
                GWT.log("Umm menu built");
                //}
                updateWebApp();
                GWT.log("Debug E");
                sayHello();
                String token = History.getToken();
                //GWT.log("Token: [+ " + token + "] -- updating screen");
                if (token.trim().equals("")) {
                    String homeToken = NameTokens.WELCOME;
                    History.newItem(homeToken);
                    GWT.log("Fired old");

                } else {
                    History.fireCurrentHistoryState();
                    GWT.log("Fired current");
                }
            }
        });
        //GWT.log("Debug 4");

    }

    private void sayHello() {
        String name = "";
        /*String suffix = ". Please ensure you read through the site. <ul><li>Sign in to view full menu.</li><li>Use the course menu to track your" +
				" learning progress.</li><li>Use the admin menu to purchase products and services</li></ul> Email apply@appsworkforce.com "
				+ " if you have any questions and or concerns";*/
        if (ClientUtils.alreadyLoggedIn()) {
            NPMBUserCookie uc = ClientUtils.NPMBUserCookie.getCookie();
            name = uc.getUserName();
            name = " " + name;
            if (uc.isOps()) {
                String suffix = "Visit the <a href='https://www.appsworkforce.com/map/'>staff only portion of the portal @ M.A.P</a> to get a list of recent applicants";
                CustomerAppHelper.showInfoMessage(name + ", " + suffix);
            }
        }
        CustomerAppHelper.showInfoMessage(
                "Welcome <i>" + name + "</i> to the best <a target='_blank' href='http://about.appsworkforce.com/learn/curriculum'>computer science curriculum" +
                        " <b>available</b> in Nigeria</a>. This website provides you with the tools to track java, python and ruby MOOCS at your own pace ", 12);
    }

    private void setupHistoryHandler() {
        History.addValueChangeHandler(this);
    }


    private ListDropDown getLinks(String heading, Hyperlink[] links, IconType[] icons) {
        ListDropDown result = new ListDropDown();
        AnchorButton header = new AnchorButton();
        header.setDataToggle(Toggle.DROPDOWN);
        header.setText(heading);
        result.add(header);
        DropDownMenu menuContent = new DropDownMenu();

        for (int i = 0; i < links.length; i++) {
            AnchorListItem alink = getLink(links[i].getText(), links[i].getTargetHistoryToken(), true);
            alink.setIcon(icons[i]);
            menuContent.add(alink);
        }
        result.add(menuContent);
        return result;
    }

    private ListDropDown getLinks(String heading, HyperlinkedPanel[] menuPanels) {
        ListDropDown result = new ListDropDown();
        AnchorButton header = new AnchorButton();
        header.setDataToggle(Toggle.DROPDOWN);
        header.setText(heading);
        result.add(header);
        DropDownMenu menuContent = new DropDownMenu();

        for (int i = 0; i < menuPanels.length; i++) {
            Hyperlink links = menuPanels[i].getLink();
            String menuName = NameTokens.capitalize(links.getText().toLowerCase());
            AnchorListItem alink = getLink(menuName, links.getTargetHistoryToken(), true);
            alink.setIcon(menuPanels[i].getIcon());
            menuContent.add(alink);
        }
        result.add(menuContent);
        return result;
    }

    private ListDropDown getLinks(String heading, String[] links, IconType[] icons) {
        ListDropDown result = new ListDropDown();
        AnchorButton header = new AnchorButton();
        header.setDataToggle(Toggle.DROPDOWN);
        header.setText(heading);
        result.add(header);
        DropDownMenu menuContent = new DropDownMenu();

        for (int i = 0; i < links.length; i++) {
            String[] urlInfo = links[i].split(",");
            AnchorListItem alink = getLink(urlInfo[0], urlInfo[1], false);
            alink.setIcon(icons[i]);
            menuContent.add(alink);
        }
        result.add(menuContent);

        String[] x = {

        };

        return result;
    }

    private AnchorListItem getLink(String text, String href, boolean useTarget) {
        AnchorListItem result = new AnchorListItem();
        result.setText(text);
        if (useTarget)
            result.setTargetHistoryToken(href);
        else
            result.setHref(href);
        return result;
    }

    private void setupLoggedOutMenu() {
        loginDropHeader.setText("sign in");
        loginDropHeader.setIcon(IconType.SIGN_IN);
        loginDropHeader.setIconPosition(IconPosition.RIGHT);
        loginDropHeader.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final int authProvider = ClientUtils.getAuthProvider("Google");
                //Window.alert("Calling Login");
                OAuthLoginService.Util.getAuthorizationUrl(authProvider);
            }
        });


    }

    private void setupMenu() {
    	Window.alert("Made it Here 0");
        nav.clear();
        //TODO: might still need this?
        //if(!ClientUtils.NPMBUserCookie.getCookie().isLicensed())
          //  return;


        //learn
        HyperlinkedPanel[] mooc = MoocPanelsHelper.getMoocPanels();
        HyperlinkedPanel[] admin = CustomerAppHelper.getVisionPanels(); //bio
        mooc = GeneralFunctions.joinPanels(mooc, admin);
        
        

        boolean addDivider = false; // no seperator for the first menu item
        //css float reverses order, so we count backwards to preserve ordering
        for(int i = mooc.length-1; i >= 0; i--) {  
            Hyperlink item = mooc[i].getLink();
            item.setStyleName(cssResource.css().dashMenu());
            if(addDivider)
                nav.add(getDivider());

            nav.add(item);
            addDivider = true;
        }
    }
    


    public  HTML getDivider()
    {
        HTML divider = new HTML(" / ");
        divider.addStyleName(cssResource.css().dashSeperators());
        return divider;
    }



    AnchorListItem getLink(Hyperlink item) {
        return getLink(item.getText(), item.getTargetHistoryToken(), true);
    }

    private void setupLoggedInMenu(final String displayName) /*
													 * , final String url, final
													 * String providerName)
													 */ {
        loginDropHeader.setText(displayName + ", sign out");
        loginDropHeader.setIcon(IconType.SIGN_OUT);
        loginDropHeader.setIconPosition(IconPosition.RIGHT);
        final String url = DTOConstants.GOOGLE_CLOUD_LOGOUT;

        loginDropHeader.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ;

                Frame f = new Frame(url);
                f.addLoadHandler(new LoadHandler() {
                    @Override
                    public void onLoad(LoadEvent event) {
                        new Timer() {

                            @Override
                            public void run() {
                                OAuthLoginService.Util.logout();
                            }
                        }.schedule(1000 * 2); // allow network 2 seconds to log
                        // out b4 we log out of sang
                    }
                });
                GWT.log("F: " + f);
                f.setSize("15px", "15px");
                loginDropHeader.setText("Logging out...");
                loginDropHeader.add(f);
                new Timer() {
                    @Override
                    public void run() {
                        OAuthLoginService.Util.logout();
                    }
                }.schedule(1000 * 20); // if we're not logged out 20 seconds,
                // try alternate option (opens an
                // additional window)
            }
        });
    }

    private final String TOKEN_COOKIE = "com.appsworkforce.www.pubapparg";
    private final String TOKEN_COOKIE_ARG = "com.appsworkforce.www.pub1619apparg";

    // private String[] tokenArgument = new String[3];
    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
        GWT.log("value change event received: " + event.getValue());
        GWT.log("HISTORY: " + event.getValue());
        String[] urlParams = event.getValue().split("/");
        String token = urlParams[0];
        String args = null;
        String extra = null;
        if (urlParams.length == 2 && urlParams[1].trim().length() > 0)
            args = urlParams[1].trim();
        if (args != null && args.startsWith(CustomerAppHelper.ARG_HACK_PREFIX)) {
            extra = args.replaceAll(CustomerAppHelper.ARG_HACK_PREFIX, "");
            args = null;
        }
        //GWT.log("Testing untlil Noon: TOKEN: " + token + " Args: " + args + " Extra: " + extra);
        // tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX] = args;
        long expiryTime = new Date().getTime(); // now
        expiryTime += 30 * (60 * 1000); // 30 minutes from now in milliseconds
        if (args != null && args.length() > 0)
            Cookies.setCookie(TOKEN_COOKIE, args, new Date(expiryTime));
        if (extra != null && extra.length() > 0)
            Cookies.setCookie(TOKEN_COOKIE_ARG, extra, new Date(expiryTime));
        // GWT.log("Just set cookie with: " +
        // Cookies.getCookie(TOKEN_COOKIE));
        Window.addCloseHandler(new CloseHandler<Window>() {

            @Override
            public void onClose(CloseEvent<Window> event) {
                // GWT.log("Close Handler Called, wiping token arg");
                Cookies.setCookie(TOKEN_COOKIE, null);
                Cookies.setCookie(TOKEN_COOKIE_ARG, null);
            }
        });
        Window.addWindowClosingHandler(new ClosingHandler() {

            @Override
            public void onWindowClosing(ClosingEvent event) {
                // GWT.log("Close handler called, wiping token arg");
                Cookies.setCookie(TOKEN_COOKIE, null);
                Cookies.setCookie(TOKEN_COOKIE_ARG, null);
            }
        });
        if (shouldLoadNewPanel(token)) {
            fetchUser();
        }
    }

    private void loadCurrentDisplay(String[] args, boolean isOps, boolean isLoggedIn, boolean isLicensed) {
        //GWT.log("attempting load");
        //StringBuffer errorBuffer = new StringBuffer();
        try {
            Widget w = currentContent.getPanelWidget(isLoggedIn, isOps, isLicensed, args);
            //errorBuffer.append(" | Line 494");
            if (w == null || contentWidget.getWidget() == w)
                return;
            try {
                //	errorBuffer.append(" | Line 498");
                contentWidget.clear();
            } catch (Exception e) {
            } // firefox hack

            //GWT.log("setting widget: " + (w != null));

            contentWidget.setWidget(w);

            //GWT.log("done setting");

            //errorBuffer.append(" | Line 503");
            String url = "https://www.appsworkforce.com /lab/#" + currentContent.getLink().getTargetHistoryToken();
            //errorBuffer.append(" | Line 505: ").append(url);
            CustomerAppHelper.trackPageview(url);
            //errorBuffer.append(" | Line 507");
        } catch (Exception e) {
            contentWidget.setWidget(new HTML("<b>Error creating panel widget</b> " + e.getMessage()));// + errorBuffer.toString()));
        }
    }

    private boolean shouldLoadNewPanel(String newToken) {

        // String newToken = lastRequestedPage;
        HyperlinkedPanel newContent = null;
        if (newToken.equals(NameTokens.WELCOME)) {
            GWT.log("Homepage requested");
            newContent = homePage;
        } else {
            // determine what widget user link points to //TODO will need to search through other arrays see setupMenu
            for (HyperlinkedPanel cursorPanel : appPanels) {
                if (newToken.equals(cursorPanel.getLink().getTargetHistoryToken())) {
                    GWT.log("Other panel requested");
                    newContent = cursorPanel;
                    break;
                }
            }
        }

        // update current widget to widget specified by user above
        if (newContent != null && currentContent != newContent) {
            currentContent = newContent;
            GWT.log("Current content setup: " + currentContent.getLink().getTargetHistoryToken());
            return true;
        }
        return false;
    }
}
