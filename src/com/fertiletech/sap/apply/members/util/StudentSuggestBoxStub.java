package com.fertiletech.sap.apply.members.util;

import com.fertiletech.sap.shared.oauth.ClientUtils;

public class StudentSuggestBoxStub {

    public String getSelectedUser()
    {
        return ClientUtils.NPMBUserCookie.getCookie().getLoanID();
    }

    public String getSelectedUserDisplay()
    {
        return ClientUtils.NPMBUserCookie.getCookie().getUserName();
    }
}
