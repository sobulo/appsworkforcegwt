package com.fertiletech.sap.apply.members.util;

import org.gwtbootstrap3.client.ui.AnchorListItem;

public class CustomAnchorListItem extends AnchorListItem{

    public CustomAnchorListItem()
    {
        super();
        anchor.setTarget("_blank");
    }
}
