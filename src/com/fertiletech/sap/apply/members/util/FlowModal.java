package com.fertiletech.sap.apply.members.util;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.html.Paragraph;

public class FlowModal extends Composite {
    interface FlowModalUiBinder extends UiBinder<Widget, FlowModal> {
    }

    private static FlowModalUiBinder ourUiBinder = GWT.create(FlowModalUiBinder.class);

    @UiField
    Modal titleBar;

    @UiField
    Paragraph message;

    public void showHTML(String html)
    {
        message.setHTML(html);
        titleBar.show();
    }

    public void setTitle(String title)
    {
        titleBar.setTitle(title);

    }

    public FlowModal() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    public FlowModal(String title)
    {
        this();
        setTitle(title);
    }
}