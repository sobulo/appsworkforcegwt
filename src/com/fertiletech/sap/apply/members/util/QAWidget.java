package com.fertiletech.sap.apply.members.util;

import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.extras.card.client.ui.Card;

public class QAWidget extends Composite {

    @UiField
    Paragraph question;

    @UiField
    Small answer;


    interface QAWidgetUiBinder extends UiBinder<Widget, QAWidget> {
    }

    private static QAWidgetUiBinder ourUiBinder = GWT.create(QAWidgetUiBinder.class);

    public QAWidget()
    {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    public QAWidget(String questionText, String answerText, String moreUrl)
    {
        this();
        setQuestion(questionText);
        setAnswer(answerText, moreUrl);
    }

    public QAWidget(String questionText, String answerText)
    {
        this(questionText, answerText, null);
    }

    public void setQuestion(String text)
    {
        question.setText(text);
    }

    public void setAnswer(String text, String moreUrl)
    {
   

            String anchorText = (moreUrl == null) ? "" : ("<br/><a target='_blank' href='" + moreUrl + "'>" + "more..." + "</a>");
            answer.setHTML(text + anchorText);

    }
}