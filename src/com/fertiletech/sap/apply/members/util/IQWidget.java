package com.fertiletech.sap.apply.members.util;

import com.fertiletech.sap.apply.resources.AppMenu;
import com.fertiletech.sap.apply.resources.QAMenu;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconPosition;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;

public class IQWidget extends Composite {


    private FlowPanel questionSlot;
    private Button iq;


    private  String showQ = "Show AWesome Tips";
    private  String hideQ = "Hide AW Tips";

    private static int qIdx = 0;

    //license
    public final static int Q_ZT_IDX = qIdx++;
    public final static int Q_ST_IDX = qIdx++;
    public final static int Q_CT_IDX = qIdx++;
    
    //no license
    public final static int Q_AB_IDX = qIdx++;
    public final static int Q_AW_IDX = qIdx++;
    public final static int Q_CM_IDX = qIdx++;
    
    //schools
    public final static int Q_MIT_IDX = qIdx++;
    public final static int Q_HVD_IDX = qIdx++;
    public final static int Q_BKL_IDX = qIdx++; 
    public final static int Q_GAL_IDX = qIdx++;
    
    //facebook
    public final static int Q_FB_IDX = qIdx++;
    public final static int Q_MESSENGER_IDX = qIdx++;
    
    //contact
    public final static int Q_EMAIL_IDX = qIdx++;
    public final static int Q_WHATSAPP_IDX = qIdx++;
    
    //platforms
    public final static int Q_EDX_IDX = qIdx++;
    public final static int Q_CA_IDX = qIdx++;
    public final static int Q_PY_IDX = qIdx++;
    
    //platforms
    public final static int Q_CER_EDX_IDX = qIdx++;
    public final static int Q_CER_CA_IDX = qIdx++;
    public final static int Q_CER_AW_IDX = qIdx++;
    
    //states
    public final static int Q_LI_IDX = qIdx++;
    public final static int Q_RG_IDX = qIdx++;
    public final static int Q_NR_IDX = qIdx++;
    public final static int Q_WT_IDX = qIdx++;
    
    
    QAMenu cssResource = GWT.create(QAMenu.class);

    private static QAWidget[] getAllQuestions() {
    		QAWidget[] IQ_LIST = new QAWidget[qIdx];
            IQ_LIST[Q_MIT_IDX] = new QAWidget("Learn from Massachusetts Institute of Technology (MIT)",
                    "MIT's first programming course (6.00x) is mandatory for all AW members. ", "https://eecs.scripts.mit.edu/eduportal/who_is_teaching_what/S/2019/");
            IQ_LIST[Q_HVD_IDX] = new QAWidget("Learn from Harvard University",
                    "Learn how to build web and cross platform mobile apps from the \"Business School\".", "https://online-learning.harvard.edu/course/cs50s-web-programming-python-and-javascript");
            IQ_LIST[Q_BKL_IDX] = new QAWidget("Learn from University of California, Berkeley (UC Berkley)",
                    "Learn to build for large scale systems from silicon valley's top destination for recruiting graduates", "https://www2.eecs.berkeley.edu/Courses/CS169/");
            IQ_LIST[Q_GAL_IDX] = new QAWidget("Learn from Galileo Universidad",
                    "Learn to build Android apps for emerging markets from an emerging market school", "https://blog.edx.org/galileo-university-joins-edx");
            
            IQ_LIST[Q_FB_IDX] = new QAWidget("AppsWorkforce Facebook page",
                    "Join the conversation. Like AW on Facebook to get notifications on future AW posts", "http://www.facebook.com/appsworkforce");
            IQ_LIST[Q_EMAIL_IDX] = new QAWidget("Email AppsWorkforce",
                    "Email us for general enquiries. It's the best way to reach us.", "mailto:hello@appsworkforce.com");
            IQ_LIST[Q_MESSENGER_IDX] = new QAWidget("Use FB messenger to contact AW",
                    "Got MOOC? Send us a private message on FB ", "https://m.me/appsworkforce");
            IQ_LIST[Q_WHATSAPP_IDX] = new QAWidget("AW on whatsapp",
                    "Hello! Send AW a whatsapp message", "https://wa.me/2349061766041?text=Hello%2C%20I%27d%20like%20to%20join%20AppsWorkforce");
            
            IQ_LIST[Q_AB_IDX] = new QAWidget("What is AppsWorkforce?",
                    "A platform that promotes programming as a viable career option in Nigeria", "http://about.appsworkforce.com/home/what-is-appsworkforce");
            IQ_LIST[Q_AW_IDX] = new QAWidget("How does it work?",
                    "Decades of experience monitoring your pace of learning online", "http://about.appsworkforce.com/news-update/bigbrother");
            IQ_LIST[Q_CM_IDX] = new QAWidget("AppsWorkforce Curriculum Suggestions",
                    "Our opinions on FREE courses you can and should take at your own convenience"
           , "http://about.appsworkforce.com/learn");
            
            
            IQ_LIST[Q_ZT_IDX] = new QAWidget("AppsWorkforce Requirments",
                    "Be prepared to learn with integrity", "http://about.appsworkforce.com/learn/requirements");
            IQ_LIST[Q_ST_IDX] = new QAWidget("Tell us you have started taking a course on our curriculum",
                    "Step by step guide to tracking courses, using learn python as an example", "http://about.appsworkforce.com/learn/start");
            IQ_LIST[Q_CT_IDX] = new QAWidget("Share your learning experience. What works? What doesn't?",
                    "Provide regular status updates detailing your learning progress and/or challenges", "http://about.appsworkforce.com/learn/continue");

        
            IQ_LIST[Q_PY_IDX] = new QAWidget("Just do it! Get started now.",
                    "It's free to learn. Step by step guide to learning python"
           , "http://about.appsworkforce.com/learn/start/python");
            IQ_LIST[Q_EDX_IDX] = new QAWidget("Register for free on edX",
                    "ALL courses offered on edX are FREE. This includes MIT and Harvard MOOCs"
           , "https://courses.edx.org/register");
            IQ_LIST[Q_CA_IDX] = new QAWidget("Register for free on codecademy",
                    "Take select introductory lessons for free on codeacademy"
           , "https://www.codecademy.com/register");
            
            IQ_LIST[Q_CER_AW_IDX] = new QAWidget("AW transcrripts and verification",
                    "Verification available for any course being tracked at AW"
           , "http://about.appsworkforce.com/learn/certificate/#aw");
            IQ_LIST[Q_CER_EDX_IDX] = new QAWidget("Audit/take courses on edX and get a certificate",
                    "Free honor code and paid verified certificates (both optionally available)"
           , "http://about.appsworkforce.com/learn/certificate/#edx");
            IQ_LIST[Q_CER_CA_IDX] = new QAWidget("Take introductory courses for free on codecademy",
                    "Paid certificates of completion available via \"Upgrade to Pro\" option (NOT recommended!)"
           , "http://about.appsworkforce.com/learn/certificate/#ca");
            
            IQ_LIST[Q_LI_IDX] = new QAWidget("Getting access to AppsWorkforce membership services",
                    "You need a license to track courses on AW. Already have one, login to continue"
           , "http://www.appsworkforce.com/#start");
            IQ_LIST[Q_RG_IDX] = new QAWidget("Already registered? Email AppsWorkforce for a license.",
                    "Membership license based on your tutoring/counselling needs. Flexible pricing available."
           , "mailto:hello@appsworkforce.com");
            IQ_LIST[Q_NR_IDX] = new QAWidget("I want to register for AppsWorkforce",
                    "Join our open house hangout session and introduce yourself. You need a google/gmail account for AW"
           , "http://about.appsworkforce.com/home/contact-us");
            IQ_LIST[Q_WT_IDX] = new QAWidget("Who should register for AppsWorkforce?",
                    "Millenials (age 18 - 37) living in Nigeria"
           , "http://about.appsworkforce.com/news-update/winterishere");
        GWT.log("Exiting initialization");
        return IQ_LIST;
    }



    private void addMultipleQuestions(QAWidget[] qList)
    {
        questionSlot.clear();
        if (qList == null ) 
        {
        	Window.alert("Umm I got called wuth null");
            qList = getAllQuestions();
        }
        else if(qList.length == 0)
        {
        	Window.alert("Umm I got called with empty list");
            qList = getAllQuestions();
        
        }
        else if(qList[0] == null)
        {
        	Window.alert("Umm I got null element. from array size: " + qList.length);
            qList = getAllQuestions();
        }
        GWT.log("Question Widgets: " + qList.length);
        for (QAWidget q : qList)
            addQuestion(q);
    }


    private void addQuestion(QAWidget qa) {
        questionSlot.add(qa);
    }

    private void toggle() {

        if (questionSlot.isVisible()) {
            GWT.log(" Turning visibility off: " + questionSlot.getWidgetCount());
            questionSlot.setVisible(false);
            iq.setText(showQ);
            iq.setIcon(IconType.CHEVRON_DOWN);

        } else {
            GWT.log(" Turning visibility ON: " + questionSlot.getWidgetCount());
            questionSlot.setVisible(true);
            iq.setText(hideQ);
            iq.setIcon(IconType.CHEVRON_UP);
        }
    }

    public static QAWidget[] getQuetionsPanels(int[] qList) {
    	QAWidget[] allQuestions = getAllQuestions();
    	if(qList == null)
    		return allQuestions;
        QAWidget[] result = new QAWidget[qList.length];
        for (int i = 0; i < qList.length; i++)
            result[i] = allQuestions[qList[i]];
        return result;
    }
    
    public IQWidget(QAWidget[] qws) 
    {
    	this(qws, null, null);
    }
    public IQWidget(QAWidget[] qws, String showText, String hideText) {
    	cssResource.css().ensureInjected();
    	
    	if(showText!=null && showText.length() > 0)
    	{
        	showQ = showText;
        	hideQ = hideText;
    	}
        GWT.log("IQ");
        FlowPanel container;
        iq = new Button(hideQ);
        iq.addStyleName(cssResource.css().qaMenu());
        iq.setType(ButtonType.LINK);
        iq.setIconPosition(IconPosition.LEFT);
        container = new FlowPanel();
        GWT.log("Creating question slot");
        questionSlot = new FlowPanel();
        GWT.log("Question Size: " + qws.length);
        addMultipleQuestions(qws);
        GWT.log("Added questions");
        container.add(iq);
        container.add(questionSlot);
       GWT.log("Finished with container " + qws[0]);
        GWT.log("Co");
        iq.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                toggle();
            }
        });
        
        initWidget(container);
        //hack hide toggle
        //iq.setVisible(false);
        toggle();
    }
    
    public void disableToggling()
    {
    	questionSlot.setVisible(true); //show questions
    	iq.setVisible(false); //hide toggle button
    }
}
