package com.fertiletech.sap.apply.members;


import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;
import org.gwtbootstrap3.client.ui.html.Br;
import org.gwtbootstrap3.client.ui.html.Hr;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.HyperlinkedPanel;
import com.fertiletech.sap.apply.NameTokens;
import com.fertiletech.sap.apply.images.GeneralImageBundle;
import com.fertiletech.sap.apply.members.util.IQWidget;
import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.OAuthLoginService;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.WorkflowStateInstance;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class SchoolPanel implements HyperlinkedPanel{
    @Override
    public Hyperlink getLink() {
        return new Hyperlink("Profile", NameTokens.SCHOOL);
    }

    @Override
    public IconType getIcon() {
        return IconType.USER_SECRET;
    }
    private static GeneralImageBundle primaryBundle = CustomerAppHelper.GENERAL_IMAGES;
    FlowPanel display;
    @Override
    public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args) { 
        //GWT.log("DEBUG1");
        if(display != null) //compute just once?
            return display;
        //GWT.log("DEBUG12");

        if(!isLicensed) {
            //GWT.log("DEBUG13");
            MembersOnlyPanel mp = new MembersOnlyPanel();
            mp.setDefaultMessages(isLoggedIn);
            FlowPanel container = new FlowPanel();
            Paragraph intro = new Paragraph("<b>Featured MOOC</b><br/>"
            		+ "<a target='_blank' href='http://about.appsworkforce.com/learn/mit-python-1'>MIT Python 1 (6.00.1x) - notes/sample code.</a><br/><br/>.");
          
            
        	//quote
        	ImageResource[] images = {primaryBundle.banner3()};
        	int imgIdx = 0;
        	Image quote = new 	Image(images[imgIdx]);
        	quote.addStyleName("img-responsive");
        	container.add(quote);
        	container.add(new Br());
         	container.add(new Hr());
         	container.add(intro);
         	//Small more = new Small("Click on any of the links below to learn more about AW.");
        	//container.add(more);
            container.add(new Paragraph("<a href='https://www.appsworkforce.com/david'>Getting Started ...</a>"));
            
 
        	container.add(new Hr());
        	container.add(new Br());
            container.add(mp);
            display = container;
            GWT.log("Debug L");
            
            /*AsyncCallback<String> echoHandler = new AsyncCallback<String>() {
				
				@Override
				public void onSuccess(String result) {
					Window.alert("Success: " + result);
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Error: " + caught.getMessage());
				}
			};*/
            
            //echo debug -- TODO: add log to calls on server side
            //CustomerAppHelper.READ_SERVICE.echo("Echo", echoHandler);

            //OAuthLoginService.Util.getInstance().echo("Echo B", echoHandler);
            return display;
        }

        //GWT.log("DEBUG2");

        MembersOnlyPanel mop = new MembersOnlyPanel();

        ClientUtils.NPMBUserCookie cookie = ClientUtils.NPMBUserCookie.getCookie();
        StringBuilder body = new StringBuilder();
        //GWT.log("DEBUG2");
        String licenseType = cookie.getLicenseType();
        body.append(getTableRow("License Type: ", licenseType));
        body.append(getTableRow("License Expires: ",
                GUIConstants.DEFAULT_DATE_FORMAT.format(cookie.getExpration())));
        //GWT.log("DEBUG3");
        body.append(getTableRow("Membership ID: ", cookie.getMemberID()));
        GWT.log("DEBUG4");
        body.append(getTableRow("Course Tracking: ", getTrackLinks()));
        body.append(getTableRow("HC Points: ", DTOConstants.getDefaultLicenseHC(licenseType)));
        /*body.append(getTableRow("Courses Verified: ", "0"));
        body.append(getTableRow("Independent Team Project: ", "N/A"));
        body.append(getTableRow("Resume/CV on file: ", "No"));
        body.append(getTableRow("Upcoming Job Interview: ", "N/A"));
        body.append(getTableRow("Internship: ", "N/A"));
        body.append(getTableRow("Study Center: ", "N/A"));
        body.append(getTableRow("360 Performance Review: ", "N/A"));
        body.append(getTableRow("Support Requests: ", "N/A"));*/
        body.append(getTableRow("24/7 Hang out: ", getHangoutLink()));
        GWT.log("DEGUG5");
        mop.setDescription(getTable(body.toString()));
        FlowPanel container = new FlowPanel();
        container.add(mop);
    	container.add(new Hr());
    	container.add(new Br());
        addQASectiion(container);     
        display = container;
        return display;
    }
    
    private void addQASectiion(FlowPanel section)
    {

	    int [] qLista = { IQWidget.Q_ST_IDX, IQWidget.Q_CT_IDX, IQWidget.Q_ZT_IDX, IQWidget.Q_AW_IDX,};
	    section.add(new IQPanel(qLista, "How to track your courses", "Welcome home, this is AppsWorkforce!"));
	    return;
    }

    public static String getTrackLinks()
    {
        StringBuffer result = new StringBuffer();
        result.append("<a href='https://www.appsworkforce.com/lab/#start'>Start tracking a course</a></br>");
        result.append("<a href='https://www.appsworkforce.com/lab/#continue'>Provide updates on courses you're currently tracking</a>");
        return result.toString();
    }

    private String getTable(String body)
    {
        return "<table style ='border:none' padding='5' cellspacing='5'>" + body + "</table>";
    }

    private String getTableRow(String label, String val)
    {

        return "<tr><th style='text-align:right;border-spacing:15px;'>" + label +
                "<td>&nbsp;&nbsp</td><td style='border-bottom: 2px solid green;'>" + val + "</td></tr>";
    }

    private String getHangoutLink()
    {
       return  getLink("https://hangouts.google.com/group/auOQkjsZWTicz4OG3", "David's Lab");
    }

    public static String [] getGroupLinks(String licenseName)
    {
        GWT.log("DEBUG G1");
        String licenseType = DTOConstants.getLicenseType(licenseName);
        GWT.log("DEBUG G2");
        String[] monthlyGroup = {"https://groups.google.com/a/appsworkforce.com/forum/#!forum/monthly"};

        String[] annualGroup = {"https://groups.google.com/a/appsworkforce.com/forum/#!forum/annual",
                "https://groups.google.com/a/appsworkforce.com/forum/#!forum/friends",
                "https://groups.google.com/a/appsworkforce.com/forum/#!forum/careers"};

        String[] workforceGroup = {"https://groups.google.com/a/appsworkforce.com/forum/#!forum/workforce",
                "https://groups.google.com/a/appsworkforce.com/forum/#!forum/friends",
                "https://groups.google.com/a/appsworkforce.com/forum/#!forum/careers"};

        GWT.log("Debug G3");
        if(licenseType.equals(DTOConstants.LICENSE_MONTHLY))
        {
            GWT.log("Debug G4A");
            monthlyGroup[0] = getLink(monthlyGroup[0], "Monthly discussion group");
            return monthlyGroup;
        }
        else if(licenseType.equals(DTOConstants.LICENSE_ANNUAL))
        {
            GWT.log("Debug G4B");
            annualGroup[0] = getLink(annualGroup[0], "Annual discussion group");
            GWT.log("Debug G4B1");
            annualGroup[1] = getLink(annualGroup[1], "Friends discussion group");
            annualGroup[2] = getLink(annualGroup[2], "Careers discussion group");
            GWT.log("Debug G4B2");
            return annualGroup;
        }
        else if(licenseType.equals(DTOConstants.LICENSE_WORKFORCE))
        {
            GWT.log("Debug G4C");
            workforceGroup[0] = getLink(workforceGroup[0], "Workforce discussion group");
            GWT.log("Debug G4C1");
            workforceGroup[1] = getLink(workforceGroup[1], "Friends discussion group");
            workforceGroup[2] = getLink(workforceGroup[2], "Careers discussion group");
            GWT.log("Debug G4C2");
            return workforceGroup;
        }
        String[] unsupported = {"<p>" + licenseName + " is not recognoized. Please contact labs@appsworkforce.com to report this as an issue</p>"};
        return unsupported;

    }



    public String getFlattendGroupInfo(String licenseName)
    {
        GWT.log("DEBUG F1");
        String result = "";
        String[] groupInfo = getGroupLinks(licenseName);
        GWT.log("DEBUG F2");
        boolean useDemilter = false;
        for(String g: groupInfo)
        {
            String delimeter = useDemilter?" | ":"";
            result = result + delimeter + g;
            useDemilter = true;
        }
        return result;
    }

    private void updateDisplay()
    {
        WorkflowStateInstance state = null;
        if(!ClientUtils.alreadyLoggedIn() || state == null)
            return;
        if(state.ordinal() >= WorkflowStateInstance.APPLICATION_CONFIRM.ordinal())
        {
            //change text to registration submitted
        }
        //find smallest amount license
        //change button text to u selected license name

        //step 3 always enabled?? no wizard



    }

    private static  String getLink(String url, String name)
    {
        return "<a href='" + url + "'>" + name + "</a>";
    }
}
