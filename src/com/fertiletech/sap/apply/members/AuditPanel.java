package com.fertiletech.sap.apply.members;


import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.html.Span;
import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.HyperlinkedPanel;
import com.fertiletech.sap.apply.NameTokens;
import com.fertiletech.sap.apply.images.GeneralImageBundle;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class AuditPanel implements HyperlinkedPanel{
    @Override
    public Hyperlink getLink() {
        return new Hyperlink("Bio & Trail", NameTokens.BIO);
    }

    @Override
    public IconType getIcon() {
        return IconType.USER_SECRET; //deprecated
    }
    private static GeneralImageBundle primaryBundle = CustomerAppHelper.GENERAL_IMAGES;
    Span display;
    @Override
    public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args) {
    	if(display != null)
    		return display;
    	
    	if(!isLoggedIn)
    	{
    		display = new Span("You must first login to view your audit trail");
    		return display;
    	}
    	ClientUtils.NPMBUserCookie cookie = ClientUtils.NPMBUserCookie.getCookie();
    	String memberId = cookie.getMemberID();
    	
    	String header = "<b style='color:#0a650c'>Member Bio Summary</b><br/>";
    	if(memberId == null)
    	{
    		display = new Span( header + "No registration found on file. Send an email to <a href='mailto:appply@appsworkforce.com'>apply@appsworkforce.com</a> for registration/pricing enquiries");
    		return display;
    	}
    	if(isLicensed)
    	{
    		header = "<b style='color:#0a650c'>AW Member ID</b><br/>" + memberId + "<br/><a href='http://www.appsworkforce.com/#" + 
    	NameTokens.SCHOOL + "'>See more</a>";
    	}
    	else
    	{
    		header = header + memberId;
    	}
    	
    	final String trailPrefix = header + "<hr/><br/><br/><b style='color:#ca3515'>AW Trail and Notes</b><br/>";
    	CustomerAppHelper.READ_SERVICE.getActivityComments(new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				display.setHTML(trailPrefix + result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to fetch audit history! Error message" +
			caught.getMessage());
				
			}
		});
    	display = new Span("Loading ,... please wait ");
        return display;
    }
}
