package com.fertiletech.sap.apply.members;

import org.gwtbootstrap3.client.ui.gwt.FlowPanel;

import com.fertiletech.sap.apply.members.util.IQWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class IQPanel extends Composite {
	
	@UiField
	FlowPanel qaSlot;

    interface IQPanelUiBinder extends UiBinder<Widget, IQPanel> {
    }

    private static IQPanelUiBinder ourUiBinder = GWT.create(IQPanelUiBinder.class);

    public IQPanel(int[] qList, String show, String hide) {
        initWidget(ourUiBinder.createAndBindUi(this));
        IQWidget qw = new IQWidget(IQWidget.getQuetionsPanels(qList), show, hide);
        qaSlot.add(qw);
    	      
    }
    
    
    public IQPanel(int[] qList) {
    	this(qList, null, null);
    }
}