package com.fertiletech.sap.apply.members;


import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.HyperlinkedPanel;
import com.fertiletech.sap.apply.NameTokens;
import com.fertiletech.sap.apply.images.GeneralImageBundle;
import com.fertiletech.sap.apply.members.util.IQWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class FAQPanel implements HyperlinkedPanel{
    @Override
    public Hyperlink getLink() {
        return new Hyperlink("FAQ", NameTokens.IQ);
    }

    @Override
    public IconType getIcon() {
        return IconType.USER_SECRET;
    }
    private static GeneralImageBundle primaryBundle = CustomerAppHelper.GENERAL_IMAGES;
    FlowPanel display;
    @Override
    public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args) { 
        //GWT.log("DEBUG1");
        if(display != null) //compute just once?
            return display;
        //GWT.log("DEBUG12");


        FlowPanel container = new FlowPanel();
        Paragraph intro = new Paragraph("<b>A</b>pps<b>W</b>orkforce (AW) promotes learning programming via  MOOCs (<b>M</b>assively <b>O</b>pen <b>O</b>nline <b>C</b>ourse<b>s</b>).<br/><br/>"
        		+ "<b>More on AppsWorkforce ...</b><br/>");
     	container.add(intro);
        addQASectiion(container);;
        display = container;
        GWT.log("Debug L");
        return display;
    }
    
    private void addQASectiion(FlowPanel section)
    {  	

        int [] qLista = { IQWidget.Q_ST_IDX, IQWidget.Q_CT_IDX, IQWidget.Q_ZT_IDX};
        section.add(new IQPanel(qLista, "How to track your courses", "Welcome home, this is AppsWorkforce!"));

         int [] qListb = {IQWidget.Q_CM_IDX, IQWidget.Q_AW_IDX, IQWidget.Q_AB_IDX};
        section.add(new IQPanel(qListb, "This is AppsWorkforce!", "Got MOOC? Choose a good curriculum"));
        
        int [] qList1 = {IQWidget.Q_MIT_IDX, IQWidget.Q_HVD_IDX, IQWidget.Q_BKL_IDX, IQWidget.Q_GAL_IDX};                                  
        section.add(new IQPanel(qList1, "Learn to program from really good schools", "Nigerians are tracking courses offered by"));
        //section.add(new Br());
        int [] qListc = {IQWidget.Q_EDX_IDX, IQWidget.Q_CA_IDX, IQWidget.Q_PY_IDX,};
        section.add(new IQPanel(qListc, "It's FREE to learn with MOOCs", "Self paced learning at zero cost"));
      
        int [] qListx = {IQWidget.Q_CER_CA_IDX, IQWidget.Q_CER_EDX_IDX, IQWidget.Q_CER_AW_IDX,};
        section.add(new IQPanel(qListx, "Paid certificates and verification available", "Certifcates are OPTIONAL, it's free to learn"));        
        
        //section.add(new Br());
        
        int [] qListd = {IQWidget.Q_LI_IDX, IQWidget.Q_RG_IDX, IQWidget.Q_NR_IDX,IQWidget.Q_WT_IDX};
        section.add(new IQPanel(qListd, "It's NOT free to join AppsWorforce", "Your MOOCs are free but our time isn't"));
        //section.add(new Br());
        
        int [] qList2 = {IQWidget.Q_FB_IDX, IQWidget.Q_MESSENGER_IDX};
        section.add(new IQPanel(qList2, "Connect with AW on Facebook", "Interesting conversations"));
        //section.add(new Br());
        int [] qList3 = {IQWidget.Q_WHATSAPP_IDX,IQWidget.Q_EMAIL_IDX};
        section.add(new IQPanel(qList3, "Contact AppsWorkforce", "Share your thoughts with us"));
    }


}
