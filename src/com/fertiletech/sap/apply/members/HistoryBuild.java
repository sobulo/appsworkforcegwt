package com.fertiletech.sap.apply.members;

import org.gwtbootstrap3.client.ui.constants.IconType;

import com.fertiletech.sap.apply.HyperlinkedPanel;
import com.fertiletech.sap.apply.NameTokens;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class HistoryBuild extends Composite implements HyperlinkedPanel{
    interface HistoryBuildUiBinder extends UiBinder<Widget, HistoryBuild> {
    }





    private static HistoryBuildUiBinder ourUiBinder = GWT.create(HistoryBuildUiBinder.class);

    public HistoryBuild() {
        initWidget(ourUiBinder.createAndBindUi(this));

    }

	@Override
	public Hyperlink getLink() {
		return new Hyperlink("History", "history");
	}

	@Override
	public IconType getIcon() {
		return IconType.QUESTION; //deprecated
	}
	

	@Override
	public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args) {
		return this;
	}




}