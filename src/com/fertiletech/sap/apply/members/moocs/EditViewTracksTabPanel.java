package com.fertiletech.sap.apply.members.moocs;

import com.fertiletech.sap.apply.members.moocs.widgets.NewTrackView;
import com.fertiletech.sap.apply.members.moocs.widgets.ViewTracks;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.TabListItem;
import org.gwtbootstrap3.client.ui.TabPane;

public class EditViewTracksTabPanel extends Composite {
    interface EditViewTracksTabPanelUiBinder extends UiBinder<Widget, EditViewTracksTabPanel> {
    }

    private static EditViewTracksTabPanelUiBinder ourUiBinder = GWT.create(EditViewTracksTabPanelUiBinder.class);
    @UiField
    TabPane addTrack;
    @UiField
    TabPane viewTrack;
    @UiField
    TabListItem viewTab;


    public EditViewTracksTabPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
        NewTrackView newTrackView = new NewTrackView();
        ViewTracks trackViewer = new ViewTracks();
        addTrack.add(newTrackView);
        viewTrack.add(trackViewer);
        newTrackView.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                viewTab.showTab();
            }
        });
        newTrackView.addValueChangeHandler(trackViewer);

    }

}