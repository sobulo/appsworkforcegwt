package com.fertiletech.sap.apply.members.moocs;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.members.moocs.widgets.CourseView;
import com.fertiletech.sap.apply.table.TablesView;
import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.fertiletech.sap.shared.VerificationStatus;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.shared.event.TabShowEvent;
import org.gwtbootstrap3.client.shared.event.TabShowHandler;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class CourseTrackingTabPanel extends Composite implements ValueChangeHandler{

    public final  List<TableMessage> ALL_COURSES = new ArrayList<TableMessage>();
    private final HashMap<String, List<TableMessage>>  CATEGORY_COUURSE_MAP = new HashMap<String, List<TableMessage>>();
    private final HashSet<String> EXISTING_TRACKS_COURSE_NAMES = new HashSet<String>();

    private final AsyncCallback<List<TableMessage>> allCoursesCallback = new AsyncCallback<List<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
            CustomerAppHelper.showErrorMessage("Unable to fetch list of messages because of: " + caught.getMessage());
        }

        @Override
        public void onSuccess(List<TableMessage> result) {
            ALL_COURSES.clear(); //sanity clear for readability purposes, list should be empty at this point
            if(result.size() < 2) {
                CustomerAppHelper.showErrorMessage("No courses found in the database");
                return;
            }
            TableMessage header = result.get(0);
            if(!(header instanceof TableMessageHeader))
            {
                CustomerAppHelper.showErrorMessage("Badly formed DTO, missing header. Contact hello@appsworkforce.com for support");
                return;
            }
            else
                result.remove(0); //remove header

            ALL_COURSES.addAll(result);
            updateCategoryCourseMap();
            final String[] ORDER_HACK = {"Programming Intern", "Programming Analyst","Programming Elective", "English Elective"};
            if(ORDER_HACK.length != CATEGORY_COUURSE_MAP.size())
            {
            	String errorMessage = "Argument mismatch: " + ORDER_HACK.length + " vs " + CATEGORY_COUURSE_MAP.size();
            	Window.alert(errorMessage);
            	throw new IllegalArgumentException(errorMessage);
            }
            
            Set<String> HACK_CHECK = CATEGORY_COUURSE_MAP.keySet();
            for(String c : ORDER_HACK)
            {
            	if(HACK_CHECK.contains(c))
            		category.addItem(c);
            	else
            	{
            		String errMsg = "Unrecognized category: " + c;
            		Window.alert(errMsg);
            		throw new IllegalArgumentException(errMsg);
            	}
            }
            category.addChangeHandler(new ChangeHandler() {
                @Override
                public void onChange(ChangeEvent event) {
                    updateCourseListView();
                }
            });
            category.setTitle("Select a category to view list of courses");
            updateCourseListView();
        }
    };
    
    private void updateCourseListView()
    {
    	String cat = category.getSelectedValue();
    	
        List<TableMessage> courses = CATEGORY_COUURSE_MAP.get(cat);
        final boolean isRegistered = ClientUtils.NPMBUserCookie.getCookie().isRegistered();
        final boolean isLicensed = ClientUtils.NPMBUserCookie.getCookie().isLicensed();
        setupCoursePanel(cat, courses, isRegistered, isLicensed);
    }

    private void updateCategoryCourseMap()
    {
        HashMap<String, List<TableMessage>> result = CATEGORY_COUURSE_MAP;
        for(TableMessage m : ALL_COURSES)
        {
            String category = m.getText(DTOConstants.COURSE_CATEGORY_TXT_IDX);
            if(!result.containsKey(category))
                result.put(category, new ArrayList<TableMessage>());
            List<TableMessage> courses = result.get(category); //get reference to course list
            courses.add(m);
            result.put(category, courses); //sanity put for sake of readability

        }

        //sort lists
        for(String category : result.keySet())
        {
            List<TableMessage> candidateList = result.get(category);
            TableMessage.sort(candidateList, DTOConstants.COURSE_ORDER_NUM_IDX, TableMessageHeader.TableMessageContent.NUMBER, true);
            result.put(category, candidateList); //sanity put for sake of readability
        }

    }

    @Override
    public void onValueChange(ValueChangeEvent event) {
        refreshTrackList = true;
        trackTab.showTab(refreshTrackList);
    }

    interface CourseTrackingTabPanelUiBinder extends UiBinder<Widget, CourseTrackingTabPanel> {
    }

    private static CourseTrackingTabPanelUiBinder ourUiBinder = GWT.create(CourseTrackingTabPanelUiBinder.class);

    @UiField
    PanelBody courseList;

    @UiField
    PanelBody trackList;

    @UiField
    ListBox category;

    @UiField
    TabListItem trackTab;
    
    @UiField
    Description sg;
    
    @UiField
    Description vr;

    @UiField
    ProgressBar progress;
	private AsyncCallback<List<TableMessage>> existingTracksCallback = new AsyncCallback<List<TableMessage>>() {
		
		@Override
		public void onSuccess(List<TableMessage> result) {
			//get rid of header
			result.remove(0);
			
			for(TableMessage m : result)
				EXISTING_TRACKS_COURSE_NAMES.add(m.getText(DTOConstants.TRACK_COURSE_TXT_IDX));
			//get existing tracks
			Moocs.App.getInstance().getAllCourses(allCoursesCallback);
			
		}
		
		@Override
		public void onFailure(Throwable caught) {
        	if(caught instanceof LoginExpiredException)
        		CustomerAppHelper.delayedReload((LoginExpiredException) caught);
        	else
        		CustomerAppHelper.showErrorMessage("Error fetching track list, try refreshing your browser");
			
		}
	};

    public CourseTrackingTabPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
        if(ClientUtils.NPMBUserCookie.getCookie().isLicensed())
        	Moocs.App.getInstance().getAllMemberTracks(existingTracksCallback ); //first fetch existin
        else
        	Moocs.App.getInstance().getAllCourses(allCoursesCallback); //just fetch course list
        setupTrackPanel();
    }

    private void setupCoursePanel(String Category, List<TableMessage> courseDTOs, boolean isRegistered, boolean isLicensed)
    {
        PanelBody container = courseList;
        container.clear();
        
        for(TableMessage cdto : courseDTOs)
        {
        	 final String courseId = cdto.getText(DTOConstants.COURSE_NAME_TXT_IDX);
            CourseView view = new CourseView();
            if(EXISTING_TRACKS_COURSE_NAMES.contains(courseId))
            	view.disableTracking();
            else
            	view.addValueChangeHandler(this);
            view.initView(cdto, isRegistered, isLicensed);
            container.add(view);
        }
    }
    
   

    private boolean refreshTrackList = true;
    private void setupTrackPanel()
    {
        final boolean isRegistered = ClientUtils.NPMBUserCookie.getCookie().isRegistered();
        final boolean isLicensed = ClientUtils.NPMBUserCookie.getCookie().isLicensed();
        if(isLicensed)
        {
            trackTab.addShowHandler(new TabShowHandler() {
                @Override
                public void onShow(TabShowEvent event) {
                    AsyncCallback<List<TableMessage>> trackListCallback = new AsyncCallback<List<TableMessage>>() {
                        @Override
                        public void onFailure(Throwable caught) {
                        	if(caught instanceof LoginExpiredException)
                        		CustomerAppHelper.delayedReload((LoginExpiredException) caught);
                        	else
                        		CustomerAppHelper.showErrorMessage("Unable to fetch your track list: " + caught.getMessage());

                        }

                        @Override
                        public void onSuccess(List<TableMessage> result) {
                            //update progress bar
                            double averageAI = TableMessage.columnAverage(result, DTOConstants.TRACK_PROGRESS_NUM_IDX);
                            averageAI = Math.max(averageAI, 1.0); //at least 1 percent
                            averageAI = Math.min(averageAI, 100.0); //at most 100 percent
                            progress.setPercent(averageAI);

                            //update table
                            trackList.clear();
                            TablesView trackTable = new TablesView();
                            trackTable.setWidth("100%");
                            trackList.add(trackTable);
                            trackTable.showTable(result);
                            refreshTrackList = false;
                            setDescriptopn(sg, VerificationStatus.getSelfGradeLegendHack());
                            setDescriptopn(vr, VerificationStatus.getLegend());
                            
                            
                        }
                    };
                    if(refreshTrackList) {
                        Moocs.App.getInstance().getAllMemberTracks(trackListCallback);
                    }
                }
            });

        }
        else
        {
            trackTab.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    if(!isRegistered)
                        CustomerAppHelper.showErrorMessage("Login and register before attempting to view your tracks");
                    else if(!isLicensed)
                        CustomerAppHelper.showErrorMessage("Tracking disabled. Membership license required.");
                    else
                        return;
                    trackTab.setEnabled(false);
                }
            });
        }
    }
    
    public void setDescriptopn(Description d, String htmlText)
    {
    	Iterator<Widget> di = d.iterator();
    	int found = 0;
        while(di.hasNext())
        {
        	Widget w = di.next();
        	if(w instanceof DescriptionData)
        	{
        		((DescriptionData) w).setHTML("<span font-size:8pt>" + htmlText + "</span>");
        		found++;
        	}
        }
        if (found != 1)
        {
        	String errorMsg = "Found " + found + "Elements instead of 1";
        	Window.alert(errorMsg);
        	throw new IllegalStateException( errorMsg);
        }
    	
    }
}