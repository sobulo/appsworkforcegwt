package com.fertiletech.sap.apply.members.moocs.widgets;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.table.TableViewButtonAction;
import com.fertiletech.sap.apply.table.TablesView;
import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.constants.IconPosition;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;
import org.gwtbootstrap3.client.ui.html.Br;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.client.ui.html.Strong;

import java.util.List;

public class ViewTracks extends Composite implements ValueChangeHandler<String> {

    TablesView trackList;
    TablesView feedbackLoop;
    CourseNamesTypeAhead courseNames;
    Button refreshTrack = new Button("Fetch Track");
    private AsyncCallback<List<TableMessage>[]> callBack = new AsyncCallback<List<TableMessage>[]>() {
        @Override
        public void onFailure(Throwable caught) {
        	if(caught instanceof LoginExpiredException)
        		CustomerAppHelper.delayedReload((LoginExpiredException) caught);
        	else
        		CustomerAppHelper.showErrorMessage("Unable to fetch course track because of: " + caught.getMessage());
        }

        @Override
        public void onSuccess(List<TableMessage>[] result) {
            GWT.log("Received result of size: " + result.length);
            List<TableMessage> trackTable = result[DTOConstants.COURSE_TRACK_LIST_IDX];
            List<TableMessage> feedTable = result[DTOConstants.COURSE_FEED_LIST_IDX];
            GWT.log("Track Size: " + trackTable.size());
            GWT.log("Feed Size: " + feedTable.size());
            boolean init = trackList.isInitialized();
            trackList.showTable(trackTable);
            GWT.log("Show table completed for track");
            feedbackLoop.showTable(feedTable);
            GWT.log("Show table completed for feed");

            if (!init) {
                //Window.alert("Adding view button");
                trackList.addButton("View", htmlTrackViewer, ButtonType.DEFAULT, IconType.EYE, "Update");
                //Window.alert("View Button Added");
                feedbackLoop.addButton("View", htmlFeedViewer, ButtonType.DEFAULT, IconType.EYE, "Feedback");
                //Window.alert("Feedback added");
            }
        }
    };

    private AsyncCallback<String> viewerCallback = new AsyncCallback<String>() {
        @Override
        public void onFailure(Throwable caught) {
        	if(caught instanceof LoginExpiredException)
        		CustomerAppHelper.delayedReload((LoginExpiredException) caught);
        	else
        		CustomerAppHelper.showErrorMessage("Couldn't loading details because of: " + caught.getMessage());
        }

        @Override
        public void onSuccess(String result) {
            CustomerAppHelper.showInfoBox(result);
        }
    };
    TableViewButtonAction htmlTrackViewer = new TableViewButtonAction() {
        @Override
        public void doClick(TableMessage m) {
            String course = courseNames.getCourseName();
            int feedId = (int) Math.round(m.getNumber(DTOConstants.FEED_TRACK_POS_NUM_IDX));
            Moocs.App.getInstance().getTrack(feedId, course, viewerCallback);
        }
    };

    TableViewButtonAction htmlFeedViewer = new TableViewButtonAction() {
        @Override
        public void doClick(TableMessage m) {
            String course = courseNames.getCourseName();
            int feedId = (int) Math.round(m.getNumber(DTOConstants.FEED_BACK_POS_NUM_IDX));
            Moocs.App.getInstance().getFeed(feedId, course, viewerCallback);
        }
    };

    public ViewTracks() {
        FlowPanel panel = new FlowPanel();
        Row courseSearch = new Row();
        Row container = new Row();;
        Strong trackHeader = new Strong("Course Tracks");

        Strong appHeader = new Strong("Track Reviews");

        Column courseCol = new Column(ColumnSize.XS_12, ColumnSize.MD_6);
        Column buttonCol = new Column(ColumnSize.XS_12, ColumnSize.MD_6);
        Column trackCol = new Column(ColumnSize.XS_12, ColumnSize.MD_6);
        Column feedCol = new Column(ColumnSize.XS_12, ColumnSize.MD_6);
        courseNames = new CourseNamesTypeAhead();
        trackList = new TablesView();
        feedbackLoop = new TablesView();

        //setup search
        courseCol.add(courseNames);
        courseNames.addChangeNofication(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                trackList.clear();
                feedbackLoop.clear();
            }
        }); //clears the tables if change occurs

        //setup Buttton
        refreshTrack.setBlock(true);
        refreshTrack.setIconPosition(IconPosition.LEFT);
        refreshTrack.setIcon(IconType.SEARCH_PLUS);
        refreshTrack.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String course = courseNames.getCourseName();
                if (course == null) {
                    CustomerAppHelper.showErrorMessage("Unable to retrieve course information");
                    return;
                }
                if(ClientUtils.NPMBUserCookie.getCookie().isLicensed())
                	setCourse(course, false);
                else if(ClientUtils.alreadyLoggedIn()) 
                	CustomerAppHelper.showErrorMessage("Current membership license required");
                else
                	CustomerAppHelper.showErrorMessage("Login to proceed");
            }
        });

        buttonCol.add(refreshTrack);

        //setup tracks
        trackCol.add(trackHeader);
        trackCol.add(trackList);

        //setup feedback
        feedCol.add(appHeader);
        feedCol.add(feedbackLoop);

        //setup widget
        container.add(trackCol);
        container.add(feedCol);
        courseSearch.add(courseCol);
        courseSearch.add(buttonCol);
        panel.add(courseSearch);
        panel.add(new Br());
        panel.add(container);
        initWidget(panel);

    }

    private void setCourse(String courseName, boolean updateDisplay) {
        if (courseNames.isSteadyState(courseName)) {
            if (updateDisplay)
                courseNames.setCourseName(courseName, true);
            Moocs.App.getInstance().getMemberTrackFeed(courseName, callBack);
        }

    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
        final boolean isRegistered = ClientUtils.NPMBUserCookie.getCookie().isRegistered();
        final boolean isLicensed = ClientUtils.NPMBUserCookie.getCookie().isLicensed();
        if (isLicensed)
            setCourse(event.getValue(), true);
        else if (!isRegistered)
            CustomerAppHelper.showErrorMessage("Login and register before attempting to view track updates");
        else if (!isLicensed)
            CustomerAppHelper.showErrorMessage("Purchase a license before attempting to view track updates. License pricing starts at $1 (360 Naira).");

    }
}
