package com.fertiletech.sap.apply.members.moocs.widgets;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.TableMessage;
import com.fertiletech.sap.shared.TableMessageHeader;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import org.gwtbootstrap3.extras.typeahead.client.base.StringDataset;
import org.gwtbootstrap3.extras.typeahead.client.ui.Typeahead;

import java.util.HashSet;
import java.util.List;

public class CourseNamesTypeAhead extends Composite{
    private Typeahead<String> courseNames;

    private  StringDataset  COURSE_SET;
    AsyncCallback<List<TableMessage>> courseListCallBack = new AsyncCallback<List<TableMessage>>() {
        @Override
        public void onFailure(Throwable caught) {
            CustomerAppHelper.showErrorMessage("unable to initialize list of courses " + caught.getMessage());
        }

        @Override
        public void onSuccess(List<TableMessage> result) {
            HashSet<String> courseSet = new HashSet<String>();
            if((result.size() >0) && (result.get(0) instanceof TableMessageHeader))
                result.remove(0);
            if(result.size() == 0 )
            {
                CustomerAppHelper.showErrorMessage("No courses found in the databse");
                return;
            }
            for(TableMessage m : result)
            {
                String courseName = m.getText(DTOConstants.COURSE_NAME_TXT_IDX);
                courseSet.add(courseName);
            }

            COURSE_SET = new StringDataset(courseSet);
            courseNames.setDatasets(COURSE_SET);
            courseNames.reconfigure();
            courseNames.setPlaceholder("Enter name of course");

            if(notifyHandler != null) { //this might yield a race condition
                addChangeNofication(notifyHandler);
                notifyHandler = null;
            }
        }
    };


    public CourseNamesTypeAhead()
    {
        courseNames = new Typeahead<>();
        initWidget(courseNames);
        Moocs.App.getInstance().getAllCourses(courseListCallBack);
    }

    public void setCourseName(String name)
    {
        setCourseName(name, false);

    }

    public void setCourseName(String name, boolean fireEvent)
    {
        if(isSteadyState(name)) {
            this.courseNames.setValue(name, fireEvent);
        }

    }

    ValueChangeHandler notifyHandler = null;
    public void addChangeNofication(ValueChangeHandler<String> handler)
    {
        if(courseNames == null)
            notifyHandler = handler;
        else
            courseNames.addValueChangeHandler(handler);
    }


    public String getCourseName()
    {
        String val = courseNames.getValue();
        if(isSteadyState(val))
            return val;
        else
            return null;
    }

    public boolean isSteadyState(String name)
    {

        if (COURSE_SET == null )
        {
            CustomerAppHelper.showErrorMessage("Course list not yet initialized. Try refreshing your browser");
            return false;
        }
        else if (COURSE_SET.getData().contains(name))
            return true;
        else
        {
            CustomerAppHelper.showErrorMessage("Specified " + name + " not found in AW curriculum suggestions" +
                    ". Please note that course names are case sensitive. Type exactly a coursename that matches " +
                    "  text displayed in your suggestion box");
            return false;
        }
    }
}
