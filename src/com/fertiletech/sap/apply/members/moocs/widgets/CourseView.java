package com.fertiletech.sap.apply.members.moocs.widgets;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.client.GUIConstants;
import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.shared.DTOConstants;
import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;

import java.util.ArrayList;
import java.util.List;

public class CourseView extends Composite implements HasValueChangeHandlers<String>{

    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return addHandler(handler, ValueChangeEvent.getType());
    }

    interface CourseViewUiBinder extends UiBinder<Widget, CourseView> {
    }

    private static CourseViewUiBinder ourUiBinder = GWT.create(CourseViewUiBinder.class);


    @UiField
    Anchor courseLink;

    @UiField
    Button track;
    


    public CourseView() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    public void initView(final TableMessage courseDTO, final boolean isRegistered, final boolean isLicense)
    {
        final String courseId = courseDTO.getText(DTOConstants.COURSE_NAME_TXT_IDX);
        
        /*accronym.setText();
        updateField("Course", DTOConstants.COURSE_NAME_TXT_IDX, courseName, courseDTO);
        updateField("Institution", , instituton, courseDTO);
        instructor.setHTML());*/
        
        
       
        courseLink.setText(courseId); 
        courseLink.addClickHandler(new ClickHandler() {	
			@Override
			public void onClick(ClickEvent event) {
				
	        	StringBuilder display = new StringBuilder("<p style='margin-left: 10px'>");
	        	display.append("<div style='font-weight: bold'>").append(courseDTO.getText(DTOConstants.COURSE_INST_ACCRONYM_TXT_IDX)).append("</div>");
	        	display.append("<div><b>Institution: </b>").append(courseDTO.getText(DTOConstants.COURSE_INSTITUTION_TXT_IDX)).append("</div>");
	        	display.append("<div><b>Course: </b>").append(courseId).append("</div><br/><b>Instructor(s)</b><br/>");
	        	display.append("<div style='font-size: small'>").append(getListOfProfessors(courseDTO.getText(DTOConstants.COURSE_PROF_TXT_IDX))).append("</div><br/>");
	   
	        	String coursePage = courseDTO.getText(DTOConstants.COURSE_LINK_TXT_IDX);
	        	display.append(getUrl("MOOC Page", coursePage));
	 
	        	
	        	String category = courseDTO.getText(DTOConstants.COURSE_CATEGORY_TXT_IDX);
	        	double order = courseDTO.getNumber(DTOConstants.COURSE_ORDER_NUM_IDX);
	        	String orderStr = GUIConstants.DEFAULT_NUMBER_FORMAT.format(order);
				CustomerAppHelper.showInfoBox(category + " - " + orderStr + "a", display.append("</p>").toString());
			}
		});
        track.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {

                startTrack(courseId, isRegistered, isLicense);
            }
        });
        
    }
    
    private String getUrl(String name, String url)
    {
    	return " <a target='_blank' href = '"+url +"'>" + name + "</a> ";
    }
    
    public void disableTracking()
    {
    	track.setText("started");
    	track.setEnabled(false);
    }

    private void startTrack(final String val, final boolean isRegistered, final boolean isLicense)
    {
        AsyncCallback<String> trackCallback = new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
            	if(caught instanceof LoginExpiredException)
            		CustomerAppHelper.delayedReload((LoginExpiredException) caught);
            	else
            		CustomerAppHelper.showErrorMessage("Couldn't start a track for:  "
                + val + " because of: "+ caught.getMessage());
            }

            @Override
            public void onSuccess(String result) {
                CustomerAppHelper.showInfoBox("Looks Good","Self pace requested for " + val + " has been approved. You may visit the edit/view tracks " +
                        " panel to sart tracking " + result);
                disableTracking();
                fireChange(result);

            }
        };
        if(isLicense)
            Moocs.App.getInstance().startTrack(val, trackCallback);
        else if(isRegistered)
            CustomerAppHelper.showErrorMessage("Oops, you must have a license to start tracking courses.");
        else
            CustomerAppHelper.showErrorMessage("Login to proceed.");
    }

    private void fireChange(String val)
    {
        ValueChangeEvent.fire(this, val);
    }

    private static String[] getProfessorName(String data){
        String[] dataArray = data.split("[,]");
        return dataArray;
    }

    private static String getListOfProfessors(String data){
        if(data == null)
            return "";
        List<String[]> listOfProfessors = new ArrayList<>();
        String[] splittedDataArray = data.split("[|]");
        for(String i : splittedDataArray){
            listOfProfessors.add(getProfessorName(i));
        }

        //format to string
        StringBuffer result = new StringBuffer();
        for(String[] prof : listOfProfessors) {

            for(int i = 0; i < prof.length; i++) {
                String val = prof[i];
                if (i > 0) {
                    val = "<i>"  +val + "</i>";
                }
                result.append(val).append("<br/>");
            }
            result.append("<center><hr style='width: 60%;'/></center>");
        }

        return result.toString();
    }
}