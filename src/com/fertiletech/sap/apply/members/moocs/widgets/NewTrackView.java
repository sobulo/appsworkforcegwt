package com.fertiletech.sap.apply.members.moocs.widgets;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.client.Moocs;
import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.extras.summernote.client.ui.Summernote;

import java.util.HashSet;

public class NewTrackView extends Composite implements HasValueChangeHandlers<String>{
    private AsyncCallback<String> addTrackCallBack = new AsyncCallback<String>() {
        @Override
        public void onFailure(Throwable caught) {
        	if(caught instanceof LoginExpiredException)
        		CustomerAppHelper.delayedReload((LoginExpiredException) caught);
        	else
        		CustomerAppHelper.showErrorMessage("add track failed: " + caught.getMessage());
        }

        @Override
        public void onSuccess(String result) {
            CustomerAppHelper.showInfoMessage("Successuflly updated " + result);
            notes.clear();
            fireChange(result);
        }
    };

    private void fireChange(String val)
    {
        ValueChangeEvent.fire(this, val);
    }

    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return addHandler(handler, ValueChangeEvent.getType());
    }

    interface NewTrackViewUiBinder extends UiBinder<Widget, NewTrackView> {
    }

    private static NewTrackViewUiBinder ourUiBinder = GWT.create(NewTrackViewUiBinder.class);

    @UiField
    Summernote notes;

    @UiField
    Button newTrack;

    @UiField
    Button sampleTrack;

    @UiField
    CourseNamesTypeAhead courseNames;


    public static final String SAMPLE_UPDATE = "<p>Here is my work report from 04-DECEMBER-2017 to 08-DECEMBER-2017 "
            + "<br/><b>MONDAY [04-12-17] :</b>  Continued with the Week 4's videos of the MITx Intro. to Python class and solved the finger exercises."
            + "<br/><b>TUESDAY [05-12-17] :</b>  I was finally through with Week 4's videos and on to the Week 4's problem sets."
            + "<br/><b>WEDNESDAY [06-12-17] :</b> I was advised to take it slow and steady, so I watched the week 4's video again to get more ideas on how to solve the problem sets."
            + "<br/><b>THURSDAY  [07-12-17] :</b> Continued with the Wednesday's task above."
            + "<br/><b>FRIDAY [08-12-17] :</b>  I downloaded the course codes meant to setup the problem sets and also spent the whole day trying to "
            + "come up with how to solve one of the question of the problem</p>";

    public NewTrackView() {
        initWidget(ourUiBinder.createAndBindUi(this));
        HashSet<String> courseSet = new HashSet<>();

        newTrack.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final boolean isRegistered = ClientUtils.NPMBUserCookie.getCookie().isRegistered();
                final boolean isLicensed = ClientUtils.NPMBUserCookie.getCookie().isLicensed();
                if(isLicensed) {
                    String update = notes.getCode();
                    if (update.length() < (SAMPLE_UPDATE.length() / 2)) {
                        CustomerAppHelper.showErrorMessage("You need to provide a more detailed status update. This one is too" +
                                " short. Clice on 'view sample update' for an example. You do not have to use the same format as the example but your update" +
                                " should provide a similar level of detail");
                        return;
                    }
                    String courseName = courseNames.getCourseName();
                    if (courseName == null) {
                        //simply return, because null value indicates course selector already displayed an error message
                        return;
                    }
                    Moocs.App.getInstance().addTrack(courseName, update, addTrackCallBack);
                }
                else
                {
                    if(!isRegistered)
                        CustomerAppHelper.showErrorMessage("Login and register before attempting to update tracks");
                    else if(!isLicensed)
                        CustomerAppHelper.showErrorMessage("Purchase a license before attempting to update tracks. License pricing starts at $1 (360 Naira)");
                }
            }
        });
        sampleTrack.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CustomerAppHelper.showInfoMessage(
                        SAMPLE_UPDATE, 20, true);
            }
        });

    }
}