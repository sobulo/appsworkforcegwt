package com.fertiletech.sap.apply.members.moocs;

import com.fertiletech.sap.apply.CustomerAppHelper;
import com.fertiletech.sap.apply.HyperlinkedPanel;
import com.fertiletech.sap.apply.NameTokens;
import com.fertiletech.sap.client.Accounting;
import com.fertiletech.sap.shared.DTOConstants;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.HashMap;
import java.util.HashSet;

public class MoocPanelsHelper {

    private static int moocIdx = 0;
    private final static int MOOC_DAVID_IDX = moocIdx++; //advise on courses to take -- creates track list
    private final static int MOOC_TRACK_IDX = moocIdx++; //tracking progress on those courses
    //private final static int MOOC_LEARN_IDX = moocIdx++; //tracking progress on those courses
    private final static HyperlinkedPanel[] moocPanels = new HyperlinkedPanel[moocIdx];





    public static HyperlinkedPanel[] getMoocPanels() {
    	if(moocPanels[0] != null && moocPanels[0] instanceof HyperlinkedPanel)
    		return moocPanels;

        moocPanels[MOOC_DAVID_IDX] = new HyperlinkedPanel() {


            Hyperlink link = null;
            CourseTrackingTabPanel display = null;
            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Start MOOC", NameTokens.MOOC_ADVISE);
                }
                return link;
            }

            @Override
            public IconType getIcon() {
                return IconType.FOLDER_OPEN;
            }

            @Override
            public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args) {
                if(display != null) //compute just once?
                    return display;

                display = new CourseTrackingTabPanel();
                return display;
            }
        };

        moocPanels[MOOC_TRACK_IDX] = new HyperlinkedPanel() {


            Hyperlink link = null;
            EditViewTracksTabPanel display = null;
            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Monitor MOOC", NameTokens.MOOC_TRACK);
                }
                return link;
            }

            @Override
            public IconType getIcon() {
                return IconType.CALENDAR_CHECK_O;
            }

            @Override
            public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args) {
                if(display != null)
                    return display;
                display = new EditViewTracksTabPanel();
                return display;

            }
        };

        /*moocPanels[MOOC_LEARN_IDX] = new HyperlinkedPanel() {
            HashMap<String, String> TEMPLATE_MAP = null;

            AsyncCallback<HashMap<String, String>> billList = new AsyncCallback<HashMap<String, String>>() {
                @Override
                public void onFailure(Throwable caught) {
                    CustomerAppHelper.showErrorMessage("Referesh your browser: " + caught.getMessage());
                }

                @Override
                public void onSuccess(HashMap<String, String> resultBloated) {
                    HashMap<String, String> result = new HashMap<String, String>();
                    TEMPLATE_MAP = resultBloated;
                    HashSet<String> requests = new HashSet(DTOConstants.AW_LEARN.keySet());
                    //requests.add(DTOConstants.AW_REQUEST_LETTER);
                    final int LIMIT = 2;
                    int count = 0;
                    Row body = new Row();
                }
            };


            Hyperlink link = null;
            FlowPanel display = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("MOOC Support", NameTokens.MOOC_LEARN);
                }
                return link;
            }

            @Override
            public IconType getIcon() {
                return IconType.PIE_CHART;
            }

            @Override
            public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args) {
                if (display != null) //compute just once?
                    return display;





                //setup display
                display = new FlowPanel();





                return display;
            }


            private String getBillKey(String billTemplateName)
            {
                for(String key : TEMPLATE_MAP.keySet())
                    if (TEMPLATE_MAP.get(key).equals(billTemplateName))
                        return key;
                return null;
            }
        };*/
        return moocPanels;
    }
}
