package com.fertiletech.sap.apply.members;

import org.gwtbootstrap3.client.ui.html.Small;

import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MembersOnlyPanel extends Composite{
    interface MembersOnlyPanelUiBinder extends UiBinder<Widget, MembersOnlyPanel> {
    }





    private static MembersOnlyPanelUiBinder ourUiBinder = GWT.create(MembersOnlyPanelUiBinder.class);

    public MembersOnlyPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));

    }

    public void setDefaultMessages(boolean logedin)
    {
        String message = "Sign in with your google/gmail account to continue tracking your MOOCs pace. Email <a href='mailto:hello@appsworkforce.com'>hello@appsworkforce.com</a> for general enquiries.";
        if(logedin)
        {
            ClientUtils.NPMBUserCookie cookie = ClientUtils.NPMBUserCookie.getCookie();
            String name = cookie.getUserName();
            String email =  cookie.getEmail();
            if(name == null)
                name = "";
            else
                name += ", ";
            String memberId = cookie.getMemberID();
            if(memberId == null)
                message = "<p>Hello " + name + " No registration found for: " + email 
                + ", send an email to  <a href='mailto:apply@appsworkforce.com'>apply@appsworkforce.com</a> for registration/pricing enquries";
            else
            message = "<i>Call 0906-176-6041 or send an email to <a href='mailto:apply@appsworkforce.com'>apply@appsworkforce.com</a> to request a license</i><br/><br/>" + memberId;
        }
        description.setHTML(message);
    }


    @UiField
    Small description;

    public void setDescription(String htmlDescription) {description.setHTML(htmlDescription);}


}