package com.fertiletech.sap.apply.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface QAMenu extends ClientBundle{
    @Source ("qamenu.css")
    QACss css();
}