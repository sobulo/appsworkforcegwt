package com.fertiletech.sap.apply.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface AppMenu extends ClientBundle{
    @Source ("appmenu.css")
    AppCss css();
}