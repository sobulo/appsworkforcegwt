package com.fertiletech.sap.apply.resources;

import com.google.gwt.resources.client.CssResource;

public interface AppCss extends CssResource{
    String dashMenu();
    String dashMenuAlt();
    String dashSeperators();
}