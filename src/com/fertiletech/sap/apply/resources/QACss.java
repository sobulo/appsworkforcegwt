package com.fertiletech.sap.apply.resources;

import com.google.gwt.resources.client.CssResource;

public interface QACss extends CssResource{
    String qaMenu();
    String qaMenuAlt();
    String qaSeperators();
}