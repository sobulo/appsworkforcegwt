/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.sap.apply;

import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.constants.IconType;

/**
 *
 * @author Administrator
 */
public interface HyperlinkedPanel {
    public Hyperlink getLink();
    public IconType getIcon();
    public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, boolean isLicensed, String[] args);
}
