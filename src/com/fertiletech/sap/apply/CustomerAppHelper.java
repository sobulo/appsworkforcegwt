/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.sap.apply;

import com.fertiletech.sap.apply.images.GeneralImageBundle;
import com.fertiletech.sap.apply.members.AuditPanel;
import com.fertiletech.sap.apply.members.FAQPanel;
import com.fertiletech.sap.apply.members.HistoryBuild;
import com.fertiletech.sap.apply.members.IQPanel;
import com.fertiletech.sap.apply.members.SchoolPanel;
import com.fertiletech.sap.apply.members.moocs.MoocPanelsHelper;
import com.fertiletech.sap.apply.members.util.FlowModal;
import com.fertiletech.sap.client.*;
import com.fertiletech.sap.shared.LoginExpiredException;
import com.fertiletech.sap.shared.oauth.ClientUtils;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Geolocation.PositionOptions;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.extras.animate.client.ui.constants.Animation;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.HashSet;

//import org.gwtbootstrap3.extras.growl.client.ui.GrowlType;

/**
 *
 * @author Segun Razaq Sobulo
 * 
 *         IMPORTANT: only use package access or private access in this class.
 *         This is to prevent unnecessary loading of certain gwt files in parts
 *         of the app that are public
 * 
 *         TODO run gwt code splitting analytic tools to explicitly confirm
 *         which classes get loaded by public sections of the app
 */
public class CustomerAppHelper {

	//private static int indexCounter = 0;
	//private final static int APPLICATION_FORM_IDX = indexCounter++;
	//private final static int UPLOAD_IDX = indexCounter++;
	//private final static int PRINT_FORM_IDX = indexCounter++;
	//private final static int BILL_SIGN_UP_IDX = indexCounter++;
	//private final static int STATUS_TABLE_IDX = indexCounter++;
	//private static HyperlinkedPanel[] appPanels = new HyperlinkedPanel[indexCounter];

	//private static int billIdx = 0;
	//private final static int BILL_HSTRY_IDX = billIdx++;
	//private final static int BILL_INVOICE_DL_IDX = billIdx++;
	//private final static int BILL_INDICATE_IDX = billIdx++;
	//private final static HyperlinkedPanel[] billPanels = new HyperlinkedPanel[billIdx];

	//private static int moocIdx = 0;
	//private final static int VISION_SCHOOL_IDX = moocIdx++;
	//private final static HyperlinkedPanel[] visionPanels = new HyperlinkedPanel[moocIdx];

	public final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
	public final static MortgageServiceAsync LOAN_MKT_SERVICE = GWT.create(MortgageService.class);
	public static GeneralImageBundle GENERAL_IMAGES = GWT.create(GeneralImageBundle.class);
	private final static String APP_PAGE_URL_PREFIX = GWT.getHostPageBaseURL();
	final static String APP_PAGE_URL;
	final static int TOKEN_ARGS_PARAM_IDX = 0;
	final static int TOKEN_ARGS_RECENTLOAN_IDX = 1;
	final static int TOKEN_ARGS_EMAIL_IDX = 2;
	final static int TOKEN_ARGS_EXTRA_IDX = 3;

	static {
		if (!GWT.isProdMode()) // true if dev mode
			APP_PAGE_URL = APP_PAGE_URL_PREFIX + GUIConstants.GWT_CODE_SERVER;
		else
			APP_PAGE_URL = APP_PAGE_URL_PREFIX;
	}

	static HyperlinkedPanel[] initializePanels()
	{
		//put all panels in one array


		HyperlinkedPanel[] vision = getVisionPanels();
		HyperlinkedPanel[] moocs = MoocPanelsHelper.getMoocPanels();
		HyperlinkedPanel[] result = new HyperlinkedPanel[moocs.length + vision.length];

		int count = 0;

		for(int idx = 0; idx < vision.length; idx++) {
			result[count] = vision[idx];
			count++;
		}

		for(int idx = 0; idx < moocs.length; idx++) {
			result[count] = moocs[idx];
			count++;
		}
		return result;
	}



	
	 private final static HyperlinkedPanel[] visPanels = new HyperlinkedPanel[3];
	public static HyperlinkedPanel[] getVisionPanels() {
		Window.alert("Made it Here");
		if(visPanels[0] != null && visPanels[0] instanceof  HyperlinkedPanel)
			return visPanels;
		Window.alert("Made it Here 2");
		HyperlinkedPanel[] panels = {new AuditPanel(),new FAQPanel(), new HistoryBuild()};
		for(int p = 0; p < panels.length; p++)
			visPanels[p] = panels[p];
		Window.alert("Made it Here 3");
		return visPanels;
	}
	




	/*static Icon ico = new Icon();
	static Modal infoBox = new Modal();
	static HTML message = new HTML();

	static {
		ico.setSize(IconSize.TIMES3);
		HorizontalPanel temp = new HorizontalPanel();
		temp.setSpacing(10);
		temp.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		temp.add(ico);
		temp.add(message);
		temp.setSpacing(20);
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(10);
		Button dismiss = new Button("OK");
		dismiss.setType(ButtonType.PRIMARY);
		dismiss.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				infoBox.hide();
			}
		});
		dismiss.setBlock(true);
		container.add(temp);
		container.add(dismiss);
		infoBox.add(container);
	}*/

	private static NotifySettings goGood, goBad, goDelay;

	static {
		
		 goGood = NotifySettings.newSettings(); goBad =NotifySettings.newSettings(); goDelay = NotifySettings.newSettings();
		 goGood.setType(NotifyType.SUCCESS); goBad.setType(NotifyType.DANGER); goDelay.setType(NotifyType.WARNING);
		 goBad.setAnimation(Animation.FADE_IN,
		 Animation.FADE_OUT);
		 //goBad.setAnimation(Animation.TADA, Animation.BOUNCE_OUT_DOWN);
		 goDelay.setAnimation(Animation.LIGHTSPEED_IN, Animation.LIGHTSPEED_OUT);
		goGood.setPlacement(NotifyPlacement.BOTTOM_CENTER);
		goBad.setPlacement(NotifyPlacement.TOP_CENTER);
		goDelay.setPlacement(NotifyPlacement.TOP_LEFT);
	}

	public static void showErrorMessage(String error) { Notify.notify("AW: ",error,IconType.HEARTBEAT,goGood);
	}

	public static void showInfoMessage(String info) {
		//Notify.notify("Looks Good - ", info, IconType.SMILE_O, goGood);
	}
	public static void showInfoMessage(String info, int secondsDelay)
	{
		showInfoMessage(info,secondsDelay, false); //does nothing, false disables display
	}
	public static void showInfoMessage(String info, int secondsDelay, boolean forceShow)
	{
		if(forceShow) {
			goDelay.setDelay(secondsDelay * 1000);
			Notify.notify("FYI - ", info, IconType.EXCLAMATION_CIRCLE, goDelay);
		}
	}
	public static String ARG_HACK_PREFIX = "aw-";
	public static String getLoanID(String[] args) {


		return (args[TOKEN_ARGS_PARAM_IDX] == null ? args[TOKEN_ARGS_RECENTLOAN_IDX] : args[TOKEN_ARGS_PARAM_IDX]);
	}

	public static String getArgument(String[] args)
	{
		return   args[TOKEN_ARGS_EXTRA_IDX]; //remove hack
	}

	public static void setupCompanyOracle(SuggestBox suggestBox) {
		String[] companyListVals = {};
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) suggestBox.getSuggestOracle();
		for (int counter = 0; counter < companyListVals.length; counter++) {
			oracle.add(companyListVals[counter]);
		}
	}

	private static HashSet<String> longitudeCache = new HashSet<String>();

	public static void updateLatLngValsDeprecated(final String loanId) {
		if (ClientUtils.NPMBUserCookie.getCookie().isOps())
			return; // ensure only applicant positions are tracked
		if (longitudeCache.contains(loanId))
			return;
		if (Geolocation.isSupported()) {
			PositionOptions opts = new Geolocation.PositionOptions();
			opts.setHighAccuracyEnabled(true);
			opts.setTimeout(15 * 1000); // wait 15 seconds
			Geolocation.getIfSupported().getCurrentPosition(new Callback<Position, PositionError>() {

				@Override
				public void onSuccess(Position result) {
					CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, result.getCoordinates().getLatitude(),
							result.getCoordinates().getLongitude(), null);
					longitudeCache.add(loanId);
				}

				@Override
				public void onFailure(PositionError reason) {
					// Window.alert("Error getting gps coordinates: " +
					// reason.getMessage());
					CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
				}
			}, opts);
		} else {
			CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
			longitudeCache.add(loanId);
		}
	}
	
	public static void markLocked(Widget container, boolean locked)
	{
		if(locked)
		{
			container.removeStyleName("markOpened");
			container.addStyleName("markLocked");
		}
		else
		{
			container.removeStyleName("markLocked");
			container.addStyleName("markOpened");
		}
	}

	private static String getStatusDisplay(String msg, boolean isError)
	{
		String errorColor ="red";
		String okColor="green";
		String displayStr = "<p style='color:" + (isError?errorColor:okColor) +
				"'>" + msg + "</p>";
		return displayStr;

	}

	public static void updateStatus(Paragraph status, String msg, boolean isError)
	{
		status.setHTML(getStatusDisplay(msg, isError));
	}

	public static void updateStatus(Small status, String msg, boolean isError)
	{
		status.setHTML(getStatusDisplay(msg,isError));
	}


	public static void showInfoBox(String msg) {
		FlowModal popup = new FlowModal("Information");
		popup.showHTML(msg);
	}

	public static void showInfoBox(String title, String msg) {
		FlowModal popup = new FlowModal(title);
		popup.showHTML(msg);
	}

	public static void displaySlowInternetX(Throwable caught)
	{
		if (caught instanceof StatusCodeException)
			showErrorMessage("Looks like your internet connection is currently slow. Try connecting later");
	}


	//http://www.sitepoint.com/google-analytics-track-javascript-ajax-events/ for how to use
	public static native void trackEvent(String category, String action, String label) /*-{
		$wnd._gaq.push([ '_trackEvent', category, action, label ]);
	}-*/;

	public static native void trackEvent(String category, String action, String label, int intArg) /*-{
		$wnd._gaq.push([ '_trackEvent', category, action, label, intArg ]);
	}-*/;

	public static native void trackPageview(String url) /*-{
		$wnd._gaq.push([ '_trackPageview', url ]);
	}-*/;
	
	private static native void forceReload() /*-{
    $wnd.location.reload(true);
   }-*/;
	
	public static void delayedReload(LoginExpiredException ex)
	{
		final int DELAY = 5;
		
		showErrorMessage(ex.getMessage() + " .Please wait, your browser will refresh in " + DELAY + " seconds");
		Timer t = new Timer() {
			
			@Override
			public void run() {
				forceReload();
			}
		};
		t.schedule(DELAY * 1000);
		
	}
}
