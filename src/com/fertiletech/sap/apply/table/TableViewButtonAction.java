package com.fertiletech.sap.apply.table;

import com.fertiletech.sap.shared.TableMessage;

public interface TableViewButtonAction {
    void doClick(TableMessage m);
}
