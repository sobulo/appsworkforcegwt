<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START list] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="container">
      <section id="main-body">
        <div class="intro item">
          <h1 class="m-heading">Who Are We?</h1>
          <p>
            AW is a workforce cloud that scales to meet your development,
            operational and marketing needs. AW index is surging and our cloud
            is scaling during covid, membership availability is high and for
            this reason our capable workforce is available for free. AW members
            are MITx certified.
          </p>

          <p>
            Join our remote workforce. We've been hiring and training programmers since inception (2016). To learn more about how to build a meaningful career in the 21st
            century, click the button below.
          </p>


          <!-- #006600; -->
          <br />
          <a class="btn" href="https://docs.google.com/forms/d/1s5O8w2Z4XeJWbCRbaM8CraIhdZiZroJgS2FncrOqBlk/edit" target="_blank">Career Development Form</a>
        </div>

        <div class="item img"></div>
      </section>
    </div>


    <!-- Place this exact code where you want the Jobscore job widget to appear on the page.  The widget will expand to the size of the containing div -->
  <!--  
    <div class='jobscore-jobs' data-group-by='department' data-link-text-color='#3486d3' data-show-talent-network='false' >
<div class='js-fallback' style='color:#888'><a style='color:#888;text-decoration:underline;' href='https://careers.jobscore.com/jobs/appsworkforce'>AppsWorkforce jobs</a> powered by <a style='color:#888' href='https://www.jobscore.com/'><img style='padding:0;' align='bottom' src='https://careers.jobscore.com/images/new/jobscore_logo_embed.png' alt='Applicant Tracking, Social Recruiting and Software Hiring Solution - JobScore'></a></div>
</div>
-->
<!-- /jobscore-jobs -->

<!-- [END list] -->
