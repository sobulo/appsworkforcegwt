<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START form] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.fertiletech.sap.client.NigeriaConstants"%>
<div class="container">
	<h3>
	AW Cloud
	</h3>
AW has been running on <a href="cloud.google.com">Google's cloud</a> since 2015. We share our experiences to help get you started programming on the cloud. 
At end of the training series, you will know how to deploy AI on the cloud<br/>
<hr/>
<br/>

	<form method="POST" action="${destination}">

		<div class=row>
	<div class="form-group col-sm-4">
				<label for="id">AW ID </label>
					<input type="text" name="id" id="id"/>
			</div>
			
			    <div class="form-group col-sm-4" >
      <label for="program">Program? </label>
      <select name="program" id="program">
      <option>Data Science</option> <!-- see 6x2 bullets -->
      <option>Java</option> <!-- see codecademy curriculum  -->
      <option>Logging</option> <!-- mention stack driver -->
      <option>Computational thinking</option> <!-- list appengine mention 6x2 features  -->
      <option>Object Oriented Database</option> <!-- see datastore features  -->
      <option>Parallel Processing</option> <!-- appengine link -->
      <option>Artificial Intelligence</option> <!-- appengine link -->

      </select>
      
      
    </div>
    
        <div class="form-group col-sm-4" >
      <label for="laptop">Laptop? </label>
      <select name="laptop" id="laptop">
      <option>yes</option>
      <option>no</option>
          <option>faulty</option>  
      </select>
    </div>


</div>


		
		<div class=row>
		
		        <div class="form-group col-sm-4" >
      <label for="6x">Education? </label>
      <select name="6x" id="6x">
      <option>MITx</option>
      <option>edX</option> 
           <option>None of the above</option> 
      </select>
    </div>
    
            <div class="form-group col-sm-4" >
      <label for="role">Role?</label>
      <select name="role" id="role">
      <option>Parent</option>
      <option>Student</option>
          <option>Recruiter</option>  
      </select>
    </div>








		



       
		
		<div class="col-sm-4"><button type="submit" class="btn btn-primary btn-block">Next</button></div>
</div>
</form>
</div>
<!-- [END form] -->
