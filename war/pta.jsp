<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START form] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.fertiletech.sap.client.NigeriaConstants"%>
<div class="container">
	<h3>
	Career Kickstart
	</h3>
Learn java<br/>
<br/>
Mobile and web app development using java. Prior completion of 6.0.0.1x problem set 1 (week 1) is strongly recommended to get the most out of training sessions. 
<hr/>
<br/>

	<form method="POST" action="${destination}">

		<div class=row>
			<div class="form-group col-sm-4">
				<label for="state">Resident State </label><select name="state" id="state" class="form-control">
					<%
						
						for (String st : NigeriaConstants.STATE_MAP.keySet()) {
							String sel = "";
							if(st.equals("Oyo State"))
								sel = "selected";
							out.println("<option " + sel + ">" + st + "</option>");
		
						}
					%>
				</select>
			</div>

			<div class="form-group col-sm-4">
				<label for="name">Surname, First name</label>  <input
					type="text" name="name" id="name"
					value="${fn:escapeXml(book.name)}" class="form-control" />
			</div>
		
		

			<div class="form-group col-sm-4">
				<label for="email">Email</label> <input type="text" name="email" id="email"
					value="${fn:escapeXml(book.email)}" class="form-control" />
			</div>
</div>


		
		<div class=row>




    <div class="form-group col-sm-8" >
      <label for="message">What program are you interested in?</label>
      <select name="message" id="message">
      <option>3 days java workshop</option>
      <option>3 days jsp AppEngine</option>
      <option>3 days cloud Storage</option>
      <option>3 days Datastore</option>
      <option>3 days SQL</option>
        <option>3 days endpoints</option>
          <option>3 days android apps</option>
      </select>
      
      
    </div>



		



       
		
		<div class="col-sm-4"><button type="submit" class="btn btn-primary btn-block">Next</button></div>
</div>
</form>
</div>
<!-- [END form] -->
