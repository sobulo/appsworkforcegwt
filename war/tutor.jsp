<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START form] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="container">
	<h3>
	Programming Fundamentals Teacher
	</h3>
<a href="https://www.youtube.com/watch?v=ww2BdhILIio">MITx ECE department demonstrates problem solving with python</a><br/>
<br/>
<b>Topics: </b>
Arithmetic, logic/conditionals, counting, data-structures (lists and dictionaries), 
iterative/recursive methods and word problems<br/><br/>

<b>Finance Project</b><br/>
Prerequisite:Prior Mathematics  of compound interest
Amortized Credit (spreadsheet and charts) Library
<br/>
<hr/>
<br/>

	<form method="POST" action="${destination}">

		<div class=row>
	<div class="form-group col-sm-4">
				<label for="id">AW ID</label>
					<input type="text" name="id" id="id"/>
					<input type="hidden" name ="type" value="${page}"/>
			</div>
			
			    <div class="form-group col-sm-4" >
      <label for="program">Program?</label>
      <select name="program" id="program">
      <option>I need a tutor in my state</option>
      <option>I want to teach in my state</option>
      </select>
      
      
    </div>
    
<div class="form-group col-sm-4"><button type="submit" class="btn btn-primary">Next</button></div>


</div>


		

</form>
</div>
<!-- [END form] -->
