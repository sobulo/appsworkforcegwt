<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START view] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="container">
	<h3>Review</h3>
	Use the back button if want to edit
	any fields. &nbsp; &nbsp;
<button onclick="goBack()" class="btn ${backstyle}">Go Back</button> &nbsp;&nbsp;
	<form method="POST" action="customer">

		<div class="form-group">
			<label for="id">AW ID:</label> <span>${book.state}</span> <span style=""font-size: smaller;"> 
			  <c:choose>
			  <c:when test="${empty lead}">
			  			(${awerr} - <a href="/update">New user? Click to join AW</a>
					)

  </c:when>
   <c:otherwise>
     ( current member <input type="hidden" name="lead" value="${lead}"/>)

					</c:otherwise>
					</c:choose>
			</span>
			<input type="hidden" name="id" value="${book.state}"/>
		</div>



		<div class="form-group">
			<label for="program">Program: </label> <span>${book.name}</span><span style="font-size: smaller;"> (MOOC program)</span> 
			<input type="hidden" name="program" value="${book.name}"/>
		</div>

		<div class="form-group">
			<label for="laptop">PC: </label> <span>${book.email}</span> <span style="font-size: smaller;"> (personal computer?
					)</span>
			<input type="hidden" name="laptop" value="${book.email}"/>
		</div>
	
		
				<div class="form-group">
			<label for="6x">Got MITx?: </label> <span>${book.message}</span> <span style="font-size: smaller;"> (your personal
					email address)</span>
			<input type="hidden" name="6x" value="${book.message}"/>
		</div>
		
						<div class="form-group">
			<label for="role">Role?: </label> <span>${book.ofykey}</span> <span style="font-size: smaller;"> (your personal
					email address)</span>
			<input type="hidden" name="role" value="${book.ofkey}"/>
		</div>






		
		<button type="submit" class="btn btn-success" ${book.style}>Submit Request${book.style}</button>
	</form>

	<script>
		function goBack() {
			window.history.back();
		}
	</script>
	

	




</div>

<!-- [END view] -->
