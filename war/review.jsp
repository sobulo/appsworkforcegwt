<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START view] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="container">
	<h3>Review</h3>
	Confirm the information below looks ok then hit submit button below. Use the back button if want to edit
	any fields. &nbsp; &nbsp;
<button onclick="goBack()" class="btn ${backstyle}">Go Back</button> &nbsp;&nbsp;
	<form method="POST" action="create">

		<div class="form-group">
			<label for="state">State:</label> <span>${book.state}</span> <span style="font-size: smaller;"> (the state that you
					live and learn in)</span>
			<input type="hidden" name="state" value="${book.state}"/>
		</div>



		<div class="form-group">
			<label for="name">Name: </label> <span>${book.name}</span><span style="font-size: smaller;"> (last name, first name)</span> 
			<input type="hidden" name="name" value="${book.name}"/>
		</div>

		<div class="form-group">
			<label for="email">Email: </label> <span>${book.email}</span> <span style="font-size: smaller;"> (your personal
					email address)</span>
			<input type="hidden" name="email" value="${book.email}"/>
		</div>



		<div class="form-group">
			<label for="message">Got MOOC?: </label> <span>${book.message}</span> <span style="font-size: smaller;"> (hint: google edX)</span>
			<input type="hidden" name="message" value="${book.message}"/>
		</div>


		
		<button type="submit" class="btn btn-success" ${book.style}>Click to join AppsWorkforce ${book.style}</button>
	</form>

	<script>
		function goBack() {
			window.history.back();
		}
	</script>
	

	




</div>

<!-- [END view] -->
