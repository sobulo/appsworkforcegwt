<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START base] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.fertiletech.sap.client.NigeriaSchools"%>
<html lang="en">
  <head>
          <title>AppsWorkforce | Earn, learn and have fun programming.</title>
        <meta name="description" content="Learn programming and get remote jobs">
    <meta name="keywords" content="entry level programming jobs in Nigeria">
    <meta name="author" content="appsworkforce.com">
    <meta charset="utf-8">
        <script
      src="https://kit.fontawesome.com/641ce4cc5f.js"
      crossorigin="anonymous"
    ></script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-71470923-1']);
  _gaq.push(['_trackPageview', '/${page}.jsp']);
  
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
     
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
     <link rel="stylesheet" href="style.css" />
  <style>
.minisel{
  font-size: xx-small;
}
</style>
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
          <a href = "/" ><img src="logo.png"/></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
         <!--    <li><a href="/verified?type=research">Africa MOOC ER</a></li> -->
           <li><a href="/lab/">Lab School</a></li>
                 <li><a href="/update">Programmer Training</a></li>
                 <li><a href="/verified">Human Capital</a></li>
                    <!--    <li><a href="http://about.appsworkforce.com/about/">About</a></li> -->
                    <!--  <li><a href="http://about.appsworkforce.com/learn/">Start Learning</a></li> -->
        </ul>
        ></div>
      </div>
    </nav>
    <c:import url="/${page}.jsp" />
    
    <!-- 
      <div class="row justify-content-md-center">
    <div class="col col-lg-2">
      &nbsp;
    </div>
    <div class="col-md-auto">
      <a href="#"><img src="images/banner1.jpg" class="img-responsive" alt="Learn programming" /></a>
    </div>
    <div class="col col-lg-2">
       &nbsp;
    </div>
  </div>
  -->

  

    <footer>
      <div class="items container">
        <div class="item m-right">
          <img src="img/logo.png" />
          <p class="left-align">
            AW is a defensive analytics MOOC compaign, we recommend you
            experience MITx and HarvardX on
            <a href="https://edx.org" class="link" target="_blank">edX</a>.
          </p>
          <p class="left-align">
            For clients, we offer solutions geared towards amplifying your
            revenue exponentially. Integrity is at the core of everything we do.
            We are Nigerians and we value our community.
          </p>

          <p class="left-align">
            Email
            <a class="link" href="mailto:segun.sobulo@appsworkforce.com"
              >Segun Sobulo</a
            >
            to book an appointment to learn more about how to better use
            technology to grow your business.
          </p>

          <div class="icons">
            <a href="https://www.linkedin.com/in/segun-sobulo/" target="_blank"
              ><i class="fab fa-linkedin fa-2x"></i
            ></a>
            <a
              href="https://www.youtube.com/channel/UC67Fv_ISmjK_J0vsGkQYXuQ/featured"
              target="_blank"
              ><i class="fa fa-youtube-play fa-2x"></i
            ></a>
            <a href="mailto:hello@appsworkforce.com" target="_blank"
              ><i class="fab fa-google fa-2x"></i
            ></a>
            <a href="https://www.facebook.com/pg/appsworkforce/" target="_blank"
              ><i class="fab fa-facebook fa-2x"></i
            ></a>
            <a href="https://instagram.com/appsworkforce" target="_blank"
              ><i class="fab fa-instagram fa-2x"></i
            ></a>
            <a
              href="https://api.whatsapp.com/send?phone=2349061766041"
              target="_blank"
              ><i class="fab fa-whatsapp fa-2x"></i
            ></a>
          </div>
        </div>
        <div class="map item">
          <div class="card">
            <h3>Startup Meetings</h3>
            <h5>By Appointment Only</h5>
            <div class="address">
              <ul>
                <li>
                  Workstation, Bar Beach. 7 Ibiyinka Olorunbe Close, Victoria
                  Island, Lagos.
                </li>
                <li>
                  Workstation, Maryland Mall. Ikorodu Rd, Maryland, Ikeja,
                  Lagos.
                </li>
              </ul>
            </div>
            <h5 class="seconday-font m-top">
              Ask for Segun - Member ID: 0004816
            </h5>

            <!-- <iframe
              width="100%"
              height="300"
              frameborder="0"
              scrolling="no"
              marginheight="0"
              marginwidth="0"
              src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=genesis%20mall%20maryland+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
            ></iframe> -->
          </div>
        </div>
      </div>
      <p class="copy">
        &copy; 2020 AppsWorkforce. Powered by Fertile-Tech Business Systems RC
        889539. All rights reserved.
      </p>
    </footer>

  





    <!-- Place this script right before the closing /body tag.  Only one copy of the code below is needed per page. -->
   <!--  
    <script type='text/javascript'>
    (function(d, s, c) {
      if (window._jobscore_loader) { return; } else { window._jobscore_loader = true; }
      var o = d.createElement(s); o.type = 'text/javascript'; o.async = true;
      var sc = d.getElementsByTagName(s)[0]; sc.parentNode.insertBefore(o, sc);
      o.src = ('https:' == d.location.protocol ? 'https:' : 'http:') + '//widgets.jobscore.com/jobs/' + c + '/widget.js';
    })(document, 'script', 'appsworkforce');
    </script>
    -->
  
  </body>
</html>
<!-- [END base]-->
