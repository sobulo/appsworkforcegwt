<!--
Copyright 2016 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->
<!-- [START view] -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.fertiletech.sap.server.pub.AuditHelper"%>
<div class="container">
	<h3>Human Capital (HC)</h3>
Search for <b>research</b> on Google Sites search bar on AW reference site to learn more about how we value talent.
  <c:choose>
  <c:when test="${empty awid}">
  
  <br/>Working with an AW member? Specify email address and member id below to view their verified programming reference. 
  MITx alumns have a minimum valuation of $10/hr (or 300,000 naira for partime 20 hours/week) <br/><br/>
<a href="https://www.edx.org/school/mitx" target="_blank"> MITx on edX</a><br/><br/>
  <form method="POST" action="verified">
  <div class="row">
			<div class="form-group col-sm-4">
				<label for="email">Email</label>
					<input value="${oldmail}" type="text" name="email" id="email"/>
			</div>
			<div class="form-group col-sm-4">
				<label for="id">AW ID</label>
					<input value="${oldid}" type="text" name="id" id="id"/>
			</div>
			<div class="col-sm-4">
			<button type="submit" class="btn btn-success col ">Reported MITx Experiences</button>
			</div>
			</div>
			<p style="color:brown">${awerror}</p>

</form>
   <hr/>
  <br/>
  </c:when>
  <c:otherwise>

	<div class="media">
		<div class="media-left hidden-xs">
		 <c:choose>
  <c:when test="${empty memimg}">
  <img alt="logo" src="logo.png">
  </c:when>
   <c:otherwise>
   <img alt="personal picture" src="${memimg}" style="max-width: 250px;">
   </c:otherwise>
   </c:choose>
		</div>
		<div class="media-left">
			AW ID:
			<c:out value="${fn:escapeXml(book.id)}" />
		</div>
		<div class="media-body">

          <p>Name: <c:out value="${fn:escapeXml(book.name)}" /></p>
			<p>State: <c:out value="${fn:escapeXml(book.state)}" /></p>
			<p>Type: Work Study</p>
			<p>Email: <c:out value="${fn:escapeXml(book.email)}" /></p>
			<!--  
			<p>Status: <c:out value="${fn:escapeXml(book.style)}" /></p>
			<p>Note: <c:out value="${fn:escapeXml(book.message)}" /></p>
			-->
			<p>Started: <c:out value="${fn:escapeXml(book.crdate)}" /></p>
			<p>Modified: <c:out value="${fn:escapeXml(book.mddate)}" /></p>

		</div>
	</div>
	   <hr/>
   <b>6x case studies</b><br/>
	<% out.println(AuditHelper.getTracks((String) request.getAttribute("awid"))); %>

    </c:otherwise>
  </c:choose>

</div>

<!-- [END view] -->
